<?php

class RestMessages {

    public static function mensaje($codigo, $data=[], $msg='') {

        $std = new stdClass();
        //$std->status = $codigo;
        $std->code = $codigo;
        if ($codigo!=10 && $data!=[])
            $std->data = $data;
        else if ($codigo==10)
            $std->new_token = $data;

        switch($codigo){
            case 10:
                $std->message = 'Token Regenerado';
            break;
            case 100:
                $std->message = 'OK';
            break;            
            case 101:
                $std->message = 'Hay campos obligatorios que no estan incluidos en la URI!'.$msg;
            break;
            case 102:
                $std->message = 'No se pudo procesar la persona solicitada!'.$msg;
            break;            
            case 103:
                $std->message = 'Persona en espera de analisis, se enviara un correo de resultados, o bien puede consultarlos en el Aplicativo WEB!';
            break;            
            case 104:
                $std->message = 'Api Key No valida!, se requiere Autorizacion';
            break;
            case 105:
                $std->message = 'Se esta volviendo a solicitar Ponderacion de la misma persona para el mismo puesto en el mismo lote. La Persona se encuentra en espera de analisis.';
            break;
            case 106:
                $std->message = 'No es posible insertar mas personas-puestos, ya que el lote solicitado solo era para '.$msg.' personas-puestos';
            break; 
            case 107:
                $std->message = 'Ponderacion: '.$msg;
            break; 
            case 108:
                $std->message = 'Usuario no Valido!, verifique los usuarios asignados a su contrato.';
            break; 
            case 109:
                $std->message = 'Identificador de Puesto NO Válido para el cliente';
            break;
            case 110:
                $std->message = 'La persona solicitada con el puesto indicado ya se encuentran en proceso de análisis!';
            break; 
            case 111:
                $std->message = 'Hay error en los parámetros enviados, no coinciden con lo requerido, verifique su petición REST.';
            break;  
            case 112:
                $std->message = 'Solo se permiten peticiones por método POST.';
            break;   
            case 113:
                $std->message = 'Acceso No Autorizado!';          
                break;   
            case 114:
                $std->message = 'Token ha expirado, se necesita regenerarlo, ingresa login y contraseña!'; 
                break;    
            case 115:
                $std->message = 'Token No Valido!';          
                break;     
            case 116:
                $std->message = 'El cliente del cual pertenece el usuario no tiene Contrato Valido!';          
                break;             
        } 
        return $std;
        // return json_encode($std);
    }
}