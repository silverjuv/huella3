<?php

// require APPPATH . 'libraries/OSCalls.php';

class Osint extends OSCalls {

    function ejecutarOSINTNumVerify() {
        //$p = $this->plataformasToStr($plataformas, "usufy");
        $id_persona=$_POST['data'];
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";
        $person = null;
        if($id_persona > 0) {
            $person = $this->CatPersonasModel->getPersonaById($id_persona);
            //var_dump($person);die();
        }

        $nombreP='';
        $errores=0;
        $msg='';
        $url="numverify.com_ByCel";

        if ($person!=null){
            // set API Access Key
            if (strlen($person->celular)==10){
                $row = $this->ParametrosModel->getParametroById('API_KEY_numverify.com');
                $access_key = $row->valor;

                // set phone number
                // $data=$_POST['data'];
                $phone_number = "+52".$person->celular;
                //$url="emailrep.io_ByEmail";

                // Initialize CURL:
                $ch = curl_init('http://apilayer.net/api/validate?access_key='.$access_key.'&number='.$phone_number.'');  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // Store the data:
                $json = curl_exec($ch);
                curl_close($ch);
                
                // Decode JSON response:
                $validationResult = json_decode($json, true);
                //echo $results.' ok ';

                //Guardamos en Base de datos
                $row = $this->ExpedientesModel->consultaUrlSocialIdPersona($url,$id_persona);
                if (count($row)>0){
                    // si hay persona ligada al url_asociado... solo actualizamos URLS_sociales
                    $data["url_asociado"]=trim($url);
                    $data["contenido"]="";
                    $data["json_data"]=$json;
                    $data["id_anexo"]=7;
                    $now = date('Y-m-d H:i:s');
                    $data["fecha_captura"]=$now;
                    $this->ExpedientesModel->actualizarUrlSocial("urls_sociales",$row[0]['id_anexado'],$data);
                }else{
                    $data["url_asociado"]=trim($url);
                    $data["contenido"]="";
                    $data["json_data"]=$json;
                    $data["id_anexo"]=7;
                    $now = date('Y-m-d H:i:s');
                    $data["fecha_captura"]=$now;
                    $idURlAsociado = $this->ExpedientesModel->insertarUrlSocial("urls_sociales",$data);

                    if ($idURlAsociado>0){
                        // INSERTAMOS enlace en expedientes_anexos..
                        $datax["id_persona"]=$id_persona;
                        $datax["id_anexado"]=$idURlAsociado;
                        $datax["fuente"]='R';
                        $datax["usuario"]=1;
                        $datax["calificacion"]=0;
                        $this->ExpedientesModel->insertarExpedientesAnexos("expedientes_anexos",$datax);
                    }

                }

            }else{
                $msg = 'El celular de la persona no es correcto!';
                $errores+=1;
            }
        }else{
            $msg = 'Persona no localizada en base de datos.. ';
            $errores+=1;
        }

        $results['Id_Persona'] = $id_persona;    
        $results['Persona'] = $nombreP;
        $results['Proceso'] = 'numverify.com_ByCel';
        $results['NoComentarios'] = 0;
        $results['NoUrls'] = 0;
        $results['NoErrores'] = $errores;
        $results['Mensaje'] = $msg;

        print json_encode(array(
            "name"=>$id_persona, 
            "cmd"=>'php numverify',
            "json"=>json_encode($results)
        ));
        // return ( );
    }

    function ejecutarOSINTEmailrep() {
        //$p = $this->plataformasToStr($plataformas, "usufy");
        $data=$_POST['data'];
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        //$fileName = self::OUTPUT_RESULTS."/".self::getUniqueName().".json";
        $fileName = self::getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/fromEmailrep.py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = self::OUTPUT_RESULTS."/".$fileName;
        
        $results = $this->ejecutarCmd($cmd, $fileName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>$results
        ));
        // return ( );
    }

    function ejecutarOSINTFullcontact() {
        //$p = $this->plataformasToStr($plataformas, "usufy");
        $data=$_POST['data'];
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        //$fileName = self::OUTPUT_RESULTS."/".self::getUniqueName().".json";
        $fileName = self::getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/fromFullcontact.py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = self::OUTPUT_RESULTS."/".$fileName;
        
        $results = $this->ejecutarCmd($cmd, $fileName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>$results
        ));
        // return ( );
    }    

}
