<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set ( "America/Mexico_City" );

class Fechas {

    function getDiasDeCaptura() {
        //los dias de captura definidos son de vierenes(5) al lunes(1)
        return array(5,6,7,1);
    }

    function sePuedeCapturarHoy($fechaBd=false) {
        $hoy = date("Y-m-d");
        $diaN = date("N");//dias numericos 1-lunes ... 7-domingo
        $nowTime = date("Y-m-d H:i");

        //Es lunes y se debe permitir capturar hasta las 10
        if($diaN == 1) {
            $lunesTime = $hoy." 10:00";

            if(strtotime($lunesTime) > strtotime($nowTime)) {
                return true; //si aun no se llega a la hr establecida
            } else if(!empty($fechaBd)) {
                //se verifica si existe alguna extension de tiempo de captura en la bd
                if(strtotime($fechaBd) > strtotime($nowTime)) {
                    return true;
                } else {
                    //fecha en bd activa(1) pero vencida (ya paso la fecha guardada)
                    return false;
                }
            } else {
                //es lunes, pero no es antes de las 10 y no hay limite de extension captura en la bd
                return false;
            }

        } else if(in_array($diaN, $this->getDiasDeCaptura())) {
            //el dia actual es uno de los demas dias de captura definidos Viernes, Sabado, Domingo
            return true;
        } else if(!empty($fechaBd)) {
            //por ultimo si no es dia de captura y no es lunes antes de las 10 entonces quiza se halla establecido en la db una fecha de extension de tiempo
            if(strtotime($fechaBd) > strtotime($nowTime)) {
                //plazo vigente
                return true;
            } else {
                //ya se vencio el plazo
                return false;
            }
        } else {
            //no es lunes, no es uno de los demas dias de captura y no hay fecha en bd
            //significa que es martes o miercoles o jueves
            return false;
        }

    }

    //obtiene el dia inicial y final de la semana siguiente a la actual
    function getRangoSemanal() {
        $hoy = date("Y-m-d");
        $diaN = date("N");//dias numericos 1-lunes ... 7-domingo
        $sumar = (7 - $diaN) + 1;

        //cuando es lunes se debe mostrar la semana actual no la siguiente
        if($diaN == 1) {
            return $this->getRangoSemanalActual();
        }

        //calculo del rango inicial
        $fecha= new DateTime($hoy);
        $fecha->add(new DateInterval('P'.$sumar.'D'));
        $inicio = $fecha->format("Y-m-d");
        
        $fecha= new DateTime($inicio);
        $fecha->add(new DateInterval('P6D'));
        $fin = $fecha->format("Y-m-d");
        

        return array("inicio"=>$inicio, "fin"=>$fin);
    }

    //obtiene el dia inicial y final de la semana actual
    function getRangoSemanalActual() {
        $hoy = date("Y-m-d");
        $restar = date("N") - 1;//dias numericos 1-lunes ... 7-domingo

        //calculo del rango inicial
        $fecha= new DateTime($hoy);
        $fecha->sub(new DateInterval('P'.$restar.'D'));
        $inicio = $fecha->format("Y-m-d");
        
        $fecha= new DateTime($inicio);
        $fecha->add(new DateInterval('P6D'));
        $fin = $fecha->format("Y-m-d");

        return array("inicio"=>$inicio, "fin"=>$fin);
    }

    //obtiene el rango semanal a partir de una fecha no necesariamente la fecha de hoy
    function getRangoSemanalDeFecha($f) {
        
        $fecha = new DateTime($f);
        $restar = $fecha->format("N") - 1; //dias numericos 1-lunes ... 7-domingo
        
        $fecha->sub(new DateInterval('P'.$restar.'D'));
        $inicio = $fecha->format("Y-m-d");
        
        $fecha= new DateTime($inicio);
        $fecha->add(new DateInterval('P6D'));
        $fin = $fecha->format("Y-m-d");

        return array("inicio"=>$inicio, "fin"=>$fin);
    }

    //determina si una fecha esta dentro de un rango inclusivo, convirtiendo a timestamps las fechas
    function estaFechaEnRango($fecha, $rango) {
        $fecha = date_create($fecha); //se convierte a objeto fecha
        $ft = date_timestamp_get($fecha);//se convierte a timestamp
        
        $ri = date_create($rango["inicio"]);
        $rit = date_timestamp_get($ri);

        $rf = date_create($rango["fin"]);
        $rft = date_timestamp_get($rf);

        return ($ft>=$rit && $ft<=$rft);
    }

    //determina el rango semanal de una fecha en el pasado o futuro
    function getRangoSemanalDeFechaPlaneada($fecha) {
        return $this->getRangoSemanalDeFecha($fecha);
    }

    //determina diferencia en segundos de 2 fechas... 
    // primer parametro fecha inicial, segundo fecha final
    static function time_diff($dt1,$dt2){
        $y1 = substr($dt1,0,4);
        $m1 = substr($dt1,5,2);
        $d1 = substr($dt1,8,2);
        $h1 = substr($dt1,11,2);
        $i1 = substr($dt1,14,2);
        $s1 = substr($dt1,17,2);   
    
        $y2 = substr($dt2,0,4);
        $m2 = substr($dt2,5,2);
        $d2 = substr($dt2,8,2);
        $h2 = substr($dt2,11,2);
        $i2 = substr($dt2,14,2);
        $s2 = substr($dt2,17,2);   
    
        $r1=date('U',mktime($h1,$i1,$s1,$m1,$d1,$y1));
        $r2=date('U',mktime($h2,$i2,$s2,$m2,$d2,$y2));
        return ($r1-$r2);
    
    }
 
}