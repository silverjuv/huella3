<?php 
// require APPPATH . 'libraries/RedesSociales.php';
require APPPATH . 'vendor/autoload.php';

use Elasticsearch\ClientBuilder;

class Elasticsearch{

    public $client;
    public $clientCentos;

    public function __construct(){
        // $this->client = ClientBuilder::create()->build();
        $hosts = [
            // 'http://elastic:7ZcCmPOXc53wL1YvmnXS@localhost:9200',       // HTTP Basic Authentication
            // 'https://elastic:7ZcCmPOXc53wL1YvmnXS@192.168.137.183:9001',       // HTTP Basic Authentication
            // 'https://elastic:7ZcCmPOXc53wL1YvmnXS@192.168.77.16:9001',       // HTTP Basic Authentication  desarrollo
            'https://elastic:7ZcCmPOXc53wL1YvmnXS@192.168.19.32:9001',       // HTTP Basic Authentication  desarrollo

            // 'http://user2:pass2@other-host.com:9200' // Different credentials on different host
        ];
        // $hosts = [
        //     'http://elastic:6DMhbrMYY2djoBJzn9ee@localhost:9200',       // HTTP Basic Authentication
        //     // 'http://user2:pass2@other-host.com:9200' // Different credentials on different host
        // ];

        // $this->client = ClientBuilder::create()
        //                     ->setHosts($hosts)
        //                     ->build();

        // $hosts = [
        //     'http://192.168.77.20:9200',       // HTTP Basic Authentication
        //     // 'http://user2:pass2@other-host.com:9200' // Different credentials on different host
        // ];        

        $this->client = ClientBuilder::create()
                            ->setHosts($hosts)
                            ->setSSLVerification(false)
                            ->build();        
    } 
}