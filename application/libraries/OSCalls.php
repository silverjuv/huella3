<?php

class OSCalls {

    const OUTPUT_RESULTS = "/var/www/threats/www/huella/assets/documents";
    //const OUTPUT_RESULTS = "assets/documents";
    const MAX_TIME = 0;
    
	/**
	 * Identificador de sesion activa
	 *
	 * @var string
	 */
    public static $id = NULL;  

    /**
	 * Comprueba si existe una variable en la sesion
	 *
	 * @param string $var
	 *        	Nombre de la variable
	 * @return boolean
	 */
	public  function isVar($var) {
		return isset ( $_SESSION [self::$id] ) && isset ( $_SESSION [self::$id] [$var] ) ? true : false;
    }
        
	/**
	 * Regresa un valor de una variable en la sesion
	 *
	 * @param string $var
	 *        	Nombre de la variable de session
	 * @return mixed Regresa el valor de la variable de session
	 */
	public  function get_idsession($var) {
		return  $this->isVar ( $var ) ? $_SESSION [self::$id] [$var] : false;
    }
        
    public function getUniqueName() {
        $micro = str_replace(".","",microtime()); 
		return $this->get_idsession('id')."_".str_replace(" ","",$micro);	
    }    

    public function ejecutarCmd($cmd, $jsonFile) {
        $completeCmd = $cmd;//self::PYTHON_PATH." ". self::LAUNCHER ." ".$cmd;
        set_time_limit(self::MAX_TIME);
        
        $o = shell_exec ($completeCmd." 2>&1");
        //var_dump($o);

        return $this->getResultsAndCleanPath($jsonFile);
    }
    
    public  function getResultsAndCleanPath($file) {
        $result = "";
        try {
            if(file_exists($file)) {
                $result = file_get_contents($file);
                // echo $file."<br />".$result."<br />";
                // die();
                @unlink($file);
            }else{
                $result="[]";
            }
            
            return $result;    
        } catch(Exception $e){ 
            echo $e."<br />";
             die();
            return $result; 
        }
    }   

}
