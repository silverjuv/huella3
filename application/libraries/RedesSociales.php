<?php

// require APPPATH . 'libraries/OSCalls.php';

class RedesSociales extends OSCalls {

/**
     * Script de scrap de red social youtube.com
     */
    public function ejecutarScrapRS($data,$redsocial) {
        //$p = $this->plataformasToStr($plataformas, "usufy");
        // $data=$_POST['data'];
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        $fileName = self::getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/scrap_".$redsocial.".py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = self::OUTPUT_RESULTS."/".$fileName;
        
        $results = self::ejecutarCmd($cmd, $fileName);
        // echo $cmd; die();
        //echo $results.' ok ';

        // print json_encode(array(
        //     "name"=>$data, 
        //     "cmd"=>$cmd,
        //     "json"=>$results
        // ));
        // return ( );
        return $results;
    }

    public function ejecutarScript($data,$proceso) {
        //$p = $this->plataformasToStr($plataformas, "usufy");
        // $data=$_POST['data'];
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        $fileName = self::getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/".$proceso.".py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = self::OUTPUT_RESULTS."/".$fileName;
        
        $results = self::ejecutarCmd($cmd, $fileName);
        // echo $cmd; die();
        //echo $results.' ok ';

        // print json_encode(array(
        //     "name"=>$data, 
        //     "cmd"=>$cmd,
        //     "json"=>$results
        // ));
        // return ( );
        return $results;
    }    

}
