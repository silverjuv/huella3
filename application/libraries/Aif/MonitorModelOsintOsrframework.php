<?php

class MonitorModelOsintOsrframework {

    static function getUsufyPlatforms($data) {
        return self::getPlatforms("usufy", $data);
    }

    static function getSearchfyPlatforms($data) {
        return self::getPlatforms("searchfy", $data);
    }

    static function getPlatforms($module, $search='') {
        
        AifDb::stringVal($search);
        return AifDb::toArray("Select plataforma as id, plataforma as text from plataformas_osr_usufy Where plataforma like '{$search}%' and modulo = '{$module}' ");

    }
}