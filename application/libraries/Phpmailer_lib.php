<?php
defined('BASEPATH') or exit('No direct script access allowed');

// Include PHPMailer library files
require_once APPPATH . 'third_party/PHPMailer/PHPMailer/Exception.php';
require_once APPPATH . 'third_party/PHPMailer/PHPMailer/PHPMailer.php';
require_once APPPATH . 'third_party/PHPMailer/PHPMailer/SMTP.php';
/**
 * CodeIgniter PHPMailer Class
 *
 * This class enables SMTP email with PHPMailer
 *
 * @category    Libraries
 * @author      CodexWorld
 * @link        https://www.codexworld.com
 */

// use PHPMailerPHPMailerPHPMailer;
use PHPMailer\PHPMailer\Exception;

class PHPMailer_Lib
{
    public function __construct()
    {
        log_message('Debug', 'PHPMailer class is loaded.');
    }

    public function load()
    {


        $mail = new PHPMailer\PHPMailer\PHPMailer(true);
        $mail->SetLanguage("es", APPPATH . "third_party/PHPMailer/PHPMailer/language/");
        return $mail;
    }
}
