<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

defined('PERFIL_ADMIN') OR define('PERFIL_ADMIN', 1);
// defined('PERFIL_LIDER') OR define('PERFIL_LIDER', 2);
// defined('PERFIL_USUARIO') OR define('PERFIL_USUARIO', 3);

// Conexion para API REST..
// defined('HOST_API_REST') OR define('HOST_API_REST', 'localhost');
// defined('USERNAME_API_REST') OR define('USERNAME_API_REST', 'user1x');
// defined('PASS_API_REST') OR define('PASS_API_REST', '123456789');
// defined('DBNAME_API_REST') OR define('DBNAME_API_REST', 'huelladb');

// produccion
defined('HOST_API_REST') OR define('HOST_API_REST', 'localhost');
defined('USERNAME_API_REST') OR define('USERNAME_API_REST', 'userdb');
defined('PASS_API_REST') OR define('PASS_API_REST', 'Cru54D45#.M3d1c45');
defined('DBNAME_API_REST') OR define('DBNAME_API_REST', 'huelladb');

// Used for JWT authentication.
// Please change the key below to make your website secure.
//[security]
defined('SECRETKEY_JWT') OR define('SECRETKEY_JWT', 'UhT0MaDpVrUgGnPkP7dTEkBfEU2DUcorx');

// otras constantes... 
// tester.. 
// defined('PATH_SCRIPTS_PYHUELLA') OR define('PATH_SCRIPTS_PYHUELLA', '/opt/scripts_huella/pyhuella/');
// defined('PYTHON_VER') OR define('PYTHON_VER', 'python3');
// defined('OUTPUT_RESULTS_OSINT_W') OR define('OUTPUT_RESULTS_OSINT_W', '/var/www/html/huella/assets/documents');
// defined('OUTPUT_RESULTS_PPERSONAS') OR define('OUTPUT_RESULTS_PPERSONAS', '/opt/scripts_huella/pyhuella/documents');

// produccion (threats)
defined('PATH_SCRIPTS_PYHUELLA') OR define('PATH_SCRIPTS_PYHUELLA', '/home/threats/demon/crontab_proccess/huella_proccess/scripts_analys/');
defined('PYTHON_VER') OR define('PYTHON_VER', 'python');
defined('OUTPUT_RESULTS_OSINT_W') OR define('OUTPUT_RESULTS_OSINT_W', '/home/threats/demon/crontab_proccess/huella_proccess/scripts_analys/documents');
defined('OUTPUT_RESULTS_PPERSONAS') OR define('OUTPUT_RESULTS_PPERSONAS', '/home/threats/demon/crontab_proccess/huella_proccess/scripts_analys/documents');


// // codigos id_anexo segun tabla cat_anexos..
// defined('IDANEXO_EMAIL_REP') OR define('IDANEXO_EMAIL_REP', 6);
// defined('IDANEXO_NUMVERIFY') OR define('IDANEXO_NUMVERIFY', 7);

// nombres procesos en background..
defined('PROCESOS_OSINT') OR define('PROCESOS_OSINT', 'procesaProcesosOSINT.py');
defined('PROCESOS_PERSONAS') OR define('PROCESOS_PERSONAS', 'procesaPersonas.py');
defined('PROCESA_PUESTOPERSONAS') OR define('PROCESA_PUESTOPERSONAS', 'analizaPuestosPersonas.py');
defined('CAPTURA_NOTICIAS') OR define('CAPTURA_NOTICIAS', 'captureNews.py');
defined('CAPTURA_DARKWEB') OR define('CAPTURA_DARKWEB', '/home/threats/demon/dark/src');