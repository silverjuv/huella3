<?php
set_time_limit(0);
defined('BASEPATH') or exit('No direct script access allowed');

class ConfigAdminModel extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getRowById($id) {
        $id = intval($id);
        $r = $this->db->from("cat_clientes")->
            where("id_cliente", $id)->
            where("borrado='0'")->
			get();
		
		if($r && $r->num_rows()>0) {
			$data = $r->row();
			return $data;
		} else {
			return array("data"=>array());
		}
    }
        
    // function actualizaDefaultCliente($id_cliente, $id)
    // {
    //     $id = intval($id);
    //     $this->db->where("id_usuario", $id);
    //     $this->db->set("id_cliente", $id_cliente);

    //     if ($this->db->update("cat_usuarios")) {
    //         return array("title" => "Info", "msg" => "Registro actualizado", "type" => "success");
    //     } else {
    //         return array("title" => "Error", "msg" => "No fue posible actualizar el registro", "type" => "error");
    //     }
    // }
}
