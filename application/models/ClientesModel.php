<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClientesModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function getClientesForCombo() {

        $filtroAdicional = '';
		if ($_SESSION['id_perfil']==2){
			// filtrado para clientes...
			$filtroAdicional = ' and id_cliente='.$_SESSION['id_cliente'];
        }
        
        $r = $this->db->select("id_cliente as id, descripcion as text")->
            from("cat_clientes")->where("borrado='0' ".$filtroAdicional)->get();
        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }
    }

    function listar($exportar=false) {
        $r = $this->db->from("cat_clientes")->
            where("borrado='0'")->get();

        if($r && $r->num_rows()>0) {
            // return array("data"=>$r->result_array());

            $arr = array();
            foreach ($r->result_array() as $row) {            
                $f = $this->db->select(" ltrim(rtrim(correo)) as correo ")->from("cat_usuarios as cu")->
                where("id_cliente", $row['id_cliente'])->
                where("borrado=0")->
                get();

                if($f && $f->num_rows()>0) {
                    $users = $f->result_array();
                    $users = array_column($users, "correo");

                    $row['usuarios'] = $users;
                } else {
                    $row['usuarios'] = array();
                }

                array_push($arr, $row);
            }
            $data["data"] = $arr;
            
            return $data;

        } else {
            return array("data"=>array());
        }
    }

    function insertar($d) {
        if($this->db->insert("cat_clientes", $d)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"success", "lastid"=>$this->db->insert_id());
        } else {
            array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function actualizar($id, $d) {
        $id = intval($id);
        if($this->db->update("cat_clientes", $d, "id_cliente=".$id)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"success");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function getRowById($id) {
        $id = intval($id);
        $r = $this->db->from("cat_clientes")->
            where("id_cliente", $id)->
            where("borrado='0'")->
			get();
		
		if($r && $r->num_rows()>0) {
			$data = $r->row();
			return $data;
		} else {
			return array("data"=>array());
		}
    }

    function deleteRowById($id) {
        $id = intval($id);
        if($this->db->update("cat_clientes", array("borrado"=>"1"), "id_cliente=".$id)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha borrado el registro", "type"=>"success");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible borrar el registro", "type"=>"error");
        }
    }


}