<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LayoutsUploaderModel extends CI_Model
{
    const LAYOUTS_PATH = FCPATH . "assets/layouts_temp";

    function __construct()
    {
        parent::__construct();
        $this->load->helper("file");
        $this->load->model("ApiRestModel");
    }

    function getIP()
    {
        if (getenv("HTTP_CLIENT_IP") and preg_match("/^[0-9\.]*?[0-9\.]+$/is", getenv("HTTP_CLIENT_IP")) && getenv("HTTP_CLIENT_IP") != '127.0.0.1') {
            $ip = getenv("HTTP_CLIENT_IP");
        } elseif (getenv("HTTP_X_FORWARDED_FOR") && preg_match("/^[0-9\.]*?[0-9\.]+$/is", getenv("HTTP_X_FORWARDED_FOR")) && getenv("HTTP_X_FORWARDED_FOR") != '127.0.0.1') {
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        } else {
            $ip = getenv("REMOTE_ADDR");
        }
        return $ip;
    }

    function validarPuesto($idpuesto, $idcliente)
    {
        $rPuesto = $this->ApiRestModel->getPuestoByCliente($idpuesto, $idcliente);
        if ($rPuesto && $rPuesto->num_rows() > 0) {
            return true;
        }

        return false;
    }

    function validarPersonaPuestoUnico($idpuesto, $idpersona, $apikey)
    {
        $rPuesto = $this->ApiRestModel->getPersonaPuestoByCliente($idpuesto, $idpersona, $apikey, 'E');
        if ($rPuesto && $rPuesto->num_rows() > 0) {
            return true;
        }

        return false;
    }

    function validarPersona(&$row)
    {
        $regCurp = "#([a-zA-Z]{4}[0-9]{6})([\M\H]{1})([a-zA-Z]{5})([0-9]{2})$#";
        $regIne = "#(\d{10,13})$#";
        $regCelular = "#\d{10}$#";
        $regEmail = "#^(((([a-z\d][\.\-\+_]?)*)[a-z0-9])+)\@(((([a-z\d][\.\-_]?){0,62})[a-z\d])+)\.([a-z\d]{2,6})$#i";
        $error = "0";
        $msg = "";
        if (empty($row['nombre'])) {
            $msg .= ", el parametro 'nombre' es obligatorio";
            $error = "1";
        }
        if (empty($row['paterno'])) {
            $msg .= ", el parametro 'paterno' es obligatorio";
            $error = "1";
        }
        // if (empty($row['materno'])){
        //     $msg.=", el parametro 'materno' es obligatorio";
        //     $error="1";
        // }
        if (empty($row['direccion'])) {
            $msg .= ", el parametro 'direccion' es obligatorio";
            $error = "1";
        }
        if (empty($row['celular']) || (!empty($row['celular']) && strlen(trim($row['celular'])) != 10 || (!empty($row['celular']) && preg_match($regCelular, $row["celular"]) == false))) {
            $msg .= ", el parametro 'celular' es obligatorio a 10 posiciones con valor numerico";
            $error = "1";
        }
        if (empty($row['curp']) || (!empty($row['curp']) && strlen(trim($row['curp'])) != 18) || (!empty($row['curp']) && preg_match($regCurp, $row["curp"]) == false)) {
            $msg .= ", el parametro 'curp' es obligatorio a 18 posiciones";
            $error = "1";
        }
        if (empty($row['email']) || (!empty($row['email']) &&  preg_match($regEmail, $row["email"]) == false)) {
            $msg .= ", el parametro 'email' es obligatorio";
            $error = "1";
        }
        if (empty($row['ine']) || (!empty($row['ine']) && preg_match($regIne, $row["ine"]) == false)) {
            $msg .= ", el parametro 'ine' es obligatorio, 10 posiciones numericas minimo";
            $error = "1";
        }
        // if (!empty($row['id_cliente']) && intval($row['id_cliente'])>0 ){            
        // }else{
        //     $msg.=", el parametro 'ine' es obligatorio, 10 posiciones numericas minimo";
        //     $error="1";
        // }
        return array("error" => $error, "msg" => $msg);
    }

    function validarSitios($row)
    {
        $error = "0";
        $msg = "";
        $regTarget  = "#/(http|https?):\/\/(\w+:?\w*@)?(\S+)(:[0-9]+)?(\/([\w!:.?+=&%@!\/-])?)?/#";
        $regAprocesar = "#\d{1}$#";
        $regCategoria = "#\d{1}$#";
        $regPais = "#[a-zA-Z ]#";
        if (!empty($row['target']) && preg_match($regTarget, $row["target"]) == false && !filter_var($row["target"], FILTER_VALIDATE_URL)) {
            $msg .= ", el parametro 'sitio' es obligatorio ejemplo:https://wwww.ejemplo.com.mx";
            $error = "1";
        }
        if (!empty($row['aprocesar']) && preg_match($regAprocesar, $row["aprocesar"]) == false || intval($row["aprocesar"]) > 1) {
            $msg .= ", el parametro 'aprocesar' es obligatorio, con valores de 1 ó 0";
            $error = "1";
        }
        if (!empty($row['id_categoria']) && (intval($row["id_categoria"]) < 1 || intval($row["id_categoria"]) > 6)) {
            $msg .= ", el parametro 'Categoria' es obligatorio, con valores enteros entre 1 y 6";
            $error = "1";
        }
        if (!empty($row['peso']) && (intval($row["peso"]) < 1 || intval($row["peso"]) > 10)) {
            $msg .= ", el parametro 'Ponderación' es obligatorio, con valores enteros rango de 1 a 10";
            $error = "1";
        }
        if (!empty($row['cve_pais']) && preg_match($regPais, $row["cve_pais"]) == false || strlen(trim($row['cve_pais'])) > 3) {
            $msg .= ", el parametro 'CVE pais' es obligatorio, con caracteres máximo 3";
            $error = "1";
        }
        if (!empty($row['entidad']) && preg_match($regPais, $row["entidad"]) == false || strlen(trim($row['entidad'])) > 40) {
            $msg .= ", el parametro 'Entidad' es obligatorio, con caracteres máximo 40";
            $error = "1";
        }
        return array("error" => $error, "msg" => $msg);
    }

    private function existeURL($data)
    {
        $n = $this->db->from("cat_sitiosweb")->where("target", $data)->count_all_results();
        return $n > 0;
    }

    function subirLayout()
    {

        if (empty($this->session->id_usuario)) {
            return array("error" => "1", "title" => "Error", "msg" => "Debe estar autentificado para poder hacer uso de esta pantalla", "type" => "error");
        }
        if (empty($_FILES)) {
            return array("error" => "1", "title" => "Error", "msg" => "No se envio el archivo adjunto", "type" => "error");
        }
        if ($_FILES["file"]["size"] > 0) {
            $layout = strval($this->input->post("layout"));
            $pi = pathinfo(basename($_FILES['file']['name']));
            $ext = (empty($pi["extension"])) ? "" : $pi["extension"];
            $fn = generarString() . "." . $ext;
            $path = self::LAYOUTS_PATH . DIRECTORY_SEPARATOR . $fn;
            if (is_dir(self::LAYOUTS_PATH) && is_writable(self::LAYOUTS_PATH)) {
                if (move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                    $respuesta = array("title" => "Error", "msg" => "No se realizo ninguna acción", "type" => "error");
                    if ($layout == "P") {
                        $respuesta = $this->procesarLayoutPersonas($path);
                    } else if ($layout == "S") {
                        $respuesta = $this->procesarLayoutSitios($path);
                    } else if ($layout == "A") {
                        $respuesta = $this->procesarLayoutPersonasPuestos($path);
                    }
                    return $respuesta;
                } else {
                    return array("title" => "Error", "msg" => "No fue posible guardar el archivo", "type" => "error");
                }
            } else {
                return array("title" => "Error", "msg" => "El directorio no existe o no se tiene permisos de escritura", "type" => "error");
            }
        } else {
            return array("error" => "1", "title" => "Error", "msg" => "El archivo esta vacio", "type" => "error");
        }
    }

    function procesarLayoutPersonas($ruta)
    {

        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel.php';
        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel/IOFactory.php';
        // $this->load->model("ApiRestModel");
        // PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true); //optional
        $objPHPExcel = $objReader->load($ruta);
        $objWorksheet = $objPHPExcel->getSheet(0);
        $highestRow = $objWorksheet->getHighestRow();
        $maxCell = $objWorksheet->getHighestRowAndColumn();
        $actualizado = false;
        $insertado = false;
        $msgError = "";
        $error = "0";
        for ($i = 2; $i <= $maxCell['row']; ++$i) {
            $obj = array();
            $obj['ine'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue()) );
            $obj['curp'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("B$i")->getValue()) );
            $obj['paterno'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("C$i")->getValue()) );
            $obj['materno'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("D$i")->getValue()) );
            $obj['nombre'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("E$i")->getValue()) );
            $obj['email'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("F$i")->getValue()) );
            $obj['celular'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("G$i")->getValue()) );
            $obj['alias'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("H$i")->getValue()) );
            $obj['direccion'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("I$i")->getValue()) );
            $obj['twitter'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("J$i")->getValue()) );
            $obj['linkedin'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("K$i")->getValue()) );
            $obj['id_cliente'] = $_SESSION['id_cliente'];
            $actualizado = false;
            $r = [];
            if (!empty($obj['ine']) && strlen(trim($obj['ine'])) > 0 || !empty($obj['curp']) && strlen(trim($obj['curp'])) > 0) {
                $r = $this->validarPersona($obj);
            }
            if (isset($r["error"]) && $r["error"] === "0") {
                // primero checamos que no exista CURP o INE, en caso de que existan actualizamos registro pero para EL CLIENTE...
                if (!empty($obj['ine']) && strlen(trim($obj['ine'])) > 0) {
                    $rPersona = $this->ApiRestModel->getPersonaByField('ine', trim($obj['ine']), $obj['id_cliente']);
                    $rPersona = (array) $rPersona;
                    if (count($rPersona) > 0) {
                        if (!$actualizado = $this->ApiRestModel->actualizarPersonaByField('ine', $obj['ine'], $obj, $obj['id_cliente'])) {
                            $msgError .= "No se ha podido actualizar el registro" . $i;
                            $error = "1";
                        }
                    } else if (!empty($obj['curp'])  && strlen(trim($obj['curp'])) > 0) {
                        $rPersona = $this->ApiRestModel->getPersonaByField('curp', trim($obj['curp']), $obj['id_cliente']);
                        $rPersona = (array) $rPersona;
                        //return $rPersona;
                        if (count($rPersona) > 0) {
                            if (!$actualizado = $this->ApiRestModel->actualizarPersonaByField('curp', $obj['curp'], $obj, $obj['id_cliente'])) {
                                $msgError .= "No se ha podido actualizar el registro" . $i;
                                $error = "1";
                            }
                        }
                    }
                } else if (!empty($obj['curp'])  && strlen(trim($obj['curp'])) > 0) {
                    $rPersona = $this->ApiRestModel->getPersonaByField('curp', trim($obj['curp']), $obj['id_cliente']);
                    $rPersona = (array) $rPersona;
                    if (count($rPersona) > 0) {
                        if (!$actualizado = $this->ApiRestModel->actualizarPersonaByField('curp', $obj['curp'], $obj, $obj['id_cliente'])) {
                            $msgError .= "No se ha podido actualizar el registro" . $i;
                            $error = "1";
                        }
                    }
                }
                // En caso de que no se haya actualizado el registro de la persona y el cliente, insertamos
                if (!$actualizado) {
                    if (!empty($obj['ine']) && strlen(trim($obj['ine'])) > 0) {
                        if (!$insertado = $this->ApiRestModel->insertarPersona($obj)) {
                            $msgError .= "No se ha podido insertar el registro" . $i;
                            $error = "1";
                        }
                    }
                }
            } else {
                if (count($r) > 0) {
                    $msgError .= "Registro numero: " . $i . " errores: " . $r["msg"];
                }
            }
        }
        if (file_exists($ruta)) {
            if (!@unlink($ruta)) { //se elimina el archivo del expediente
                $msgError .= "No fue posible eliminar  el archivo zip";
            }
        }
        if ($error == "0") {
            return array("title" => "Info", "msg" => "Se ha procesado el layout con exito" . $msgError, "type" => "success");
        } else {
            return array("title" => "Error", "msg" => $msgError, "type" => "error");
        }
    }

    function procesarLayoutPersonasPuestos($ruta)
    {
        // validamos el api_key que se encuentre activo

        $rContrato = $this->ApiRestModel->isApiKeyActiva(($_SESSION['api_key'] != NULL ? $_SESSION['api_key'] : ''));
        if (!$rContrato) {
            return array("title" => "Info", "msg" => "Su contrato no esta Activo", "type" => "success");
        }
        $loteX = date('Y-m-d H:i:s');

        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel.php';
        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel/IOFactory.php';

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true); //optional
        $objPHPExcel = $objReader->load($ruta);
        $objWorksheet = $objPHPExcel->getSheet(0);
        $highestRow = $objWorksheet->getHighestRow();
        $maxCell = $objWorksheet->getHighestRowAndColumn();
        $actualizado = false;
        $insertado = false;
        $msgError = "";
        $error = "0";
        for ($i = 2; $i <= $maxCell['row']; ++$i) {
            $obj = array();
            $obj['ine'] =  str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue()) );
            $obj['curp'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("B$i")->getValue()) );
            $obj['paterno'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("C$i")->getValue()) );
            $obj['materno'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("D$i")->getValue()) );
            $obj['nombre'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("E$i")->getValue()) );
            $obj['email'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("F$i")->getValue()) );
            $obj['celular'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("G$i")->getValue()) );
            $obj['alias'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("H$i")->getValue()) );
            $obj['direccion'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("I$i")->getValue()) );
            $obj['twitter'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("J$i")->getValue()) );
            $obj['linkedin'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("K$i")->getValue()) );
            $obj['id_cliente'] = $_SESSION['id_cliente'];
            $actualizado = false;
            $r = [];
            if (!empty($obj['ine']) && strlen(trim($obj['ine'])) > 0 || !empty($obj['curp']) && strlen(trim($obj['curp'])) > 0) {
                $r = $this->validarPersona($obj);
            }
            if (isset($r["error"]) && $r["error"] === "0") {
                // primero checamos que no exista CURP o INE, en caso de que existan actualizamos registro pero para EL CLIENTE...
                // primero intentamos con los 2 valores
                $rPersona = NULL;
                if (!empty($obj['ine']) && strlen(trim($obj['ine'])) > 0 && !empty($obj['curp'])  && strlen(trim($obj['curp'])) > 0) {
                    // 
                    $rPersona = $this->ApiRestModel->getPersonaBy2Field('ine', trim($obj['ine']), 'curp', trim($obj['curp']), $obj['id_cliente']);
                    $rPersona = (array) $rPersona;
                    if (count($rPersona) > 0) {
                        $obj['borrado']=0;
                        if (!$actualizado = $this->ApiRestModel->actualizarPersonaBy2Field('ine', $obj['ine'], 'curp', trim($obj['curp']), $obj, $obj['id_cliente'])) {
                            $msgError .= "No se ha podido actualizar el registro" . $i;
                            $error = "1";
                        }
                    } 

                }else if (!empty($obj['ine']) && strlen(trim($obj['ine'])) > 0) {
                    $rPersona = $this->ApiRestModel->getPersonaByField('ine', trim($obj['ine']), $obj['id_cliente']);
                    $rPersona = (array) $rPersona;
                    if (count($rPersona) > 0) {
                        $obj['borrado']=0;
                        if (!$actualizado = $this->ApiRestModel->actualizarPersonaByField('ine', $obj['ine'], $obj, $obj['id_cliente'])) {
                            $msgError .= "No se ha podido actualizar el registro" . $i;
                            $error = "1";
                        }
                    } else if (!empty($obj['curp'])  && strlen(trim($obj['curp'])) > 0) {
                        $rPersona = $this->ApiRestModel->getPersonaByField('curp', trim($obj['curp']), $obj['id_cliente']);
                        $rPersona = (array) $rPersona;
                        //return $rPersona;
                        if (count($rPersona) > 0) {
                            $obj['borrado']=0;
                            if (!$actualizado = $this->ApiRestModel->actualizarPersonaByField('curp', $obj['curp'], $obj, $obj['id_cliente'])) {
                                $msgError .= "No se ha podido actualizar el registro" . $i;
                                $error = "1";
                            }
                        }
                    }
                } else if (!empty($obj['curp'])  && strlen(trim($obj['curp'])) > 0) {
                    $rPersona = $this->ApiRestModel->getPersonaByField('curp', trim($obj['curp']), $obj['id_cliente']);
                    $rPersona = (array) $rPersona;
                    if (count($rPersona) > 0) {
                        if (!$actualizado = $this->ApiRestModel->actualizarPersonaByField('curp', $obj['curp'], $obj, $obj['id_cliente'])) {
                            $msgError .= "No se ha podido actualizar el registro" . $i;
                            $error = "1";
                        }
                    }
                }
                // En caso de que no se haya actualizado el registro de la persona y el cliente, insertamos
                if (!$actualizado) {
                    if (!empty($obj['ine']) && strlen(trim($obj['ine'])) > 0) {
                        $insertado = $this->ApiRestModel->insertarPersona($obj);
                        if (!$insertado) {
                            $msgError .= "No se ha podido insertar el registro" . $i;
                            $error = "1";
                        } else {
                            // obtenemos... persona
                            if (!empty($obj['ine'])) {
                                $rPersona = $this->ApiRestModel->getPersonaByField('ine', $obj['ine'], $obj['id_cliente']);
                            } else if (!empty($obj['curp'])) {
                                $rPersona = $this->ApiRestModel->getPersonaByField('curp', $obj['curp'], $obj['id_cliente']);
                            }

                            // INSERTAMOS evento en api_consultas
                            $dataEventos['api_key'] = $_SESSION['api_key'];
                            $dataEventos['id_usuario'] = $_SESSION['id_usuario'];
                            $dataEventos['id_evento'] = '3';
                            $dataEventos['id_puesto'] = $obj['id_puesto'];
                            $dataEventos['lote'] = $loteX;
                            $dataEventos['id_persona'] = $insertado['lastID'];
                            //$dataEventos['params']=$data;
                            $dataEventos['params'] = '';
                            $dataEventos['fecha_accion'] = date("Y-m-d H:i:s");
                            $dataEventos['ip_publica'] = $this->getIP();
                            $dataEventos['status_evento'] = 'P';
                            $dataEventos['tipo_entrada'] = 'L';
                            $actualizado = $this->ApiRestModel->insertarApiEvento($dataEventos);
                        }
                    }
                }

                if ($rPersona != NULL) {
                    // ya existe la persona... insertamos en api_consultas.. 
                    $obj['id_puesto'] = trim($objPHPExcel->getActiveSheet()->getCell("L$i")->getValue());
                    // echo $rPersona['id_persona'];
                    // var_dump($rPersona);die();


                    if ($rContrato) {

                        // hay que validar el puesto tambien..
                        if ($this->validarPuesto($obj['id_puesto'], $_SESSION['id_cliente'])) {

                            // primero consultamos calificacion para el perfil, si existe el status seria 'P'
                            $status_ev = 'E';
                            ////////////////////////////////////////////////////////////////////////////////////////////
                            /// Revisamos en una tabla de resultados si existe la persona asociada a 1 perfil y si ya tiene calificacion
                            /// si es asi ponemos el status en 'P'
                            $ponderacion = '';
                            $rCalificacion = NULL;
                            $reprocesar = $this->ApiRestModel->isReprocesarPersonaPuestoActiva($rContrato->id_cliente);
                            if ($reprocesar == false) {
                                $rCalificacion = $this->ApiRestModel->getCalificacion($rPersona['id_persona'], $obj['id_puesto']);
                                //var_dump($rCalificacion);
                                if ($rCalificacion) {
                                    // Ya existe un resultado...
                                    $status_ev = 'P';
                                    $ponderacion = '' . ($rCalificacion->ponderacion + 1);
                                }
                            }

                            // verificamos que no se repita la persona y el puesto y el status_evento='E'
                            if (strlen($ponderacion) == 0 && $reprocesar == false && !$this->validarPersonaPuestoUnico($obj['id_puesto'], $rPersona['id_persona'], $_SESSION['api_key'])) {
                                $dataEventos['api_key'] = $_SESSION['api_key'];
                                $dataEventos['id_usuario'] = $_SESSION['id_usuario'];
                                $dataEventos['id_evento'] = '2';
                                $dataEventos['id_puesto'] = $obj['id_puesto'];
                                $dataEventos['lote'] = $loteX;
                                $dataEventos['id_persona'] = $rPersona['id_persona'];
                                //$dataEventos['params']=$data;
                                $dataEventos['params'] = '';
                                $dataEventos['fecha_accion'] = date("Y-m-d H:i:s");
                                $dataEventos['ip_publica'] = $this->getIP();
                                $dataEventos['status_evento'] = 'E';
                                $dataEventos['tipo_entrada'] = 'L';
                                $actualizado = $this->ApiRestModel->insertarApiEvento($dataEventos);
                            } else if (strlen($ponderacion) > 0 && $reprocesar == true && !$this->validarPersonaPuestoUnico($obj['id_puesto'], $rPersona['id_persona'], $_SESSION['api_key'])) {
                                $dataEventos['api_key'] = $_SESSION['api_key'];
                                $dataEventos['id_usuario'] = $_SESSION['id_usuario'];
                                $dataEventos['id_evento'] = '2';
                                $dataEventos['id_puesto'] = $obj['id_puesto'];
                                $dataEventos['lote'] = $loteX;
                                $dataEventos['id_persona'] = $rPersona['id_persona'];
                                //$dataEventos['params']=$data;
                                $dataEventos['params'] = '';
                                $dataEventos['fecha_accion'] = date("Y-m-d H:i:s");
                                $dataEventos['ip_publica'] = $this->getIP();
                                $dataEventos['status_evento'] = 'E';
                                $dataEventos['tipo_entrada'] = 'L';
                                $actualizado = $this->ApiRestModel->insertarApiEvento($dataEventos);
                            } else if (strlen($ponderacion) > 0 && $reprocesar == false) {
                                // El el caso de que la peticion persona-puesto que se busca analizar ya esta analizada y la opcion reprocesar esta
                                // en false, entonces.. SE COPIAN los resultados obtenidos en ese lote y se genera un nuevo registro en API_CONSULTAS
                                // $this->ApiRestModel->actualizarApiConsultasByField($loteX,$obj['id_puesto'] , $rPersona['id_persona'] , $_SESSION['api_key']);
                                $dataEventos['api_key'] = $_SESSION['api_key'];
                                $dataEventos['id_usuario'] = $_SESSION['id_usuario'];
                                $dataEventos['id_evento'] = '2';
                                $dataEventos['id_puesto'] = $obj['id_puesto'];
                                $dataEventos['lote'] = $loteX;
                                $dataEventos['id_persona'] = $rPersona['id_persona'];
                                //$dataEventos['params']=$data;
                                $dataEventos['params'] = '';
                                $dataEventos['fecha_accion'] = date("Y-m-d H:i:s");
                                $dataEventos['ip_publica'] = $this->getIP();
                                $dataEventos['status_evento'] = $status_ev;
                                $dataEventos['tipo_entrada'] = 'L';
                                $actualizado = $this->ApiRestModel->insertarApiEvento($dataEventos);
                                $this->ApiRestModel->copiaResultados($rCalificacion->id_api, $actualizado['lastID']);
                            }
                        }
                    }
                }
            } else {
                if (count($r) > 0) {
                    $msgError .= "Registro numero: " . $i . " errores: " . $r["msg"];
                }
            }
        }
        if (file_exists($ruta)) {
            if (!@unlink($ruta)) { //se elimina el archivo del expediente
                $msgError .= "No fue posible eliminar  el archivo zip";
            }
        }
        if ($error == "0") {
            $statuscode = "off-on\n";
            // Escribir los contenidos en el fichero,
            // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
            // se manda mensaje de inicio del demonio hilos.py
            // $ok = APPPATH . '../../scripts_huella/pyhuella/statusCodeHuellamon.txt';
            // echo $ok; die();
            file_put_contents('../scripts_huella/pyhuella/statusCodeHuellamon.txt', $statuscode, LOCK_EX);
            
            return array("title" => "Info", "msg" => "Se ha procesado el layout con exito" . $msgError, "type" => "success");
        } else {
            return array("title" => "Error", "msg" => $msgError, "type" => "error");
        }
    }

    function procesarLayoutSitios($ruta)
    {
        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel.php';
        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel/IOFactory.php';
        // $this->load->model("ApiRestModel");
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true); //optional
        $objPHPExcel = $objReader->load($ruta);
        $objWorksheet = $objPHPExcel->getSheet(0);
        $highestRow = $objWorksheet->getHighestRow();
        $maxCell = $objWorksheet->getHighestRowAndColumn();
        $msgError = "";
        $error = "0";
        for ($i = 2; $i <= $maxCell['row']; ++$i) {
            $obj = array();
            $obj['target'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue()));
            $obj['aprocesar'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("B$i")->getValue()));
            $obj['id_categoria'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("C$i")->getValue()));
            $obj['peso'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("D$i")->getValue()));
            $obj['cve_pais'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("E$i")->getValue()));
            $obj['entidad'] = str_replace(array('\'', '"'), '', trim($objPHPExcel->getActiveSheet()->getCell("F$i")->getValue()));
            $obj['id_usuario'] = $_SESSION['id_usuario'];
            $actualizado = false;
            $r = [];
            if (!empty($obj['target']) && strlen(trim($obj['target'])) > 0) {
                $r = $this->validarSitios($obj);
            }
            if (isset($r["error"]) && $r["error"] === "0") {
                if ($this->existeURL($obj['target'])) {
                    if (!$this->db->update("cat_sitiosweb", $obj, "target='" . $obj['target'] . "'")) {
                        $msgError .= "No se ha podido actualizar el registro" . $i;
                        $error = "1";
                    }
                } else {
                    if (!$this->db->insert("cat_sitiosweb", $obj)) {
                        $msgError .= "No se ha podido insertar el registro" . $i;
                        $error = "1";
                    }
                }
            } else {
                if (count($r) > 0) {
                    $msgError .= "Registro numero: " . $i . " errores: " . $r["msg"];
                }
            }
        }
        if (file_exists($ruta)) {
            if (!@unlink($ruta)) { //se elimina el archivo del expediente
                $msgError .= "No fue posible eliminar  el archivo zip";
            }
        }
        if ($error == "0") {
            return array("title" => "Info", "msg" => "Se ha procesado el layout con exito" . $msgError, "type" => "success");
        } else {
            return array("title" => "Error", "msg" => $msgError, "type" => "error");
        }
    }
}
