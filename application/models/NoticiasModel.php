<?php
set_time_limit (0);
defined('BASEPATH') OR exit('No direct script access allowed');

class NoticiasModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function getPalabrasArray($str) {
        $p = explode(" ", $str);
        $len = count($p);
        
        $palabras = array();
        for($i=0; $i<$len; $i++) {
            if(strlen($p[$i]) > 3) {
                $palabras[] = $p[$i];
            }
        }

        if($len== 0 && strlen($str) > 3) {
          $palabras[] = $str;
        }

        return $palabras;
    }

    function madSafety($string)
{
    $tags = array( 'p', 'span','html');
    $string = preg_replace( '#<(' . implode( '|', $tags) . ')>.*?<\/$1>#s', '', $string);

       //$string = stripslashes($string);
       //$string = strip_tags($string);
       //$string = mysql_real_escape_string($string);
       return $string;
}

function strip_tags_content($text, $tags = '', $invert = FALSE) {

    preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
    $tags = array_unique($tags[1]);
     
    if(is_array($tags) AND count($tags) > 0) {
      if($invert == FALSE) {
        return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
      }
      else {
        return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
      }
    }
    elseif($invert == FALSE) {
      return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
    }
    return $text;
  } 

  function ciertoContenidoHTML($html){
      //Obtener el texto de la página web
    // $html = file_get_contents($url);
    $html = htmlspecialchars($html);

    //Generar el DOM
    $doc = new DOMDocument;
    $doc->loadHTML($html);

    
    //Obtener el elemento por el id "textoejemplo"
    // $textoejemplo = $doc->getElementById('textoejemplo');
    $textoejemplo = $doc->getElementsByTagName('article');
    
    //Obtener el texto del elemento
    // $html = $textoejemplo->;
    foreach ($textoejemplo as $book) {
        echo $book->nodeValue, PHP_EOL;
    }
    
    //Imprimir el resultado
    //echo "Texto: " . $texto;
    return $html;
  }

  public function insertAutocompleteIndex ($key){
    // if ($words) {
    //   $rr = explode(',', $words);
    //   if (count($rr)) {
        
    //     $key = Dark::datDec($rr[0]);

        //$elak = new Elasticsearch();

        // default only word
        $params = [
          'index' => 'huelladb_autocomplete',
          'body'  => [
              'query' => [
                  "bool" => [
                      "must" => [
                        "term" => [ "name.keywordstring" => $key ],                  
                      ],
                      "filter" => [
                        "term" => [ "modulo" => "noticias" ]
                        // "term" => [ "target" => "input_search" ]
                      ]
                  ]
              ]
              
          ]
        ];

        
        // $query = $elak->client->count($params);
        $query  = $this->elasticsearch->client->count($params);
        $total_rows = !isset($query['count']) ? 0 : $query['count'];
        // echo $key.$total_rows; var_dump($params); die();
        if ($total_rows==0){
          
          // insertamos...
          // $idUser = AifSession::get('id');
          $params = [        
            'index' => 'huelladb_autocomplete',
            'body'  => [
              'id'        => $key,
              'modulo' =>  "noticias",
              // 'target' =>  "input_search",        
              'name'   => $key,
              'created_at' => strtotime("-1d"),
              'icon'      => '',
              'id_user'      => intval($this->session->userdata("id_usuario"))
            ]        
          ];
          
          
          // $response = $elak->client->index($params);
          $query  = $this->elasticsearch->client->index($params);
        }
    //   }
    // }
  }

  function getSearchAutocompleteELK($keyword) {

    $i = 0;
    // echo $keyword; die();
    if ($keyword) {

        // $elak = new Elasticsearch();
        
        $params = [
            'index' => 'huelladb_autocomplete',
            'body'  => [
              "query" => [
                "bool" => [
                  "must" => [
                      "match" => [
                        "name.edgengram" => [
                          "query" =>  "".$keyword,
                          "fuzziness"  =>  1,
                          //"prefix_length" =>  1
                        ]
                      ],                  
                    // "match" => [
                    //     "modulo" =>  "search"
                    // ]
                    // "match" => [
                    //     "target" =>  "input_keyWordx"
                    // ]
                    ],
                    // "must" => [
                    //   "match" => [
                    //         "modulo" =>  "search"
                    //     ]  
                    // ],
                    "filter" => [
                      "term" => [ "modulo" => "noticias" ]
                      // "term" => [ "target" => "input_search" ]
                    ]
                ]
              ]
            ]
          ];
          
         
          // $query                 = $elak->client->search($params);
          // $this->load->library('elasticsearch');
          // var_dump($this->elasticsearch);

          $query  = $this->elasticsearch->client->search($params);
          $len = $hits                  = sizeof($query['hits']['hits']);
          $hit                   = $query['hits']['hits'];

          $i = 0;
          // echo $len; die();
          $arr;
          while ($i < $hits) {  

            $arr2['id'] = $query['hits']['hits'][$i]['_source']['id'];
            $arr2['text'] = $query['hits']['hits'][$i]['_source']['name'];
            $arr2['icon'] = '';
            
            $arr [] = $arr2;
              
            $i++;

          }      
        }

      if ($i==0){
        return null;
      }
      //return new ArrayObject($arr);    
      return ($arr);    
    
  }

  function xyz (){
    $config = array(
        'indent'         => true,
        'output-xhtml'   => true,
        'wrap'           => 200);
    
    // Tidy to avoid errors during load html
    $tidy = new tidy;
    $tidy->parseString($bill->bill_text, $config, 'utf8');
    $tidy->cleanRepair();
    
    $domDocument = new DOMDocument();
    $domDocument->loadHTML(mb_convert_encoding($tidy, 'HTML-ENTITIES', 'UTF-8'));
  }

    function listar($limit, $start, $search , $urlx) {
        $search = addslashes(trim(strtolower($search)));
        
        // $sql = "Select p.url, hash_body, p.titulo_articulo, p.fecha_articulo, ltrim(rtrim(p.body_dom)) as pg,  
        // MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."') as relevance 
        // from paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN NATURAL LANGUAGE MODE) order by relevance desc limit ".$start.", ".$limit." ";
        $sql = "Select p.url, hash_body, p.titulo_articulo, p.fecha_articulo, ltrim(rtrim(p.body_dom)) as pg,  
        MATCH(p.body_dom,p.titulo_articulo) AGAINST('\"".$search."\"') as relevance 
        from paginas as p WHERE MATCH(p.body_dom,p.titulo_articulo) AGAINST('\"".$search."\"' IN BOOLEAN MODE) and length(ltrim(rtrim(p.body_dom)))>500 order by relevance desc limit ".$start.", ".$limit." ";

        // $sql = "Select '',p.url, p.titulo_articulo, p.fecha_articulo, p.body_dom as pg,  
        // MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."') as relevance, 'periodicos' as fuente 
        // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN BOOLEAN MODE)";

        // $sql .= " UNION ";

        // $sql .="Select CONCAT(dp.idSite, hash),ds.url, dp.title,'0000-00-00', dp.content,
        // MATCH( dp.content) AGAINST('".$search."') as relevance, 'darkdb' as fuente
        // from darkDB.pages as dp join darkDB.sites as ds on dp.idSite=ds.idSite WHERE MATCH(dp.content) AGAINST('".$search."' IN BOOLEAN MODE) GROUP BY 1 ";
        
        // $sql .= " order by relevance desc limit $start, $limit";
// die($sql);
        $r = $this->db->query($sql);
        
        /*
        $r = $this->db->select("p.url, p.titulo_articulo, p.fecha_articulo, p.body_dom as pg,  MATCH(p.titulo_articulo,p.body_dom) AGAINST('$search') as relevance")->
            from("paginas as p")->
            where("MATCH(p.titulo_articulo,p.body_dom) AGAINST('$search' )  ", NULL, FALSE)->
            order_by("fecha_articulo asc, relevance desc")->
            limit($limit, $start)->
            get();
        */


        if($r && $r->num_rows()>0) {
            $data = $r->result_array();
            $len = count($data);
            $words = $this->getPalabrasArray($search); 
            
            $this->load->library('Globals');

            for($i=0; $i<$len; $i++) {

                $data[$i]["pg"] =  preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $data[$i]["pg"]);
                $data[$i]["pg"] =  preg_replace('/<style\b[^>]*>(.*?)<\/style>/is', "", $data[$i]["pg"]);
                $data[$i]["pg"] =  preg_replace('/<ul\b[^>]*>(.*?)<\/ul>/is', "", $data[$i]["pg"]);
                $data[$i]["pg"] =  preg_replace('/<li\b[^>]*>(.*?)<\/li>/is', "", $data[$i]["pg"]);
                
                //$data[$i]["pg"] = htmlspecialchars($data[$i]["pg"]);
                //$data[$i]["pg"] = $this->ciertoContenidoHTML($data[$i]["pg"]);
                $data[$i]["pg"] = strip_tags($data[$i]["pg"]);
                //$data[$i]["pg"] = strip_tags($data[$i]["pg"],"<article>");

                //$data[$i]["pg"] = strip_tags($data[$i]["pg"],"<article>");
                // echo $data[$i]["pg"];
                // die();
                

                // quitar palabras especiales
                $data[$i]["pg"] = Globals::eliminarPalabrasEspeciales ($data[$i]["pg"]);

                $tokenContent = $this->getPalabrasArray(strtolower($data[$i]["pg"])); 
                // $tokenSearch = $this->getPalabrasArray(strtolower($search), $del); 
                // var_dump($tokenContent); die();
                $contentjoin = implode(" ", $tokenContent);
                // $searchjoin = implode(" ", $tokenSearch);
                // var_dump($searchjoin); die();
                $searchjoin = implode(" ", $words);
                // var_dump($searchjoin); 
                // var_dump($contentjoin); die();
                $posi = strpos($contentjoin,$searchjoin);
                // echo $posi; die();

                // $data[$i]["pg"]=Globals::eliminarAcentos($data[$i]["pg"]);
                $tantos = 0;
                foreach($words as $w) {
                    // $wsinacentos=Globals::eliminarAcentos ($w);
                    $wsinacentos = $w;
                    $data[$i]["pg"] = preg_replace('/'.$wsinacentos.'/i', '<code>'.$w.'</code>', $data[$i]["pg"]);

                    preg_match('/'.$wsinacentos.'/i', $data[$i]["pg"], $matches, PREG_OFFSET_CAPTURE);
                    //print_r($matches);
                    $tantos+=count($matches);
                    // poner color a toda la variación de caracteres especiales con acentos y ñ
                }
                // echo $tantos; die();
                    // solo poner el extracto donde aparece la busqueda completa.. si coincide sino las primeros 1000 caracteres
                    // declaring delimiters 
                // $del = " "; 
                  
                // calling strtok() function 

                    if ($posi>=0){
                      $tantos = $tantos*12; // 26 es el tamaño de <strong style="color:red">
                      $data[$i]["pg"] = substr($data[$i]["pg"],$posi,1000);
                      // tokenizamos y quitamos la ultima palabra... para no tener problemas con etiquetas HTML abiertas <strong>

                    }else{
                      $data[$i]["pg"] = substr($data[$i]["pg"],0,1000);
                    }

                // echo $data[$i]["pg"];
                // die();
                
                
            }

            // $sql = "Select count(*) as n
            // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN NATURAL LANGUAGE MODE) ";
            $sql = "Select count(*) as n From ( ";
            $sql .= "Select MATCH(body_dom,titulo_articulo) AGAINST('\"".$search."\"') as relevance 
            from paginas  WHERE MATCH(body_dom,titulo_articulo) AGAINST('\"".$search."\"' IN BOOLEAN MODE) and length(ltrim(rtrim(body_dom)))>500 ) as x ";

            // $sql .= "Select p.id_pagina 
            // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN BOOLEAN MODE)";

            // $sql .= " UNION ";

            // $sql .="Select DISTINCT(CONCAT(dp.idSite, hash))
            // from darkDB.pages as dp join darkDB.sites as ds on dp.idSite=ds.idSite  WHERE MATCH(dp.content) AGAINST('".$search."' IN BOOLEAN MODE) ";

            // $sql .= ") as x";
            

            $t = $this->db->query($sql);
            
                

            $len = 0; 
            if($t && $t->num_rows()>0) {
                $len = $t->row()->n;
                // if ($len>1000)
                //   $len=1000;
            }

            return array("data"=>$data, "total"=>$len);
        } else {
            return array("data"=>array(), "total"=>0);
        }
    }

    function listarElk($limit, $start, $search , $urlx) {
      // echo $start.' - '.$limit; die();
      $this->load->library('Globals');
      // $search=Globals::eliminarAcentos ($search);
      $search = addslashes(trim(strtolower($search)));
      //echo $search; die();
      
      // $sql = "Select p.url, hash_body, p.titulo_articulo, p.fecha_articulo, ltrim(rtrim(p.body_dom)) as pg,  
      // MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."') as relevance 
      // from paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN NATURAL LANGUAGE MODE) order by relevance desc limit ".$start.", ".$limit." ";
      // // // $sql = "Select p.url, hash_body, p.titulo_articulo, p.fecha_articulo, ltrim(rtrim(p.body_dom)) as pg,  
      // // // MATCH(p.body_dom,p.titulo_articulo) AGAINST('\"".$search."\"') as relevance 
      // // // from paginas as p WHERE MATCH(p.body_dom,p.titulo_articulo) AGAINST('\"".$search."\"' IN BOOLEAN MODE) and length(ltrim(rtrim(p.body_dom)))>500 order by relevance desc limit ".$start.", ".$limit." ";

      // $sql = "Select '',p.url, p.titulo_articulo, p.fecha_articulo, p.body_dom as pg,  
      // MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."') as relevance, 'periodicos' as fuente 
      // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN BOOLEAN MODE)";

      // $sql .= " UNION ";

      // $sql .="Select CONCAT(dp.idSite, hash),ds.url, dp.title,'0000-00-00', dp.content,
      // MATCH( dp.content) AGAINST('".$search."') as relevance, 'darkdb' as fuente
      // from darkDB.pages as dp join darkDB.sites as ds on dp.idSite=ds.idSite WHERE MATCH(dp.content) AGAINST('".$search."' IN BOOLEAN MODE) GROUP BY 1 ";
      
      // $sql .= " order by relevance desc limit $start, $limit";
// die($sql);
      // // // $r = $this->db->query($sql);
      
      /*
      $r = $this->db->select("p.url, p.titulo_articulo, p.fecha_articulo, p.body_dom as pg,  MATCH(p.titulo_articulo,p.body_dom) AGAINST('$search') as relevance")->
          from("paginas as p")->
          where("MATCH(p.titulo_articulo,p.body_dom) AGAINST('$search' )  ", NULL, FALSE)->
          order_by("fecha_articulo asc, relevance desc")->
          limit($limit, $start)->
          get();
      */
      //////////////////////////////////////////////////////////////////////////////////////////////////
      // $client = $this->elasticsearch;
        $result = array();

      // //   $params = [
      // //     'index' => 'huelladb_paginas',
      // //     'body' => [
      // //         'settings' => [
      // //           'index.max_result_window' => 100000
      // //       ],  
      // //     ]
      // // ];
      
      // // $response = $this->elasticsearch->client->indices()->putSettings($params);        
        // $i = 0;
        // $params = [    
        //   'index' => 'huelladbidx_paginas_x',
        //   'type'  => 'noticias',
        //   'body'  => [
        //       'query' => [
        //         'bool' => [
        //           'must' => [
        //             // 'term' => ['body_dom' => $search],
        //             'term' => ['titulo_articulo' => $search],
        //           ],
        //         ],
        //       ],
        //   ],
        // ];
        // // // $params = [    
        // // //   'index' => 'huelladb_paginas',
        // // //   // 'type'  => 'noticias',
        // // //   'body'  => [      
        // // //       'query' => [
        // // //         'bool' => [
        // // //           'must' => [
        // // //             'multi_match' => [
        // // //                 'query' => $search,
        // // //                 "type" =>       "phrase",
        // // //                 'fields' => ['body_dom^3','titulo_articulo^2','url','body_dom.folded^3','titulo_articulo.folded^2','url.folded'],
        // // //             ],
        // // //             // 'match_phrase' => [ 
        // // //             //   "about"  => "rock climbing"
        // // //             // ]
        // // //           ],
        // // //         ],
        // // //       ],
        // // //   ],
        // // // ];   
        // $params = [    
        //   'index' => 'huelladb_urls_mejorado',
        //   // 'type'  => 'noticias',
        //   'body'  => [      
        //       'query' => [
        //         'bool' => [
        //           'must' => [
        //             'multi_match' => [
        //                 'query' => $search,
        //                 "type" =>       "phrase",
        //                 'fields' => ['body_dom^2','titulo_articulo^2','url'],
        //             ],
        //             // 'match_phrase' => [ 
        //             //   "about"  => "rock climbing"
        //             // ]
        //           ],
        //         ],
        //       ],
        //   ],
        // ];       
        $params = [    
          'index' => 'huelladb_urls_mejorado',
          // 'type'  => 'noticias',
          'body'  => [      
              'query' => [
                'bool' => [
                  // "filter"=> [
                  //   "range"=> [
                  //     "@fecha_articulo"=> [
                  //       "gte" => "now-10Y"
                  //     ]
                  //   ]
                  // ],
                  'should' => [
                    [
                      "match_phrase"=> [
                        "body_dom"=> $search
                      ]
                    ]
                    // // ,
                    // // [
                    // //   "match_phrase"=> [
                    // //     "titulo_articulo"=>  $search
                    // //   ]
                    // // ],
                    // // [
                    // //   "match_phrase"=> [
                    // //     "url"=>  $search
                    // //   ]
                    // // ],
                    // 'multi_match' => [
                    //     'query' => $search,
                    //     "type" =>       "cross_fields",
                    //     'fields' => ['body_dom^2','titulo_articulo^2','url'],
                    //     "operator" =>   "and"  512
                    // ],
                    // 'match_phrase' => [ 
                    //   "about"  => "rock climbing"
                    // ]
                    // "multi_match" => [
                    //   "query" => $search,
                    //   "fields" => [ 'body_dom^2','titulo_articulo^2','url' ]  39429
                    // ]   
                    // "match_phrase"=> [
                    //   "body_dom"=> [
                    //     "query"=>  $search 
                    //   ]
                    // ]
                  ]
                ],
              ],
          ],
        ];           
        $query                 = $this->elasticsearch->client->count($params);
        $total_rows = $query['count'];
        // var_dump($query); die();


        // $params = [
        //     "from" =>  $start,
        //     "size" =>  $limit,   
        //     'index' => 'huelladb_urls_mejorado',
        //     // 'type'  => 'noticias',
        //     'body'  => [                
        //       'query' => [
        //         'bool' => [
        //           'must' => [
        //             'multi_match' => [
        //                 'query' => $search,
        //                 "type" =>       "phrase",
        //                 'fields' => ['body_dom^3','titulo_articulo^2','url','body_dom.folded^3','titulo_articulo.folded^2','url.folded'],
        //             ],
        //             // 'match_phrase' => [ 
        //             //   "about"  => "rock climbing"
        //             // ]
        //           ],
        //         ],
        //       ],
        //       '_source' => ['body_dom','titulo_articulo','fecha_articulo','url','target','hash_body']
        //     ],
        // ];
        $params = [
          "from" =>  $start,
          "size" =>  $limit,   
          'index' => 'huelladb_urls_mejorado',
          // 'type'  => 'noticias',
          'body'  => [                
            'query' => [
              'bool' => [
                // 'must' => [
                //   'multi_match' => [
                //       'query' => $search,
                //       "type" =>       "cross_fields",
                //       'fields' => ['body_dom^3','titulo_articulo^2','url'],
                //       "operator" =>   "and"
                //   ],
                  // 'match_phrase' => [ 
                  //   "about"  => "rock climbing"
                  // ]
                // ],
                'should' => [
                  [
                    "match_phrase"=> [
                      "body_dom"=> $search
                    ]
                  ]
                  // // ,
                  // // [
                  // //   "match_phrase"=> [
                  // //     "titulo_articulo"=>  $search
                  // //   ]
                  // // ],
                  // // [
                  // //   "match_phrase"=> [
                  // //     "url"=>  $search
                  // //   ]
                  // // ]
                ]
              ],
            ],
            '_source' => ['body_dom','titulo_articulo','fecha_articulo','url','target','hash_body'],
            "sort" => [
              [ "fecha_articulo" => ["order" => "desc"]],
              "_score"
            ]
          ],
        ];        
        $query                 = $this->elasticsearch->client->search($params);
        $len = $hits                  = sizeof($query['hits']['hits']);
        $hit                   = $query['hits']['hits'];
        // $result['searchfound'] = $hits;
        // // while ($i < $hits) {

        //     $result['result'][$i] = $query['hits']['hits'][$i]['_source'];

        //     $i++;
        // }

        //return  $result;
        //////////////////////////////////////////////////////////////////////////////////////////////////


      // // // if($r && $r->num_rows()>0) {
      if($hits>0) {
          // // // $data = $r->result_array();
          // // // $len = count($data);
          $words = $this->getPalabrasArray($search); 
          // var_dump($words); die();
          
          

          // // for($i=0; $i<$len; $i++) {
          $i = 0;
          while ($i < $hits) {  

            $data[$i]["pg"] = $query['hits']['hits'][$i]['_source']['body_dom'];
            $data[$i]["titulo_articulo"] = $query['hits']['hits'][$i]['_source']['titulo_articulo'];
            $data[$i]["fecha_articulo"] = $query['hits']['hits'][$i]['_source']['fecha_articulo'];
            $data[$i]["hash_body"] = $query['hits']['hits'][$i]['_source']['hash_body'];
            $data[$i]["url"] = $query['hits']['hits'][$i]['_source']['url'];
            $data[$i]["target"] = $query['hits']['hits'][$i]['_source']['target'];
            
            

              $data[$i]["pg"] =  preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $data[$i]["pg"]);
              $data[$i]["pg"] =  preg_replace('/<style\b[^>]*>(.*?)<\/style>/is', "", $data[$i]["pg"]);
              $data[$i]["pg"] =  preg_replace('/<ul\b[^>]*>(.*?)<\/ul>/is', "", $data[$i]["pg"]);
              $data[$i]["pg"] =  preg_replace('/<li\b[^>]*>(.*?)<\/li>/is', "", $data[$i]["pg"]);
              // echo $data[$i]["pg"];
              
              //$data[$i]["pg"] = htmlspecialchars($data[$i]["pg"]);
              //$data[$i]["pg"] = $this->ciertoContenidoHTML($data[$i]["pg"]);
              
              $data[$i]["pg"] = strip_tags($data[$i]["pg"]);
              
              //$data[$i]["pg"] = strip_tags($data[$i]["pg"],"<article>");

              //$data[$i]["pg"] = strip_tags($data[$i]["pg"],"<article>");
              // echo $data[$i]["pg"];
              // die();
              

              // quitar palabras especiales
              $data[$i]["pg"] = Globals::eliminarPalabrasEspeciales ($data[$i]["pg"]);
              $data[$i]["pg"] = html_entity_decode ($data[$i]["pg"]);
              //echo $data[$i]["pg"]; die();
              

              $tokenContent = $this->getPalabrasArray(strtolower($data[$i]["pg"])); 
              // $tokenSearch = $this->getPalabrasArray(strtolower($search), $del); 
              // var_dump($tokenContent); die();
              $contentjoin = implode(" ", $tokenContent);
              // $searchjoin = implode(" ", $tokenSearch);
              // var_dump($searchjoin); die();
              $searchjoin = implode(" ", $words);
              // var_dump($searchjoin); 
              // var_dump($contentjoin); die();
              $posi = strpos($contentjoin,$searchjoin);
              // echo $posi; die();

              // $data[$i]["pg"]=Globals::eliminarAcentos($data[$i]["pg"]);
              $tantos = 0;
              foreach($words as $w) {
                  
                  // echo 'ok ';
                  $wsinacentos = $w;
                  $data[$i]["pg"] = preg_replace('/'.$wsinacentos.'/i', '<code>'.$w.'</code>', $data[$i]["pg"]);

                  $wsinacentos=Globals::eliminarAcentos ($w);
                  $tempx  = Globals::eliminarAcentos($data[$i]["pg"]);
                  

                  preg_match('/'.$wsinacentos.'/i', $tempx, $matches, PREG_OFFSET_CAPTURE);
                  //print_r($matches);
                  $tantos+=count($matches);
                  // echo $tantos.'- '.$wsinacentos.' - '.$tempx; die();
                  //  echo 'ok ';
                  // poner color a toda la variación de caracteres especiales con acentos y ñ
              }
              // var_dump($words);
              // echo $tantos; die();
                  // solo poner el extracto donde aparece la busqueda completa.. si coincide sino las primeros 1000 caracteres
                  // declaring delimiters 
              // $del = " "; 
                
              // calling strtok() function 

                  if ($posi>0){
                    
                    //$tantos = $tantos*12; // 26 es el tamaño de <strong style="color:red">
                    //$data[$i]["pg"] = substr($data[$i]["pg"],$posi,1000);
                    // $temp = substr($data[$i]["pg"],$posi,1000);
                    $temp = substr($data[$i]["pg"],$posi,300);
                    $ultimaCoin2x = strripos($temp,'</c');
                    if ($ultimaCoin2x>995){
                      //$data[$i]["pg"] = substr($data[$i]["pg"],$posi,1000+(1000-$ultimaCoin2x));
                      $data[$i]["pg"] = substr($data[$i]["pg"],$posi,300 );
                    }
                    //echo $data[$i]["pg"]; die();
                    
                    // echo 'longitud: '.strlen(trim($data[$i]["pg"])).' - '.$ultimaCoin;
                    if ($tantos>=1){
                      // tokenizamos y quitamos la ultima palabra... para no tener problemas con etiquetas HTML abiertas <strong>
                      // $ultimaCoin = strripos($data[$i]["pg"],'<code>');
                      // $data[$i]["pg"] = substr($data[$i]["pg"],0,$ultimaCoin-1);
                      $ultimaCoin = strpos($data[$i]["pg"],'</code>');
                      //$data[$i]["pg"] = substr($data[$i]["pg"],0,$ultimaCoin+7);
                      $inicio = 0; 
                      if ($ultimaCoin>300){
                        $inicio = $ultimaCoin-300;
                      }
                      $data[$i]["pg"] = substr($data[$i]["pg"],$inicio,$ultimaCoin+7);
                       //echo $ultimaCoin.'- '.$data[$i]["pg"]; die();
                    }
                    // else{
                    //   $data[$i]["pg"].="</code>";
                    //   // echo  $data[$i]["pg"]; die();
                    // }
                    // echo $ultimaCoin.'-'.$data[$i]["pg"]; die();
                    // echo ' = '.strlen(trim($data[$i]["pg"]));
                  }else{
                    // no se encontro en el body, solo en el titulo
                    $data[$i]["pg"] = substr($data[$i]["pg"],0,300);
                    // $ultimaCoin2x = strripos($temp,'</c');
                    // if ($ultimaCoin2x>995){
                    //   $data[$i]["pg"] = substr($data[$i]["pg"],$posi,1000+(1000-$ultimaCoin2x));
                    // }
                  }

                  // al final se convierte a codigos html si se requiere
                  //$data[$i]["pg"] =  htmlentities ($data[$i]["pg"]);
              // echo $data[$i]["pg"];
              // die();
              
            $i++;

          }
          // die();

          // $sql = "Select count(*) as n
          // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN NATURAL LANGUAGE MODE) ";
          // // // $sql = "Select count(*) as n From ( ";
          // // // $sql .= "Select MATCH(body_dom,titulo_articulo) AGAINST('\"".$search."\"') as relevance 
          // // // from paginas  WHERE MATCH(body_dom,titulo_articulo) AGAINST('\"".$search."\"' IN BOOLEAN MODE) and length(ltrim(rtrim(body_dom)))>500 ) as x ";

          // $sql .= "Select p.id_pagina 
          // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN BOOLEAN MODE)";

          // $sql .= " UNION ";

          // $sql .="Select DISTINCT(CONCAT(dp.idSite, hash))
          // from darkDB.pages as dp join darkDB.sites as ds on dp.idSite=ds.idSite  WHERE MATCH(dp.content) AGAINST('".$search."' IN BOOLEAN MODE) ";

          // $sql .= ") as x";
          

          // // // $t = $this->db->query($sql);
          
              

          // // // $len = 0; 
          // // // if($t && $t->num_rows()>0) {
          // // //     $len = $t->row()->n;
          // // //     // if ($len>1000)
          // // //     //   $len=1000;
          // // // }

          return array("data"=>$data, "total"=>$total_rows);
      } else {
          return array("data"=>array(), "total"=>0);
      }
  }
}
