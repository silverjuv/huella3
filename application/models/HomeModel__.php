<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeModel extends CI_Model{

	function __construct() {
		parent::__construct();
    }

    function actualizarPass($id_usuario, $pass, $passn) {
        $id_usuario = intval($id_usuario);

        $r = $this->db->select("pass")->from("cat_usuarios")->
            where("id_usuario", $id_usuario)->
            where("borrado='0'")->
            get();
        
        if($r && $r->num_rows()>0) {
            $r = $r->row();
            if(password_verify($pass, $r->pass)) {
                $passn = password_hash($passn, PASSWORD_BCRYPT);
                $this->db->where("id_usuario", $id_usuario)->set("pass", $passn);
                if($this->db->update("cat_usuarios")) {
                    return array("title"=>"Info", "msg"=>"La contraseña se cambio exitosamente", "type"=>"success");
                } else {
                    return array("title"=>"Atención", "msg"=>"No fue posible cambiar la contraseña", "type"=>"error");
                }
            }else{
                return array("title"=>"Atención", "msg"=>"Contraseña anterior invalida!", "type"=>"error");
            }
        }
        
    }

    function obtenerDatosCliente($cliente){
        $r = $this->db->select("u.descripcion as cliente, cc.*")->from("cat_clientes as u")->
        join("clientes_contratos as cc","cc.id_cliente=u.id_cliente and cc.status='A'",'left')->
        where("u.id_cliente", $cliente)->
        // where("borrado='0'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row();
        }
    }

    function obtenerTotalPersonasCliente($cliente){
        $r = $this->db->select("count(*) as total ")->from("cat_personas")->
        where("id_cliente", $cliente)->
        // where("borrado='0'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalUsuarios(){
        $r = $this->db->select("count(*) as total ")->from("cat_usuarios")->
        where("inactivo", 0)->
        // where("borrado='0'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalNoticias(){
        $r = $this->db->select("count(*) as total ")->from("paginas")->
        // where("inactivo", 0)->
        // where("borrado='0'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalPuestosCliente($cliente){
        $r = $this->db->select("count(*) as total ")->from("clientes_puestos")->
        where("id_cliente", $cliente)->
        where("activo=1")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalClientes(){
        $r = $this->db->select("count(*) as total ")->from("cat_clientes")->
        // where("id_cliente", $cliente)->
        // where("activo=1")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalContratos(){
        $r = $this->db->select("count(*) as total ")->from("clientes_contratos")->
        //where('id_usuario IN (SELECT id_usuario FROM cat_usuarios WHERE id_cliente='.$cliente.')', NULL, FALSE)->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalExpedientesCliente($cliente){
        $r = $this->db->select("count(*) as total ")->from("expedientes_anexos")->
        where('id_usuario IN (SELECT id_usuario FROM cat_usuarios WHERE id_cliente='.$cliente.')', NULL, FALSE)->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalConsumosCliente($cliente){
        $r = $this->db->select("num_consumos ")->from("clientes_contratos")->
        where("id_cliente", $cliente)->
        where("status='A'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->num_consumos;
        }
        return 0;
    }

    function obtenerTotalConsumos(){
        $r = $this->db->select("num_consumos ")->from("clientes_contratos")->
        // where("id_cliente", $cliente)->
        // where("status='A'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->num_consumos;
        }
        return 0;
    }

    // function obtenerTotalRiesgos(){
    //     $r = $this->db->select("num_consumos ")->from("clientes_contratos")->
    //     // where("id_cliente", $cliente)->
    //     // where("status='A'")->
    //     get();
    
    //     if($r && $r->num_rows()>0) {
    //         return $r->row()->num_consumos;
    //     }
    //     return 0;
    // }

    function obtenerTotalPromedioResultados($cliente){
        $r = $this->db->select("avg(ponderacion) as promedio")->from("resultados")->
        where("id_cliente", $cliente)->
        // where("status='A'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->promedio;
        }
        return 0;
    }

    function obtenerTotalProcesosMensuales($cliente){
        $r = $this->db->select("Month(fecha_resultado) as mes, count(*) as total ")->from("resultados")->
        where('fecha_resultado >= makedate(year(curdate()), 1)
        and fecha_resultado < makedate(year(curdate()) + 1, 1) and id_cliente='.$cliente, NULL, FALSE)->
        group_by('Month(fecha_resultado)')->
        order_by('mes', 'asc')->
        get();
        // die($this->db->last_query());
    
        $lista = array(0,0,0,0,0,0,0,0,0,0,0,0);
        if($r && $r->num_rows()>0) {
            
            foreach ($r->result() as $row)
            {
                $lista[$row->mes-1]=$row->total;
            }
            return $lista;
        }
        return $lista;
    }

    function obtenerTotalProcesosPorMes(){
        $r = $this->db->select("Month(fecha_resultado) as mes, count(*) as total ")->from("resultados")->
        // where('fecha_resultado >= makedate(year(curdate()), 1)
        // and fecha_resultado < makedate(year(curdate()) + 1, 1) ', NULL, FALSE)->
        group_by('Month(fecha_resultado)')->
        order_by('mes', 'asc')->
        get();
        // die($this->db->last_query());
    
        $lista = array(0,0,0,0,0,0,0,0,0,0,0,0);
        if($r && $r->num_rows()>0) {
            
            foreach ($r->result() as $row)
            {
                $lista[$row->mes-1]=$row->total;
            }
            return $lista;
        }
        return $lista;
    }

    function obtenerTotalRiesgosEncontrados($cliente){
        $r = $this->db->select("riesgo, count(*) as total ")->from("cat_riesgos as ries")->
        join("resultados as res","ries.id=res.id_riesgo_asociado ",'left')->
        where(' id_cliente='.$cliente, NULL, FALSE)->
        group_by('riesgo')->
        // order_by('mes', 'asc')->
        get();
        // die($this->db->last_query());
    
        if($r && $r->num_rows()>0) {
            $listaRiesgos = array();
            $listaTotales = array();
            $colorsBack = array();
            $ix = 0;
            foreach ($r->result() as $row)
            {
                $listaRiesgos[$ix]=$row->riesgo;
                $listaTotales[$ix]=$row->total;
                $colorsBack[$ix++]='rgba('.random_int(0, 255).', '.random_int(0, 255).', '.random_int(0, 255).', 0.2)';
            }
            return ['riesgos'=>$listaRiesgos, 'totales' => $listaTotales, 'colorsBack' => $colorsBack];
        }

        return ['riesgos'=>[], 'totales' => [], 'colorsBack' => []];
    }

    function obtenerTotalRiesgos(){
        $r = $this->db->select("count(*) as riesgos ")->from("cat_riesgos as ries")->
        join("resultados as res","ries.id=res.id_riesgo_asociado ",'left')->
        // where(' id_cliente='.$cliente, NULL, FALSE)->
        get();
        // die($this->db->last_query());
    
        if($r && $r->num_rows()>0) {
            return $r->row()->riesgos;
        }
        return 0;
    }

    function obtenerTotalRiesgosMasLocalizados(){
        $r = $this->db->select("riesgo, count(*) as total ")->from("cat_riesgos as ries")->
        join("resultados as res","ries.id=res.id_riesgo_asociado ",'left')->
        // where(' id_cliente='.$cliente, NULL, FALSE)->
        group_by('riesgo')->limit(10)->
        order_by('total', 'desc')->
        get();
        // die($this->db->last_query());
    
        if($r && $r->num_rows()>0) {
            $listaRiesgos = array();
            $listaTotales = array();
            $colorsBack = array();
            $ix = 0;
            foreach ($r->result() as $row)
            {
                $listaRiesgos[$ix]=$row->riesgo;
                $listaTotales[$ix]=$row->total;
                $colorsBack[$ix++]='rgba('.random_int(0, 255).', '.random_int(0, 255).', '.random_int(0, 255).', 0.5)';
            }
            return ['riesgos'=>$listaRiesgos, 'totales' => $listaTotales, 'colorsBack' => $colorsBack];
        }

        return ['riesgos'=>[], 'totales' => [], 'colorsBack' => []];
    }

    function obtenerTotalColoresRiesgos($cliente){
        $r = $this->db->select("ponderacion")->from("resultados")->
        where(' id_cliente='.$cliente, NULL, FALSE)->
        get();
    
        $lista = array(0,0,0,0);
        if($r && $r->num_rows()>0) {            
            foreach ($r->result() as $row)
            {
                switch($row->ponderacion){
                    case 0:
                        $lista[0]=$lista[0]+1;
                    break;
                    case 1:
                    case 2:
                    case 3:
                        $lista[1]=$lista[1]+1;
                    break;
                    case 4:
                    case 5:
                    case 6:
                        $lista[2]=$lista[2]+1;
                    break;
                    case 7:
                    case 8:
                    case 9:
                        $lista[3]=$lista[3]+1;
                    break;
                }
            }
        }
        return $lista;
    }

}