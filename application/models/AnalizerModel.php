<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/models/GenericModel.php';

class AnalizerModel extends GenericModel {

	private $tabla;

	function __construct() {
		parent::__construct();
		$this->tabla = "analisis";
	}
	

	function generaAnalisis($d) {
		$this->db->set('fecha_programacion', 'NOW()', FALSE);
		return $this->insertRow($this->tabla, $d);
	}

	function getFuentes() {
		$r = $this->db->select("fuente ")->
			from("contratos_fuentes as cf")->
			join("clientes_contratos as cc", "cc.id_contrato=cf.id_contrato")->
			where("cc.id_cliente=".$_SESSION['id_cliente']." and cc.status='A'")->get();

			if($r && $r->num_rows()>0) {
				// return $datos = $r->result_array();
				$arrx = array();
				foreach($r->result_array() as $row){
					array_push($arrx,$row['fuente']);
				}
				return implode(',',$arrx);
			}else{
				return '';
			}
	}

	function listarPersonas($exportar=false) {
		$draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        
        $search = $this->input->post("search");
		$search = ( empty($search["value"]) )? "": addslashes($search["value"]);
		
		$filtroAdicional = '';
		if ($_SESSION['id_perfil']!=PERFIL_ADMIN){
			// filtrado para clientes...
			$filtroAdicional = ' and c.id_cliente='.$_SESSION['id_cliente'];
		}

		$this->db->select("p.id_persona, p.curp, CONCAT(coalesce(p.paterno, ''), ' ', coalesce(p.materno, ''), ' ', p.nombre) as nombre, 
						p.email, p.celular ")->
			from("cat_personas as p")->
			join("cat_clientes as c", "p.id_cliente=c.id_cliente")->
			where("p.borrado=0 ".$filtroAdicional)->
			group_start()->
			like("CONCAT(p.paterno,' ', COALESCE(p.materno,''),' ',p.nombre)", $search, "both")->
			or_like("p.curp", $search, "both")->
			or_like("p.email", $search, "both")->
			or_like("c.descripcion", $search, "both")->
			group_end();
			if ($exportar==false)
				$this->db->limit($length, $start);
            $this->db->order_by("p.nombre");
            $r = $this->db->get();
        // die($this->db->last_query());
        if($r && $r->num_rows()>0) {
			$nRegistros = $this->db->from("cat_personas as p")->
				join("cat_clientes as c", "p.id_cliente=c.id_cliente")->
                where("p.borrado=0")->
				group_start()->
				like("CONCAT(p.paterno,' ', COALESCE(p.materno,''),' ',p.nombre)", $search, "both")->
				or_like("p.curp", $search, "both")->
				or_like("p.email", $search, "both")->
				or_like("c.descripcion", $search, "both")->
				group_end()->
                count_all_results();

				$datos = $r->result_array();
				$datosX = array();
				$sec = 0;
				foreach($datos as $row){
					$programados = $this->getProgramados($row['id_persona'] );
					$cadx = '';
					$analisados='';
					foreach($programados as $analisis){
						$analisados.= ", <strong>".$analisis['puesto']."</strong> (".$analisis['totales'].")";
					}

					$row['seleccionar'] = '<div style="width:100%; text-align:center; font-size:9px"><input type="checkbox" id="ck'.$row['id_persona'].'" /></div>';
					$row['analisis'] = '<div style="width:100%; text-align:left; font-size:9px">'.substr($analisados,1).'</div>';
					$sec++;
					array_push($datosX,$row);
				}
		
				$data["data"] = $datosX;      

            $data["recordsTotal"] = $r->num_rows();
            $data["recordsFiltered"] = $nRegistros;
            $data["draw"] = $draw;
            // $data["data"] = $r->result_array();
            
            return $data;
        } else {
            $data["recordsTotal"] = 0;
            $data["recordsFiltered"] = 0;
            $data["draw"] = $draw;
            $data["data"] = array();
            return $data;
        }	
	}

	function getProgramados($id_persona){
        $this->db->select("pr.puesto, count(*) as totales");
        $this->db->from("analisis as aa");
		$this->db->join("cat_puestos as pr","aa.id_puesto=pr.id");
        $this->db->where("id_persona",$id_persona);
		$this->db->group_by("puesto");
        $r=$this->db->get();	
		// die($this->db->last_query());
		return $r->result_array();
    }
}