<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatPuestosRiesgosModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function listarClientes(){
        $this->db->select("id_cliente as id,descripcion as text");
        $this->db->from("cat_clientes");
        $this->db->where("borrado",0);
        $this->db->order_by("descripcion");
        $r=$this->db->get();
        $datos = $r->result_array();
        $datos=($datos);
        return $datos;
    }

    function listarPuestos(){
        // $id_cliente = intval($this->input->post("id_cliente"));
        $where["id_cliente"]=$_SESSION['id_cliente'];
        $where["activo"]=intval(1);
        // $this->db->select("cp.id, cp.puesto as text");
        $this->db->select("cp.id, cp.puesto as text");
        $this->db->from("cat_puestos cp");
        // $this->db->join("clientes_puestos pp","cp.id=pp.id_puesto");
        $this->db->where($where);
        $r = $this->db->get();
         if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }
    }

    function listarRiesgos(){
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,id as id_riesgo,riesgo");
        $this->db->from("cat_riesgos");
        $this->db->like("riesgo", $search, "both");
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->order_by("id");
        $r=$this->db->get();
        $datos = $r->result_array();
        // $datos=($datos);
        $datosX = array();
        foreach($datos as $row){
            $subriesgos = $this->getRiesgosSubriesgos($row['id_riesgo'] );
            $cadx = '';
            foreach($subriesgos as $riesgo){
                $cadx.= ", ".$riesgo['subriesgo'];
            }
            $puntos = '';
            if (strlen($cadx)>100)
                $puntos = '...';
            $row['subriesgos'] = '<div style="width:100%; text-align:center; font-size:9px">'.substr($cadx,1,100).$puntos.'</div>';
            array_push($datosX,$row);
        }

        $result["data"] = $datosX;      

        $result["recordsFiltered"] = $totalRows;
        // $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }

    function getRiesgosSubriesgos($id_riesgo){
        $this->db->select("subriesgo");
        $this->db->from("riesgos_subriesgos");
        $this->db->where("id_riesgo",$id_riesgo);
        $r=$this->db->get();	
		return $r->result_array();
    }

    function listarRiesgosSubriesgos($id_riesgo){
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,id_riesgo,subriesgo as riesgo");
        $this->db->from("riesgos_subriesgos");
        $this->db->where("id_riesgo",$id_riesgo);
        $this->db->like("subriesgo", $search, "both");
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->order_by("id_riesgo");
        $r=$this->db->get();
        $datos = $r->result_array();
        $datos=($datos);
        $result["recordsFiltered"] = $totalRows;
        $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }

    function listarPuestosRiesgos($id_puesto,$id_cliente){
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,r.id as id_riesgo,r.riesgo");
        $this->db->from("cat_riesgos as r");
        $this->db->join("puestos_riesgos as pr","r.id=pr.id_riesgo");
        $this->db->where("pr.id_puesto",$id_puesto);
        // $this->db->where("pr.id_cliente",$id_cliente);
        $this->db->like("r.riesgo", $search, "both");
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->order_by("r.id");
        $r=$this->db->get();
        $datos = $r->result_array();
        // $datos=($datos);
        $datosX = array();
        foreach($datos as $row){
            $subriesgos = $this->getRiesgosSubriesgos($row['id_riesgo'] );
            $cadx = '';
            foreach($subriesgos as $riesgo){
                $cadx.= ", ".$riesgo['subriesgo'];
            }
            $puntos = '';
            if (strlen($cadx)>100)
                $puntos = '...';
            $row['subriesgos'] = '<div style="width:100%; text-align:center; font-size:9px">'.substr($cadx,1,100).$puntos.'</div>';
            array_push($datosX,$row);
        }

        $result["data"] = $datosX;  
        
        $result["recordsFiltered"] = $totalRows;
        // $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }

    private function puestoTieneRiesgo($id_puesto, $id_riesgo) {
        // ,$id_cliente
        $id_puesto = intval($id_puesto);
        $id_riesgo = intval($id_riesgo);
        // $id_cliente = intval($id_cliente);
        $n = $this->db->from("puestos_riesgos")->
            where("id_puesto", $id_puesto)->
            where("id_riesgo", $id_riesgo)->
            // where("id_cliente", $id_cliente)->
            count_all_results();
        return ($n>0);
    }


    function insertarPuesto($id_cliente,$d) {
        $ban=TRUE;//bandera para determinar si la transaccion se realiza o se da rollback
		$msgError="";//variable para retornar el mensaje de error al usuario	
        $this->db->trans_begin();
        $d["id_cliente"]=intval($id_cliente);
        // $d["activo"] =intval(1);
        // var_dump($d); die();
        $this->db->set('fecha_captura', 'NOW()', FALSE);
        if(!$this->db->insert("cat_puestos", $d)) {
            $ban=FALSE;
            $msgError.="No fue posible guardar el registro en catalogo de puestos";
        }else{
            // echo $this->db->_error_message(); die();
            // $sqlscope="SELECT LAST_INSERT_ID() as scope";
		    // $queryscope = $this->db->query($sqlscope);
		    // $datascope = $queryscope->result_array();
            // $id_puesto=$datascope[0]['scope'];
            // $clientes_puestos["id_cliente"]=intval($id_cliente);
            // $clientes_puestos["id_puesto"] =intval($id_puesto);
            // $clientes_puestos["activo"] =intval(1);
            // if(!$this->db->insert("clientes_puestos", $clientes_puestos)) {
                $ban=TRUE;
                // $msgError.="No fue posible guardar el registro en clientes_puestos";
                // $msgError.="No fue posible guardar el registro en catalogo de puestos";
            // }
        }
        if($this->db->trans_status() === TRUE && $ban==TRUE){	
			$this->db->trans_commit();//si todo ha resultado bien se realiza la transaccion		
			return array("error"=>"0","msg"=>"Se a creado el puesto");
		}else if ($this->db->trans_status() === FALSE || $ban==FALSE){
			$this->db->trans_rollback();//si ocurrio algun error en el ciclo se da rollback a la transaccion
			return array("error"=>"1","msg"=>$msgError);
		}         
    }

    function actualizarPuesto($id_puesto,$id_cliente,$d) {
        $id_puesto = intval($id_puesto);
        if($this->db->update("cat_puestos", $d, "id=".$id_puesto)) {
            return array("error"=>"0","msg"=>"Se ha actualizado el registro");
        } else {
            return array("error"=>"1","msg"=>"No fue posible guardar el registro");
        }
    }

    function insertar($id_puesto, $id_riesgo) {
        // ,$id_cliente
        $id_puesto = intval($id_puesto);
        $id_riesgo = intval($id_riesgo);
        if($this->puestoTieneRiesgo($id_puesto, $id_riesgo)) {
            // ,$id_cliente
            return array("error"=>"1", "title"=>"Atención", "msg"=>"El riesgo fue asociado con anterioridad...", "type"=>"notice");
        } else {
            $r = array("id_puesto"=>$id_puesto, "id_riesgo"=>$id_riesgo);
            // ,"id_cliente"=>$id_cliente
            if($this->db->insert("puestos_riesgos", $r)) {
                return array("error"=>"0", "title"=>"Info", "msg"=>"El riesgo fue asociada con exito...", "type"=>"success");
            } else {
                return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
            }
        }
    }

    function quitarRiesgoPuesto($id_puesto, $id_riesgo) {
        // ,$id_cliente
        $id_puesto = intval($id_puesto);
        $id_riesgo = intval($id_riesgo);
        // $id_cliente = intval($id_cliente);
        if($this->db->delete("puestos_riesgos", "id_puesto=".$id_puesto." and id_riesgo=".$id_riesgo."")) {
            //  and id_cliente=".$id_cliente
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha quitado el riesgo con exito...", "type"=>"success");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible quitar el riesgo", "type"=>"error");
        }
    }

}