<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatPerfilesModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function getPerfilesForCombo() {
        $r = $this->db->select("id_perfil as id, descripcion as text")->get("cat_perfiles");
        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
            print "";
        }
    }

    function listar() {
        $r = $this->db->select("p.*")->
            from("cat_perfiles as p")->
            where("borrado='0'")->get();

        if($r && $r->num_rows()>0) {
            return array("data"=>$r->result_array());
        } else {
            return array("data"=>array());
        }
    }

    function insertar($d) {
        if($this->db->insert("cat_perfiles", $d)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function actualizar($id, $d) {
        $id = intval($id);
        if($this->db->update("cat_perfiles", $d, "id_perfil=".$id)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function getPerfilById($id_perfil) {
        $id_perfil = intval($id_perfil);
        $r = $this->db->select("*")->from("cat_perfiles")->
            where("id_perfil", $id_perfil)->
            where("borrado='0'")->
			get();
		
		if($r && $r->num_rows()>0) {
			$data = $r->row();
			$d = ($data);
			return $d;
		} else {
			return array("data"=>array());
		}
    }

    function deletePerfilById($id) {
        $id = intval($id);
        if($this->db->update("cat_perfiles", array("borrado"=>"1"), "id_perfil=".$id)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha borrado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible borrar el registro", "type"=>"error");
        }
    }


}