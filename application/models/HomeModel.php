<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeModel extends CI_Model{

	function __construct() {
		parent::__construct();
    }

    function actualizarPass($id_usuario, $pass, $passn) {
        $id_usuario = intval($id_usuario);

        $r = $this->db->select("pass")->from("cat_usuarios")->
            where("id_usuario", $id_usuario)->
            where("borrado='0'")->
            get();
        
        if($r && $r->num_rows()>0) {
            $r = $r->row();
            if(password_verify($pass, $r->pass)) {
                $passn = password_hash($passn, PASSWORD_BCRYPT);
                $this->db->where("id_usuario", $id_usuario)->set("pass", $passn);
                if($this->db->update("cat_usuarios")) {
                    return array("title"=>"Info", "msg"=>"La contraseña se cambio exitosamente", "type"=>"success");
                } else {
                    return array("title"=>"Atención", "msg"=>"No fue posible cambiar la contraseña", "type"=>"error");
                }
            }
        }
        
    }

    function obtenerDatosCliente($cliente){
        $r = $this->db->select("u.descripcion as cliente, cc.*")->from("cat_clientes as u")->
        join("clientes_contratos as cc","cc.id_cliente=u.id_cliente and cc.status='A'",'left')->
        where("u.id_cliente", $cliente)->
        // where("borrado='0'")->
        get();
    
        if($r && $r->num_rows()>0) {

            $arr = array();
            foreach ($r->result_array() as $row) {
                $f = $this->db->select("fuente, text as fuentex")->from("contratos_fuentes as cf")->
                join("cat_fuentes as catf", "catf.id=cf.fuente")->
                where("id_contrato", $row['id_contrato'])->get();

                if($f && $f->num_rows()>0) {
                    $fuentes = $f->result_array();
                    $fuentes = array_column($fuentes, "fuentex");

                    $row['fuentes'] = $fuentes;
                } else {
                    $row['fuentes'] = array();
                }
                // array_push($arr, $row);
                $arr = $row;
            }
            // $data["data"] = $arr;

            // return $r->row();
            return $arr;
        }
    }

    function obtenerTotalPersonasCliente($cliente){
        $r = $this->db->select("count(*) as total ")->from("cat_personas")->
        where("id_cliente", $cliente)->
        // where("borrado='0'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalUsuarios(){
        $r = $this->db->select("count(*) as total ")->from("cat_usuarios")->
        where("inactivo", 0)->
        // where("borrado='0'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalNoticias(){
        $r = $this->db->select("count(*) as total ")->from("paginas2")->
        // where("inactivo", 0)->
        // where("borrado='0'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalPuestosCliente($cliente){
        $r = $this->db->select("count(*) as total ")->from("cat_puestos")->
        where("id_cliente", $cliente)->
        where("activo=1")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalClientes(){
        $r = $this->db->select("count(*) as total ")->from("cat_clientes")->
        // where("id_cliente", $cliente)->
        // where("activo=1")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalContratos(){
        $r = $this->db->select("count(*) as total ")->from("clientes_contratos")->
        //where('id_usuario IN (SELECT id_usuario FROM cat_usuarios WHERE id_cliente='.$cliente.')', NULL, FALSE)->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalExpedientesCliente($cliente){
        $r = $this->db->select("count(*) as total ")->from("expedientes_anexos")->
        where('id_usuario IN (SELECT id_usuario FROM cat_usuarios WHERE id_cliente='.$cliente.')', NULL, FALSE)->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->total;
        }
        return 0;
    }

    function obtenerTotalAnalisisCliente($cliente){

        if ($cliente==-1){
            $r = $this->db->select("resultado ")->from("analisis as analy")->
            join("cat_personas as catper","catper.id_persona=analy.id_persona ",'left')->        
            where("status='P'");
        }else{
            $r = $this->db->select("resultado ")->from("analisis as analy")->
            join("cat_personas as catper","catper.id_persona=analy.id_persona ",'left')->        
            where("catper.id_cliente", $cliente)->
            where("status='P'");
        }
    
        return $this->db->count_all_results();
        // return 0;
    }

    function obtenerTotalConsumosCliente($cliente){
        $r = $this->db->select("num_consumos ")->from("clientes_contratos")->
        where("id_cliente", $cliente)->
        where("status='A'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->num_consumos;
        }
        return 0;
    }

    function obtenerTotalConsumos(){
        $r = $this->db->select("num_consumos ")->from("clientes_contratos")->
        // where("id_cliente", $cliente)->
        // where("status='A'")->
        get();
    
        if($r && $r->num_rows()>0) {
            return $r->row()->num_consumos;
        }
        return 0;
    }

    // function obtenerTotalRiesgos(){
    //     $r = $this->db->select("num_consumos ")->from("clientes_contratos")->
    //     // where("id_cliente", $cliente)->
    //     // where("status='A'")->
    //     get();
    
    //     if($r && $r->num_rows()>0) {
    //         return $r->row()->num_consumos;
    //     }
    //     return 0;
    // }

    // function obtenerTotalPromedioResultados($cliente){
    //     // $r = $this->db->select("avg(ponderacion) as promedio")->from("resultados")->
    //     // where("id_cliente", $cliente)->
    //     // // where("status='A'")->
    //     // get();
    //     $r = $this->db->select("round(avg(ponderacion)) as promedio")->from("analisis as analy")->
    //     join("cat_personas as catper","catper.id_persona=analy.id_persona ",'left')->
    //     where("catper.id_cliente", $cliente)->
    //     where("status='P'")->
    //     where("resultado>0")->
    //     get();
    
    //     if($r && $r->num_rows()>0) {
    //         return $r->row()->promedio;
    //     }
    //     return 0;
    // }
    function obtenerTotalPromedioResultados($cliente){
        // $r = $this->db->select("round(avg(ponderacion)) as promedio")->from("analisis as analy")->
        $r = $this->db->select("resultado")->from("analisis as analy")->
        join("cat_personas as catper","catper.id_persona=analy.id_persona ",'left')->
        where("catper.id_cliente", $cliente)->
        where("status='P'")->
        where("resultado>=0")->
        get();
        // die($this->db->last_query());
    
        if($r && $r->num_rows()>0) {
            
            $suma = 0;
            $ix = 0;

            $riesgo0_2 = 0;
            $riesgo3_5 = 0;
            $riesgo6_8 = 0;
            $riesgo9_10 = 0;
            foreach ($r->result() as $row)
            {
                $suma += $row->resultado;
                if ($row->resultado>=0 && $row->resultado<=2)
                    $riesgo0_2++;
                if ($row->resultado>=3 && $row->resultado<=5)
                    $riesgo3_5++;
                if ($row->resultado>=6 && $row->resultado<=8)
                    $riesgo6_8++;
                if ($row->resultado>=9 && $row->resultado<=10)
                    $riesgo9_10++;

                $ix++;
            }
            
            return ['promediosRiesgos'=>[$riesgo0_2,$riesgo3_5,$riesgo6_8,$riesgo9_10], 'promedio' => round($suma/$ix)];
        }

        return ['promediosRiesgos'=>[], 'promedio' => 0];
    }    

    function obtenerTotalAptos($cliente){
        // $r = $this->db->select("round(avg(ponderacion)) as promedio")->from("analisis as analy")->

        // ano actual
        $this->db->select("resultado")->from("analisis as analy");
        $this->db->join("cat_personas as catper","catper.id_persona=analy.id_persona ",'left');
        if ($cliente>0){
            $this->db->where("catper.id_cliente", $cliente);
        }
        
        $this->db->where("status='P'");
        $this->db->where("resultado!=-1");
        $this->db->where("year(fecha_finalizacion)=".date('Y'));
        $r = $this->db->get();
        // die($this->db->last_query());
    
        $aptosAAct = 0;
        $noaptosAAct = 0;
        $nofindedAAct = 0;        
        if($r && $r->num_rows()>0) {
            
            foreach ($r->result() as $row)
            {
                if ($row->resultado==-2)
                    $nofindedAAct++;
                if ($row->resultado>=0 && $row->resultado<=4)
                    $aptosAAct++;
                if ($row->resultado>=5 && $row->resultado<=10)
                    $noaptosAAct++;
            }
                        
        }


        // ano anterior
        $this->db->select("resultado")->from("analisis as analy");
        $this->db->join("cat_personas as catper","catper.id_persona=analy.id_persona ",'left');
        if ($cliente>0){
            $this->db->where("catper.id_cliente", $cliente);
        }
        
        $this->db->where("status='P'");
        $this->db->where("resultado!=-1");
        $this->db->where("year(fecha_finalizacion)=".(date('Y')-1));
        $r = $this->db->get();
        // die($this->db->last_query());
    
        $aptosAAnt = 0;
        $noaptosAAnt = 0;
        $nofindedAAnt = 0;        
        if($r && $r->num_rows()>0) {
            
            foreach ($r->result() as $row)
            {
                if ($row->resultado==-2)
                    $nofindedAAnt++;
                if ($row->resultado>=0 && $row->resultado<=4)
                    $aptosAAnt++;
                if ($row->resultado>=5 && $row->resultado<=10)
                    $noaptosAAnt++;
            }
                        
        }

        return ['nofinded'=>[$nofindedAAct,$nofindedAAnt], 'aptos' => [$aptosAAct,$aptosAAnt], 'noaptos' => [$noaptosAAct,$noaptosAAnt]];
    } 

    function obtenerTotalPorHerramienta($cliente){

        // primero sitios noticias
        $r = $this->db->select("fuente")->from("expedientes_anexos as expe")->
        join("cat_personas as catper","catper.id_persona=expe.id_persona ",'left')->
        where("catper.id_cliente", $cliente)->where("expe.fuente", 'N');

        $newSites = $this->db->count_all_results();

        // darkw
        $r = $this->db->select("fuente")->from("expedientes_anexos as expe")->
        join("cat_personas as catper","catper.id_persona=expe.id_persona ",'left')->
        where("catper.id_cliente", $cliente)->where("expe.fuente", 'D');

        $darkW = $this->db->count_all_results();

        // web finders
        $r = $this->db->select("fuente")->from("expedientes_anexos as expe")->
        join("cat_personas as catper","catper.id_persona=expe.id_persona ",'left')->
        where("catper.id_cliente", $cliente)->where("expe.fuente", 'X');

        $finders = $this->db->count_all_results();

        // redSocials
        $r = $this->db->select("fuente")->from("expedientes_anexos as expe")->
        join("cat_personas as catper","catper.id_persona=expe.id_persona ",'left')->
        where("catper.id_cliente", $cliente)->where("expe.fuente", 'R');

        $redSocials = $this->db->count_all_results();
    
        // osint
        $r = $this->db->select("fuente")->from("expedientes_anexos as expe")->
        join("cat_personas as catper","catper.id_persona=expe.id_persona ",'left')->
        where("catper.id_cliente", $cliente)->where("expe.fuente", 'O');
        // die($this->db->last_query());

        $osint = $this->db->count_all_results();

        // produccion
        // return [$osint, $redSocials, $finders, $newSites, $darkW];

        return [213, 123, 211, 123, 78];
    }

    function obtenerTotalAniosBarras($cliente){

        $anios = [date('Y'), date('Y')-1, date('Y')-2];

        // ano actual
        $this->db->select('month(fecha_finalizacion) as mes, count(*) as total');
        $this->db->from('analisis AS analy');
        $this->db->where("status='P'");
        $this->db->where("resultado!=-1");
        $this->db->where("year(fecha_finalizacion)=".date('Y'));
        $this->db->group_by('month(fecha_finalizacion)'); 
        $r=$this->db->get();
        // die($this->db->last_query());
    
        $mes1 = 0; $mes2 = 0; $mes3 = 0; $mes4 = 0; $mes5 = 0; $mes6 = 0; $mes7 = 0; $mes8 = 0; $mes9 = 0; $mes10 = 0; $mes11 = 0; $mes12 = 0;        

        if($r && $r->num_rows()>0) {
            
            foreach ($r->result() as $row)
            {
                switch($row->mes){
                    case 1: $mes1 = $row->total; break;
                    case 2: $mes2 = $row->total; break;
                    case 3: $mes3 = $row->total; break;
                    case 4: $mes4 = $row->total; break;
                    case 5: $mes5 = $row->total; break;
                    case 6: $mes6 = $row->total; break;
                    case 7: $mes7 = $row->total; break;
                    case 8: $mes8 = $row->total; break;
                    case 9: $mes9 = $row->total; break;
                    case 10: $mes10 = $row->total; break;
                    case 11: $mes11 = $row->total; break;
                    case 12: $mes12 = $row->total; break;
                }
            }                    
        }
        $anioAct = [$mes1,$mes2, $mes3, $mes4, $mes5, $mes6, $mes7, $mes8, $mes9, $mes10, $mes11, $mes12]; 


        // ano anterior
        $this->db->select('month(fecha_finalizacion) as mes, count(*) as total');
        $this->db->from('analisis AS analy');
        $this->db->where("status='P'");
        $this->db->where("resultado!=-1");
        $this->db->where("year(fecha_finalizacion)=".(date('Y')-1));
        $this->db->group_by('month(fecha_finalizacion)'); 
        $r = $this->db->get();
        // die($this->db->last_query());
    
        $mes1 = 0; $mes2 = 0; $mes3 = 0; $mes4 = 0; $mes5 = 0; $mes6 = 0; $mes7 = 0; $mes8 = 0; $mes9 = 0; $mes10 = 0; $mes11 = 0; $mes12 = 0;        

        if($r && $r->num_rows()>0) {
            
            foreach ($r->result() as $row)
            {
                switch($row->mes){
                    case 1: $mes1 = $row->total; break;
                    case 2: $mes2 = $row->total; break;
                    case 3: $mes3 = $row->total; break;
                    case 4: $mes4 = $row->total; break;
                    case 5: $mes5 = $row->total; break;
                    case 6: $mes6 = $row->total; break;
                    case 7: $mes7 = $row->total; break;
                    case 8: $mes8 = $row->total; break;
                    case 9: $mes9 = $row->total; break;
                    case 10: $mes10 = $row->total; break;
                    case 11: $mes11 = $row->total; break;
                    case 12: $mes12 = $row->total; break;
                }
            }                    
        }
        $anioAnt = [$mes1,$mes2, $mes3, $mes4, $mes5, $mes6, $mes7, $mes8, $mes9, $mes10, $mes11, $mes12]; 


        // ano anterior anterior
        $this->db->select('month(fecha_finalizacion) as mes, count(*) as total');
        $this->db->from('analisis AS analy');
        $this->db->where("status='P'");
        $this->db->where("resultado!=-1");
        $this->db->where("year(fecha_finalizacion)=".(date('Y')-2));
        $this->db->group_by('month(fecha_finalizacion)'); 
        $r = $this->db->get();
        // die($this->db->last_query());

        $mes1 = 0; $mes2 = 0; $mes3 = 0; $mes4 = 0; $mes5 = 0; $mes6 = 0; $mes7 = 0; $mes8 = 0; $mes9 = 0; $mes10 = 0; $mes11 = 0; $mes12 = 0;        

        if($r && $r->num_rows()>0) {
            
            foreach ($r->result() as $row)
            {
                switch($row->mes){
                    case 1: $mes1 = $row->total; break;
                    case 2: $mes2 = $row->total; break;
                    case 3: $mes3 = $row->total; break;
                    case 4: $mes4 = $row->total; break;
                    case 5: $mes5 = $row->total; break;
                    case 6: $mes6 = $row->total; break;
                    case 7: $mes7 = $row->total; break;
                    case 8: $mes8 = $row->total; break;
                    case 9: $mes9 = $row->total; break;
                    case 10: $mes10 = $row->total; break;
                    case 11: $mes11 = $row->total; break;
                    case 12: $mes12 = $row->total; break;
                }
            }                    
        }
        $anioAntAnt = [$mes1,$mes2, $mes3, $mes4, $mes5, $mes6, $mes7, $mes8, $mes9, $mes10, $mes11, $mes12];

        return ['anioAct'=> $anioAct, 'anioAnt' => $anioAnt, 'anioAntAnt' => $anioAntAnt];
    } 

    function obtenerTotalProcesosMensuales($cliente){
        $r = $this->db->select("Month(fecha_resultado) as mes, count(*) as total ")->from("resultados")->
        where('fecha_resultado >= makedate(year(curdate()), 1)
        and fecha_resultado < makedate(year(curdate()) + 1, 1) and id_cliente='.$cliente, NULL, FALSE)->
        group_by('Month(fecha_resultado)')->
        order_by('mes', 'asc')->
        get();
        // die($this->db->last_query());
    
        $lista = array(0,0,0,0,0,0,0,0,0,0,0,0);
        if($r && $r->num_rows()>0) {
            
            foreach ($r->result() as $row)
            {
                $lista[$row->mes-1]=$row->total;
            }
            return $lista;
        }
        return $lista;
    }

    function obtenerTotalProcesosPorMes(){
        $r = $this->db->select("Month(fecha_resultado) as mes, count(*) as total ")->from("resultados")->
        // where('fecha_resultado >= makedate(year(curdate()), 1)
        // and fecha_resultado < makedate(year(curdate()) + 1, 1) ', NULL, FALSE)->
        group_by('Month(fecha_resultado)')->
        order_by('mes', 'asc')->
        get();
        // die($this->db->last_query());
    
        $lista = array(0,0,0,0,0,0,0,0,0,0,0,0);
        if($r && $r->num_rows()>0) {
            
            foreach ($r->result() as $row)
            {
                $lista[$row->mes-1]=$row->total;
            }
            return $lista;
        }
        return $lista;
    }

    function obtenerTotalRiesgosEncontrados($cliente){
        $r = $this->db->select("riesgo, count(*) as total ")->from("cat_riesgos as ries")->
        join("resultados as res","ries.id=res.id_riesgo_asociado ",'left')->
        where(' id_cliente='.$cliente, NULL, FALSE)->
        group_by('riesgo')->
        // order_by('mes', 'asc')->
        get();
        // die($this->db->last_query());
    
        if($r && $r->num_rows()>0) {
            $listaRiesgos = array();
            $listaTotales = array();
            $colorsBack = array();
            $ix = 0;
            foreach ($r->result() as $row)
            {
                $listaRiesgos[$ix]=$row->riesgo;
                $listaTotales[$ix]=$row->total;
                $colorsBack[$ix++]='rgba('.random_int(0, 255).', '.random_int(0, 255).', '.random_int(0, 255).', 0.2)';
            }
            return ['riesgos'=>$listaRiesgos, 'totales' => $listaTotales, 'colorsBack' => $colorsBack];
        }

        return ['riesgos'=>[], 'totales' => [], 'colorsBack' => []];
    }

    function obtenerTotalRiesgos(){
        $r = $this->db->select("count(*) as riesgos ")->from("cat_riesgos as ries")->
        join("resultados as res","ries.id=res.id_riesgo_asociado ",'left')->
        // where(' id_cliente='.$cliente, NULL, FALSE)->
        get();
        // die($this->db->last_query());
    
        if($r && $r->num_rows()>0) {
            return $r->row()->riesgos;
        }
        return 0;
    }

    function obtenerTotalRiesgosMasLocalizados(){
        $r = $this->db->select("riesgo, count(*) as total ")->from("cat_riesgos as ries")->
        join("resultados as res","ries.id=res.id_riesgo_asociado ",'left')->
        // where(' id_cliente='.$cliente, NULL, FALSE)->
        group_by('riesgo')->limit(10)->
        order_by('total', 'desc')->
        get();
        // die($this->db->last_query());
    
        if($r && $r->num_rows()>0) {
            $listaRiesgos = array();
            $listaTotales = array();
            $colorsBack = array();
            $ix = 0;
            foreach ($r->result() as $row)
            {
                $listaRiesgos[$ix]=$row->riesgo;
                $listaTotales[$ix]=$row->total;
                $colorsBack[$ix++]='rgba('.random_int(0, 255).', '.random_int(0, 255).', '.random_int(0, 255).', 0.5)';
            }
            return ['riesgos'=>$listaRiesgos, 'totales' => $listaTotales, 'colorsBack' => $colorsBack];
        }

        return ['riesgos'=>[], 'totales' => [], 'colorsBack' => []];
    }

    function obtenerTotalFechasLogueos(){
        $r = $this->db->select("cast(fecha_logueo as Date) as fechax, count(*) as total  ")->from("log_accesos")->
        // where(' logueado=1' , NULL, FALSE)->
        group_by('fechax')->limit(15)->
        order_by('fechax', 'desc')->
        get();
        // die($this->db->last_query());
    
        if($r && $r->num_rows()>0) {
            $listaFechas = array();
            $listaTotales = array();
            $colorsBack = array();
            $ix = 0;
            foreach ($r->result() as $row)
            {
                $listaFechas[$ix]=$row->fechax;
                $listaTotales[$ix]=$row->total;
                $colorsBack[$ix++]='rgba('.random_int(0, 255).', '.random_int(0, 255).', '.random_int(0, 255).', 0.5)';
            }
            return ['fechas'=>$listaFechas, 'totales' => $listaTotales, 'colorsBack' => $colorsBack];
        }

        return ['fechas'=>[], 'totales' => [], 'colorsBack' => []];
    }

    function obtenerTotalMasRiesgosUsados(){
        $r = $this->db->select("cr.riesgo, count(*) as total  ")->from("puestos_riesgos as pr")->
        join("cat_riesgos as cr","cr.id = pr.id_riesgo",'left')->
        // where(' logueado=1' , NULL, FALSE)->
        group_by('cr.riesgo')->limit(20)->
        order_by('total', 'desc')->
        get();
        // die($this->db->last_query());
    
        if($r && $r->num_rows()>0) {
            $listaRiesgo = array();
            $listaTotales = array();
            $colorsBack = array();
            $sizes = array();
            $ix = 0;
            foreach ($r->result() as $row)
            {
                $listaRiesgo[$ix]=$row->riesgo;
                $listaTotales[$ix]=$row->total;
                $colorsBack[$ix]='rgba('.random_int(0, 255).', '.random_int(0, 255).', '.random_int(0, 255).', 0.5)';
                $sizes[$ix]=((20-($ix+5))*3).'px';
                $ix++;
            }
            return ['riesgos'=>$listaRiesgo, 'totales' => $listaTotales, 'colores' => $colorsBack, 'sizes' => $sizes];
        }

        return ['riesgos'=>[], 'totales' => [], 'colores' => [], 'sizes' => []];
    }
    
    function obtenerTotalColoresRiesgos($cliente){
        $r = $this->db->select("ponderacion")->from("resultados")->
        where(' id_cliente='.$cliente, NULL, FALSE)->
        get();
    
        $lista = array(0,0,0,0);
        if($r && $r->num_rows()>0) {            
            foreach ($r->result() as $row)
            {
                switch($row->ponderacion){
                    case 0:
                        $lista[0]=$lista[0]+1;
                    break;
                    case 1:
                    case 2:
                    case 3:
                        $lista[1]=$lista[1]+1;
                    break;
                    case 4:
                    case 5:
                    case 6:
                        $lista[2]=$lista[2]+1;
                    break;
                    case 7:
                    case 8:
                    case 9:
                        $lista[3]=$lista[3]+1;
                    break;
                }
            }
        }
        return $lista;
    }

}