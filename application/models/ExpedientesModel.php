<?php
ini_set("max_execution_time", 0);
defined('BASEPATH') OR exit('No direct script access allowed');

class ExpedientesModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function insertarUrlSocial($tabla,$d) {
        $this->insertRow($tabla, $d);
       $lastid = $this->db->insert_id();
       return $lastid;
    }

   function insertarExpedientesAnexos($tabla,$d) {
      return $this->insertRow($tabla, $d);
    }
   
   function actualizarUrlSocial($tabla,$id, $d) {
       $id = intval($id);
       return $this->updateRow($tabla, $d, "id_anexado=$id");
   }
   
   function consultaUrlSocialIdPersona($url, $idpersona){
        $this->db->select("urls.id_anexado, urls.url_asociado");
        $this->db->from("urls_sociales as urls");
        $this->db->join("expedientes_anexos as expa", "urls.id_anexado=expa.id_anexado");
        $this->db->where("url_asociado='".$url."' AND id_persona=".$idpersona);
        $totalRows = $this->db->count_all_results('', FALSE);
        $r=$this->db->get();
        $datos = $r->result_array();

        return $datos;
    }

    function insertRow($tabla, $datos) {
        if($this->db->insert($tabla, $datos)) {
            return array("title"=>"Operación exitosa", "msg"=>"Registro guardado", "type"=>"success");
        } else {
            return array("title"=>"Operación fallida", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function updateRow($tabla, $datos, $where) {
        if($this->db->update($tabla, $datos, $where)) {
            return array("title"=>"Operación exitosa", "msg"=>"Registro actualizado", "type"=>"success");
        } else {
            return array("title"=>"Operación fallida", "msg"=>"No fue posible actualizar el registro", "type"=>"error");
        }
    }

    function listarFuentes(){
        $this->db->select("combo as text , fuente as id");
        $this->db->from("cat_anexos");
        $this->db->group_by("fuente");
        $r=$this->db->get();
        $datos = $r->result_array();
        return $datos;
    }

    function existeProcesoPersona($idpersona, $idanexo){
        $this->db->select("status");
        $this->db->from("procesa_personas");
        $this->db->where("id_persona = ".$idpersona." and id_anexo IN (".$idanexo.") and status = 0");
        $r=$this->db->get();
        $datos = $r->result_array();
        return count($datos)>0 ? true : false;
    }

    function listarPersonas(){
        $id_cliente=intval($this->session->userdata("id_cliente"));
        $id_perfil=intval($this->session->userdata("id_perfil"));
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,p.id_persona, p.curp, CONCAT(p.nombre, ' ', coalesce(p.paterno, ''), ' ',coalesce(p.materno, '')) as nombre, p.email, p.celular,p.ine,p.direccion,p.alias,c.descripcion as empresa,p.id_cliente,(select count(fuente) from expedientes_anexos where fuente='R' and id_persona=p.id_persona) as rsociales,(select count(fuente) from expedientes_anexos where fuente='D' and id_persona=p.id_persona) as darkweb,(select count(fuente) from expedientes_anexos where fuente='O' and id_persona=p.id_persona) as osint,(select count(fuente) from expedientes_anexos where fuente='N' and id_persona=p.id_persona)  as noticias,(select count(fuente) from expedientes_anexos where fuente='X' and id_persona=p.id_persona)  as web");
        $this->db->from("cat_personas as p");
        $this->db->join("cat_clientes as c", "p.id_cliente=c.id_cliente");
        $this->db->where("p.borrado=0");
        // if($id_perfil==2){
            $this->db->where("p.id_cliente",$id_cliente);
        // }
		$this->db->group_start();
        $this->db->like("CONCAT(p.paterno,' ', COALESCE(p.materno,''),' ',p.nombre)", $search, "both");
        $this->db->or_like("p.curp", $search, "both");
        $this->db->or_like("p.email", $search, "both");
        $this->db->or_like("c.descripcion", $search, "both");
        $this->db->group_end();
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->group_by("p.id_persona");
        $this->db->order_by("p.nombre");
        $r=$this->db->get();
        // $datos = $r->result_array();
        // $datos=($datos);
        $datos2 = array();
        foreach ($r->result_array() as $row){
            if ($this->existeProcesoPersona($row['id_persona'],'3'))
                $row['noticias']= '<img src="'.base_url() . 'assets/imgs/loader_procesos_personas.gif" width="32px" />';
            if ($this->existeProcesoPersona($row['id_persona'],'6,7,8,9,10'))
                $row['osint']= '<img src="'.base_url() . 'assets/imgs/loader_procesos_personas.gif" width="32px" />';
            if ($this->existeProcesoPersona($row['id_persona'],'5,2'))
                $row['rsociales']= '<img src="'.base_url() . 'assets/imgs/loader_procesos_personas.gif" width="32px" />';    
            if ($this->existeProcesoPersona($row['id_persona'],'11'))
                $row['darkweb']= '<img src="'.base_url() . 'assets/imgs/loader_procesos_personas.gif" width="32px" />';
            if ($this->existeProcesoPersona($row['id_persona'],'12,14,16'))
                $row['web']= '<img src="'.base_url() . 'assets/imgs/loader_procesos_personas.gif" width="32px" />';

            array_push($datos2,$row);
        }


        $result["recordsFiltered"] = $totalRows;
        $result["data"] = $datos2;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();
        $result["id_perfil"] = $id_perfil;	
		return $result;
    }

    function verExpediente() {
        $id_persona = intval($this->input->post("id_persona"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $draw = intval($this->input->post("draw"));
        $fuente = strval($this->input->post("fuente"));
        $nombre = strval($this->input->post("nombre"));
        if($fuente =="D"){    
            $this->db->select("p.idPage as id_pagina,'' as url, p.content as body_dom, '' as target,'' as noticia,'' as hash_url,ea.id_persona");
            $this->db->from("darkDB.pages as p");
            $this->db->join("expedientes_anexos ea","p.idPage=ea.id_anexado"); 
        }else if($fuente=="N"){
            $this->db->select("p.id_pagina,p.url, p.body_dom, p.url as target,'' as noticia,hash_url,ea.id_persona");
            $this->db->from("paginas as p");
            $this->db->join("expedientes_anexos ea","p.id_pagina=ea.id_anexado");
        }else{
            $this->db->select("p.id_anexado as id_pagina,p.url_asociado as url, p.json_data as body_dom,'' as target,'' as noticia,'' as hash_url,ea.id_persona");
            $this->db->from("urls_sociales as p");
            $this->db->join("expedientes_anexos ea","p.id_anexado=ea.id_anexado");
        }
        $this->db->where("ea.id_persona",$id_persona);
        $this->db->where("ea.fuente",$fuente);
        $this->db->order_by("ea.id_anexado desc");
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $r = $this->db->get();
        $datos = $r->result_array();
        $datos=$this->limpiarBody($datos,$nombre);
        $result["recordsFiltered"] = $totalRows;
        $result["data"] = $datos;
        $result['draw'] = $draw;
        $result["recordsTotal"] = $r->num_rows();	
        return $result;
    }

    function abrirExpediente($limit_per_page, $offset){
        $id_persona=intval($this->input->post("id_persona"));
        $nombre=strval($this->input->post("nombre"));
        $fuente=strval($this->input->post("fuente"));
        if($fuente =="D"){    
            $this->db->select("p.idPage as id_pagina,'' as url, p.content as body_dom, '' as target,'' as noticia,'' as hash_url,ea.id_persona");
            $this->db->from("darkDB.pages as p");
            $this->db->join("expedientes_anexos ea","p.idPage=ea.id_anexado"); 
        }else if($fuente=="N"){
            $this->db->select("p.id_url, p.body_dom, urls.url as target,'' as noticia, hash_url, ea.id_persona");
            $this->db->from("paginas2 as p");
            $this->db->join("urls ","p.id_url=urls.id_url");
            $this->db->join("expedientes_anexos ea","p.id_url=ea.id_anexado");
        }else if($fuente=="X"){
            $this->db->select("p.id_anexado as id_pagina,p.url_asociado as url, p.contenido as body_dom,'' as target,'' as noticia,'' as hash_url,ea.id_persona");
            $this->db->from("urls_sociales as p");
            $this->db->join("expedientes_anexos ea","p.id_anexado=ea.id_anexado");
        }else{    
            $this->db->select("p.id_anexado as id_pagina,p.url_asociado as url, p.json_data as body_dom,'' as target,'' as noticia,'' as hash_url,ea.id_persona");
            $this->db->from("urls_sociales as p");
            $this->db->join("expedientes_anexos ea","p.id_anexado=ea.id_anexado");
        }
        $this->db->where("ea.id_persona",$id_persona);
        $this->db->where("ea.fuente",$fuente);
        $this->db->order_by("ea.id_anexado desc");
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($limit_per_page, $offset);
        //return $this->db->get_compiled_select();
        $r = $this->db->get();
        $datos = $r->result_array();
        $datos=$this->limpiarBody($datos,$nombre);
        $result["total"] = $totalRows;
        $result["data"] = $datos;
        return $result;
    }

    function abrirExpedienteExtra($idper, $fuente){

        $datos = '';
        $totalRows=0;

        // Dependiendo de la fuente, se aplican los id_anexo
        // if ($fuente == 'O'){
            // codigos id_anexo de OSINT.. 
            $queryy = $this->db->query("select id_anexo from cat_anexos where fuente = '".$fuente."' and disponible = 1");
            // $rowxy = $queryy->result();
            foreach ($queryy->result() as $row){
                $queryx = $this->db->query("select outputDirFile from procesa_personas pp where fecha_termino = 
                ( select max(fecha_termino) from procesa_personas where id_persona = ".$idper." and id_anexo = ".$row->id_anexo." and status = '1')
                and  id_persona = ".$idper."  and id_anexo = ".$row->id_anexo." and status = '1'");

                $rows = $queryx->result();

                if (count($rows)>0 && file_exists(OUTPUT_RESULTS_PPERSONAS.'/'.$rows[0]->outputDirFile)){
                    $strJsonFileContents = file_get_contents(OUTPUT_RESULTS_PPERSONAS.'/'.$rows[0]->outputDirFile);
                    // Convert to array 
                    $arrayData = json_decode($strJsonFileContents, true);
                    $totalRows++;
                    // var_dump($arrayData);

                    if ($_SESSION['id_perfil']==1){
                        $datos.="En proceso : ".$arrayData[0]['Proceso'].", ".(isset($arrayData[0]['Log']) && strlen($arrayData[0]['Log'])>0 ? $arrayData[0]['Log'] : $arrayData[0]['Mensaje']).'<br />';
                    }else{
                        $datos.="En proceso : ".$arrayData[0]['Proceso'].", ".$arrayData[0]['Mensaje'].'<br />';
                    }
                }
            }
        // }

        $result["totalProccess"] = $totalRows;
        $result["data"] = $datos;
        return $result;
    }

    function buscarNoticias() {
        $datosPersona = $this->input->post("datosPersona");
        $fuente = strval(trim($this->input->post("fuente")));
        $ban=TRUE;//bandera para determinar si la transaccion se realiza o se da rollback
        $msgOk="";//variable para retornar el mensaje de error al usuario	
        $msgError="";
        if(count($datosPersona)>0){   
            $this->db->trans_begin(); 
            for($j=0; $j<count($datosPersona); $j++){
                $where["cc.id_cliente"]=intval($datosPersona[$j]['id_cliente']);
                $where["cc.status"]=strval("A");
                $where["cc.borrado"]=intval(0);
                $where["cf.fuente"]=$fuente;
                $this->db->select("cc.id_contrato");
                $this->db->from("clientes_contratos cc");
                $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
                $this->db->where($where);
                //return $this->db->get_compiled_select();  
                $rC = $this->db->get();
                if($rC && $rC->num_rows()>0) {    
                    $this->db->select("id_anexado");
                    $this->db->from("expedientes_anexos");
                    $this->db->where("fuente","N");
                    $this->db->where("id_persona",intval($datosPersona[$j]['id_persona']));
                    $nombre = trim('"'.$datosPersona[$j]['nombre'].'"');
                    $curp = trim('"'.$datosPersona[$j]['curp'].'"');
                    $email = trim('"'.$datosPersona[$j]['email'].'"');
                    $celular = trim('"'.$datosPersona[$j]['celular'].'"');
                    $ine = trim('"'.$datosPersona[$j]['ine'].'"');
                    $direccion = trim('"'.$datosPersona[$j]['direccion'].'"');
                    $alias = trim('"'.$datosPersona[$j]['alias'].'"');
                    $expedientes_anexos = $this->db->get_compiled_select();
                    $id_persona=intval($datosPersona[$j]['id_persona']);
                    $this->db->select("p.id_pagina,MATCH(p.body_dom,titulo_articulo) AGAINST('".$nombre."' IN BOOLEAN MODE) as relevance");
                    $this->db->from("paginas as p");
                    $this->db->where("MATCH(p.body_dom,titulo_articulo) AGAINST('".$nombre."".$curp."".$email."".$celular."".$ine."".$direccion."".$alias."' IN BOOLEAN MODE ) and length(rtrim(ltrim(p.body_dom)))>500 ", NULL, FALSE);
                    $this->db->where_not_in('p.id_pagina', $expedientes_anexos,false);
                    $this->db->order_by("relevance desc,length(p.body_dom) asc");
                    //return $this->db->get_compiled_select();
                    $r = $this->db->get();
                    $noticias = $r->result_array();
                    if(count($noticias)>0){
                        $countAnexos=0;
                        for($i=0; $i<count($noticias); $i++){
				            $expediente_anexo['id_persona'] =$id_persona;
				            $expediente_anexo['id_anexado']=$noticias[$i]['id_pagina'];
				            $expediente_anexo['fuente'] =strval("N");
				            $expediente_anexo['fecha'] = date("Y-m-d");
                            $expediente_anexo['usuario']=intval($this->session->userdata("id_usuario"));
				            if(!$this->db->insert("expedientes_anexos", $expediente_anexo)){
					            $ban=FALSE;
					            $msgError.="No ha sido posible agregar el anexo al expediente.";
				            }else{
                                $countAnexos=$countAnexos+1;
                            }
                        }
                        $msgOk.="Se agregaron ".$countAnexos." anexos al expediente de:".$datosPersona[$j]['nombre']; 
                    }else{
			            $msgOk.="No se encontro contenido para ".$datosPersona[$j]['nombre'];
                    }    
                }else{
                    $msgOk.="La empresa: ".$datosPersona[$j]['empresa'].", no tiene acceso a esta fuente en su contrato activo";
                }     
            }   
        }else{
            return array("error"=>"0","msg"=>"No se enviarón personas para su busqueda");
        }    
        if($this->db->trans_status() === TRUE && $ban==TRUE){	
			$this->db->trans_commit();//si todo ha resultado bien se realiza la transaccion		
			return array("error"=>"0","msg"=>$msgOk);
		}else if ($this->db->trans_status() === FALSE || $ban==FALSE){
			$this->db->trans_rollback();//si ocurrio algun error en el ciclo se da rollback a la transaccion
			return array("error"=>"1","msg"=>$msgError);
		} 
    }

    function buscarDarkWeb() {
        $datosPersona = $this->input->post("datosPersona");
        $fuente = strval(trim($this->input->post("fuente")));
        $ban=TRUE;//bandera para determinar si la transaccion se realiza o se da rollback
        $msgOk="";//variable para retornar el mensaje de error al usuario	
        $msgError="";
        if(count($datosPersona)>0){    
            $this->db->trans_begin();
            for($j=0; $j<count($datosPersona); $j++){
                $where["cc.id_cliente"]=intval($datosPersona[$j]['id_cliente']);
                $where["cc.status"]=strval("A");
                $where["cc.borrado"]=intval(0);
                $where["cf.fuente"]=$fuente;
                $this->db->select("cc.id_contrato");
                $this->db->from("clientes_contratos cc");
                $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
                $this->db->where($where);
                //return $this->db->get_compiled_select();  
                $rC = $this->db->get();
                if($rC && $rC->num_rows()>0) {
                    $this->db->select("id_anexado");
                    $this->db->from("expedientes_anexos");
                    $this->db->where("fuente","D");
                    $this->db->where("id_persona",intval($datosPersona[$j]['id_persona']));
                    $expedientes_anexos = $this->db->get_compiled_select();
                    $nombre = trim('"'.$datosPersona[$j]['nombre'].'"');
                    $curp = trim('"'.$datosPersona[$j]['curp'].'"');
                    $email = trim('"'.$datosPersona[$j]['email'].'"');
                    $celular = trim('"'.$datosPersona[$j]['celular'].'"');
                    $ine = trim('"'.$datosPersona[$j]['ine'].'"');
                    $direccion = trim('"'.$datosPersona[$j]['direccion'].'"');
                    $alias = trim('"'.$datosPersona[$j]['alias'].'"');
                    $id_persona=intval($datosPersona[$j]['id_persona']);
                    $this->db->select("p.idPage,MATCH(p.content) AGAINST('".$nombre."' IN BOOLEAN MODE) as relevance");
                    $this->db->from("darkDB.pages as p");
                    $this->db->where("MATCH(p.content) AGAINST('".$nombre." ".$curp." ".$email."".$celular."".$ine."".$direccion."".$alias."' IN BOOLEAN MODE )", NULL, FALSE);
                    $this->db->where_not_in('p.idPage', $expedientes_anexos,false);
                    $this->db->order_by("relevance desc");
                    //return $this->db->get_compiled_select();
                    $r = $this->db->get();
                    $darWeb = $r->result_array();
                    if(count($darWeb)>0){
                        $countAnexos=0;
                        for($i=0; $i<count($darWeb); $i++){ 
				            $expediente_anexo['id_persona'] =$id_persona;
				            $expediente_anexo['id_anexado']=$darWeb[$i]['idPage'];
				            $expediente_anexo['fuente'] =strval("D");
				            $expediente_anexo['fecha'] = date("Y-m-d");
                            // $expediente_anexo['usuario']=intval($this->session->userdata("id_usuario"));
				            if(!$this->db->insert("expedientes_anexos", $expediente_anexo)){
					            $ban=FALSE;
					            $msgError.="No ha sido posible agregar el anexo al expediente.";
				            }else{
                                $countAnexos=$countAnexos+1;
                            }
                        }
                        $msgOk.="Se agregaron ".$countAnexos." anexos al expediente de:".$datosPersona[$j]['nombre'];
                    }else{
			            $msgOk.="No se encontro contenido para ".$datosPersona[$j]['nombre'];
                    }
                }else{
                    $msgOk.="La empresa: ".$datosPersona[$j]['empresa'].", no tiene acceso a esta fuente en su contrato activo";
                }     
            }
        }else{
            return array("error"=>"0","msg"=>"No se enviarón personas para su busqueda");
        }    
        if($this->db->trans_status() === TRUE && $ban==TRUE){	
			$this->db->trans_commit();//si todo ha resultado bien se realiza la transaccion		
			return array("error"=>"0","msg"=>$msgOk);
		}else if ($this->db->trans_status() === FALSE || $ban==FALSE){
			$this->db->trans_rollback();//si ocurrio algun error en el ciclo se da rollback a la transaccion
			return array("error"=>"1","msg"=>$msgError);
		} 
    }

    function limpiarBody($data,$nombre){
        if(count($data)>0){
            $p = explode(" ", $nombre);
            $nombres = array();
            for($i=0; $i<count($p); $i++) {
                if(strlen($p[$i]) > 3) {
                    $nombres[] = $p[$i];
                }
            }
            //var_dump($nombres);
            for($i=0; $i<count($data); $i++) {
                $data[$i]["body_dom"] =  preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $data[$i]["body_dom"]);
                $data[$i]["body_dom"] =  preg_replace('/<style\b[^>]*>(.*?)<\/style>/is', "", $data[$i]["body_dom"]);
                $data[$i]["body_dom"] =  preg_replace('/<ul\b[^>]*>(.*?)<\/ul>/is', "", $data[$i]["body_dom"]);
                $data[$i]["body_dom"] =  preg_replace('/<li\b[^>]*>(.*?)<\/li>/is', "", $data[$i]["body_dom"]);
                $data[$i]["body_dom"] = strip_tags($data[$i]["body_dom"]);
                $this->load->library('Globals');
                $Globals = new Globals();
                // quitar palabras especiales
                $data[$i]["body_dom"] = $Globals->eliminarPalabrasEspeciales ($data[$i]["body_dom"]);

                //foreach($nombres as $w) {
                    $data[$i]["body_dom"] = preg_replace('/'.$Globals->eliminarAcentos ($nombre).'/i', '<strong style="color:red">'.$nombre.'</strong>', Globals::eliminarAcentos($data[$i]["body_dom"]));
                //}
            }
        }
        return $data;    
    }

}