<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatPantallasModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function listar() {
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        
        if(!empty($search)) {
            $this->db->like("nombre_menu", $search)->
                or_like("controlador", $search);
            
        }

        $r = $this->db->select("*")->
            from("cat_pantallas")->
            limit($length, $start)->
            get();

        $this->db->from("cat_pantallas");
        if(!empty($search)) {
            $this->db->where("nombre_menu like '".$search."%'");
        }
        $totalRows = $this->db->count_all_results();

        if($r && $r->num_rows()>0) {
            $data["data"] = $r->result_array();
            $data["recordsTotal"] = $r->num_rows();
            $data["recordsFiltered"] = $totalRows;
            $data["draw"] = $draw;

            return $data;
        } else {
            $data["recordsTotal"] = 0;
            $data["recordsFiltered"] = 0;
			$data["draw"] = $draw;
            $data["data"] = array();
            
			return $data;
        }
    }

    function listarPantallas() {
        $r = $this->db->get("cat_pantallas")->result_array();
        if(empty($r)) {
            return array();
        } else {
            return array("data"=>$r);
        }
    }

    function insertar($d) {
        if($this->db->insert("cat_pantallas", $d)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function actualizar($id, $d) {
        $id = intval($id);
        if($this->db->update("cat_pantallas", $d, "id_pantalla=".$id)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function getPantallaById($id_pantalla) {
        $id_pantalla = intval($id_pantalla);
        $r = $this->db->select("*")->from("cat_pantallas")->
			where("id_pantalla",$id_pantalla)->
			get();
		
		if($r && $r->num_rows()>0) {
			$data = $r->row();
			$d = ($data);
			return $d;
		} else {
			return array("data"=>array());
		}
    }

    private function estaAsignada($id_pantalla) {
        $id_pantalla = intval($id_pantalla);
        $n = $this->db->from("perfiles_pantallas")->
            where("id_pantalla", $id_pantalla)->
            count_all_results();

        return $n>0;
    }

    function deletePantallaById($id) {
        $id = intval($id);
        if($this->estaAsignada($id)) {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible borrar el registro, porque al menos un perfil de usuario tiene asociada dicha pantalla", "type"=>"error");
        } else {
            if($this->db->delete("cat_pantallas", "id_pantalla=".$id)) {
                return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha borrado el registro", "type"=>"info");
            } else {
                array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible borrar el registro", "type"=>"error");
            }
        }
    }

    function getMenusPadresForCombo() {
        $r = $this->db->select("id_pantalla as id, nombre_menu as text")->
            from("cat_pantallas")->
            where("sin_menu = '0'")->
            where("tiene_hijos = '1'")->
            where("(id_padre is null or id_padre = '0')")->
            order_by("orden")->get();
        
        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }
    }

}