<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatUsuariosModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function listar($exportar=false) {
        //$r = $this->db->select("u.id_usuario, u.correo, CONCAT(u.paterno,' ', COALESCE(u.materno,''),' ',u.nombre) as nombre, p.descripcion as perfil, a.api_key,Case u.inactivo when '1' then 'Si' when '0' then 'No' end as inactivo")->
        $r = $this->db->select("u.id_usuario, u.correo, cc.descripcion as empresa, CONCAT(u.paterno,' ', COALESCE(u.materno,''),' ',u.nombre) as nombre, p.descripcion as perfil, Case u.inactivo when '1' then 'Si' when '0' then 'No' end as inactivo")->
            from("cat_usuarios as u")->
            join("cat_perfiles as p", "u.id_perfil=p.id_perfil")->
            join("cat_clientes as cc", "cc.id_cliente=u.id_cliente")->
            // join("api_usuarios as a", "a.id_usuario=u.id_usuario and a.activa=1","left")->
            where("u.borrado='0'")->get();

        //die($this->db->last_query());
        if($r && $r->num_rows()>0) {
            return array("data"=>$r->result_array());
        } else {
            return array("data"=>array());
        }
    }

    function listarUsuariosForCombo() {
        $r = $this->db->select("u.id_usuario as id, CONCAT(u.paterno,' ', COALESCE(u.materno,''),' ',u.nombre) as text")->
            from("cat_usuarios as u")->
            where("u.borrado='0' and u.inactivo='0'")->get();

        //die($this->db->last_query());
        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }
    }

    function listarUsuarios() {
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": $search["value"];

        $r = $this->db->select("u.id_usuario, u.correo, CONCAT(u.paterno,' ', COALESCE(u.materno,''),' ',u.nombre) as nombre, p.descripcion as perfil")->
            from("cat_usuarios as u")->
            join("cat_perfiles as p", "u.id_perfil=p.id_perfil")->
            where("u.borrado='0' and u.inactivo='0'")->
            like("CONCAT(u.paterno,' ', COALESCE(u.materno,''),' ',u.nombre)", $search, "both")->
            limit($length, $start)->
            order_by("nombre")->
            get();
        //die($this->db->last_query());
        if($r && $r->num_rows()>0) {
            $nRegistros = $this->db->from("cat_usuarios as u")->
                join("cat_perfiles as p", "u.id_perfil=p.id_perfil")->
                where("u.borrado='0'")->
                like("CONCAT(u.paterno,' ', COALESCE(u.materno,''),' ',u.nombre)", $search, "both")->
                count_all_results();

            $data["recordsTotal"] = $r->num_rows();
            $data["recordsFiltered"] = $nRegistros;
            $data["draw"] = $draw;
            $data["data"] = $r->result_array();
            
            return $data;
        } else {
            $data["recordsTotal"] = 0;
            $data["recordsFiltered"] = 0;
            $data["draw"] = $draw;
            $data["data"] = array();
            return $data;
        }
    }

    function insertar($d) {
        if($this->db->insert("cat_usuarios", $d)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"info");
        } else {
            array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function actualizar($id, $d) {
        $id = intval($id);
        if($this->db->update("cat_usuarios", $d, "id_usuario=".$id)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function getUsuarioById($id_usuario) {
        $id_usuario = intval($id_usuario);
        $r = $this->db->select("u.id_usuario, u.id_perfil, cp.descripcion as perfil, paterno, materno, u.nombre, correo, id_cliente")->from("cat_usuarios as u")->
			join("cat_perfiles as cp","u.id_perfil=cp.id_perfil")->
			where("u.id_usuario",$id_usuario)->
			get();
		
		if($r && $r->num_rows()>0) {
			$data = $r->row();
			$d = ($data);
			return $d;
		} else {
			return array("data"=>array());
		}
    }

    function deleteUsuarioById($id) {
        $id = intval($id);
        if($this->db->update("cat_usuarios", array("borrado"=>"1"), "id_usuario=".$id)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha borrado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible borrar el registro", "type"=>"error");
        }
    }

    function isUsuarioInactivo($id) {
        $id = intval($id);
        $r = $this->db->select("inactivo")->where("id_usuario", $id)->get("cat_usuarios")->row();

        if($r) {
            return ($r->inactivo == "1");
        } else {
            return false;
        }
        
    }

    function deshabilitarUsuarioById($id) {
        $id = intval($id);
        $isInactive = !($this->isUsuarioInactivo($id));

        if($this->db->update("cat_usuarios", array("inactivo"=>$isInactive), "id_usuario=".$id)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha actualizado el registro", "type"=>"success");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible actualizar el registro", "type"=>"error");
        }
    }

    private function tieneActiva($data) {
        $n = $this->db->from("api_usuarios")->
            where($data)->
            count_all_results();
        return $n>0;
    }

    function crearApikey($d,$activa) {
        if($this->tieneActiva($activa)) {
            if($this->db->update("api_usuarios", array("activa"=>0), "id_usuario=".$d["id_usuario"])) {
                if($this->db->insert("api_usuarios", $d)) {
                    return array("error"=>"0","title"=>"Info","msg"=>"Se ha guardado el registro", "type"=>"success");
                } else {
                    return array("error"=>"1", "msg"=>"No fue posible guardar el registro");
                }
            } else {
                return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible actualizar el registro", "type"=>"error");
            }
        } else {
            if($this->db->insert("api_usuarios", $d)) {
                return array("error"=>"0","title"=>"Info","msg"=>"Se ha guardado el registro", "type"=>"success");
            } else {
                return array("error"=>"1", "msg"=>"No fue posible guardar el registro");
            }
        } 
    }

}