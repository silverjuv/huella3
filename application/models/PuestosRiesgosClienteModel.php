<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PuestosRiesgosClienteModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function listarPuestos(){
        $id_cliente=intval($this->session->userdata("id_cliente"));
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,p.id,p.puesto");
        $this->db->from("cat_puestos as p");
        // $this->db->join("clientes_puestos as cp","p.id=cp.id_puesto");
        // $this->db->where("p.id_cliente",$id_cliente);
        $this->db->where("p.id_cliente",$id_cliente);
        $this->db->where("p.activo=1");
        $this->db->like("puesto", $search, "both");
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->order_by("id");
        $r=$this->db->get();
        $datos = $r->result_array();
        $datos=($datos);
        $result["recordsFiltered"] = $totalRows;
        $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }

    function listarPuestosRiesgos($id_puesto){
        $id_cliente=intval($this->session->userdata("id_cliente"));
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,r.id as id_riesgo,r.riesgo");
        $this->db->from("cat_riesgos as r");
        $this->db->join("puestos_riesgos as pr","r.id=pr.id_riesgo");
        $this->db->where("pr.id_puesto",$id_puesto);
        $this->db->where("pr.id_cliente",$id_cliente);
        $this->db->like("r.riesgo", $search, "both");
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->order_by("r.id");
        $r=$this->db->get();
        $datos = $r->result_array();
        // $datos=($datos);
        $datosX = array();
        foreach($datos as $row){
            $subriesgos = $this->getRiesgosSubriesgos($row['id_riesgo'] );
            $cadx = '';
            foreach($subriesgos as $riesgo){
                $cadx.= ", ".$riesgo['subriesgo'];
            }

            $row['subriesgos'] = '<div style="width:100%; text-align:center; font-size:9px">'.substr($cadx,1).'</div>';
            array_push($datosX,$row);
        }

        $result["data"] = $datosX;  

        $result["recordsFiltered"] = $totalRows;
        // $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		    return $result;
    }

    function getRiesgosSubriesgos($id_riesgo){
      $this->db->select("subriesgo");
      $this->db->from("riesgos_subriesgos");
      $this->db->where("id_riesgo",$id_riesgo);
      $r=$this->db->get();	
    return $r->result_array();
  }

    function listarRiesgosSubriesgos($id_riesgo){
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,id_riesgo,subriesgo as riesgo");
        $this->db->from("riesgos_subriesgos");
        $this->db->where("id_riesgo",$id_riesgo);
        $this->db->like("subriesgo", $search, "both");
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->order_by("id_riesgo");
        $r=$this->db->get();
        $datos = $r->result_array();
        $datos=($datos);
        $result["recordsFiltered"] = $totalRows;
        $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }

}