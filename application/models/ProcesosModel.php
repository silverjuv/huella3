<?php
ini_set("max_execution_time", 0);
defined('BASEPATH') OR exit('No direct script access allowed');

class ProcesosModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }
    
    function insertar($d) {
        $ban=TRUE;//bandera para determinar si la transaccion se realiza o se da rollback
		$msgError="";//variable para retornar el mensaje de error al usuario	
        $this->db->trans_begin();
        // $d["id_cliente"]=intval($id_cliente);
        // $d["activo"] =intval(1);
        // var_dump($d); die();
        $id = -1;
        if(!$this->db->insert("procesos", $d)) {
            $ban=FALSE;
            $msgError.="No fue posible guardar el registro en procesos";
        }else{
            // echo $this->db->_error_message(); die();
            // $sqlscope="SELECT LAST_INSERT_ID() as scope";
		    // $queryscope = $this->db->query($sqlscope);
		    // $datascope = $queryscope->result_array();
            // $id_puesto=$datascope[0]['scope'];
            // $id = $this->db->insert_id();
            $id = $this->db->insert_id();
                $ban=TRUE;

        }
        if($this->db->trans_status() === TRUE && $ban==TRUE){	
			$this->db->trans_commit();//si todo ha resultado bien se realiza la transaccion	
            	
			return array("error"=>"0","msg"=>"Se a creado el proceso", "id" => $id, "status" => "1");
		}else if ($this->db->trans_status() === FALSE || $ban==FALSE){
			$this->db->trans_rollback();//si ocurrio algun error en el ciclo se da rollback a la transaccion
			return array("error"=>"1","msg"=>$msgError, "status" => "0");
		}         
    }

    function getResultadoProceso($id_) {
        $where["id"] =intval($id_);
        
        $r = $this->db->select("*")->from("procesos")->
          where($where)->get();
        // return $r->result_array();
        if($r && $r->num_rows()==1) {
            return $r->row();
        } else {
            return array();
        }
    }

    function existeProcesoPersona($id_, $id_anexo, $status) {
        $where["id_persona"] =intval($id_);
        $where["id_anexo"] =intval($id_anexo);
        $where["status"] =$status;
        
        $r = $this->db->select("*")->from("procesa_personas")->
          where($where)->get();
        //   var_dump($where);
        // var_dump($r->result_array()); die();
        // return $r->result_array();
        if($r && $r->num_rows()>0) {
            return true;
        } else {
            return false;
        }
    }

    function eliminar($id_puesto,$id_cliente) {
      $id_puesto = intval($id_puesto);
      $id_cliente = intval($id_cliente);
      if($this->tieneRiesgosAsignados($id_puesto,$id_cliente)) {
          return array("error"=>"1","msg"=>"No fue posible borrar el registro, porque tiene riesgos asociados");
      } else {
          $puesto["activo"]=0;
          $where["id_cliente"]=intval($id_cliente);
          $where["id"]=intval($id_puesto);
        //   if($this->db->update("clientes_puestos",$puesto,$where)) {
          if($this->db->update("cat_puestos",$puesto,$where)) {
              return array("error"=>"0","msg"=>"Se ha quitado el registro");
          } else {
              return array("error"=>"1", "msg"=>"No fue posible quitar el registro");
          }
      }
    }

    function insertarProcesaPersonas($d) {
        $ban=TRUE;//bandera para determinar si la transaccion se realiza o se da rollback
		$msgError="";//variable para retornar el mensaje de error al usuario	
        $this->db->trans_begin();

        $id = -1;
        if(!$this->db->insert("procesa_personas", $d)) {
            $ban=FALSE;
            $msgError.="No fue posible guardar el registro en procesos";
        }else{

            $id = $this->db->insert_id();
                $ban=TRUE;

        }
        if($this->db->trans_status() === TRUE && $ban==TRUE){	
			$this->db->trans_commit();//si todo ha resultado bien se realiza la transaccion	
            	
			return array("error"=>"0","msg"=>"Se ha creado el proceso", "id" => $id, "status" => "1");
		}else if ($this->db->trans_status() === FALSE || $ban==FALSE){
			$this->db->trans_rollback();//si ocurrio algun error en el ciclo se da rollback a la transaccion
			return array("error"=>"1","msg"=>$msgError, "status" => "0");
		}         
    }
}    