<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PerfilesPantallasModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function listarPantallasDePerfil($id_perfil) {
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);

        $this->db->select("pp.id_pantalla, pan.descripcion")->
            from("cat_pantallas as pan")->
            join("perfiles_pantallas as pp", "pan.id_pantalla=pp.id_pantalla")->
            join("cat_perfiles as per", "per.id_perfil=pp.id_perfil")->
            where("pp.id_perfil", $id_perfil);
            
        if(!empty($search)) {
            $this->db->like("pan.descripcion", $search);
        }
        $r = $this->db->get();

        $totalRows = 0;
        $this->db->from("cat_pantallas as pan")->
            join("perfiles_pantallas as pp", "pan.id_pantalla=pp.id_pantalla")->
            join("cat_perfiles as per", "per.id_perfil=pp.id_perfil")->
            where("pp.id_perfil", $id_perfil);

        if(!empty($search)) {
            $totalRows = $this->db->like("pan.descripcion", $search)->
                count_all_results();
        }

        if($r && $r->num_rows()>0) {
            $data["data"] = $r->result_array();
            $data["recordsTotal"] = $r->num_rows();
            $data["recordsFiltered"] = $totalRows;
            $data["draw"] = $draw;

            return $data;
        } else {
            $data["recordsTotal"] = 0;
            $data["recordsFiltered"] = 0;
			$data["draw"] = $draw;
            $data["data"] = array();
            
			return $data;
        }
    }

    private function perfilTienePantalla($id_perfil, $id_pantalla) {
        $id_perfil = intval($id_perfil);
        $id_pantalla = intval($id_pantalla);

        $n = $this->db->from("perfiles_pantallas")->
            where("id_perfil", $id_perfil)->
            where("id_pantalla", $id_pantalla)->
            count_all_results();

        return ($n>0);
    }

    function insertar($id_perfil, $id_pantalla) {
        $id_perfil = intval($id_perfil);
        $id_pantalla = intval($id_pantalla);
        if($this->perfilTienePantalla($id_perfil, $id_pantalla)) {
            return array("error"=>"1", "title"=>"Atención", "msg"=>"La pantalla fue asociada con anterioridad...", "type"=>"notice");
        } else {
            $r = array("id_perfil"=>$id_perfil, "id_pantalla"=>$id_pantalla);
            if($this->db->insert("perfiles_pantallas", $r)) {
                return array("error"=>"0", "title"=>"Info", "msg"=>"La pantalla fue asociada con exito...", "type"=>"success");
            } else {
                return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
            }
        }
    }

    function quitarPantallaDePerfil($id_perfil, $id_pantalla) {
        $id_perfil = intval($id_perfil);
        $id_pantalla = intval($id_pantalla);

        if($this->db->delete("perfiles_pantallas", "id_perfil=".$id_perfil." and id_pantalla=".$id_pantalla)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha quitado la pantalla con exito...", "type"=>"success");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible quitar la pantalla", "type"=>"error");
        }
    }

}