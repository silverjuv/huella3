<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MiPerfilModel extends CI_Model {

    const AVATARS_PATH = FCPATH."assets/imgs/avatars";
	function __construct() {
        parent::__construct();
        $this->load->helper("file");
    }


    function subirAvatar() {
        if(empty($this->session->id_usuario)) {
            return array("error"=>"1", "title"=>"Error", "msg"=>"Debe estar autentificado para poder hacer uso de esta pantalla", "type"=>"error");
        }

        if(!empty($_FILES)) {
            //print_r($_FILES);die();
            if( substr($_FILES["file"]["type"], 0, 5) === "image" ) {
                $id_usuario = $this->session->id_usuario;
                if($_FILES["file"]["size"]>0) {
                    $pi = pathinfo(basename($_FILES['file']['name']));
                    $ext = (empty($pi["extension"]))?"":$pi["extension"];
                    $fn = $id_usuario."_".generarString().".".$ext;
                    $path = self::AVATARS_PATH .DIRECTORY_SEPARATOR. $fn;
                    if(is_dir(self::AVATARS_PATH) && is_writable(self::AVATARS_PATH)) {
                        if(move_uploaded_file($_FILES['file']['tmp_name'], $path)) {

                            $actualAvatar = $this->getAvatarById($id_usuario);

                            if($this->db->where("id_usuario=".$id_usuario)->set('avatar', $fn)->update("cat_usuarios")) {
                                if($actualAvatar != "" && $actualAvatar != "user.png") {
                                    if(file_exists(self::AVATARS_PATH .DIRECTORY_SEPARATOR.$actualAvatar)) {
                                        try { 
                                            unlink(self::AVATARS_PATH .DIRECTORY_SEPARATOR.$actualAvatar);
                                        } catch(Exception $e) {

                                        }
                                        
                                    }
                                }
                                $this->session->set_userdata('avatar', $fn);
                                return array("title"=>"Info", "msg"=>"Se ha guardado su avatar", "type"=>"success");
                            } else {
                                return array("title"=>"Error", "msg"=>"No fue posible guardar el archivo (1)", "type"=>"error");
                            }

                            
                        } else {
                            return array("title"=>"Error", "msg"=>"No fue posible guardar el archivo (2)", "type"=>"error");
                        }
                    } else {
                        return array("title"=>"Error", "msg"=>"El directorio no existe o no se tiene permisos de escritura: ".$path, "type"=>"error");
                    }
                    
                } else {
                    return array("title"=>"Error", "msg"=>"El archivo esta vacío", "type"=>"error");
                }
            } else {
                return array("title"=>"Error", "msg"=>"Solo se permite subir imagenes", "type"=>"error");
            }
            
        }
    }

    function getAvatarById($id_usuario) {
        $id_usuario = intval($id_usuario);
        $u = $this->db->query("select avatar from cat_usuarios where id_usuario=$id_usuario");
        
        if(!empty($u) && $u->num_rows()>0) {
            return $u->row()->avatar;
        } else {
            return "";
        }
        
    }

}