<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model{

	function __construct() {
		parent::__construct();
    }
    
    function getUserPass($email) {
		$r = $this->db->select("pass")->
			from("cat_usuarios")->
			where("correo", $email)->
      where("borrado='0'")->
      where("inactivo='0'")->
			get();

		if($r->num_rows()>0) {
			return $r->result_array();
		}else {
			return array();
		}
    }
    
    function getUserByEmailAndHash($email, $hash) {
        $r = $this->db->select("u.*, cc.api_key, p.descripcion as perfil, catc.descripcion as cliente")->
            from("cat_usuarios as u")->
            join("cat_perfiles as p","u.id_perfil=p.id_perfil")->
            join("cat_clientes as catc","catc.id_cliente=u.id_cliente")->
            join("clientes_contratos as cc","cc.id_cliente=u.id_cliente and status='A'",'left')->
            where("u.correo", $email)->
            where("u.pass", $hash)->
            get();

            // die($this->db->last_query());
        return $r->row();
    }

    function insertarLogAcceso($d) {
      $ban=TRUE;//bandera para determinar si la transaccion se realiza o se da rollback
      $msgError="";//variable para retornar el mensaje de error al usuario	
      $this->db->trans_begin();
      if(!$this->db->insert("log_accesos", $d)) {
          $ban=FALSE;
          // echo $this->db->_error_message();
          // var_dump($d);
          $msgError.="No fue posible guardar el registro en catalogo de puestos";
      }else{
              $ban=TRUE;
              $msgError.="ok";
      }

      if($this->db->trans_status() === TRUE && $ban==TRUE){	
        $this->db->trans_commit();//si todo ha resultado bien se realiza la transaccion		
        return array("error"=>"0","msg"=>"Se a creado el puesto");
      }else if ($this->db->trans_status() === FALSE || $ban==FALSE){
        $this->db->trans_rollback();//si ocurrio algun error en el ciclo se da rollback a la transaccion
        return array("error"=>"1","msg"=>$msgError);
      }         
  }
}