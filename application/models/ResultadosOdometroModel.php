<?php
ini_set("max_execution_time", 0);
date_default_timezone_set ( "America/Mexico_City" );
defined('BASEPATH') OR exit('No direct script access allowed');

class ResultadosOdometroModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function getRowById($tabla, $where) {
        $r = $this->db->where($where)->get($tabla);
        if($r && $r->num_rows()==1) {
            return $r->row();
        } else {
            return array();
        }
    }
        
    function verResultados($exportar=false){
        // ... 
        $id_perfil = intval($this->session->userdata("id_perfil"));
        $id_cliente = intval($this->session->userdata("id_cliente"));
        #Create where clause
        // para obtener el ultimo resultado procesado de la persona-puesto
        // $this->db->select('distinct(concat(id_persona,max(fecha_resultado)))');
        // $this->db->from('resultados');
        // $this->db->where("p.id_cliente=".$id_cliente);
        // $this->db->group_by("id_persona");
        // $where_clause = $this->db->get_compiled_select();        
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,ac.tipo_entrada, ac.id as id_api, r.fecha_resultado, p.id_persona, p.curp, CONCAT(p.nombre, ' ', coalesce(p.paterno, ''), ' ',coalesce( p.materno, '')) as nombre, p.email, p.celular,p.ine,p.direccion,c.descripcion as empresa,r.ponderacion,cp.puesto,cr.riesgo, ac.lote");
        $this->db->from("api_consultas as ac");
        $this->db->join("cat_personas as p","ac.id_persona=p.id_persona");
        $this->db->join("cat_clientes as c", "p.id_cliente=c.id_cliente");
        // $this->db->join("resultados as r", "r.id_persona=p.id_persona");
        $this->db->join("resultados as r", "r.id_api=ac.id");
        $this->db->join("cat_puestos as cp", "r.id_puesto=cp.id");
        $this->db->join("cat_riesgos as cr", "r.id_riesgo_asociado=cr.id","left");
        $this->db->where("p.borrado=0");
        $this->db->where("ac.id_evento",intval(2));
        // if($id_perfil==2){
            $this->db->where("p.id_cliente",$id_cliente);
            // $this->db->where("concat(r.id_persona,fecha_resultado) IN ($where_clause)", NULL, FALSE);
        // }
        $this->db->group_start();
        $this->db->like("CONCAT(p.paterno,' ', COALESCE(p.materno,''),' ',p.nombre)", $search, "both");
        $this->db->or_like("c.descripcion", $search, "both");
        $this->db->or_like("cp.puesto", $search, "both");
        $this->db->group_end();
        $totalRows = $this->db->count_all_results('', FALSE);
        if ($exportar==false)
            $this->db->limit($length, $start);
        $this->db->order_by("p.nombre");
        $r=$this->db->get();
        // die($this->db->last_query());

        $datos = $r->result_array();
        $datos=($datos);
        $result["recordsFiltered"] = $totalRows;
        $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }

    function verRiesgosAsociados(){
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $id_persona = intval($this->input->post("id_persona"));
        $this->db->select("'' as acciones,r.ponderacion,p.puesto,cr.riesgo, ca.combo, r.id_anexado_asociado");
        $this->db->from("resultados_detalles as r");
        $this->db->join("cat_puestos as p", "r.id_puesto=p.id");
        $this->db->join("cat_riesgos as cr", "r.id_riesgo_asociado=cr.id");
        $this->db->join("cat_anexos as ca", "r.id_anexo_asociado=ca.id_anexo");
        $this->db->where("r.id_persona",$id_persona);
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->order_by("r.ponderacion");
        $r=$this->db->get();
        $datos = $r->result_array();
        // echo $this->db->last_query(); die();

        $datos=($datos);
        $result["recordsFiltered"] = $totalRows;
        $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }

    // function listarAPIconsultas(){
    //     $id_perfil = intval($this->session->userdata("id_perfil"));
    //     $id_cliente = intval($this->session->userdata("id_cliente"));
    //     $draw = intval($this->input->post("draw"));
    //     $start = intval($this->input->post("start"));
    //     $length = intval($this->input->post("length"));
    //     $search = $this->input->post("search");
    //     $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
    //     $this->db->select("'' as acciones,cc.descripcion as cliente,ac.id_persona,CONCAT(cp.nombre, ' ', coalesce(cp.paterno, ''), ' ',coalesce(cp.materno, '')) as nombre,p.puesto,ac.fecha_accion,CONCAT(cu.nombre, ' ', coalesce(cu.paterno, ''), ' ',coalesce(cu.materno, '')) as usuario,ac.tipo_entrada,ac.status_evento");
    //     $this->db->from("api_consultas as ac");
    //     $this->db->join("cat_personas as cp","ac.id_persona=cp.id_persona");
    //     $this->db->join("cat_clientes as cc","cp.id_cliente=cc.id_cliente");
    //     $this->db->join("cat_puestos as p", "ac.id_puesto=p.id");
    //     $this->db->join("cat_usuarios as cu", "ac.id_usuario=cu.id_usuario");
    //     $this->db->where("ac.id_evento",intval(2));
	// 	$this->db->group_start();
    //     $this->db->like("CONCAT(COALESCE(cp.paterno,''),' ', COALESCE(cp.materno,''),' ',cp.nombre)", $search, "both");
    //     $this->db->or_like("p.puesto", $search, "both");
    //     $this->db->or_like("cc.descripcion", $search, "both");
    //     $this->db->group_end();
    //     // if($id_perfil==2){
    //         $this->db->where("cp.id_cliente",$id_cliente);
    //     // }
    //     $totalRows = $this->db->count_all_results('', FALSE);
    //     $this->db->limit($length, $start);
    //     $this->db->order_by("cp.nombre","desc");
    //     $r=$this->db->get();
    //     $datos = $r->result_array();
    //     $datos=($datos);
    //     $result["recordsFiltered"] = $totalRows;
    //     $result["data"] = $datos;
    //     $result['draw'] = intval($this->input->post("draw"));
    //     $result["recordsTotal"] = $r->num_rows();	
	// 	return $result;
    // }

    function listarAnalisis(){
        $id_perfil = intval($this->session->userdata("id_perfil"));
        $id_cliente = intval($this->session->userdata("id_cliente"));
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones, cc.descripcion as cliente, ac.id_persona, ac.id_proceso,
        CONCAT(cp.nombre, ' ', coalesce(cp.paterno, ''), ' ',coalesce(cp.materno, '')) as nombre, p.puesto, ac.fecha_programacion, 
        CONCAT(cu.nombre, ' ', coalesce(cu.paterno, ''), ' ',coalesce(cu.materno, '')) as usuario, ac.tipo_entrada, ac.status, 
        escenarios, escenarios_realizados");
        $this->db->from("analisis as ac");
        $this->db->join("cat_personas as cp","ac.id_persona=cp.id_persona");
        $this->db->join("cat_clientes as cc","cp.id_cliente=cc.id_cliente");
        $this->db->join("cat_puestos as p", "ac.id_puesto=p.id");
        $this->db->join("log_accesos as lo", "ac.id_session=lo.id_session");
        $this->db->join("cat_usuarios as cu", "lo.id_usuario=cu.id_usuario");
        $this->db->where("ac.status!='P'");
		$this->db->group_start();
        $this->db->like("CONCAT(COALESCE(cp.paterno,''),' ', COALESCE(cp.materno,''),' ',cp.nombre)", $search, "both");
        $this->db->or_like("p.puesto", $search, "both");
        $this->db->or_like("cc.descripcion", $search, "both");
        $this->db->group_end();
        // if($id_perfil==2){
            $this->db->where("cp.id_cliente",$id_cliente);
        // }
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->order_by("cp.nombre","desc");
        $r=$this->db->get();
        $datos = $r->result_array();
        // $datos=($datos);
        $datosX = array();
        foreach($datos as $row){
            // $status = $this->getStatusNoticias($row['id_proceso'] );
            // echo strpos($row['escenarios'], 'N' ).'ok'; 
            // var_dump($row);
            // echo (preg_match('/O/i', $row['escenarios'])).' -- '.(preg_match('/N/i', $row['escenarios']));
            // die();

            if (preg_match('/N/i', $row['escenarios'])){
                // se tiene que usar escenario noticias.. 
                $row['status_noticias'] = 'E';
                if (preg_match('/N/i', $row['escenarios_realizados'])){
                    $row['status_noticias'] = 'P';
                }
            }else{
                $row['status_noticias'] = 'N';
            }

            if (preg_match('/D/i', $row['escenarios'])){
                // se tiene que usar escenario noticias.. 
                $row['status_dark'] = 'E';
                if (preg_match('/D/i', $row['escenarios_realizados'])){
                    $row['status_dark'] = 'P';
                }
            }else{
                // NO APLICA
                $row['status_dark'] = 'N';
            }

            if (preg_match('/O/i', $row['escenarios'])){
                // se tiene que usar escenario noticias.. 
                $row['status_osint'] = 'E';
                if (preg_match('/O/i', $row['escenarios_realizados'])){
                    $row['status_osint'] = 'P';
                }
            }else{
                // NO APLICA
                $row['status_osint'] = 'N';
            }

            if (preg_match('/R/i', $row['escenarios'])){
                // se tiene que usar escenario noticias.. 
                $row['status_redes'] = 'E';
                if (preg_match('/R/i', $row['escenarios_realizados'])){
                    $row['status_redes'] = 'P';
                }
            }else{
                // NO APLICA
                $row['status_redes'] = 'N';
            }

            if (preg_match('/X/i', $row['escenarios'])){
                // se tiene que usar escenario noticias.. 
                $row['status_web'] = 'E';
                if (preg_match('/X/i', $row['escenarios_realizados'])){
                    $row['status_noticias'] = 'P';
                }
            }else{
                // NO APLICA
                $row['status_web'] = 'N';
            }

            array_push($datosX,$row);
        }

        $result["data"] = $datosX;          

        $result["recordsFiltered"] = $totalRows;
        // $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }    

    function listarPersonas(){
        $id_perfil = intval($this->session->userdata("id_perfil"));
        $id_cliente = intval($this->session->userdata("id_cliente"));
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,id_persona,CONCAT(nombre, ' ', coalesce(paterno, ''), ' ',coalesce( materno, '')) as nombre,curp,ine,alias");
        $this->db->from("cat_personas");
        $this->db->group_start();
        $this->db->like("CONCAT(COALESCE(paterno,''),' ', COALESCE(materno,''),' ',nombre)", $search, "both");
        $this->db->or_like("curp", $search, "both");
        $this->db->or_like("ine", $search, "both");
        $this->db->or_like("alias", $search, "both");
        $this->db->group_end();
        $totalRows = $this->db->count_all_results('', FALSE);
        // if($id_perfil==2){    
            $this->db->where("id_cliente",$id_cliente);
        // }
        $this->db->limit($length, $start);
        $this->db->order_by("id_persona","desc");
        $r=$this->db->get();
        $datos = $r->result_array();
        $datos=($datos);
        $result["recordsFiltered"] = $totalRows;
        $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }

    function listarPuestos(){
        $id_perfil = intval($this->session->userdata("id_perfil"));
        $id_cliente = intval($this->session->userdata("id_cliente"));
        $this->db->select("cp.id, ltrim(rtrim(cp.puesto)) as text");
        $this->db->from("cat_puestos cp");
        // $this->db->join("clientes_puestos pp","cp.id=pp.id_puesto");
        $this->db->where("activo",1);
        // if($id_perfil==2){
            $this->db->where("id_cliente",$id_cliente);
        // }
        $r = $this->db->get();
         if($r && $r->num_rows()>0) {
             
            // return $r->result_array();
            $datos = $r->result_array();
            // $datos=($datos);
            $datosX = array();
            foreach($datos as $row){
                // $status = $this->getStatusNoticias($row['id_proceso'] );

                // $row['id'] = 'E';                
                // $this->db->select("id_riesgo");
                // $this->db->from("puestos_riesgos");
                // $this->db->where("id_puesto",intval($row['id']));
                // $totalRows = $this->db->count_all_results('', FALSE);
                $this->db->select("id_riesgo");
                $this->db->from("puestos_riesgos");
                $this->db->where("id_puesto",intval($row['id']));
                $totalRows = $r->num_rows();
                // $totalRows=0;
                $row['text'] = $row['text']." -- ".$totalRows."";
                $r = $this->db->get();
                array_push($datosX,$row);
            }

            // $result["data"] = $datosX; 
            return $datosX;
        } else {
            return array();
        }
    }

    function isReprocesarPersonaPuestoActiva($id) {
		// $id = intval($id);
        //return $this->getRowById('api_usuarios', "api_key='$id' and activa=1 ");
        $row = $this->getRowById('config_clientes', "id_config_cliente=1 and id_cliente=".$id);   
        if($row && $row->valor==1){
            return true;
        }
        // echo $this->db->last_query();     
        return false;
    }

    function getCalificacion($idPerson, $idPuesto) {
        // $id = intval($id);
        
        $row = $this->getRowById('resultados', "id_persona='$idPerson' and id_puesto='$idPuesto'");
        // echo $this->db->last_query();
        return $row;
	}    
        
    function enviarAnalisis(){

        // validamos si no tiene ya resultado la persona.. 

        $id_puesto= intval($this->input->post("id_puesto"));
        $id_persona = intval($this->input->post("id_persona"));
        $id_usuario = intval($this->session->userdata("id_usuario"));
        $this->db->select("id_persona");
        $this->db->from("api_consultas");
        $this->db->where("id_persona",$id_persona);
        $this->db->where("id_puesto",$id_puesto);
        $this->db->where("status_evento","E");
        $this->db->where("id_evento",intval(2));
        $r = $this->db->get();
        if($r && $r->num_rows()>0) {
            return array("error"=>"1","msg"=>"Ya se encuentra esta persona con ese puesto en espera de analisis");
        }    

        $rowPonderacion = $this->getCalificacion($id_persona,$id_puesto);
        $reprocesar = $this->isReprocesarPersonaPuestoActiva($_SESSION['id_cliente']);
        if ($rowPonderacion && $reprocesar==false){
            return array("error"=>"1","msg"=>"Debe marcar la casilla de reprocesamiento en pantalla Administración / Configuración, ya que esa persona-puesto ya tiene resultado");
        }


        //return array($id_puesto,$id_persona,$id_usuario);
        $where["cu.id_usuario"]=intval($id_usuario);
        $where["cc.status"]=strval("A");
        $where["cc.borrado"]=intval(0);
        $this->db->select("cc.api_key,tipo_servicio,fecha_inicio,fecha_fin,num_procesamientos,num_consumos");
        $this->db->from("cat_usuarios cu");
        $this->db->join("clientes_contratos cc","cu.id_cliente=cc.id_cliente");
        $this->db->where($where);
        //return $this->db->get_compiled_select();  
        $r = $this->db->get();
        if($r && $r->num_rows()>0) {
            $row = $r->row(); 
            $api_key=$row->api_key;
            $tipo_servicio = $row->tipo_servicio;
            if($tipo_servicio=="T"){
                $fecha_actual= date("Y-m-d");
                $fecha_inicio=$row->fecha_inicio;
                $fecha_fin=$row->fecha_fin;
                if($fecha_actual<$fecha_inicio||$fecha_actual>$fecha_fin){
                    return array("error"=>"1","msg"=>"Se encuentra fuera del rango de su contrato activo de esta empresa. Fecha inicial:".$fecha_inicio.", Fecha final:".$fecha_fin);
                }   
            }else if($tipo_servicio=="P"){
                $num_procesamientos=$row->num_procesamientos;
                $num_consumos=$row->num_consumos;
                if($num_procesamientos==$num_consumos){
                    return array("error"=>"1","msg"=>"Ya se alcanzo el numero total de procesamientos(".$num_procesamientos.") de su contrato activo.");
                }
            }
            $apiConsulta["api_key"]=strval($api_key);
            $apiConsulta["id_evento"] =intval(2);
            $apiConsulta["fecha_accion"] =date("Y-m-d H:i:s");
            $apiConsulta["status_evento"] =strval("E");
            $apiConsulta["id_persona"] =intval($id_persona);
            $apiConsulta["id_puesto"] =intval($id_puesto);
            $apiConsulta["lote"] =date("Y-m-d H:i:s");
            $apiConsulta["id_usuario"] =intval($id_usuario);
            $apiConsulta["tipo_entrada"] =strval("W");
            if($this->db->insert("api_consultas", $apiConsulta)) {
                return array("error"=>"0","msg"=>"La persona ha sido puesta en espera de analisis.");
            }else{
                return array("error"=>"1","msg"=>"La persona no ha sido puesta en espera de analisis.");
            }
        }else{
            return array("error"=>"1","msg"=>"No existe un contrato activo de esta empresa.");
        }
    }


    function getResultadosPyP($idapi) {
        $where =  "id_api=".$idapi;

        $this->db->select("'' as acciones,ac.tipo_entrada, r.id_cliente, ac.id as id_api, r.fecha_resultado, p.id_persona, p.curp, CONCAT(p.nombre, ' ', coalesce(p.paterno, ''), ' ',coalesce( p.materno, '')) as nombre, p.email, p.celular,p.ine,p.direccion,r.ponderacion,cp.puesto,cr.riesgo");
        $this->db->from("resultados as r");
        $this->db->join("cat_personas as p", "r.id_persona=p.id_persona");
        $this->db->join("api_consultas as ac","ac.id_persona=p.id_persona");        
        $this->db->join("cat_puestos as cp", "r.id_puesto=cp.id");
        $this->db->join("cat_riesgos as cr", "r.id_riesgo_asociado=cr.id","left");
        $this->db->where($where);
        $r=$this->db->get();
        // $datos = $r->result_array();
        //die($this->db->last_query());

        if($r && $r->num_rows()>1) {
            return $r->row();
        } else {
            return array();
        }

    } 

    function getIDsApis($idlote) {
        $where =  " lote='".$idlote."'";

        $this->db->select(" id");
        $this->db->from("api_consultas ");
        $this->db->where($where);
        $r=$this->db->get();
        // $datos = $r->result_array();

        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }

    } 

    function getEmailCliente($idcliente) {
        $where =  " id_cliente=".$idcliente;

        $this->db->select(" email_contacto");
        $this->db->from("cat_clientes ");
        $this->db->where($where);
        $r=$this->db->get();
        // $datos = $r->result_array();

        if($r && $r->num_rows()==1) {
            return $r->row();
        } else {
            return array();
        }

    } 
    

    function getResultadosDetalles($idfuente, $idapi) {
        $where =  "id_api=".$idapi." and id_anexo_asociado in (".$idfuente.")";
        $r = NULL;
        switch($idfuente){
            case '3':
                // noticias... 
                $this->db->select("rd.*, rx.riesgo, p.url");
                $this->db->from("resultados_detalles as rd");
                $this->db->join("paginas as p", "rd.id_anexado_asociado=p.id_pagina");
                $this->db->join("cat_riesgos as rx", "rx.id=rd.id_riesgo_asociado");
                $this->db->where($where);
                $r=$this->db->get();        
            break;
            case '11':
                // darkweb... 
                $sql = "Select rd.*, rx.riesgo, ds.url, hash as hash_body, dp.title as titulo_articulo, '0000-00-00' as fecha_articulo, ltrim(rtrim(dp.content)) as pg
                from huelladb.resultados_detalles as rd join huelladb.cat_riesgos as rx on rx.id=rd.id_riesgo_asociado join darkDB.pages as dp on dp.idPage=rd.id_anexado_asociado join darkDB.sites as ds on dp.idSite=ds.idSite WHERE  ".$where;

                $r = $this->db->query($sql);      
            break;
            case "'2','5'":
                // buscadores web... 
                $this->db->select("rd.*, rx.riesgo, p.url_asociado");
                $this->db->from("resultados_detalles as rd");
                $this->db->join("urls_sociales as p", "rd.id_anexado_asociado=p.id_anexado");
                $this->db->join("cat_riesgos as rx", "rx.id=rd.id_riesgo_asociado");
                $this->db->where($where);
                $r=$this->db->get();        
            break;
            case "'12','13','14'":
                // buscadores web... 
                $this->db->select("rd.*, rx.riesgo, p.url_asociado");
                $this->db->from("resultados_detalles as rd");
                $this->db->join("urls_sociales as p", "rd.id_anexado_asociado=p.id_anexado");
                $this->db->join("cat_riesgos as rx", "rx.id=rd.id_riesgo_asociado");
                $this->db->where($where);
                $r=$this->db->get();        
            break;
            case "'6','7','8','9','10'":
                // buscadores web... 
                $this->db->select("rd.*, ca.nombre as anexo, p.url_asociado, p.json_data");
                $this->db->from("resultados_detalles as rd");
                $this->db->join("urls_sociales as p", "rd.id_anexado_asociado=p.id_anexado");
                $this->db->join("cat_anexos as ca", "ca.id_anexo=p.id_anexo");
                $this->db->where($where);
                $r=$this->db->get();        
            break;
        }

        // $datos = $r->result_array();
        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }

	}
}    