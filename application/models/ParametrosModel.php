<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require APPPATH.'/models/GenericModel.php';

class ParametrosModel extends CI_Model {

	private $tabla;

	function __construct() {
		parent::__construct();
		$this->tabla = "parametros";
	}	

	function getParametroById($id) {
		//$id = intval($id);
		return $this->getRowById($this->tabla, " cve_param='$id'");
	}	

    function getRowById($tabla, $where) {
        $r = $this->db->where($where)->get($tabla);
        if($r && $r->num_rows()==1) {
            return $r->row();
        } else {
            return array();
        }
	}	
	
	function listar() {
        $r = $this->db->select("cve_param, valor")->
            from("parametros")->get();

        //die($this->db->last_query());
        if($r && $r->num_rows()>0) {
            return array("data"=>$r->result_array());
        } else {
            return array("data"=>array());
        }
	}
	
	function actualizar($id, $d) {
        // $id = intval($id);
        if($this->db->update("parametros", $d, "cve_param='".$id."'")) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

}