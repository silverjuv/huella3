<?php
set_time_limit (0);
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfigClienteModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function getConfiguracionCliente($id) {
        $id = intval($id);
        $r = $this->db->from("config_clientes as cc")->
            join("cat_configuraciones_clientes as ccc ","cc.id_config_cliente=ccc.id_config")->
            where("cc.id_cliente=".$id)->
			get();
            // die($this->db->last_query());

		if($r && $r->num_rows()>0) {
			//$data = $r->row();
			return array("data"=>$r->result_array());
		} else {
			return array("data"=>array());
		}
    }

    function insertar($d) {
        if($this->db->insert("config_clientes", $d)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function borrar($id) {
        $id = intval($id);
        if($this->db->delete("config_clientes", "id_cliente=".$id)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha borrado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible borrar el registro", "type"=>"error");
        }
    }    
}