<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatSitiosWebModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function listar($exportar=false) {
        // $r = $this->db->select("cs.id, cs.target, cc.categoria, Case cs.aprocesar when '1' then 'Si' when '0' then 'No' end as activo, cs.peso, pais, entidad, count(*) as paginas")->
        //     from("cat_sitiosweb as cs")->
        //     join("cat_categorias_dominios as cc", "cc.id_categoria=cs.id_categoria")->
        //     join("cat_paises as cp", "cp.cve_pais=cs.cve_pais")->
        //     join("paginas as pag", "pag.id_sitioweb=cs.id")->
        //     where("cs.borrado='0'")
        //     ->group_by(array("cs.id", "cs.target","cs.id", "activo","cs.peso", "pais", "entidad"))->get();
        $r = $this->db->select("cs.id, cs.target, cc.categoria, Case cs.aprocesar when '1' then 'Si' when '0' then 'No' end as activo, cs.peso, pais, entidad, count(*) as paginas")->
            from("cat_sitiosweb as cs")->
            join("cat_categorias_dominios as cc", "cc.id_categoria=cs.id_categoria")->
            join("cat_paises as cp", "cp.cve_pais=cs.cve_pais")->
            join("paginas2 as pag", "pag.id_url=cs.id", "left")->
            where("cs.borrado='0'")
            ->group_by(array("cs.id", "cs.target","cs.id", "activo","cs.peso", "pais", "entidad"))->get();

        // die($this->db->last_query());
        if($r && $r->num_rows()>0) {
            return array("data"=>$r->result_array());
        } else {
            return array("data"=>array());
        }
    }

    function getPaisesForCombo() {
        $r = $this->db->select("cve_pais as id, pais as text")->
            from("cat_paises ")->get();

        //die($this->db->last_query());
        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }
    }

    function getCategoriasForCombo() {
        $r = $this->db->select("id_categoria as id, CONCAT(categoria,' | ', peso) as text ")->
            from("cat_categorias_dominios ")->get();

        //die($this->db->last_query());
        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }
    }    

    // function listarUsuarios() {
    //     $draw = intval($this->input->post("draw"));
    //     $start = intval($this->input->post("start"));
    //     $length = intval($this->input->post("length"));
        
    //     $search = $this->input->post("search");
    //     $search = ( empty($search["value"]) )? "": $search["value"];

    //     $r = $this->db->select("u.id_usuario, u.correo, CONCAT(u.paterno,' ', COALESCE(u.materno,''),' ',u.nombre) as nombre, p.descripcion as perfil")->
    //         from("cat_usuarios as u")->
    //         join("cat_perfiles as p", "u.id_perfil=p.id_perfil")->
    //         where("u.borrado='0' and u.inactivo='0'")->
    //         like("CONCAT(u.paterno,' ', COALESCE(u.materno,''),' ',u.nombre)", $search, "both")->
    //         limit($length, $start)->
    //         order_by("nombre")->
    //         get();
    //     //die($this->db->last_query());
    //     if($r && $r->num_rows()>0) {
    //         $nRegistros = $this->db->from("cat_usuarios as u")->
    //             join("cat_perfiles as p", "u.id_perfil=p.id_perfil")->
    //             where("u.borrado='0'")->
    //             like("CONCAT(u.paterno,' ', COALESCE(u.materno,''),' ',u.nombre)", $search, "both")->
    //             count_all_results();

    //         $data["recordsTotal"] = $r->num_rows();
    //         $data["recordsFiltered"] = $nRegistros;
    //         $data["draw"] = $draw;
    //         $data["data"] = $r->result_array();
            
    //         return $data;
    //     } else {
    //         $data["recordsTotal"] = 0;
    //         $data["recordsFiltered"] = 0;
    //         $data["draw"] = $draw;
    //         $data["data"] = array();
    //         return $data;
    //     }
    // }

    function insertar($d) {
        if($this->db->insert("cat_sitiosweb", $d)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function actualizar($id, $d) {
        $id = intval($id);
        if($this->db->update("cat_sitiosweb", $d, "id=".$id)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha guardado el registro", "type"=>"info");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function getSitioWebById($id_site) {
        $id = intval($id_site);
        $r = $this->db->select("id, st.cve_pais, st.id_categoria, cp.pais as perfil, target, entidad, st.peso, categoria")->from("cat_sitiosweb as st")->
            join("cat_paises as cp","st.cve_pais=cp.cve_pais")->
            join("cat_categorias_dominios as cc","st.id_categoria=cc.id_categoria")->
			where("st.id",$id)->
			get();
		
		if($r && $r->num_rows()>0) {
			$data = $r->row();
			$d = ($data);
			return $d;
		} else {
			return array("data"=>array());
		}
    }

    // function deleteUsuarioById($id) {
    //     $id = intval($id);
    //     if($this->db->update("cat_usuarios", array("borrado"=>"1"), "id_usuario=".$id)) {
    //         return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha borrado el registro", "type"=>"info");
    //     } else {
    //         array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible borrar el registro", "type"=>"error");
    //     }
    // }

    function isSitioWebActivo($id) {
        $id = intval($id);
        $r = $this->db->select("aprocesar")->where("id", $id)->get("cat_sitiosweb")->row();

        if($r) {
            return ($r->aprocesar == "1");
        } else {
            return false;
        }
        
    }

    function deshabilitarSitioWebById($id) {
        $id = intval($id);
        $isInactive = !($this->isSitioWebActivo($id));

        if($this->db->update("cat_sitiosweb", array("aprocesar"=>$isInactive), "id=".$id)) {
            return array("error"=>"0", "title"=>"Info", "msg"=>"Se ha actualizado el registro", "type"=>"success");
        } else {
            return array("error"=>"1", "title"=>"Error", "msg"=>"No fue posible actualizar el registro", "type"=>"error");
        }
    }

}