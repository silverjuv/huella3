<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContratosModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function insertarContrato($r) {
        $fuentes = $r["fuentes"];
        unset($r["fuentes"]);

        if($this->db->insert("clientes_contratos", $r)) {
            $id = $this->db->insert_id();
            $len = count($fuentes);
            for($i=0; $i<$len; $i++) {
                $row = array("id_contrato"=>$id, "fuente"=>$fuentes[$i]);
                $this->db->insert("contratos_fuentes", $row);
            }

            return array("title"=>"Info", "msg"=>"Registro Guardado", "type"=>"success");
        } else {
            return array("title"=>"Error", "msg"=>"No fue posible guardar el registro", "type"=>"error");
        }
    }

    function listar($exportar=false) {
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": $search["value"];

        $this->db->select("con.id_contrato, c.descripcion as cliente, case con.status when 'A' then 'Activo' when 'S' then 'Suspendido' when 'C' then 'Cancelado' end as status,  CASE tipo_servicio When 'T' then 'Por tiempo' When 'P' then 'Por procesamiento' end as servicio, 
            fecha_inicio, fecha_fin, num_procesamientos as procesamientos, num_consumos as consumos")->
            from("clientes_contratos as con")->
            join("cat_clientes as c", "con.id_cliente=c.id_cliente")->
            // join("cat_clientes as c", "con.id_cliente=c.id_cliente")->
            where("con.borrado=0")->
            like("c.descripcion", $search, "both");
            if ($exportar==false)
                $this->db->limit($length, $start);
            $this->db->order_by("cliente asc, fecha_fin desc");
            $r = $this->db->get();


        if($r && $r->num_rows()>0) {
            $nRegistros = $this->db->from("clientes_contratos as con")->
                join("cat_clientes as c", "con.id_cliente=c.id_cliente")->
                where("con.borrado=0")->
                like("c.descripcion", $search, "both")->
                count_all_results();

            $data["recordsTotal"] = $r->num_rows();
            $data["recordsFiltered"] = $nRegistros;
            $data["draw"] = $draw;
            // $data["data"] = $r->result_array();
            // $data["data"] = array();
            $arr = array();
            foreach ($r->result_array() as $row) {
                $f = $this->db->select("fuente, text as fuentex")->from("contratos_fuentes as cf")->
                join("cat_fuentes as catf", "catf.id=cf.fuente")->
                where("id_contrato", $row['id_contrato'])->get();

                if($f && $f->num_rows()>0) {
                    $fuentes = $f->result_array();
                    $fuentes = array_column($fuentes, "fuentex");

                    $row['fuentes'] = $fuentes;
                } else {
                    $row['fuentes'] = array();
                }

                // obtenemos usuarios asociados a esos clientes...
                //$f = $this->db->select("ltrim(rtrim(nombre)) as nombre, ltrim(rtrim(paterno)) as paterno, ltrim(rtrim(correo)) as correo ")->from("cat_usuarios as cu")->
                // $f = $this->db->select(" ltrim(rtrim(correo)) as correo ")->from("cat_usuarios as cu")->
                // where("id_cliente", $row['id_cliente'])->get();

                // if($f && $f->num_rows()>0) {
                //     $fuentes = $f->result_array();
                //     $fuentes = array_column($fuentes, "correo");

                //     $row['usuarios'] = $fuentes;
                // } else {
                //     $row['usuarios'] = array();
                // }

                array_push($arr, $row);
            }
            $data["data"] = $arr;
            
            return $data;
            
        } else {
            $data["recordsTotal"] = 0;
            $data["recordsFiltered"] = 0;
            $data["draw"] = $draw;
            $data["data"] = array();
            return $data;
        }
    }

    function getContratoById($id_contrato) {
        $id_contrato = intval($id_contrato);
        $r = $this->db->from("clientes_contratos")->where("id_contrato", $id_contrato)->get();
        if($r && $r->num_rows()>0) {
            $f = $this->db->select("fuente")->from("contratos_fuentes")->where("id_contrato", $id_contrato)->get();

            if($f && $f->num_rows()>0) {
                $fuentes = $f->result_array();
                $fuentes = array_column($fuentes, "fuente");

                $res = $r->row();
                $res->fuentes = $fuentes;
            } else {
                $res = $r->row();
                $res->fuentes = array();
            }
            return $res;
        } else {
            return array();
        }
    }

    function actualizarContrato($id, $r) {
        $id = intval($id);
        $fuentes = $r["fuentes"];
        unset($r["fuentes"]);

        // si estatus que se envia ahora es Activo, 
        // se desactiva el Activo de los otros contratos del mismo cliente.
        if ($r['status'] == 'A'){
            $dx['status']='S';
            $this->db->update("clientes_contratos",$dx , "id_cliente=".$r['id_cliente']);
            // var_dump($dx);
            // var_dump($r);
            // die();
        }
        
        if($this->db->update("clientes_contratos", $r, "id_contrato=".$id)) {
            if($this->db->where("id_contrato", $id)->delete("contratos_fuentes")) {
                $len = count($fuentes);
                
                for($i=0; $i<$len; $i++) {
                    $row = array("id_contrato"=>$id, "fuente"=>$fuentes[$i]);
                    $this->db->insert("contratos_fuentes", $row);
                }
            }
            return array("title"=>"Info", "msg"=>"Registro actualizado", "type"=>"success");
        } else {
            return array("title"=>"Error", "msg"=>"No fue posible actualizar el registro", "type"=>"error");
        }
    }

    // function cancelaContratos($id_cliente, $r) {
    //     $id = intval($id);
    //     $fuentes = $r["fuentes"];
    //     unset($r["fuentes"]);
        
    //     if($this->db->update("clientes_contratos", $r, "id_cliente=".$id)) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    function deleteContratoById($id) {
        $id = intval($id);
        $this->db->where("id_contrato", $id);
        $this->db->set("status", "C");
        $this->db->set("borrado", "1");

        if($this->db->update("clientes_contratos")) {
            return array("title"=>"Info", "msg"=>"Registro eliminado", "type"=>"success");
        } else {
            return array("title"=>"Error", "msg"=>"No fue posible eliminar el registro", "type"=>"error");
        }
        
    }

    function guardarApiKey($id, $apiKey) {
        $id = intval($id);
        $this->db->where("id_contrato", $id);
        $this->db->set("api_key", $apiKey);

        if($this->db->update("clientes_contratos")) {
            return array("title"=>"Info", "msg"=>"Api Key actualizada", "type"=>"success", "apiKey"=>$apiKey);
        } else {
            return array("title"=>"Error", "msg"=>"No fue posible actualizar el Api Key", "type"=>"error");
        }
    }

    function getFuentes() {
        // $this->db->distinct();
        $r = $this->db->select(" id,  text")->
            from("cat_fuentes")->get();

        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }
    }

    function tieneContratoActivoCliente($id_cliente) {
        // $id_cliente = intval($id_cliente);
        $n = $this->db->from("clientes_contratos")->
            where("id_cliente", $id_cliente)->
            where("status = 'A'")->
            count_all_results();

        return ($n>0);
    }

    function hayOtroActivo($id_contrato, $id_cliente) {
        // $id_cliente = intval($id_cliente);
        $n = $this->db->from("clientes_contratos")->
            where("id_contrato!=".$id_contrato)->
            where("id_cliente=".$id_cliente)->
            where("status = 'A'")->
            count_all_results();

        return ($n>0);
    }

}