<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GenericModel extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function insertRow($tabla, $datos)
    {
        if ($this->db->insert($tabla, $datos)) {
            $lastId = $this->db->insert_id();
            return array("title" => "Operación exitosa", "msg" => "Registro guardado", "type" => "success", "lastID" => $lastId);
        } else {
            return array("title" => "Operación fallida", "msg" => "No fue posible guardar el registro", "type" => "error");
        }
    }

    function updateRow($tabla, $datos, $where)
    {
        if ($this->db->update($tabla, $datos, $where)) {
            return array("title" => "Operación exitosa", "msg" => "Registro actualizado", "type" => "success");
        } else {
            return array("title" => "Operación fallida", "msg" => "No fue posible actualizar el registro", "type" => "error");
        }
    }

    function getRowById($tabla, $where, $orderby='', $tipoorden='')
    {
        $r=NULL;
        if ($orderby!=''){
            $this->db->order_by($orderby, $tipoorden);
            $r = $this->db->where($where)->get($tabla);
        }else{
            $r = $this->db->where($where)->get($tabla);
        }   

        if ($r && $r->num_rows() >= 1) {
            return $r->row();
        } else {
            return array();
        }
    }

    function deleteRow($tabla, $where, $logicalDelete = true)
    {
        if ($logicalDelete) {
            $r = $this->db->where($where)->set("borrado", "1")->update($tabla);
        } else {
            $r = $this->db->where($where)->delete($tabla);
        }

        if ($r) {
            return array("title" => "Operación exitosa", "msg" => "Registro eliminado", "type" => "success");
        } else {
            return array("title" => "Operación fallida", "msg" => "No fue posible borrar el registro", "type" => "error");
        }
    }

    function DuplicateMySQLRecord($table, $primary_key_field, $primary_key_val, $key_differente, $key_newvalue, $key_differente2='', $key_newvalue2='')
    {
        /* generate the select query */
        $this->db->where($primary_key_field, $primary_key_val);
        $query = $this->db->get($table);

        foreach ($query->result() as $row) {
            foreach ($row as $key => $val) {
                // if ($key != $primary_key_field) {
                    /* $this->db->set can be used instead of passing a data array directly to the insert or update functions */
                    if ($key==$key_differente){
                        $this->db->set($key, $key_newvalue);
                    }else if ($key==$key_differente2){
                        $this->db->set($key, $key_newvalue2);
                    }else{
                        $this->db->set($key, $val);
                    }
                // } //endif              
            } //endforeach
        } //endforeach

        /* insert the new record into table*/
        return $this->db->insert($table);
    }
}
