<?php
ini_set("max_execution_time", 0);
defined('BASEPATH') OR exit('No direct script access allowed');

class CatRiesgosModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function listarRiesgos(){
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,r.id as id_riesgo,r.riesgo as descripcion,r.verbo,r.ponderacion,ca.id_area");
        $this->db->from("cat_riesgos as r");
        $this->db->join("cat_areas_riesgos ca","r.id_area_riesgo=ca.id_area");
        $this->db->like("r.riesgo", $search, "both");
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->order_by("r.id","desc");
        $r=$this->db->get();
        $datos = $r->result_array();
        // $datos=($datos);
        $datosX = array();
        foreach($datos as $row){
            $subriesgos = $this->getRiesgosSubriesgos($row['id_riesgo'] );
            $cadx = '';
            foreach($subriesgos as $riesgo){
                $cadx.= ", ".$riesgo['subriesgo'];
            }
            $puntos = '';
            // if (strlen($cadx)>100)
            //     $puntos = '...';
            $row['subriesgos'] = '<div style="width:100%; text-align:center; font-size:9px">'.substr($cadx,1).'</div>';
            array_push($datosX,$row);
        }

        $result["data"] = $datosX;

        $result["recordsFiltered"] = $totalRows;
        // $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }

    function getRiesgosSubriesgos($id_riesgo){
        $this->db->select("subriesgo");
        $this->db->from("riesgos_subriesgos");
        $this->db->where("id_riesgo",$id_riesgo);
        $r=$this->db->get();	
		return $r->result_array();
    }

    function listarSubriesgos(){
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $id_riesgo = intval($this->input->post("id_riesgo"));
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,id_riesgo,subriesgo");
        $this->db->from("riesgos_subriesgos");
        $this->db->where("id_riesgo",$id_riesgo);
        $this->db->like("subriesgo", $search, "both");
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->order_by("id_riesgo");
        $r=$this->db->get();
        $datos = $r->result_array();
        $datos=($datos);
        $result["recordsFiltered"] = $totalRows;

        $datosX = array();
        foreach($datos as $row){
            $row['borrar'] = '<div style="width:100%; text-align:center; color:red"><i class="fas fa-trash-alt" style="cursor:pointer;" onclick="delSubRiesgo('.$row['id_riesgo'].',\''.$row['subriesgo'].'\')"></i></div>';
            array_push($datosX,$row);
        }

        $result["data"] = $datosX;

        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }

    function listarAreaRiesgos(){
        $this->db->select("id_area as id,area_riesgo as text");
        $this->db->from("cat_areas_riesgos");
        $this->db->order_by("area_riesgo");
        $r=$this->db->get();
        $datos = $r->result_array();
        $datos=($datos);
        return $datos;
    }

    function insertar($d) {
      if($this->db->insert("cat_riesgos", $d)) {
          return array("error"=>"0","msg"=>"Se ha guardado el registro");
      } else {
          return array("error"=>"1", "msg"=>"No fue posible guardar el registro");
      }
    }

    function actualizar($id, $d) {
      $id = intval($id);
      if($this->db->update("cat_riesgos", $d, "id=".$id)) {
          return array("error"=>"0","msg"=>"Se ha guardado el registro");
      } else {
          return array("error"=>"1","msg"=>"No fue posible guardar el registro");
      }
    }

    private function tieneRiesgosAsignados($id_riesgo) {
      $id_riesgo = intval($id_riesgo);
      $n = $this->db->from("riesgos_subriesgos")->
          where("id_riesgo", $id_riesgo)->
          count_all_results();
      return $n>0;
    }

    private function tienePuestoAsignado($id_riesgo) {
        $id_riesgo = intval($id_riesgo);
        $n = $this->db->from("puestos_riesgos")->
            where("id_riesgo", $id_riesgo)->
            count_all_results();
  
        return $n>0;
    }

    private function tieneSubriesgo($data) {
        $n = $this->db->from("riesgos_subriesgos")->
            where($data)->
            count_all_results();
        return $n>0;
    }


    function eliminar($id) {
      $id = intval($id);
    //   if($this->tieneRiesgosAsignados($id)) {
    //       return array("error"=>"1","msg"=>"No fue posible borrar el registro, porque tiene subriesgos asociados");
    //   } 
      if($this->tienePuestoAsignado($id)) {
        return array("error"=>"1","msg"=>"No fue posible borrar el registro, porque el riesgo esta asociado a un puesto");
    }else {
        // eliminamos primero los subriesgos.. 
        if($this->tieneRiesgosAsignados($id)) {
            $this->db->delete("riesgos_subriesgos", "id_riesgo=".$id);
        }
          if($this->db->delete("cat_riesgos", "id=".$id)) {
              return array("error"=>"0","msg"=>"Se ha borrado el registro");
          } else {
              return array("error"=>"1", "msg"=>"No fue posible borrar el registro");
          }
      }
    }

    function guardarSubriesgo($d) {
        if($this->tieneSubriesgo($d)) {
            return array("error"=>"1","msg"=>"No fue posible agregar el registro, porque ya existe para este riesgo");
        } else {
            if($this->db->insert("riesgos_subriesgos", $d)) {
                return array("error"=>"0","msg"=>"Se ha guardado el registro");
            } else {
                return array("error"=>"1", "msg"=>"No fue posible guardar el registro");
            }
        }    
    }

    function eliminarSubriesgo($d) {
        if($this->db->delete("riesgos_subriesgos",$d)) {
            return array("error"=>"0","msg"=>"Se ha borrado el registro");
        } else {
            return array("error"=>"1", "msg"=>"No fue posible borrar el registro");
        }
    }
}   