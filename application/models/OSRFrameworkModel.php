<?php
set_time_limit (0);
defined('BASEPATH') OR exit('No direct script access allowed');

class OSRFrameworkModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function getUsufyPlatforms() {
        $r = $this->db->select("plats.plataforma as id, plataforma as text")->
            from("cat_plataformas as plats")->
            join('cat_osintools', 'cat_osintools.id = plats.osintool_id', 'left')
            ->where('cat_osintools.osint_tool', 'usufy')
            ->get();

        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }
    }

    function getSearchfyPlatforms() {
        $r = $this->db->select("plats.plataforma as id, plataforma as text")->
            from("cat_plataformas as plats")->
            join('cat_osintools', 'cat_osintools.id = plats.osintool_id', 'left')
            ->where('cat_osintools.osint_tool', 'searchfy')
            ->where('plats.activo', 1)
            ->get();
        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }
    }

    function getMailfyPlatforms() {
        $r = $this->db->select("plats.plataforma as id, plataforma as text")->
            from("cat_plataformas as plats")->
            join('cat_osintools', 'cat_osintools.id = plats.osintool_id', 'left')
            ->where('cat_osintools.osint_tool', 'mailfy')
            ->get();
        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }
    }

    function getPhonefyPlatforms() {
        $r = $this->db->select("plats.plataforma as id, plataforma as text")->
            from("cat_plataformas as plats")->
            join('cat_osintools', 'cat_osintools.id = plats.osintool_id', 'left')
            ->where('cat_osintools.osint_tool', 'phonefy')
            ->get();
        if($r && $r->num_rows()>0) {
            return $r->result_array();
        } else {
            return array();
        }
    }
}