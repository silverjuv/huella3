<?php
set_time_limit (0);
defined('BASEPATH') OR exit('No direct script access allowed');

class DarkWebModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function getPalabrasArray($str) {
        $p = explode(" ", $str);
        $len = count($p);
        
        $palabras = array();
        for($i=0; $i<$len; $i++) {
            if(strlen($p[$i]) > 3) {
                $palabras[] = $p[$i];
            }
        }

        return $palabras;
    }

    function madSafety($string)
{
    $tags = array( 'p', 'span','html');
    $string = preg_replace( '#<(' . implode( '|', $tags) . ')>.*?<\/$1>#s', '', $string);

       //$string = stripslashes($string);
       //$string = strip_tags($string);
       //$string = mysql_real_escape_string($string);
       return $string;
}

function strip_tags_content($text, $tags = '', $invert = FALSE) {

    preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
    $tags = array_unique($tags[1]);
     
    if(is_array($tags) AND count($tags) > 0) {
      if($invert == FALSE) {
        return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
      }
      else {
        return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
      }
    }
    elseif($invert == FALSE) {
      return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
    }
    return $text;
  } 

  function ciertoContenidoHTML($html){
      //Obtener el texto de la página web
    // $html = file_get_contents($url);
    $html = htmlspecialchars($html);

    //Generar el DOM
    $doc = new DOMDocument;
    $doc->loadHTML($html);

    
    //Obtener el elemento por el id "textoejemplo"
    // $textoejemplo = $doc->getElementById('textoejemplo');
    $textoejemplo = $doc->getElementsByTagName('article');
    
    //Obtener el texto del elemento
    // $html = $textoejemplo->;
    foreach ($textoejemplo as $book) {
        echo $book->nodeValue, PHP_EOL;
    }
    
    //Imprimir el resultado
    //echo "Texto: " . $texto;
    return $html;
  }

  public function insertAutocompleteIndex ($key){
    // if ($words) {
    //   $rr = explode(',', $words);
    //   if (count($rr)) {
        
    //     $key = Dark::datDec($rr[0]);

        //$elak = new Elasticsearch();

        // default only word
        $params = [
          'index' => 'darkdb_autocomplete',
          'body'  => [
              'query' => [
                  "bool" => [
                      "must" => [
                        "term" => [ "name.keywordstring" => $key ],                  
                      ],
                      "filter" => [
                        "term" => [ "modulo" => "darkweb" ]
                        // "term" => [ "target" => "input_search" ]
                      ]
                  ]
              ]
              
          ]
        ];

        
        // $query = $elak->client->count($params);
        //$query  = $this->elasticsearchcentos->client->count($params);
        $query  = $this->elasticsearch->client->count($params);
        $total_rows = !isset($query['count']) ? 0 : $query['count'];
        // echo $key.$total_rows; var_dump($params); die();
        if ($total_rows==0){
          
          // insertamos...
          // $idUser = AifSession::get('id');
          $params = [        
            'index' => 'darkdb_autocomplete',
            'body'  => [
              'id'        => $key,
              'modulo' =>  "darkweb",
              // 'target' =>  "input_search",        
              'name'   => $key,
              'created_at' => strtotime("-1d"),
              'icon'      => '',
              'id_user'      => intval($this->session->userdata("id_usuario"))
            ]        
          ];
          
          
          // $response = $elak->client->index($params);
          //$query  = $this->elasticsearchcentos->client->index($params);
          $query  = $this->elasticsearch->client->index($params);
        }
    //   }
    // }
  }

  function getSearchAutocompleteELK($keyword) {

    $i = 0;
    // echo $keyword; die();
    if ($keyword) {

        // $elak = new Elasticsearch();
        
        $params = [
            'index' => 'darkdb_autocomplete',
            'body'  => [
              "query" => [
                "bool" => [
                  "must" => [
                      "match" => [
                        "name.edgengram" => [
                          "query" =>  "".$keyword,
                          "fuzziness"  =>  1,
                          //"prefix_length" =>  1
                        ]
                      ],                  
                    // "match" => [
                    //     "modulo" =>  "search"
                    // ]
                    // "match" => [
                    //     "target" =>  "input_keyWordx"
                    // ]
                    ],
                    // "must" => [
                    //   "match" => [
                    //         "modulo" =>  "search"
                    //     ]  
                    // ],
                    "filter" => [
                      "term" => [ "modulo" => "darkweb" ]
                      // "term" => [ "target" => "input_search" ]
                    ]
                ]
              ]
            ]
          ];
          
         
          // $query                 = $elak->client->search($params);
          // $this->load->library('elasticsearch');
          // var_dump($this->elasticsearch);

          //$query  = $this->elasticsearchcentos->client->search($params);
          $query  = $this->elasticsearch->client->search($params);
          $len = $hits                  = sizeof($query['hits']['hits']);
          $hit                   = $query['hits']['hits'];

          $i = 0;
          // echo $len; die();
          $arr;
          while ($i < $hits) {  

            $arr2['id'] = $query['hits']['hits'][$i]['_source']['id'];
            $arr2['text'] = $query['hits']['hits'][$i]['_source']['name'];
            $arr2['icon'] = '';
            
            $arr [] = $arr2;
              
            $i++;

          }      
        }

      if ($i==0){
        return null;
      }
      //return new ArrayObject($arr);    
      return ($arr);    
    
  }

  function xyz (){
    $config = array(
        'indent'         => true,
        'output-xhtml'   => true,
        'wrap'           => 200);
    
    // Tidy to avoid errors during load html
    $tidy = new tidy;
    $tidy->parseString($bill->bill_text, $config, 'utf8');
    $tidy->cleanRepair();
    
    $domDocument = new DOMDocument();
    $domDocument->loadHTML(mb_convert_encoding($tidy, 'HTML-ENTITIES', 'UTF-8'));
  }

    function listar($limit, $start, $search , $urlx) {
        $search = addslashes(trim(strtolower($search)));
        
        // $sql = "Select p.url, hash_body, p.titulo_articulo, p.fecha_articulo, ltrim(rtrim(p.body_dom)) as pg,  
        // MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."') as relevance 
        // from paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN NATURAL LANGUAGE MODE) order by relevance desc limit ".$start.", ".$limit." ";
        // $sql = "Select p.url, hash_body, p.titulo_articulo, p.fecha_articulo, ltrim(rtrim(p.body_dom)) as pg,  
        // MATCH(p.titulo_articulo,p.body_dom) AGAINST('\"".$search."\"') as relevance 
        // from paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('\"".$search."\"' IN BOOLEAN MODE) and length(ltrim(rtrim(p.body_dom)))>500 order by relevance desc limit ".$start.", ".$limit." ";

        // $sql = "Select '',p.url, p.titulo_articulo, p.fecha_articulo, p.body_dom as pg,  
        // MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."') as relevance, 'periodicos' as fuente 
        // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN BOOLEAN MODE)";

        // $sql .= " UNION ";
        $sql = "Select ds.url, hash as hash_body, dp.title as titulo_articulo, '0000-00-00' as fecha_articulo, ltrim(rtrim(dp.content)) as pg,  
        MATCH( dp.content) AGAINST('\"".$search."\"') as relevance 
        from darkDB.pages as dp join darkDB.sites as ds on dp.idSite=ds.idSite WHERE MATCH(dp.content) AGAINST('\"".$search."\"' IN BOOLEAN MODE) and length(ltrim(rtrim(dp.content)))>500 order by relevance desc limit ".$start.", ".$limit." ";

        // $sql .="Select CONCAT(dp.idSite, hash),ds.url, dp.title,'0000-00-00', dp.content,
        // MATCH( dp.content) AGAINST('".$search."') as relevance, 'darkdb' as fuente
        // from darkDB.pages as dp join darkDB.sites as ds on dp.idSite=ds.idSite WHERE MATCH(dp.content) AGAINST('".$search."' IN BOOLEAN MODE) GROUP BY 1 ";
        
        // $sql .= " order by relevance desc limit $start, $limit";
// die($sql);
        $r = $this->db->query($sql);
        
        /*
        $r = $this->db->select("p.url, p.titulo_articulo, p.fecha_articulo, p.body_dom as pg,  MATCH(p.titulo_articulo,p.body_dom) AGAINST('$search') as relevance")->
            from("paginas as p")->
            where("MATCH(p.titulo_articulo,p.body_dom) AGAINST('$search' )  ", NULL, FALSE)->
            order_by("fecha_articulo asc, relevance desc")->
            limit($limit, $start)->
            get();
        */


        if($r && $r->num_rows()>0) {
            $data = $r->result_array();
            $len = count($data);
            $words = $this->getPalabrasArray($search); 
            
            $this->load->library('Globals');

            for($i=0; $i<$len; $i++) {

                $data[$i]["pg"] =  preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $data[$i]["pg"]);
                $data[$i]["pg"] =  preg_replace('/<style\b[^>]*>(.*?)<\/style>/is', "", $data[$i]["pg"]);
                $data[$i]["pg"] =  preg_replace('/<ul\b[^>]*>(.*?)<\/ul>/is', "", $data[$i]["pg"]);
                $data[$i]["pg"] =  preg_replace('/<li\b[^>]*>(.*?)<\/li>/is', "", $data[$i]["pg"]);
                
                //$data[$i]["pg"] = htmlspecialchars($data[$i]["pg"]);
                //$data[$i]["pg"] = $this->ciertoContenidoHTML($data[$i]["pg"]);
                $data[$i]["pg"] = strip_tags($data[$i]["pg"]);
                //$data[$i]["pg"] = strip_tags($data[$i]["pg"],"<article>");

                //$data[$i]["pg"] = strip_tags($data[$i]["pg"],"<article>");
                // echo $data[$i]["pg"];
                // die();
                

                // quitar palabras especiales
                $data[$i]["pg"] = Globals::eliminarPalabrasEspeciales ($data[$i]["pg"]);

                $tokenContent = $this->getPalabrasArray(strtolower($data[$i]["pg"])); 
                // $tokenSearch = $this->getPalabrasArray(strtolower($search), $del); 
                // var_dump($tokenContent); die();
                $contentjoin = implode(" ", $tokenContent);
                // $searchjoin = implode(" ", $tokenSearch);
                // var_dump($searchjoin); die();
                $searchjoin = implode(" ", $words);
                // var_dump($searchjoin); 
                // var_dump($contentjoin); die();
                $posi = strpos($contentjoin,$searchjoin);
                // echo $posi; die();

                // $data[$i]["pg"]=Globals::eliminarAcentos($data[$i]["pg"]);
                $tantos = 0;
                foreach($words as $w) {
                    // $wsinacentos=Globals::eliminarAcentos ($w);
                    $wsinacentos = $w;
                    $data[$i]["pg"] = preg_replace('/'.$wsinacentos.'/i', '<code>'.$w.'</code>', $data[$i]["pg"]);

                    preg_match('/'.$wsinacentos.'/i', $data[$i]["pg"], $matches, PREG_OFFSET_CAPTURE);
                    //print_r($matches);
                    $tantos+=count($matches);
                    // poner color a toda la variación de caracteres especiales con acentos y ñ
                }
                // echo $tantos; die();
                    // solo poner el extracto donde aparece la busqueda completa.. si coincide sino las primeros 1000 caracteres
                    // declaring delimiters 
                // $del = " "; 
                  
                // calling strtok() function 

                    if ($posi>=0){
                      $tantos = $tantos*12; // 26 es el tamaño de <strong style="color:red">
                      $data[$i]["pg"] = substr($data[$i]["pg"],$posi,1000);
                      // tokenizamos y quitamos la ultima palabra... para no tener problemas con etiquetas HTML abiertas <strong>

                    }else{
                      $data[$i]["pg"] = substr($data[$i]["pg"],0,1000);
                    }

                // echo $data[$i]["pg"];
                // die();
                
                
            }

            // $sql = "Select count(*) as n
            // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN NATURAL LANGUAGE MODE) ";
            // // $sql = "Select count(*) as n From ( ";
            // // $sql .= "Select MATCH(titulo_articulo,body_dom) AGAINST('\"".$search."\"') as relevance 
            // // from paginas  WHERE MATCH(titulo_articulo,body_dom) AGAINST('\"".$search."\"' IN BOOLEAN MODE) and length(ltrim(rtrim(body_dom)))>500 ) as x ";

            // $sql .= "Select p.id_pagina 
            // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN BOOLEAN MODE)";

            // $sql .= " UNION ";

            $sql ="Select count(*) as n From (Select MATCH(dp.content) AGAINST('\"".$search."\"') as relevance
            from darkDB.pages as dp WHERE MATCH(dp.content) AGAINST('\"".$search."\"' IN BOOLEAN MODE) ";

            $sql .= ") as x";

            // $sql .="Select DISTINCT(CONCAT(dp.idSite, hash))
            // from darkDB.pages as dp join darkDB.sites as ds on dp.idSite=ds.idSite  WHERE MATCH(dp.content) AGAINST('".$search."' IN BOOLEAN MODE) ";

            // $sql .= ") as x";
            

            $t = $this->db->query($sql);
            
                

            $len = 0; 
            if($t && $t->num_rows()>0) {
                $len = $t->row()->n;
                // if ($len>1000)
                //   $len=1000;
            }

            return array("data"=>$data, "total"=>$len);
        } else {
            return array("data"=>array(), "total"=>0);
        }
    }

    function totalPagesDarkwebElk(){
      
      // desarrollo
      return 54802931;

        $params = [    
          'index' => 'darkdb_pages_masmejorado',
          // 'index' => 'darkdb_pages',
          // 'type'  => 'noticias',
          'body'  => [
              'query' => [
                'match_all' => [
                  "boost" => 1.0
                ],
              ],                           
          ],
        ];        
        try {
          // $query                 = $this->elasticsearchcentos->client->count($params);
          $query                 = $this->elasticsearch->client->count($params);
          return $query['count'];
          // return 0;
        }catch (Exception $ex){
          // var_dump($ex); die();
          return 0;
        }
    }

    function listarElk($limit, $start, $search , $urlx) {
      $search = addslashes(trim(strtolower($search)));
      
      // $sql = "Select p.url, hash_body, p.titulo_articulo, p.fecha_articulo, ltrim(rtrim(p.body_dom)) as pg,  
      // MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."') as relevance 
      // from paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN NATURAL LANGUAGE MODE) order by relevance desc limit ".$start.", ".$limit." ";
      // // // $sql = "Select p.url, hash_body, p.titulo_articulo, p.fecha_articulo, ltrim(rtrim(p.body_dom)) as pg,  
      // // // MATCH(p.body_dom,p.titulo_articulo) AGAINST('\"".$search."\"') as relevance 
      // // // from paginas as p WHERE MATCH(p.body_dom,p.titulo_articulo) AGAINST('\"".$search."\"' IN BOOLEAN MODE) and length(ltrim(rtrim(p.body_dom)))>500 order by relevance desc limit ".$start.", ".$limit." ";

      // $sql = "Select '',p.url, p.titulo_articulo, p.fecha_articulo, p.body_dom as pg,  
      // MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."') as relevance, 'periodicos' as fuente 
      // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN BOOLEAN MODE)";

      // $sql .= " UNION ";

      // $sql .="Select CONCAT(dp.idSite, hash),ds.url, dp.title,'0000-00-00', dp.content,
      // MATCH( dp.content) AGAINST('".$search."') as relevance, 'darkdb' as fuente
      // from darkDB.pages as dp join darkDB.sites as ds on dp.idSite=ds.idSite WHERE MATCH(dp.content) AGAINST('".$search."' IN BOOLEAN MODE) GROUP BY 1 ";
      
      // $sql .= " order by relevance desc limit $start, $limit";
// die($sql);
      // // // $r = $this->db->query($sql);
      
      /*
      $r = $this->db->select("p.url, p.titulo_articulo, p.fecha_articulo, p.body_dom as pg,  MATCH(p.titulo_articulo,p.body_dom) AGAINST('$search') as relevance")->
          from("paginas as p")->
          where("MATCH(p.titulo_articulo,p.body_dom) AGAINST('$search' )  ", NULL, FALSE)->
          order_by("fecha_articulo asc, relevance desc")->
          limit($limit, $start)->
          get();
      */
      //////////////////////////////////////////////////////////////////////////////////////////////////
      // $client = $this->elasticsearch;
        $result = array();

      // // //   $params = [
      // // //     'index' => 'darkdb_pages',
      // // //     'body' => [
      // // //         'settings' => [
      // // //           'index.max_result_window' => 1000000
      // // //       ],  
      // // //     ]
      // // // ];
      
      // // // $response = $this->elasticsearch->client->indices()->putSettings($params);             
        // $i = 0;
        // $params = [    
        //   'index' => 'huelladbidx_paginas_x',
        //   'type'  => 'noticias',
        //   'body'  => [
        //       'query' => [
        //         'bool' => [
        //           'must' => [
        //             // 'term' => ['body_dom' => $search],
        //             'term' => ['titulo_articulo' => $search],
        //           ],
        //         ],
        //       ],
        //   ],
        // ];
        // // // $params = [    
        // // //   'index' => 'darkdb_pages_masmejorado',
        // // //   // 'type'  => 'noticias',
        // // //   'body'  => [
        // // //       // 'query' => [
        // // //       //   'multi_match' => [
        // // //       //       'query' => $search,
        // // //       //       'fields' => ['content','content.folded'],
        // // //       //   ],
        // // //       // ],
        // // //       'query' => [
        // // //         'bool' => [
        // // //           'must' => [
        // // //             'multi_match' => [
        // // //                 'query' => $search,
        // // //                 "type" =>       "phrase",
        // // //                 //'fields' => ['content','content.folded'],
        // // //                 'fields' => ['content'],
        // // //             ],
        // // //             // 'match_phrase' => [ 
        // // //             //   "about"  => "rock climbing"
        // // //             // ]
        // // //           ],
        // // //         ],
        // // //       ],               
        // // //   ],
        // // // ];        
        // // // $query                 = $this->elasticsearchcentos->client->count($params);
        // // // $total_rows = $query['count'];
        // var_dump($query); die();
        $total_rows = $this->totalPagesDarkwebElk();


        $params = [
            "from" =>  $start,
            "size" =>  $limit,          
            'index' => 'darkdb_pages_masmejorado',
            // 'index' => 'darkdb_pages',
            // 'type'  => 'noticias',
            'body'  => [
              // 'query' => [
              //   'multi_match' => [
              //       'query' => $search,
              //       'fields' => ['content','content.folded'],
              //   ],
              // ],
              'query' => [
                'bool' => [
                  'must' => [
                    'multi_match' => [
                        'query' => $search,
                        "type" =>       "phrase",
                        'fields' => ['content']
                        //'fields' => ['content','content.folded'],
                    ],
                    // 'match_phrase' => [ 
                    //   "about"  => "rock climbing"
                    // ]
                  ],
                ],
              ],              
              '_source' => ['content','title','url','name']
            ],
        ];
        //$query                 = $this->elasticsearchcentos->client->search($params);
        $query                 = $this->elasticsearch->client->search($params);
        $len = $hits                  = sizeof($query['hits']['hits']);
        $hit                   = $query['hits']['hits'];
        // $result['searchfound'] = $hits;
        // // while ($i < $hits) {

        //     $result['result'][$i] = $query['hits']['hits'][$i]['_source'];

        //     $i++;
        // }

        //return  $result;
        //////////////////////////////////////////////////////////////////////////////////////////////////


      // // // if($r && $r->num_rows()>0) {
      if($hits>0) {
          // // // $data = $r->result_array();
          // // // $len = count($data);
          $words = $this->getPalabrasArray($search); 
          // var_dump($words); die();
          
          $this->load->library('Globals');

          // // for($i=0; $i<$len; $i++) {
          $i = 0;
          while ($i < $hits) {  

            $data[$i]["pg"] = $query['hits']['hits'][$i]['_source']['content'];
            $data[$i]["titulo_articulo"] = $query['hits']['hits'][$i]['_source']['title'];
            $data[$i]["fecha_articulo"] = '0000-00-00';
            $data[$i]["hash_body"] = '';
            $data[$i]["url"] = $query['hits']['hits'][$i]['_source']['url'].$query['hits']['hits'][$i]['_source']['name'];
            $data[$i]["target"] = $query['hits']['hits'][$i]['_source']['url'];
            // echo $data[$i]["pg"];
            

              $data[$i]["pg"] =  preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $data[$i]["pg"]);
              $data[$i]["pg"] =  preg_replace('/<style\b[^>]*>(.*?)<\/style>/is', "", $data[$i]["pg"]);
              $data[$i]["pg"] =  preg_replace('/<ul\b[^>]*>(.*?)<\/ul>/is', "", $data[$i]["pg"]);
              $data[$i]["pg"] =  preg_replace('/<li\b[^>]*>(.*?)<\/li>/is', "", $data[$i]["pg"]);
              // echo $data[$i]["pg"];
              
              //$data[$i]["pg"] = htmlspecialchars($data[$i]["pg"]);
              //$data[$i]["pg"] = $this->ciertoContenidoHTML($data[$i]["pg"]);
              $data[$i]["pg"] = strip_tags($data[$i]["pg"]);
              
              //$data[$i]["pg"] = strip_tags($data[$i]["pg"],"<article>");

              //$data[$i]["pg"] = strip_tags($data[$i]["pg"],"<article>");
              // echo $data[$i]["pg"];
              // die();
              

              // quitar palabras especiales
              $data[$i]["pg"] = Globals::eliminarPalabrasEspeciales ($data[$i]["pg"]);

              $tokenContent = $this->getPalabrasArray(strtolower($data[$i]["pg"])); 
              // $tokenSearch = $this->getPalabrasArray(strtolower($search), $del); 
              // var_dump($tokenContent); die();
              $contentjoin = implode(" ", $tokenContent);
              // $searchjoin = implode(" ", $tokenSearch);
              // var_dump($searchjoin); die();
              $searchjoin = implode(" ", $words);
              // var_dump($searchjoin); 
              // var_dump($contentjoin); die();
              $posi = strpos($contentjoin,$searchjoin);
              // echo $posi; die();

              // $data[$i]["pg"]=Globals::eliminarAcentos($data[$i]["pg"]);
              $tantos = 0;
              foreach($words as $w) {
                  // $wsinacentos=Globals::eliminarAcentos ($w);
                  // echo 'ok ';
                  $wsinacentos = $w;
                  $data[$i]["pg"] = preg_replace('/'.$wsinacentos.'/i', '<code>'.$w.'</code>', $data[$i]["pg"]);

                  preg_match('/'.$wsinacentos.'/i', $data[$i]["pg"], $matches, PREG_OFFSET_CAPTURE);
                  //print_r($matches);
                  $tantos+=count($matches);
                  //  echo 'ok ';
                  // poner color a toda la variación de caracteres especiales con acentos y ñ
              }
              // var_dump($words);
              // echo $tantos; die();
                  // solo poner el extracto donde aparece la busqueda completa.. si coincide sino las primeros 1000 caracteres
                  // declaring delimiters 
              // $del = " "; 
                
              // calling strtok() function 

                  if ($posi>=0){
                    $tantos = $tantos*12; // 26 es el tamaño de <strong style="color:red">
                    $data[$i]["pg"] = substr($data[$i]["pg"],$posi,1000);
                    // tokenizamos y quitamos la ultima palabra... para no tener problemas con etiquetas HTML abiertas <strong>
                    $ultimaCoin = strripos($data[$i]["pg"],'<code>');
                    // echo 'longitud: '.strlen(trim($data[$i]["pg"])).' - '.$ultimaCoin;
                    $data[$i]["pg"] = substr($data[$i]["pg"],0,$ultimaCoin-1);
                    // echo ' = '.strlen(trim($data[$i]["pg"]));
                  }else{
                    $data[$i]["pg"] = substr($data[$i]["pg"],0,1000);
                  }

              // echo $data[$i]["pg"];
              // die();
              
            $i++;

          }
          // die();

          // $sql = "Select count(*) as n
          // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN NATURAL LANGUAGE MODE) ";
          // // // $sql = "Select count(*) as n From ( ";
          // // // $sql .= "Select MATCH(body_dom,titulo_articulo) AGAINST('\"".$search."\"') as relevance 
          // // // from paginas  WHERE MATCH(body_dom,titulo_articulo) AGAINST('\"".$search."\"' IN BOOLEAN MODE) and length(ltrim(rtrim(body_dom)))>500 ) as x ";

          // $sql .= "Select p.id_pagina 
          // from huelladb.paginas as p WHERE MATCH(p.titulo_articulo,p.body_dom) AGAINST('".$search."' IN BOOLEAN MODE)";

          // $sql .= " UNION ";

          // $sql .="Select DISTINCT(CONCAT(dp.idSite, hash))
          // from darkDB.pages as dp join darkDB.sites as ds on dp.idSite=ds.idSite  WHERE MATCH(dp.content) AGAINST('".$search."' IN BOOLEAN MODE) ";

          // $sql .= ") as x";
          

          // // // $t = $this->db->query($sql);
          
              

          // // // $len = 0; 
          // // // if($t && $t->num_rows()>0) {
          // // //     $len = $t->row()->n;
          // // //     // if ($len>1000)
          // // //     //   $len=1000;
          // // // }

          return array("data"=>$data, "total"=>$total_rows);
      } else {
          return array("data"=>array(), "total"=>0);
      }
  }
}
