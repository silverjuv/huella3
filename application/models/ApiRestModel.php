<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'/models/GenericModel.php';

class ApiRestModel extends GenericModel {

	function __construct() {
		parent::__construct();
    }

    function listarPuestoRiesgos() {
        $r = $this->db->select("p.id, p.puesto")->
            from("cat_puestos as p")->get();
            //where("borrado='0'")->get();

        if($r && $r->num_rows()>0) {
            // agregamos los riesgos como un JSON 
            $puestos = $r->result_array();
            $idx=0;
            foreach($puestos as $row){
                // var_dump($row); die();
                $r_riesgos = $this->db->select("ri.riesgo")->
                from("puestos_riesgos as pr")->
                join("cat_puestos as puestos","puestos.id=pr.id_puesto")->
                join("cat_riesgos as ri","ri.id=pr.id_riesgo")->
                where("puestos.id=".$row['id'])->get();    
                if($r_riesgos && $r_riesgos->num_rows()>0) {
                    $ariesgos = array();
                    foreach($r_riesgos->result_array() as $row_riesgos){
                        array_push($ariesgos,$row_riesgos['riesgo']);
                    }
                    $puestos[$idx++]['riesgos']=$ariesgos;

                }else{
                    // no hay riesgos para el perfil
                    $puestos[$idx++]['riesgos']=array();
                }
            }
            return $puestos;
        }else{
            // no hay puestos
            return array("data"=>array());
        }

    }   

    function listarElementosLote($api_key, $lote) {
        $r = $this->db->select("p.id_persona, p.id_puesto")->
            from("api_consultas as p")->
            where("api_key='$api_key' and lote='$lote'")->get();
        return $r;
    }

    function insertarPersona($d) {
		return $this->insertRow('cat_personas', $d);
    }
    
    function insertarApiEvento($d) {
        // $this->db->set('fecha_accion', 'NOW()');
		return $this->insertRow('api_consultas', $d);
    }
    
    function actualizarApiEventoByFecha($field, $id, $d) {
		// $id = intval($id);
		return $this->updateRow('cat_personas', $d, " $field='$id' ");
	}

	function actualizarPersonaByField($field, $id, $d, $idcliente) {
		// $id = intval($id);
		return $this->updateRow('cat_personas', $d, " $field='$id' and id_cliente=".$idcliente);
    }

    function actualizarPersonaBy2Field($field, $id, $field2, $id2, $d, $idcliente) {
		// $id = intval($id);
		return $this->updateRow('cat_personas', $d, " $field='$id' and $field2='$id2' and id_cliente=".$idcliente);
    }
    
    // function actualizarLoteApiConsultasByPyPyC($lotex , $idpuesto,$idpersona, $idcliente) {
    //     // $id = intval($id);
    //     $d['lote']='L';
	// 	return $this->updateRow('api_consultas', $d, " id_puesto=".$idpuesto." and id_persona=".$idpersona." and id_cliente=".$idcliente);
	// }
	function isTokenActive($token) {
        // $id = intval($id);
        // $sql = "select uto.id_usuario, id_cliente from usuarios_token as uto  where  UNIX_TIMESTAMP() >= UNIX_TIMESTAMP(token_expiracion) inner join cat_usuarios as cu on cu.id_usuario=uto.id_usuario where token='".$token."'";
        $r = $this->db->select("uto.id_usuario, id_cliente")->
            from("usuarios_token as uto")->
            join("cat_usuarios as cu","cu.id_usuario=uto.id_usuario")->
            where(" UNIX_TIMESTAMP() < UNIX_TIMESTAMP(token_expiracion) and token='$token'")->get();
        
            // echo $this->db->last_query(); die();

        //return $r; 
        if($r && $r->num_rows()>0) {
            return $r->result_array();        
        }else{
            return [];
        }
        // $row = $this->getRowById('cat_personas', "$field='$id' and borrado=0 and id_cliente=".$idcliente);
        // // $row = $this->getRowById('cat_personas', "$field='$id'  and id_cliente=".$idcliente);
        // // // echo $this->db->last_query();
        // // return $row;
    }

    function existeToken($token) {
        // $id = intval($id);
        // $sql = "select uto.id_usuario, id_cliente from usuarios_token as uto  where  UNIX_TIMESTAMP() >= UNIX_TIMESTAMP(token_expiracion) inner join cat_usuarios as cu on cu.id_usuario=uto.id_usuario where token='".$token."'";
        $r = $this->db->select("uto.id_usuario, id_cliente")->
            from("usuarios_token as uto")->
            join("cat_usuarios as cu","cu.id_usuario=uto.id_usuario")->
            where("token='$token'")->get();
        
        if($r && $r->num_rows()>0) {
            return $r->result_array();        
        }else{
            return [];
        }
        // if($r && $r->num_rows()>0) {
        //     return $r;        
        // }else{
        //     return NULL;
        // }
        // $row = $this->getRowById('cat_personas', "$field='$id' and borrado=0 and id_cliente=".$idcliente);
        // // $row = $this->getRowById('cat_personas', "$field='$id'  and id_cliente=".$idcliente);
        // // // echo $this->db->last_query();
        // // return $row;
    }

	function getPersonaByField($field, $id, $idcliente) {
        // $id = intval($id);
        
        // $row = $this->getRowById('cat_personas', "$field='$id' and borrado=0 and id_cliente=".$idcliente);
        $row = $this->getRowById('cat_personas', "$field='$id'  and id_cliente=".$idcliente);
        // echo $this->db->last_query(); die();
        // return $row;
        if($row ){
            return $row;
        }
        return NULL;   
    }

    function getPersonaBy2Field($field, $id, $field2, $id2, $idcliente) {
        // $id = intval($id);
        
        $row = $this->getRowById('cat_personas', "$field='$id' and $field='$id'  and id_cliente=".$idcliente);
        // echo $this->db->last_query();
        return $row;
    }

    function getPuestoByCliente($idpuesto, $idcliente) {
        // $id = intval($id);
        $r = $this->db->select("id_puesto")->
            from("clientes_puestos ")->
            where( "id_cliente=".$idcliente." and id_puesto=".$idpuesto)->get();
        return $r;
        // $row = $this->getRowById('clientes_puestos', "id_cliente=".$idcliente." and id_puesto=".$idpuesto);
        // echo $this->db->last_query();
        // return $row;
    }

    function getPersonaPuestoByContratoApiConsultas($idpuesto, $idpersona, $apikey, $status) {
        // $id = intval($id);
        $r = $this->db->select("p.id_persona, p.id_puesto")->
            from("api_consultas as p")->
            where("id_persona=".$idpersona." and id_puesto=".$idpuesto." and api_key='".trim($apikey)."' and status_evento='E'")->get();
        return $r;
        // $row = $this->getRowById('api_consultas', );
        // echo $this->db->last_query(); die();
        // return $row;
    }

    function getUsuarioByCliente($idusuario, $idcliente) {
        // $id = intval($id);
        
        $r = $this->db->select("id_usuario")->
            from("cat_usuarios")->
            where("id_cliente=$idcliente and id_usuario=$idusuario")->get();
        // echo $this->db->last_query();
        return $r;
    }
    
    function existApiConsultaRepetida($idPerson, $idPuesto, $apikey, $lote) {
        // $id = intval($id);
        
        $row = $this->getRowById('api_consultas', "id_persona='$idPerson' and id_puesto='$idPuesto' and api_key='$apikey' and lote='$lote'");
        // echo $this->db->last_query();
        return $row;
    }
    
    function getCalificacion($idPerson, $idPuesto) {
        // $id = intval($id);
        
        $row = $this->getRowById('resultados', "id_persona='$idPerson' and id_puesto='$idPuesto'", 'fecha_resultado', 'desc');
        // var_dump($row()); die();
        // return $row;
        // echo $this->db->last_query();
        if($row ){
            return $row;
        }
        return NULL;        
	}

	// // function isApiKeyActiva($id) {
	// // 	// $id = intval($id);
    // //     //return $this->getRowById('api_usuarios', "api_key='$id' and activa=1 ");
    // //     $row = $this->getRowById('clientes_contratos', "api_key='$id' and status='A' ");   
    // //     // echo $this->db->last_query();     
    // //     return $row;
    // // }

    function tieneContratoActivo($id) {
		// $id = intval($id);
        //return $this->getRowById('api_usuarios', "api_key='$id' and activa=1 ");
        $row = $this->getRowById('clientes_contratos', "id_cliente=$id and status='A' ");   
        // echo $this->db->last_query();     
        // $r = $this->db->select("id_contrato")->
        //     from("clientes_contrato as uto")->
        //     where(" UNIX_TIMESTAMP() < UNIX_TIMESTAMP(token_expiracion) and token='$token'")->get();
        // var_dump($row); die();
        if($row ){
            return $row;
        }
        return NULL;
    }
    
    function isReprocesarPersonaPuestoActiva($id) {
		// $id = intval($id);
        //return $this->getRowById('api_usuarios', "api_key='$id' and activa=1 ");
        $row = $this->getRowById('config_clientes', "id_config_cliente=1 and id_cliente=".$id);   
        if($row && $row->valor==1){
            return true;
        }
        // echo $this->db->last_query();     
        return false;
	}

    function copiaResultados($idapiorigen, $idapidestino)
    {
        // se copian los resultados de tabla RESULTADOS A TABLA RESULTADOS 
        // y de tabla RESULTADOS_DETALLES A RESULTADOS_DETALLES
        $fecha = date('Y-m-d H:i:s');
        $this->DuplicateMySQLRecord('resultados','id_api',$idapiorigen, 'id_api',$idapidestino, 'fecha_resultado',$fecha);
        $this->DuplicateMySQLRecord('resultados_detalles','id_api',$idapiorigen, 'id_api',$idapidestino, 'fecha_resultado',$fecha);
    }
}