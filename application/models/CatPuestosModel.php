<?php
ini_set("max_execution_time", 0);
defined('BASEPATH') OR exit('No direct script access allowed');

class CatPuestosModel extends CI_Model {

	function __construct() {
		parent::__construct();
    }
    
    function listarPuestos(){
        $id_cliente = intval($this->input->post("id_cliente"));
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $search = ( empty($search["value"]) )? "": addslashes($search["value"]);
        $this->db->select("'' as acciones,cp.id as id_puesto,cp.puesto as descripcion, id_cliente");
        $this->db->from("cat_puestos as cp");
        // $this->db->join("clientes_puestos as pp","cp.id=id_puesto");
        $this->db->like("cp.puesto", $search, "both");
        // $this->db->where("pp.id_cliente",$id_cliente);
        // $this->db->where("pp.activo",1);
        $this->db->where("cp.id_cliente",$id_cliente);
        $this->db->where("cp.activo",1);
        $totalRows = $this->db->count_all_results('', FALSE);
        $this->db->limit($length, $start);
        $this->db->order_by("cp.puesto");
        $r=$this->db->get();
        $datos = $r->result_array();
        // $datos=($datos);
        $datosX = array();
        foreach($datos as $row){
            $row['asignar'] = '<div style="width:100%; text-align:center; color:red"><i class="fa fa-plus-square" style="cursor:pointer;" onclick="asignarRiesgos('.$row['id_cliente'].','.$row['id_puesto'].')"></i></div>';
            array_push($datosX,$row);
        }

        $result["data"] = $datosX;

        $result["recordsFiltered"] = $totalRows;
        // $result["data"] = $datos;
        $result['draw'] = intval($this->input->post("draw"));
        $result["recordsTotal"] = $r->num_rows();	
		return $result;
    }

    function listarClientes(){
        $this->db->select("id_cliente as id,descripcion as text");
        $this->db->from("cat_clientes");
        $this->db->where("borrado",0);
        $this->db->order_by("descripcion");
        $r=$this->db->get();
        $datos = $r->result_array();
        $datos=($datos);
        return $datos;
    }

    function insertar($id_cliente,$d) {
        $ban=TRUE;//bandera para determinar si la transaccion se realiza o se da rollback
		$msgError="";//variable para retornar el mensaje de error al usuario	
        $this->db->trans_begin();
        $d["id_cliente"]=intval($id_cliente);
        // $d["activo"] =intval(1);
        // var_dump($d); die();
        if(!$this->db->insert("cat_puestos", $d)) {
            $ban=FALSE;
            $msgError.="No fue posible guardar el registro en catalogo de puestos";
        }else{
            // echo $this->db->_error_message(); die();
            // $sqlscope="SELECT LAST_INSERT_ID() as scope";
		    // $queryscope = $this->db->query($sqlscope);
		    // $datascope = $queryscope->result_array();
            // $id_puesto=$datascope[0]['scope'];
            // $clientes_puestos["id_cliente"]=intval($id_cliente);
            // $clientes_puestos["id_puesto"] =intval($id_puesto);
            // $clientes_puestos["activo"] =intval(1);
            // if(!$this->db->insert("clientes_puestos", $clientes_puestos)) {
                $ban=TRUE;
                // $msgError.="No fue posible guardar el registro en clientes_puestos";
                // $msgError.="No fue posible guardar el registro en catalogo de puestos";
            // }
        }
        if($this->db->trans_status() === TRUE && $ban==TRUE){	
			$this->db->trans_commit();//si todo ha resultado bien se realiza la transaccion		
			return array("error"=>"0","msg"=>"Se a creado el puesto");
		}else if ($this->db->trans_status() === FALSE || $ban==FALSE){
			$this->db->trans_rollback();//si ocurrio algun error en el ciclo se da rollback a la transaccion
			return array("error"=>"1","msg"=>$msgError);
		}         
    }

    function actualizar($id_puesto,$id_cliente,$d) {
        $id_puesto = intval($id_puesto);
        if($this->db->update("cat_puestos", $d, "id=".$id_puesto)) {
            return array("error"=>"0","msg"=>"Se ha actualizado el registro");
        } else {
            return array("error"=>"1","msg"=>"No fue posible guardar el registro");
        }
    }

    private function tieneRiesgosAsignados($id_puesto,$id_cliente) {
        $where["id_puesto"] =intval($id_puesto);
        $where["id_cliente"] =intval($id_cliente);
        $n = $this->db->from("puestos_riesgos")->
          where($where)->
          count_all_results();
        return $n>0;
    }

    function eliminar($id_puesto,$id_cliente) {
      $id_puesto = intval($id_puesto);
      $id_cliente = intval($id_cliente);
      if($this->tieneRiesgosAsignados($id_puesto,$id_cliente)) {
          return array("error"=>"1","msg"=>"No fue posible borrar el registro, porque tiene riesgos asociados");
      } else {
          $puesto["activo"]=0;
          $where["id_cliente"]=intval($id_cliente);
          $where["id"]=intval($id_puesto);
        //   if($this->db->update("clientes_puestos",$puesto,$where)) {
          if($this->db->update("cat_puestos",$puesto,$where)) {
              return array("error"=>"0","msg"=>"Se ha quitado el registro");
          } else {
              return array("error"=>"1", "msg"=>"No fue posible quitar el registro");
          }
      }
  }
}    