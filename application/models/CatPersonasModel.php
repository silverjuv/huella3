<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/models/GenericModel.php';

class CatPersonasModel extends GenericModel {

	private $tabla;

	function __construct() {
		parent::__construct();
		$this->tabla = "cat_personas";
	}
	
	function listar($exportar=false) {
		$draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        
        $search = $this->input->post("search");
		$search = ( empty($search["value"]) )? "": addslashes($search["value"]);
		
		$filtroAdicional = '';
		if ($_SESSION['id_perfil']!=PERFIL_ADMIN){
			// filtrado para clientes...
			$filtroAdicional = ' and c.id_cliente='.$_SESSION['id_cliente'];
		}

		$this->db->select("p.id_persona, p.curp, CONCAT(coalesce(p.paterno, ''), ' ', coalesce(p.materno, ''), ' ', p.nombre) as nombre, 
						p.email, p.celular, p.ine, p.direccion, c.descripcion as empresa")->
			from("cat_personas as p")->
			join("cat_clientes as c", "p.id_cliente=c.id_cliente")->
			where("p.borrado=0 ".$filtroAdicional)->
			group_start()->
			like("CONCAT(p.paterno,' ', COALESCE(p.materno,''),' ',p.nombre)", $search, "both")->
			or_like("p.curp", $search, "both")->
			or_like("p.email", $search, "both")->
			or_like("c.descripcion", $search, "both")->
			group_end();
			if ($exportar==false)
				$this->db->limit($length, $start);
            $this->db->order_by("p.nombre");
            $r = $this->db->get();
        // die($this->db->last_query());
        if($r && $r->num_rows()>0) {
			$nRegistros = $this->db->from("cat_personas as p")->
				join("cat_clientes as c", "p.id_cliente=c.id_cliente")->
                where("p.borrado=0")->
				group_start()->
				like("CONCAT(p.paterno,' ', COALESCE(p.materno,''),' ',p.nombre)", $search, "both")->
				or_like("p.curp", $search, "both")->
				or_like("p.email", $search, "both")->
				or_like("c.descripcion", $search, "both")->
				group_end()->
                count_all_results();

				$datos = $r->result_array();
				// $datosX = array();
				// $sec = 0;
				// foreach($datos as $row){
				// 	$row['seleccionar'] = '<div style="width:100%; text-align:center; font-size:9px"><input type="checkbox" id="ck'.$row['id_persona'].'" /></div>';
				// 	$sec++;
				// 	array_push($datosX,$row);
				// }
		
				$data["data"] = $datos;      

            $data["recordsTotal"] = $r->num_rows();
            $data["recordsFiltered"] = $nRegistros;
            $data["draw"] = $draw;
            // $data["data"] = $r->result_array();
            
            return $data;
        } else {
            $data["recordsTotal"] = 0;
            $data["recordsFiltered"] = 0;
            $data["draw"] = $draw;
            $data["data"] = array();
            return $data;
        }	
	}

	function insertarPersona($d) {
		$this->db->set('fecha_captura', 'NOW()', FALSE);
		return $this->insertRow($this->tabla, $d);
	}

	function actualizarPersona($id, $d) {
		$id = intval($id);
		return $this->updateRow($this->tabla, $d, "id_persona=$id and id_cliente=".$_SESSION['id_cliente']);
	}

	function getPersonaById($id) {
		$id = intval($id);
		return $this->getRowById($this->tabla, "id_persona=$id and borrado=0");
	}

	function getPersonaByField($val) {
		return $this->getRowById($this->tabla, " (curp='$val' or ine='$val' )and id_cliente=".$_SESSION['id_cliente']);
	}

	function borrarPersona($id) {
		$id = intval($id);
		return $this->deleteRow($this->tabla, "id_persona=$id and id_cliente=".$_SESSION['id_cliente']);
	}

}