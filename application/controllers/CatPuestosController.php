<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatPuestosController extends CI_Controller { 
    
	function __construct(){
        parent::__construct();
        $this->load->model("CatPuestosModel");
    }
 
    function index() {
        $params = array("menu_expandido"=>"3", "pantalla"=>"39");
        makeDefaultLayout(
            "catPuestosView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                // 'assets/template/plugins/abpetkov-powerange/dist/powerange.min.js',
                'assets/template/plugins/bootstrapValidator/bootstrapValidator.js',
                'assets/template/plugins/jqueryform/jquery.form.js',
                'assets/template/plugins/jqueryform/jquery.parser.js',
                'assets/js/global.js',
                'assets/js/catPuestos.js'
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css',
                // 'assets/template/plugins/abpetkov-powerange/dist/powerange.min.css',
                'assets/template/plugins/bootstrapValidator/bootstrapValidator.css',
                // 'assets/css/catPuestos.css'
            )
        );
    }

    function listarPuestos(){
        $data = $this->CatPuestosModel->listarPuestos();
        print json_encode($data);
    }

    function listarClientes(){
        $data = $this->CatPuestosModel->listarClientes();
        print json_encode($data);
    }

    function validar(&$data) {
        if(empty($data["puesto"])) {
            return array("error"=>"1","msg"=>"El campo puesto es obligatorio");
        }
        return array("error"=>"0");
    }

    function guardar() {
        $data["puesto"]=strval($this->input->post("descripcion"));      
        $r = $this->validar($data);
        if($r["error"] === "0") {
            $id_puesto = intval($this->input->post("id_puesto"));
            $id_cliente = intval($this->input->post("id_cliente"));
            if(strval($this->input->post("action")) == "0") {
                $r = $this->CatPuestosModel->actualizar($id_puesto,$id_cliente,$data);
            } else if(strval($this->input->post("action")) == "1") {
                $r = $this->CatPuestosModel->insertar($id_cliente,$data);
            }
            
        }
        print json_encode($r);
    }

    function eliminar(){
        $id_puesto = intval($this->input->post("id_puesto"));
        $id_cliente = intval($this->input->post("id_cliente"));
        $data = $this->CatPuestosModel->eliminar($id_puesto,$id_cliente);
        print json_encode($data);
    }
}    