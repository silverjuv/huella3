<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DocAdminController extends CI_Controller {

	function __construct(){
        parent::__construct();
        //$this->load->model("MonitorModel");
    }

    function index() {
        $params = array("menu_expandido"=>"35", "pantalla"=>"42");
        makeDefaultLayout(
            "docAdminView",
            $params, 
            array(
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/apidoc.js",
                "assets/template/js/jquery.highlight.js"
            ),
            array(
                'assets/template/css/jquery.highlight.css',                
            )
        );
    }    

}