<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/OSCalls.php';
class TicDefenseToolsController extends CI_Controller {

    private $objOScalls;

    function __construct(){
        parent::__construct();
        $this->load->model("ExpedientesModel");
        $this->objOSCalls = new OSCalls();

        $this->load->model("CatPersonasModel");
        $this->load->model("ParametrosModel");
    }

    function ejecutarOSINTNumVerify() {
        //$p = $this->plataformasToStr($plataformas, "usufy");
        $id_persona=$_POST['data'];
        $fuente = strval(trim($this->input->post("fuente")));
        $id_perfil=intval($this->session->userdata("id_perfil"));
        $fuente = strval(trim($this->input->post("fuente")));
        if($id_perfil==2){
            $datos["noErrores"]=1;
            $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
            print json_encode(array(
                "name"=>intval($id_persona), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        $where["cp.id_persona"]=intval($id_persona);
        $where["cc.status"]=strval("A");
        $where["cc.borrado"]=intval(0);
        $where["cf.fuente"]=$fuente;
        $this->db->select("cc.id_contrato");
        $this->db->from("clientes_contratos cc");
        $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
        $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
        $this->db->where($where);
        $rC = $this->db->get();
        if($rC->num_rows()==0) {
            //$data=Array();
            $datos["noErrores"]=1;
            $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
            //$data["json"]=$datos;
            //array_push($data["json"],$datos);
            print json_encode(array(
                "name"=>intval($id_persona), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";
        $person = null;
        if($id_persona > 0) {
            $person = $this->CatPersonasModel->getPersonaById($id_persona);
            //var_dump($person);die();
        }

        $nombreP='';
        $errores=0;
        $msg='';
        $url="numverify.com_ByCel";

        if ($person!=null){
            // set API Access Key
            if (strlen($person->celular)==10){
                $row = $this->ParametrosModel->getParametroById('API_KEY_numverify.com');
                $access_key = $row->valor;

                // set phone number
                // $data=$_POST['data'];
                $phone_number = "+52".$person->celular;
                //$url="emailrep.io_ByEmail";

                // Initialize CURL:
                $ch = curl_init('http://apilayer.net/api/validate?access_key='.$access_key.'&number='.$phone_number.'');  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // Store the data:
                $json = curl_exec($ch);
                curl_close($ch);
                
                // Decode JSON response:
                $validationResult = json_decode($json, true);
                //echo $results.' ok ';

                //Guardamos en Base de datos
                $row = $this->ExpedientesModel->consultaUrlSocialIdPersona($url,$id_persona);
                if (count($row)>0){
                    // si hay persona ligada al url_asociado... solo actualizamos URLS_sociales
                    $data["url_asociado"]=trim($url);
                    $data["contenido"]="";
                    $data["json_data"]=$json;
                    $data["id_anexo"]=7;
                    $now = date('Y-m-d H:i:s');
                    $data["fecha_captura"]=$now;
                    $this->ExpedientesModel->actualizarUrlSocial("urls_sociales",$row[0]['id_anexado'],$data);
                }else{
                    $data["url_asociado"]=trim($url);
                    $data["contenido"]="";
                    $data["json_data"]=$json;
                    $data["id_anexo"]=7;
                    $now = date('Y-m-d H:i:s');
                    $data["fecha_captura"]=$now;
                    $idURlAsociado = $this->ExpedientesModel->insertarUrlSocial("urls_sociales",$data);

                    if ($idURlAsociado>0){
                        // INSERTAMOS enlace en expedientes_anexos..
                        $datax["id_persona"]=$id_persona;
                        $datax["id_anexado"]=$idURlAsociado;
                        $datax["fuente"]='R';
                        $datax["usuario"]=1;
                        //$datax["calificacion"]=0;
                        $this->ExpedientesModel->insertarExpedientesAnexos("expedientes_anexos",$datax);
                    }

                }

            }else{
                $msg = 'El celular de la persona no es correcto!';
                $errores+=1;
            }
        }else{
            $msg = 'Persona no localizada en base de datos.. ';
            $errores+=1;
        }

        $results['Id_Persona'] = $id_persona;    
        $results['Persona'] = $nombreP;
        $results['Proceso'] = 'numverify.com_ByCel';
        $results['NoComentarios'] = 0;
        $results['NoUrls'] = 0;
        $results['NoErrores'] = $errores;
        $results['Mensaje'] = $msg;

        print json_encode(array(
            "name"=>$id_persona, 
            "cmd"=>'php numverify',
            "json"=>json_encode($results)
        ));
        // return ( );
    }

    function ejecutarOSINT(){
        // if($id_perfil!=1){
        //     $datos["noErrores"]=1;
        //     $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
        //     print json_encode(array(
        //         "name"=>intval($data), 
        //         "cmd"=>"x",
        //         "json"=>$datos
        //     ));
        //     die();
        // }        

        $this->load->model("ProcesosModel");
        $data=$_POST['data'];
        if ($this->ProcesosModel->existeProcesoPersona($data,6,'0') && $this->ProcesosModel->existeProcesoPersona($data,7,'0')
            && $this->ProcesosModel->existeProcesoPersona($data,8,'0') && $this->ProcesosModel->existeProcesoPersona($data,9,'0')
            && $this->ProcesosModel->existeProcesoPersona($data,10,'0') ){
            $msg = array("error"=>"1","msg"=>"Ya se encuentra registrada la operación solicitada para esa persona", "status" => "0");
            print json_encode(
                // "result"=>$msg
                $msg
                // "json"=>$results
            ); 
            return;
        }

        $comandoEnv = "cd ".PATH_SCRIPTS_PYHUELLA." ; source env1/bin/activate ; python ";
        $comandoEnvFinal = " ; deactivate ";


        // $d['fecha_programacion'] = mdate("%Y-%m-%d %H:%i:%s");
        $now = date('Y-m-d H:i:s');
        $d["fecha_programacion"]=$now;
        // $d['window']='O';
        $d['id_session'] = $_SESSION['id_session'];
        $d['id_persona']=intval($data);   
        
        $folderFileName = $this->objOSCalls->getUniqueName().".json"; 
        // OUTPUT_RESULTS_OSINT_W."/".
        $cmd = $comandoEnv." fromEmailrep.py ".($data)."   ".$folderFileName.$comandoEnvFinal;  
        $d['id_anexo']=6;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = $folderFileName;            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);

        $folderFileName =  $this->objOSCalls->getUniqueName().".json"; 
        $cmd = $comandoEnv." fromNumverify.py ".($data)."   ".$folderFileName.$comandoEnvFinal;  
        $d['id_anexo']=7;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = $folderFileName;            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);

        $folderFileName =  $this->objOSCalls->getUniqueName().".json";  
        $cmd = $comandoEnv." fromFullcontact.py ".($data)." ".$folderFileName.$comandoEnvFinal;         
        $d['id_anexo']=8;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = $folderFileName;            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);

        $folderFileName =  $this->objOSCalls->getUniqueName().".json";  
        $cmd = $comandoEnv." fromClearbit.py ".($data)."   ".$folderFileName.$comandoEnvFinal;         
        $d['id_anexo']=9;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = $folderFileName;            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);    
        
        $folderFileName =  $this->objOSCalls->getUniqueName().".json";  
        $cmd = $comandoEnv." fromHunter.py ".($data)."   ".$folderFileName.$comandoEnvFinal;         
        $d['id_anexo']=10;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = $folderFileName;            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);

        print json_encode(
            // "result"=>$msg
            $msg
            // "json"=>$results
        );        
        
    }

    function ejecutarOSINTEmailrep() {
        $data=$_POST['data'];
        $fuente = strval(trim($this->input->post("fuente")));
        $id_perfil=intval($this->session->userdata("id_perfil"));
        $fuente = strval(trim($this->input->post("fuente")));
        if($id_perfil==2){
            $datos["noErrores"]=1;
            $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        $where["cp.id_persona"]=intval($data);
        $where["cc.status"]=strval("A");
        $where["cc.borrado"]=intval(0);
        $where["cf.fuente"]=$fuente;
        $this->db->select("cc.id_contrato");
        $this->db->from("clientes_contratos cc");
        $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
        $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
        $this->db->where($where);
        $rC = $this->db->get();
        if($rC->num_rows()==0) {
            //$data=Array();
            $datos["noErrores"]=1;
            $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
            //$data["json"]=$datos;
            //array_push($data["json"],$datos);
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        //$fileName = self::OUTPUT_RESULTS."/".self::getUniqueName().".json";
        $fileName = $this->objOSCalls->getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/fromEmailrep.py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = $this->objOSCalls::OUTPUT_RESULTS."/".$fileName;
        
        $results = $this->objOSCalls->ejecutarCmd($cmd, $fileName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>json_encode($results)
        ));
        // return ( );
    }

    function ejecutarOSINTFullcontact() {
        $data=$_POST['data'];
        $fuente = strval(trim($this->input->post("fuente")));
        $id_perfil=intval($this->session->userdata("id_perfil"));
        $fuente = strval(trim($this->input->post("fuente")));
        if($id_perfil==2){
            $datos["noErrores"]=1;
            $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        $where["cp.id_persona"]=intval($data);
        $where["cc.status"]=strval("A");
        $where["cc.borrado"]=intval(0);
        $where["cf.fuente"]=$fuente;
        $this->db->select("cc.id_contrato");
        $this->db->from("clientes_contratos cc");
        $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
        $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
        $this->db->where($where);
        $rC = $this->db->get();
        if($rC->num_rows()==0) {
            //$data=Array();
            $datos["noErrores"]=1;
            $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
            //$data["json"]=$datos;
            //array_push($data["json"],$datos);
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        //$fileName = self::OUTPUT_RESULTS."/".self::getUniqueName().".json";
        $fileName = $this->objOSCalls->getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/fromFullcontact.py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = $this->objOSCalls::OUTPUT_RESULTS."/".$fileName;
        
        $results = $this->objOSCalls->ejecutarCmd($cmd, $fileName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>json_encode($results)
        ));
        // return ( );
    }  


    function ejecutarOSINTClearbit() {
        $data=$_POST['data'];
        $fuente = strval(trim($this->input->post("fuente")));
        $id_perfil=intval($this->session->userdata("id_perfil"));
        $fuente = strval(trim($this->input->post("fuente")));
        if($id_perfil==2){
            $datos["noErrores"]=1;
            $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        $where["cp.id_persona"]=intval($data);
        $where["cc.status"]=strval("A");
        $where["cc.borrado"]=intval(0);
        $where["cf.fuente"]=$fuente;
        $this->db->select("cc.id_contrato");
        $this->db->from("clientes_contratos cc");
        $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
        $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
        $this->db->where($where);
        $rC = $this->db->get();
        if($rC->num_rows()==0) {
            //$data=Array();
            $datos["noErrores"]=1;
            $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
            //$data["json"]=$datos;
            //array_push($data["json"],$datos);
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        //$fileName = self::OUTPUT_RESULTS."/".self::getUniqueName().".json";
        $fileName = $this->objOSCalls->getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/fromClearbit.py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = $this->objOSCalls::OUTPUT_RESULTS."/".$fileName;
        
        $results = $this->objOSCalls->ejecutarCmd($cmd, $fileName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>json_encode($results)
        ));
        // return ( );
    }    

    function ejecutarOSINTHunter() {
        $data=$_POST['data'];
        $fuente = strval(trim($this->input->post("fuente")));
        $id_perfil=intval($this->session->userdata("id_perfil"));
        $fuente = strval(trim($this->input->post("fuente")));
        if($id_perfil==2){
            $datos["noErrores"]=1;
            $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        $where["cp.id_persona"]=intval($data);
        $where["cc.status"]=strval("A");
        $where["cc.borrado"]=intval(0);
        $where["cf.fuente"]=$fuente;
        $this->db->select("cc.id_contrato");
        $this->db->from("clientes_contratos cc");
        $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
        $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
        $this->db->where($where);
        $rC = $this->db->get();
        if($rC->num_rows()==0) {
            //$data=Array();
            $datos["noErrores"]=1;
            $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
            //$data["json"]=$datos;
            //array_push($data["json"],$datos);
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        //$fileName = self::OUTPUT_RESULTS."/".self::getUniqueName().".json";
        $fileName = $this->objOSCalls->getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/fromHunter.py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = $this->objOSCalls::OUTPUT_RESULTS."/".$fileName;
        
        $results = $this->objOSCalls->ejecutarCmd($cmd, $fileName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>json_encode($results)
        ));
        // return ( );
    }   


    // public function getUniqueName() {
    //     $micro = str_replace(".","",microtime()); 
	// 	return $this->get_idsession('id')."_".str_replace(" ","",$micro);	
    // }

    // /**
	//  * Comprueba si existe una variable en la sesion
	//  *
	//  * @param string $var
	//  *        	Nombre de la variable
	//  * @return boolean
	//  */
	// public  function isVar($var) {
	// 	return isset ( $_SESSION [self::$id] ) && isset ( $_SESSION [self::$id] [$var] ) ? true : false;
	// }
	// /**
	//  * Regresa un valor de una variable en la sesion
	//  *
	//  * @param string $var
	//  *        	Nombre de la variable de session
	//  * @return mixed Regresa el valor de la variable de session
	//  */
	// public  function get_idsession($var) {
	// 	return  $this->isVar ( $var ) ? $_SESSION [self::$id] [$var] : false;
	// }

}