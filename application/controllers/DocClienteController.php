<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DocClienteController extends CI_Controller {

	function __construct(){
        parent::__construct();
        //$this->load->model("MonitorModel");
    }

    function index() {
        $params = array("menu_expandido"=>"35", "pantalla"=>"41");
        makeDefaultLayout(
            "docClienteView",
            $params, 
            array(
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/apidoc.js",
                "assets/template/js/jquery.highlight.js"
            ),
            array(
                'assets/template/css/jquery.highlight.css',
                
            )
        );
    }    

    function reloadLog(){
        $data['data'] = file_get_contents(APPPATH.'controllers/api/apidoc/'.$_POST['lengua'].'.txt');
        //$data['log'] = join(PHP_EOL,array_slice(explode("\n",file_get_contents('../scripts_huella/pyhuella/logs/huellamon.log')), -20));
        print json_encode($data);
    }    
}