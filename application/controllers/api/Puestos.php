<?php
   
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/RestMessages.php';
date_default_timezone_set('America/Mexico_City'); // Defined City For Timezone

class Puestos  extends RestController {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
//       $this->load->database();
        $this->load->model("ApiRestModel");
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get()
	{
        if (isset($_SERVER["HTTP_AUTHORIZATION"])) {
            list($type, $data) = explode(" ", $_SERVER["HTTP_AUTHORIZATION"], 2);
            if (strcasecmp($type, "Bearer") == 0) {
                // use $data
                // echo $data; die();
                // verificamos que realmente sea valido el TOKEN, que realmente exista.. 
                $rToken = $this->ApiRestModel->existeToken($data);
                if (count($rToken)>0){
                    // VERIFICAMOS QUE EL TOKEN no haya expirado.. 
                    $rUsuario = $this->ApiRestModel->isTokenActive($data);
                    // var_dump($rUsuario); die();
                    if (count($rUsuario)>0  ){
                        $this->listaPuestos($rUsuario[0]['id_usuario'], $rUsuario[0]['id_cliente']);
                    }else{
                        $this->response(RestMessages::mensaje(114), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);
                    }
                }else{
                    $this->response(RestMessages::mensaje(115), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);
                }
            } else {
                // ???
                $this->response(RestMessages::mensaje(113), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);
            }
        } else {
            // ???
            $this->response(RestMessages::mensaje(113), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);
        }
    }

    private function listaPuestos($usuario,$cliente){
            //validamos peticiones no simultaneas de una api key
            // $rUsuario = $this->ApiRestModel->getUsuarioByApiKey(trim($_GET['api_key']));
            // if ($rUsuario){
                $data = $_GET;

                unset($data['api_key']);
                $rContrato = $this->ApiRestModel->tieneContratoActivo($cliente);

                if ($rContrato){
                    // $dataEventos['api_key']=$rUsuario->api_key;
                    $dataEventos['api_key']=$rContrato->id_contrato;
                    $dataEventos['id_evento']='1';
                    // $dataEventos['id_puesto']='-1';
                    // $dataEventos['id_persona']='-1';
                    $dataEventos['params']=json_encode($data);
                    $dataEventos['fecha_accion']=date("Y-m-d H:i:s");
                    $dataEventos['ip_publica']=$this->getIP();
                    $dataEventos['status_evento']='P';
                    $dataEventos['tipo_entrada']='A';
                    $dataEventos['id_cliente']=$cliente;
                    $dataEventos['id_usuario']=$usuario;
                    
                    $actualizado = $this->ApiRestModel->insertarApiEvento($dataEventos);
                    
                    $data = $this->ApiRestModel->listarPuestoRiesgos();
                
                    $this->response(RestMessages::mensaje(100,$data), RestController::HTTP_OK);
                }else{
                    $this->response(RestMessages::mensaje(116), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);
                }
            // }else{
            //     // api key no valida...
            //     $this->response(RestMessages::mensaje(104), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);
            // }
	}
      
    // /**
    //  * Get All Data from this method.
    //  * Por default, este va a ser el metodo elegido pero, para mostrarlo se usara por ahora metodo GET
    //  *
    //  * @return Response
    // */
    // public function index_post()
    // {
    //     $input = $this->input->post();
    //     $this->db->insert('items',$input);
     
    //     $this->response(['Item created successfully.'], RestController::HTTP_OK);
    // } 
     
    // /**
    //  * Get All Data from this method.
    //  *
    //  * @return Response
    // */
    // public function index_put($id)
    // {
    //     $input = $this->put();
    //     $this->db->update('items', $input, array('id'=>$id));
     
    //     $this->response(['Item updated successfully.'], RestController::HTTP_OK);
    // }
     
    // /**
    //  * Get All Data from this method.
    //  *
    //  * @return Response
    // */
    // public function index_delete($id)
    // {
    //     $this->db->delete('items', array('id'=>$id));
       
    //     $this->response(['Item deleted successfully.'], RestController::HTTP_OK);
    // }

    // function getUserIpAddress() {

    //     foreach ( [ 'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR' ] as $key ) {
    
    //         // Comprobamos si existe la clave solicitada en el array de la variable $_SERVER 
    //         if ( array_key_exists( $key, $_SERVER ) ) {
    
    //             // Eliminamos los espacios blancos del inicio y final para cada clave que existe en la variable $_SERVER 
    //             foreach ( array_map( 'trim', explode( ',', $_SERVER[ $key ] ) ) as $ip ) {
    
    //                 // Filtramos* la variable y retorna el primero que pase el filtro
    //                 if ( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE ) !== false ) {
    //                     return $ip;
    //                 }
    //             }
    //         }
    //     }
    
    //     return '?'; // Retornamos '?' si no hay ninguna IP o no pase el filtro
    // }    
    
    
    // function getRealIP(){

    //     if (isset($_SERVER["HTTP_CLIENT_IP"])){

    //         return $_SERVER["HTTP_CLIENT_IP"];

    //     }elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){

    //         return $_SERVER["HTTP_X_FORWARDED_FOR"];

    //     }elseif (isset($_SERVER["HTTP_X_FORWARDED"])){

    //         return $_SERVER["HTTP_X_FORWARDED"];

    //     }elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){

    //         return $_SERVER["HTTP_FORWARDED_FOR"];

    //     }elseif (isset($_SERVER["HTTP_FORWARDED"])){

    //         return $_SERVER["HTTP_FORWARDED"];

    //     }else{

    //         return $_SERVER["REMOTE_ADDR"];

    //     }
    // }
}