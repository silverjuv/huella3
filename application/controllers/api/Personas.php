<?php
   
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/RestMessages.php';
date_default_timezone_set('America/Mexico_City'); // Defined City For Timezone

class Personas  extends RestController {
 
    private function validarCurp($numero){
        $reg = "#([a-zA-Z]{4}[0-9]{6})([\M\H]{1})([a-zA-Z]{5})([0-9]{2})$#";
        return preg_match($reg, $numero);
    }

    private function validarIne($numero){
        $reg = "#(\d{10,13})$#";
        return preg_match($reg, $numero);
    }

    private function validarCelular($numero){
        $reg = "#\d{10}$#";
        return preg_match($reg, $numero);
    }

    private function validarEmail($email){
        $reg = "#^(((([a-z\d][\.\-\+_]?)*)[a-z0-9])+)\@(((([a-z\d][\.\-_]?){0,62})[a-z\d])+)\.([a-z\d]{2,6})$#i";
        return preg_match($reg, $email);
    }

    private function validarLote($lote){
        // $reg = "#([0-9]{6})\#\d{1,5}$#";
        $reg = "#([0-9]{8})\#\d{1,5}$#";
        return preg_match($reg, $lote);
    }

    private function validarPuesto($idpuesto, $idcliente){
        $rPuesto= $this->ApiRestModel->getPuestoByCliente($idpuesto,$idcliente);   
        if ($rPuesto && $rPuesto->num_rows()>0){
            return true;
        }

        return false;
    }

    // private function validarPersonaPuestoUnico($idpuesto, $idpersona, $apikey){
    //     // apikey = idcontrato
    //     $rPuesto= $this->ApiRestModel->getPersonaPuestoByContratoApiConsultas($idpuesto,$idpersona,$apikey,'E');                    
    //     if ($rPuesto && $rPuesto->num_rows()>0){
    //         return true;
    //     }

    //     return false;
    // } 
    private function isPersonaPuestoContratoUnico($idpuesto, $idpersona, $apikey){
        // apikey = idcontrato
        $rPuesto= $this->ApiRestModel->getPersonaPuestoByContratoApiConsultas($idpuesto,$idpersona,$apikey,'E');                    
        if ($rPuesto && $rPuesto->num_rows()>0){
            return true;
        }

        return false;
    }        

    //     return false;
    // }

	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
//       $this->load->database();
        $this->load->model("ApiRestModel");
    }
       
/**
     * Get All Data from this method.
     * Nos envian los datos de las personas, adicional un AccesKey para validar que es un cliente valido
     * Este evento permite validar si una persona tiene una calificacion para 1 perfil
     * Los datos que deben enviarnos son: Curp (obligatorio), nombre (obligatorio), paterno (obligatorio), materno
     * ine (obligatorio), twitter, linkedin, email, celular, id_puesto (obligatorio), api_key (obligatorio)
     * 
     * @return Response
    */
	public function index_get()
	{
        $this->response(RestMessages::mensaje(112,[],'No se permiten peticiones GET.'), RestController::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * Get All Data from this method.
     * Nos envian los datos de las personas, adicional un AccesKey para validar que es un cliente valido
     * Este evento permite validar si una persona tiene una calificacion para 1 perfil
     * Los datos que deben enviarnos son: Curp (obligatorio), nombre (obligatorio), paterno (obligatorio), materno
     * ine (obligatorio), twitter, linkedin, email, celular, id_puesto (obligatorio), api_key (obligatorio)
     * 
     * @return Response
    */
	public function index_post()
	{
        if (isset($_SERVER["HTTP_AUTHORIZATION"])) {
            list($type, $data) = explode(" ", $_SERVER["HTTP_AUTHORIZATION"], 2);
            if (strcasecmp($type, "Bearer") == 0) {
                // use $data
                // echo $data; die();
                // verificamos que realmente sea valido el TOKEN, que realmente exista.. 
                $rToken = $this->ApiRestModel->existeToken($data);
                if (count($rToken)>0){
                    // VERIFICAMOS QUE EL TOKEN no haya expirado.. 
                    $rUsuario = $this->ApiRestModel->isTokenActive($data);
                    if (count($rUsuario)>0 ){
                        $this->procesaPersonas($rUsuario[0]['id_usuario'], $rUsuario[0]['id_cliente']);
                    }else{
                        $this->response(RestMessages::mensaje(114), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);
                    }
                }else{
                    $this->response(RestMessages::mensaje(115), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);
                }
            } else {
                // ???
                $this->response(RestMessages::mensaje(113), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);
            }
        } else {
            // ???
            $this->response(RestMessages::mensaje(113), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);
        }

    }

    private function procesaPersonas($usuario,$cliente){
        // en caso de que no esten completos los datos solicitados
        //      const HTTP_NOT_ACCEPTABLE = 406;
        $demonioActivado=false;

        // 0. Debemos validar que no haya muchas consultas (>1000) de una api_key o IP publica cada 10 minutos de consulta de personas
        // si se puede lo programamos AQUI
        $msg='';
        // var_dump($_POST); die();
        // echo count($_POST); die();

        // 0. Deben ser exactamente los 13 campos, ni mas ni menos...
        // || !isset($_POST['api_key']) || !isset($_POST['id_usuario'])
        if (count($_POST)!=13 || ( !isset($_POST['curp']) || !isset($_POST['ine']) 
            || !isset($_POST['id_puesto']) || !isset($_POST['nombre']) || !isset($_POST['paterno']) 
            || !isset($_POST['materno']) || !isset($_POST['alias']) || !isset($_POST['celular']) || !isset($_POST['email'])
            || !isset($_POST['lote']) || !isset($_POST['direccion']) || !isset($_POST['twitter']) || !isset($_POST['linkedin']) )){
            // echo count($_POST); die();
            $this->response(RestMessages::mensaje(111), RestController::HTTP_NOT_ACCEPTABLE);
            return;
        }
        
        // 1. Debemos validar los campos obligatorios... 
        if (( empty($_POST['curp']) && empty($_POST['ine']) )
        //|| empty($_POST['nombres']) || empty($_POST['ap_paterno']) 
                || empty($_POST['id_puesto']) ){
                    // || empty($_POST['api_key'])

            if ( empty($_POST['curp']) && empty($_POST['ine']))
                $msg=", es obligatorio el parametro 'curp' a 18 posiciones o el parametro 'ine'";
            if ( empty($_POST['id_puesto']))
                $msg=", es obligatorio el parametro 'id_puesto' que se corresponda con un puesto de nuestra base de datos";
            // if ( empty($_POST['api_key']))
            //     $msg=", es obligatorio el parametro 'api_key'";

            $this->response(RestMessages::mensaje(101,[],$msg), RestController::HTTP_NOT_ACCEPTABLE);
            //die();    
        }else{

            // 2. Debemos validar si existe curp o ine, para Actualizar los campos de la tabla personas, siempre y cuando los campos
            // obligatorios sean de longitud aceptable, no sean vacios..
            // Actualizamos si existen cualquiera de los siguientes campos: nombres, apellidos, twitter, ... 
            $actualizado = false;
            $rPersona = NULL;
            
            // $dataX = (empty($_POST['api_key']) ? '' : $_POST['api_key']);

            // $rContrato = $this->ApiRestModel->isApiKeyActiva($dataX);
            $rContrato = $this->ApiRestModel->tieneContratoActivo($cliente);
            // var_dump($rUsuario); die();
            //if ($rUsuario && count($rUsuario)>0){
            if ($rContrato){
                //validamos ahora el usuario que se envia.. para ver si es valido y pertenece al cliente especificado.. 
                // $rUsuario= $this->ApiRestModel->getUsuarioByCliente($_POST['id_usuario'],$rContrato->id_cliente);                    
                // if ($rUsuario && $rUsuario->num_rows()>0){
                    //echo ($rUsuario->row()->id_usuario); die();
                    // $idUsuario = $rUsuario->row()->id_usuario;

                    // consultamos que no se pase de total de elementos del LOTE especificado
                    // un lote nunca sera procesado si no coincide el numero de elementos con el total guardado en API_CONSULTAS
                    if (!empty($_POST['lote']) && self::validarLote($_POST['lote'])){
                        // verificamos si aun no esta lleno el LOTE en API_CONSULTAS...
                        //$rApiConsulta = $this->ApiRestModel->listarElementosLote(trim($_POST['api_key']),trim($_POST['lote']));                    
                        $rApiConsulta = $this->ApiRestModel->listarElementosLote($rContrato->id_contrato,trim($_POST['lote']));                    
                        if ($rApiConsulta && $rApiConsulta->num_rows()>=intval(substr($_POST['lote'],9))){
                            // SE LLEGO AL LIMITE
                            $this->response(RestMessages::mensaje(106,[],substr($_POST['lote'],7)), RestController::HTTP_NOT_ACCEPTABLE);
                        }else{
                            if (            
                                // !empty($_POST['twitter'])  || !empty($_POST['linkedin']) ||           
                                !empty($_POST['nombre']) && !empty($_POST['paterno'])  &&  !empty($_POST['direccion'])  
                                && !empty($_POST['email']) && !empty($_POST['celular']) 
                                ) { 
                                    // && !empty($_POST['alias'])
                                    //   solo actualizamos si todos los campos estan llenos.. 
                                    if (!empty($_POST['ine']) && !empty($_POST['curp']) ){
                                        // consultamos por los 2 campos
                                        $_POST['nombre']=str_replace(array('\'', '"'), '',trim($_POST['nombre']));
                                        $_POST['paterno']=str_replace(array('\'', '"'), '',trim($_POST['paterno']));
                                        $_POST['materno']=str_replace(array('\'', '"'), '',trim($_POST['materno']));
                                        $_POST['direccion']=str_replace(array('\'', '"'), '',trim($_POST['direccion']));
                                        $_POST['celular']=str_replace(array('\'', '"'), '',trim($_POST['celular']));
                                        $_POST['curp']=str_replace(array('\'', '"'), '',trim($_POST['curp']));
                                        $_POST['ine']=str_replace(array('\'', '"'), '',trim($_POST['ine']));
                                        $_POST['alias']=str_replace(array('\'', '"'), '',trim($_POST['alias']));

                                        $data = $_POST;
                                        unset($data['ine']);
                                        unset($data['api_key']);
                                        unset($data['id_puesto']);
                                        unset($data['lote']);
                                        unset($data['id_usuario']);                                        
                                        $rPersona = $this->ApiRestModel->getPersonaBy2Field('ine',trim($_POST['ine']),'curp',trim($_POST['curp']),$rContrato->id_cliente);
                                        if ($rPersona){
                                            $actualizado = $this->ApiRestModel->actualizarPersonaBy2Field('ine',$_POST['ine'],'curp',trim($_POST['curp']), $data, $rContrato->id_cliente);
                                        }
                                    }

                                    if ($actualizado== false && !empty($_POST['ine'])){
                                        // actualizar por INE
                                        // se le da prioridad a la INE
                                        // realizamos split a los campos a guardar
                                        $_POST['nombre']=str_replace(array('\'', '"'), '',trim($_POST['nombre']));
                                        $_POST['paterno']=str_replace(array('\'', '"'), '',trim($_POST['paterno']));
                                        $_POST['materno']=str_replace(array('\'', '"'), '',trim($_POST['materno']));
                                        $_POST['direccion']=str_replace(array('\'', '"'), '',trim($_POST['direccion']));
                                        $_POST['celular']=str_replace(array('\'', '"'), '',trim($_POST['celular']));
                                        $_POST['curp']=str_replace(array('\'', '"'), '',trim($_POST['curp']));
                                        $_POST['ine']=str_replace(array('\'', '"'), '',trim($_POST['ine']));
                                        $_POST['alias']=str_replace(array('\'', '"'), '',trim($_POST['alias']));

                                        $data = $_POST;
                                        unset($data['ine']);
                                        unset($data['api_key']);
                                        unset($data['id_puesto']);
                                        unset($data['lote']);
                                        unset($data['id_usuario']);
                                        $rPersona = $this->ApiRestModel->getPersonaByField('ine',trim($_POST['ine']),$rContrato->id_cliente);
                                        if ($rPersona){
                                            $actualizado = $this->ApiRestModel->actualizarPersonaByField('ine',$_POST['ine'], $data, $rContrato->id_cliente);
                                        }else if (!empty($_POST['curp'])){
                                            // actualizar por CURP

                                            // realizamos split a los campos a guardar
                                            $_POST['nombre']=str_replace(array('\'', '"'), '',trim($_POST['nombre']));
                                            $_POST['paterno']=str_replace(array('\'', '"'), '',trim($_POST['paterno']));
                                            $_POST['materno']=str_replace(array('\'', '"'), '',trim($_POST['materno']));
                                            $_POST['direccion']=str_replace(array('\'', '"'), '',trim($_POST['direccion']));
                                            $_POST['celular']=str_replace(array('\'', '"'), '',trim($_POST['celular']));
                                            $_POST['curp']=str_replace(array('\'', '"'), '',trim($_POST['curp']));
                                            $_POST['ine']=str_replace(array('\'', '"'), '',trim($_POST['ine']));
                                            $_POST['alias']=str_replace(array('\'', '"'), '',trim($_POST['alias']));
                                                                                    
                                            $data = $_POST;
                                        //   var_dump($data); die();
                                            unset($data['curp']);
                                            unset($data['api_key']);
                                            unset($data['id_puesto']);
                                            unset($data['lote']);
                                            unset($data['id_usuario']);
                                            $rPersona = $this->ApiRestModel->getPersonaByField('curp',trim($_POST['curp']),$rContrato->id_cliente);
                                            //var_dump($rPersona); die();
                                            if ($rPersona)
                                                $actualizado = $this->ApiRestModel->actualizarPersonaByField('curp',$_POST['curp'], $data, $rContrato->id_cliente);
                                            
                                        }
                                    }else if ($actualizado== false && !empty($_POST['curp'])){
                                        // actualizar por CURP
                                        // realizamos split a los campos a guardar
                                        $_POST['nombre']=str_replace(array('\'', '"'), '',trim($_POST['nombre']));
                                        $_POST['paterno']=str_replace(array('\'', '"'), '',trim($_POST['paterno']));
                                        $_POST['materno']=str_replace(array('\'', '"'), '',trim($_POST['materno']));
                                        $_POST['direccion']=str_replace(array('\'', '"'), '',trim($_POST['direccion']));
                                        $_POST['celular']=str_replace(array('\'', '"'), '',trim($_POST['celular']));
                                        $_POST['curp']=str_replace(array('\'', '"'), '',trim($_POST['curp']));
                                        $_POST['ine']=str_replace(array('\'', '"'), '',trim($_POST['ine']));
                                        $_POST['alias']=str_replace(array('\'', '"'), '',trim($_POST['alias']));
                                                                                
                                        $data = $_POST;
                                    //   var_dump($data); die();
                                        unset($data['curp']);
                                        unset($data['api_key']);
                                        unset($data['id_puesto']);
                                        unset($data['lote']);
                                        unset($data['id_usuario']);
                                        $rPersona = $this->ApiRestModel->getPersonaByField('curp',trim($_POST['curp']), $rContrato->id_cliente);
                                        //var_dump($rPersona); die();
                                        if ($rPersona)
                                            $actualizado = $this->ApiRestModel->actualizarPersonaByField('curp',$_POST['curp'], $data, $rContrato->id_cliente);
                                        
                                    }
                                    // echo 'ok';
                                    // die();
                                    // var_dump($actualizado); die();

                                    
                            }else{
                                // no hubo Actualizacion pero si existe CURP... 
                                // consultamos haber si existe CURP o INE... 
                                if(!empty($_POST['ine'])){
                                    $rPersona = $this->ApiRestModel->getPersonaByField('ine',$_POST['ine'], $rContrato->id_cliente);
                                    if ($rPersona  ){
                                        $actualizado = true;
                                    }
                                }else{
                                    $rPersona = $this->ApiRestModel->getPersonaByField('curp',$_POST['curp'], $rContrato->id_cliente);
                                    if ($rPersona  ){
                                        $actualizado = true;
                                    }
                                }
                                
                            }

                            // si no existe el registro, Insertamos nuevo registro
                            // si existen cuando menos nombre, paterno , curp , ine, email, celular
                            if ($actualizado==false){
                                        // si no se actualizo insertamos.. 
                                if (                       
                                    !empty($_POST['nombre']) && !empty($_POST['paterno']) && !empty($_POST['direccion'])  
                                    && !empty($_POST['celular']) && !empty($_POST['curp']) && !empty($_POST['email']) && !empty($_POST['ine'])
                                    && !empty($_POST['lote']) && strlen(trim($_POST['curp']))==18 && strlen(trim($_POST['celular']))==10
                                    && self::validarCelular(trim($_POST['celular'])) && self::validarEmail(trim($_POST['email'])) 
                                    && self::validarLote(trim($_POST['lote'])) && self::validarCurp(trim($_POST['curp']))
                                    && self::validarIne(trim($_POST['ine'])) ) {
                                        // Primero validamos que vengan bien formateados los datos
                                        // curp, ine, email, celular

                                        // realizamos split a los campos a guardar
                                        $_POST['nombre']=str_replace(array('\'', '"'), '',trim($_POST['nombre']));
                                        $_POST['paterno']=str_replace(array('\'', '"'), '',trim($_POST['paterno']));
                                        $_POST['materno']=str_replace(array('\'', '"'), '',trim($_POST['materno']));
                                        $_POST['direccion']=str_replace(array('\'', '"'), '',trim($_POST['direccion']));
                                        $_POST['celular']=str_replace(array('\'', '"'), '',trim($_POST['celular']));
                                        $_POST['curp']=str_replace(array('\'', '"'), '',trim($_POST['curp']));
                                        $_POST['ine']=str_replace(array('\'', '"'), '',trim($_POST['ine']));
                                        $_POST['alias']=str_replace(array('\'', '"'), '',trim($_POST['alias']));

                                        $data = $_POST;
                                        $data['email']=strtolower($data['email']);
                                        unset($data['api_key']);
                                        unset($data['id_puesto']);
                                        unset($data['lote']);
                                        $data['id_cliente']=$rContrato->id_cliente;
                                        $actualizado = $this->ApiRestModel->insertarPersona($data);
                                        // obtenemos... persona
                                        if(!empty($_POST['ine'])){
                                            $rPersona = $this->ApiRestModel->getPersonaByField('ine',$_POST['ine'],$rContrato->id_cliente);
                                            if ($rPersona ){
                                                $actualizado = true;
                                            }
                                        }
                                        // else{
                                        //     $rPersona = $this->ApiRestModel->getPersonaByField('curp',$_POST['ine']);
                                        //     if ($row && count($row)>0){
                                        //         $actualizado = true;
                                        //     }
                                        // }

                                        if ($actualizado){
                                            $data = $_POST;

                                            // Evento creacion de persona concluido
                                            $dataEventos['status_evento']='P';
                                            $dataEventos['id_evento']='3';
                                            
                                            unset($data['api_key']);
                                            // $dataEventos['api_key']=$rContrato->api_key;                    
                                            // $dataEventos['id_usuario']=$idUsuario; 
                                            $dataEventos['api_key']=$rContrato->id_contrato;  
                                            $dataEventos['id_puesto']=$data['id_puesto'];
                                            $dataEventos['lote']=$data['lote'];
                                            $dataEventos['id_persona']=$rPersona->id_persona;
                                            //$dataEventos['params']=$data;
                                            $dataEventos['params']=json_encode($data);
                                            $dataEventos['fecha_accion']=date("Y-m-d H:i:s");
                                            $dataEventos['ip_publica']=$this->getIP();
                                            $dataEventos['id_cliente']=$cliente;
                                            $dataEventos['id_usuario']=$usuario;

                                            $actualizado = $this->ApiRestModel->insertarApiEvento($dataEventos);
                                        }
                                    }else{
                                        if (empty($_POST['nombre']))
                                            $msg.=", el parametro 'nombre' es obligatorio";
                                        if (empty($_POST['paterno']))
                                            $msg.=", el parametro 'paterno' es obligatorio";                                        
                                        if (empty($_POST['direccion']) || strlen($_POST['direccion'])<20)
                                            $msg.=", el parametro 'direccion' es obligatorio";
                                        if (empty($_POST['celular']) || (!empty($_POST['celular']) && strlen(trim($_POST['celular']))!=10 || 
                                            (!empty($_POST['celular']) && self::validarCelular(trim($_POST['celular']))==false)))
                                            $msg.=", el parametro 'celular' es obligatorio a 10 posiciones con valor numerico";
                                        if (empty($_POST['curp']) || (!empty($_POST['curp']) && strlen(trim($_POST['curp']))!=18) || 
                                            (!empty($_POST['curp']) && self::validarCurp(trim($_POST['curp']))==false) )
                                            $msg.=", el parametro 'curp' es obligatorio a 18 posiciones";
                                        if (empty($_POST['email']) || (!empty($_POST['email']) &&  self::validarEmail(trim($_POST['email']))==false))
                                            $msg.=", el parametro 'email' es obligatorio";
                                        if (empty($_POST['ine']) || ( !empty($_POST['ine']) && self::validarIne(trim($_POST['ine']))==false ))
                                            $msg.=", el parametro 'ine' es obligatorio, 10 posiciones numericas minimo";
                                        if (empty($_POST['lote']) || (!empty($_POST['lote']) && self::validarLote(trim($_POST['lote']))==false ))
                                            $msg.=", el parametro 'lote' es obligatorio en formato (YYYYMMDD#00000) AñoMesDiaActual#PersonasLote";
                                    }
                            }

                            // 3. Si se actualizaron correctamente los datos en tabla cat_personas, se procede a revisar si hay una calificacion ya para esa
                            // persona
                            // realizamos los procesos necesarios... (insertar en tabla eventos, (status E si no hay calificacion, P si debe esperar un tiempo y recibir respuesta por email))
                            // var_dump($actualizado); die();

                            if ($actualizado){
                                $data = $_POST;

                                // primero consultamos calificacion para el perfil, si existe el status seria 'P'
                                $status_ev='E';
                                $rCalificacion=NULL;
                                ////////////////////////////////////////////////////////////////////////////////////////////
                                /// Revisamos en una tabla de resultados si existe la persona asociada a 1 perfil y si ya tiene calificacion
                                /// si es asi ponemos el status en 'P'
                                $ponderacion = '';
                                $reprocesar = $this->ApiRestModel->isReprocesarPersonaPuestoActiva($rContrato->id_cliente);
                                // if ($reprocesar==false){
                                //     $rCalificacion = $this->ApiRestModel->getCalificacion($rPersona->id_persona,$_POST['id_puesto']);
                                //     //var_dump($rCalificacion);
                                //     if ($rCalificacion ){
                                //         // Ya existe un resultado...
                                //         $status_ev='P';                     
                                //         //$ponderacion = ''.($rCalificacion->ponderacion+1);   
                                //         $ponderacion = ''.($rCalificacion->ponderacion);   
                                //     }
                                // }   
                                if ($reprocesar==true){
                                    $rCalificacion = $this->ApiRestModel->getCalificacion($rPersona->id_persona,$_POST['id_puesto']);
                                    //var_dump($rCalificacion);
                                    if ($rCalificacion ){
                                        // Ya existe un resultado...
                                        $status_ev='P';                     
                                        //$ponderacion = ''.($rCalificacion->ponderacion+1);   
                                        $ponderacion = ''.($rCalificacion->ponderacion);   
                                    }
                                }       
                                
                                if ($status_ev=='E' && $demonioActivado==false){
                                    $statuscode = "off-on\n";
                                    // Escribir los contenidos en el fichero,
                                    // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
                                    // se manda mensaje de inicio del demonio hilos.py, si esta prendido se hace caso omiso
                                    file_put_contents('../scripts_huella/pyhuella/statusCodeHuellamon.txt', $statuscode, LOCK_EX);
                                    $demonioActivado==true;
                                }

                                // var_dump($rCalificacion); die();
                                // debemos validar el puesto.. 
                                // hay que validar el puesto tambien..
                                if($this->validarPuesto($data['id_puesto'],$rContrato->id_cliente)){
                                    // verificamos que no se repita la persona y el puesto y el status_evento='E'
                                    //if (!$this->validarPersonaPuestoUnico($data['id_puesto'] , $rPersona->id_persona , $rContrato->api_key)){
                                    //if (strlen($ponderacion)==0 && $reprocesar==false && !$this->validarPersonaPuestoUnico($data['id_puesto'] , $rPersona->id_persona , $rContrato->api_key)){
                                    
                                    //} else if (strlen($ponderacion)>0 && $reprocesar==true && !$this->validarPersonaPuestoUnico($data['id_puesto'] , $rPersona->id_persona , $rContrato->api_key)){
                                    if (strlen($ponderacion)>0 && $reprocesar==true && !$this->isPersonaPuestoContratoUnico($data['id_puesto'] , $rPersona->id_persona , $rContrato->id_contrato)){
                                        // se marca para reprocesar... siempre y cuando no exista ya un registro status_evento='E'
                                        unset($data['api_key']);
                                        // $dataEventos['api_key']=$rContrato->api_key;
                                        // $dataEventos['id_usuario']=$idUsuario; 
                                        $dataEventos['api_key']=$rContrato->id_contrato;
                                        $dataEventos['id_evento']='2';
                                        $dataEventos['id_puesto']=$data['id_puesto'];
                                        $dataEventos['lote']=trim($data['lote']);
                                        $dataEventos['id_persona']=$rPersona->id_persona;
                                        //$dataEventos['params']=$data;
                                        //$dataEventos['params']=json_encode($data);
                                        $dataEventos['params']='';
                                        $dataEventos['fecha_accion']=date("Y-m-d H:i:s");
                                        $dataEventos['ip_publica']=$this->getIP();
                                        $dataEventos['status_evento']=$status_ev;
                                        $dataEventos['tipo_entrada']='A';
                                        $dataEventos['id_cliente']=$cliente;
                                        $dataEventos['id_usuario']=$usuario;

                                        $actualizado = $this->ApiRestModel->insertarApiEvento($dataEventos);

                                        $this->response(RestMessages::mensaje(107,[],$ponderacion), RestController::HTTP_OK);

                                    // } else if (strlen($ponderacion)>0 && $reprocesar==false ){
                                    //     // $dataEventos['api_key'] = $rContrato->api_key;
                                    //     // $dataEventos['id_usuario'] = $idUsuario;
                                    //     $dataEventos['api_key'] = $rContrato->id_contrato;
                                    //     $dataEventos['id_evento'] = '2';
                                    //     $dataEventos['id_puesto'] = $data['id_puesto'];
                                    //     $dataEventos['lote'] = trim($data['lote']);
                                    //     $dataEventos['id_persona'] = $rPersona->id_persona;
                                    //     //$dataEventos['params']=$data;
                                    //     $dataEventos['params'] = '';
                                    //     $dataEventos['fecha_accion'] = date("Y-m-d H:i:s");
                                    //     $dataEventos['ip_publica'] = $this->getIP();
                                    //     $dataEventos['status_evento'] = $status_ev; // PROCESADO..
                                    //     $dataEventos['tipo_entrada'] = 'A';
                                    //     $dataEventos['id_cliente']=$cliente;
                                    //     $dataEventos['id_usuario']=$usuario;

                                    //     $actualizado = $this->ApiRestModel->insertarApiEvento($dataEventos);
                                    //     $this->ApiRestModel->copiaResultados($rCalificacion->id_api, $actualizado['lastID']);

                                    // // } else if (strlen($ponderacion)>0 && $reprocesar==false){
                                    //     // se retorna el ultimo resultado localizado..
                                    //     $this->response(RestMessages::mensaje(107,[],$ponderacion), RestController::HTTP_OK);
                                    } else if (strlen($ponderacion)==0 && !$this->isPersonaPuestoContratoUnico($data['id_puesto'] , $rPersona->id_persona , $rContrato->id_contrato)){
                                        // si no existe ya el puesto-persona y reprocesar es falso

                                    // var_dump($rPersona); die();
                                    // validamos que no se repita la misma persona y puesto y api_key en el mismo LOTE
                                    // $rApiConsulta = $this->ApiRestModel->existApiConsultaRepetida($rPersona->id_persona,$_POST['id_puesto'],trim($_POST['api_key']),trim($_POST['lote']));
                                    // if ($status_ev=='E' && $rApiConsulta && count($rApiConsulta)>0){
                                    //     // no guardamos... 
                                    //     $this->response(RestMessages::mensaje(105), RestController::HTTP_OK);
                                    // }else {
                                        // Guardamos la peticion aunque se repita, y ya exista en su caso un resultado
                                        // esto es parano alterar la cantidad del LOTE PEDIDO para PROCESAR
                                        unset($data['api_key']);
                                        // $dataEventos['api_key']=$rContrato->api_key;
                                        // $dataEventos['id_usuario']=$idUsuario; 
                                        $dataEventos['api_key']=$rContrato->id_contrato;
                                        $dataEventos['id_evento']='2';
                                        $dataEventos['id_puesto']=$data['id_puesto'];
                                        $dataEventos['lote']=trim($data['lote']);
                                        $dataEventos['id_persona']=$rPersona->id_persona;
                                        //$dataEventos['params']=$data;
                                        //$dataEventos['params']=json_encode($data);
                                        $dataEventos['params']='';
                                        $dataEventos['fecha_accion']=date("Y-m-d H:i:s");
                                        $dataEventos['ip_publica']=$this->getIP();
                                        $dataEventos['status_evento']=$status_ev;
                                        $dataEventos['tipo_entrada']='A';
                                        $dataEventos['id_cliente']=$cliente;
                                        $dataEventos['id_usuario']=$usuario;

                                        $actualizado = $this->ApiRestModel->insertarApiEvento($dataEventos);

                                        // if (strlen($ponderacion)>0)
                                        //     $this->response(RestMessages::mensaje(107,[],$ponderacion), RestController::HTTP_OK);   
                                        // else
                                            $this->response(RestMessages::mensaje(103), RestController::HTTP_OK);
                                    } else {
                                        $this->response(RestMessages::mensaje(110, [], $msg), RestController::HTTP_NOT_ACCEPTABLE);    
                                    }
                                }else{
                                    $this->response(RestMessages::mensaje(109, [], $msg), RestController::HTTP_NOT_ACCEPTABLE);
                                }
                            }else{
                                $this->response(RestMessages::mensaje(102, [], $msg), RestController::HTTP_NOT_ACCEPTABLE);
                                // 
                            }
                        }
                    }else{
                        $this->response(RestMessages::mensaje(101,[],$msg), RestController::HTTP_NOT_ACCEPTABLE);
                    }
                // }else{
                //     // usuario no valido...
                //     $this->response(RestMessages::mensaje(108), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);
                // }
            }else{
                // api key no valida...
                $this->response(RestMessages::mensaje(104), RestController::HTTP_PROXY_AUTHENTICATION_REQUIRED);

                // podemos aplicar una denegacion del servicio cuando la IP publica COINCIDA y se hagan mas de 5 peticiones en 
                // menos de 30 segundos, y la api key sea INVALIDA
            }
        }
	}
      
    // /**
    //  * Get All Data from this method.
    //  * Por default, este va a ser el metodo elegido pero, para mostrarlo se usara por ahora metodo GET
    //  *
    //  * @return Response
    // */
    // public function index_post()
    // {
    //     $input = $this->input->post();
    //     $this->db->insert('items',$input);
     
    //     $this->response(['Item created successfully.'], RestController::HTTP_OK);
    // } 
     
    // /**
    //  * Get All Data from this method.
    //  *
    //  * @return Response
    // */
    // public function index_put($id)
    // {
    //     $input = $this->put();
    //     $this->db->update('items', $input, array('id'=>$id));
     
    //     $this->response(['Item updated successfully.'], RestController::HTTP_OK);
    // }
     
    // /**
    //  * Get All Data from this method.
    //  *
    //  * @return Response
    // */
    // public function index_delete($id)
    // {
    //     $this->db->delete('items', array('id'=>$id));
       
    //     $this->response(['Item deleted successfully.'], RestController::HTTP_OK);
    // }
    	
}