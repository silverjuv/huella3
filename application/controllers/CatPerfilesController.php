<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatPerfilesController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("CatPerfilesModel");
    }

    function index() {
        $params = array("menu_expandido"=>"46", "pantalla"=>"5");
        makeDefaultLayout(
            "catPerfilesView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                "assets/js/global.js",
                "assets/js/catPerfiles.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            )
        );
    }

    function listar() {
        $data = $this->CatPerfilesModel->listar();
        print json_encode($data);
    }

    function validar(&$data) {
        if(empty($data["descripcion"])) {
            return array("error"=>"1", "title"=>"Error", "msg"=>"El campo Apellido Paterno es obligatorio", "type"=>"error");
        }
        
        return array("error"=>"0");
    }

    function guardar() {
        $data = $this->input->post("data");
        $r = $this->validar($data);
        
        if($r["error"] === "0") {
            
            $id = intval($data["id_edicion"]);
            unset($data["id_edicion"]);
            
            if($data["accion"] == "editar") {
                unset($data["accion"]);
                $r = $this->CatPerfilesModel->actualizar($id, $data);
            } else if($data["accion"] == "insertar") {
                unset($data["accion"]);
                $r = $this->CatPerfilesModel->insertar($data);
            }
            
        }
        
        print json_encode($r);
    }

    function getPerfilById() {
        $id = intval($this->input->post("id_perfil"));
        if(!empty($id)) {
            $r = $this->CatPerfilesModel->getPerfilById($id);
        } else {
            $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
        }

        print json_encode($r);
    }

    function deletePerfilById() {
        $id = intval($this->input->post("id_perfil"));
        if(!empty($id)) {
            $r = $this->CatPerfilesModel->deletePerfilById($id);
        } else {
            $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
        }

        print json_encode($r);
    }


}