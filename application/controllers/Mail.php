<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;

	// require "../third_party/PHPMailer/PHPMailer/Exception.php";
	// require "../third_party/PHPMailer/PHPMailer/PHPMailer.php";
	// require "../third_party/PHPMailer/PHPMailer/SMTP.php";
	// require "/usr/local/var/www/ticdefense/huella/application/third_party/PHPMailer/PHPMailer/Exception.php";
	// require "/usr/local/var/www/ticdefense/huella/application/third_party/PHPMailer/PHPMailer/PHPMailer.php";
	// require "/usr/local/var/www/ticdefense/huella/application/third_party/PHPMailer/PHPMailer/SMTP.php";
	require "/var/www/threats/www/huella/application/third_party/PHPMailer/PHPMailer/Exception.php";
	require "/var/www/threats/www/huella/application/third_party/PHPMailer/PHPMailer/PHPMailer.php";
	require "/var/www/threats/www/huella/application/third_party/PHPMailer/PHPMailer/SMTP.php";


	$date = date("Y-m-d");
	$time = date("H:i:s");

	// if($time >= '00:00:00' && $time <= '05:59:59'){
	// 	$timebd = "AND MB.DATE_REGISB BETWEEN '" .$date. " 18:00:00' AND '" .$date. " 23:59:59'";
	// }
	// if($time >= '06:00:00' && $time <= '11:59:59'){ 
	// 	$timebd = "AND MB.DATE_REGISB BETWEEN '" .$date. " 00:00:00' AND '" .$date. " 05:59:59'";
	// }
	// if($time >= '12:00:00' && $time <= '17:59:59'){
	// 	$timebd = "AND MB.DATE_REGISB BETWEEN '" .$date. " 06:00:00' AND '" .$date. " 11:59:59'";
	// }
	// if($time >= '18:00:00' && $time <= '23:59:59'){
	// 	$timebd = "AND MB.DATE_REGISB BETWEEN '" .$date. " 12:00:00' AND '" .$date. " 17:59:59'"; 
	// }
	// echo ('qqq2x');


	function sendMails(){
		// echo 'q ... ';

		$servername = "localhost";
		$username = "userdb";
		$database = "darkDB";
		$password = getEncryptedPass(1);

		$conn = new mysqli($servername, $username, $password, $database);
		if ($conn->connect_error){ 
			
			  die("Error de Conexion\n"); 
		}else{
			#$query1 = "SELECT notify, id_empresa FROM M_USER"; 
			echo "Envio de correos iniciado...\n";
		} 

		$query1 = "select ma.idSent, ke.name, notify, email as emailUsuario, ultimosReportados, totalAlDia from keyword as ke inner join user as us on us.idUser = ke.idUser  
		inner join mailsSent as ma on ma.idKeyword = ke.idKeyword where ma.status='P'  order by dateCaptura asc ";
		$result = $conn->query($query1);
		$numfilas = $result->num_rows;

		// echo ("qqq ".$numfilas."\n");

		if(isset($result) && $result){ 
			// for ($x=0; $x < $numfilas; $x++) {
			//  	$fila = $result->fetch_object();
			while($result && $fila = $result->fetch_object()){
				// var_dump($fila);
				
				// if($fila->notify || $fila->emailUsuario){
					$emailKeyword = $fila->notify;
					$emailUser = $fila->emailUsuario;
					$ultimos = $fila->ultimosReportados;
					$totalAlDia = $fila->totalAlDia;
					// echo $email.' '.$empresa;

					// $query2 = "SELECT COUNT(MB.RESULT_FINDB) AS result
					// FROM M_BLACKLIST_RES MB
					// INNER JOIN M_DOMAINSBLACK MDB ON MDB.ID_BLACKDOMAIN = MB.ID_URLBLACK
					// INNER JOIN M_USER MU ON MU.ID_USER = MB.ID_USER_BLACK
					// INNER JOIN M_SENSORES MS ON MS.ID_SENSOR = MB.ID_SENSOR
					// WHERE MU.id_empresa = ".$empresa." ". $timebd;

					// $result2 = $conn->query($query2);
					// $fila2 = $result2->fetch_object();
					// $numResult = $fila2->result;
					
					// if($numResult != 0){
					if (strlen(trim($emailKeyword))>0 || strlen(trim($emailUser))>0) {
						try {
							// echo 'qpasa';
							echo "Intentando enviar correo..\n";
							
							$mail = new PHPMailer(); 
							// echo 'qpasa';

							$mail->isSMTP(); 
							// $mail->Host = 'smtp.dreamhost.com';
							$mail->Host = 'sub4.mail.dreamhost.com';							
							$mail->SMTPAuth = true;
							// $mail->Username = 'informes@ticdefense.com'; 
							$mail->Username = 'alertamiento@ticdefense.com'; 							
							$mail->Password = getEncryptedPass(2); 
							$mail->SMTPSecure = 'ssl';
							$mail->Port = 465;

							// $mail->setFrom('pignus.adm@gmail.com', 'Pignus-Brands');
							$mail->setFrom($mail->Username, 'Informes Tic Defense');
							$mails = '';

							$enviar=true;
							if (trim($emailKeyword)=="" || strpos($emailKeyword,"@mail.com")>0){
								$enviar=false;
							}else{								
								if (isEmailValid($emailKeyword)){
									$mail->addAddress($emailKeyword, 'Email Keyword');	
									$mails = ', '.$emailKeyword;
								}else
									$enviar=false;
							}

							if (trim($emailUser)=="" || strpos($emailUser,"@mail.com")>0){
								if ($enviar==false)
									$enviar=false;
							}else{
								if (isEmailValid($emailUser)){
									$mail->addAddress($emailUser, 'Email usuario administrador');	
									$mails .= ', '.$emailUser;
								}else if ($enviar==false)
									$enviar=false;									
							}

							if ($enviar==false){
								// Mientras no se corrijan los correos electronicos de esas keywords, siempre entrara aqui. El status no puede cambiar
								// deben corregir el correo obligatoriamente
								echo "La palabra clave \"".$fila->name."\" no cuenta con correos electronicos VALIDOS para su envio.\n";
								// return;
							}else{														

								$mail->Subject = 'Hallazgos de Palabra Clave '.$fila->name;
								$mail->isHTML(true); 
								$mail->CharSet = 'utf-8';

								$resultados = "<h1>Se han obtenido: " . $ultimos . " nuevos resultados de " . $totalAlDia . ".</h1><br>
								<a href='https://www.threats.center/pignus-darkweb'>Ver resultados en https://www.threats.center/pignus-darkweb</a>";
								// $mail->Body = $mailContent; 
								$message = '<html><body style="font-color:#000000">';
								$message .= '<img src="https://www.ticdefense.com/assets/imgs/ticdefense.png" alt="TicDefense" />';
								// $message .= '<h2>Se env&iacute;a an&aacute;lisis detallado del lote:' . $_POST['lote'] . '</h2>';
								$message  .= $resultados;
								$message .= "<br/><br/><br/><table><tr><td colspan='3'>Copyright &copy; 2020 | Tic Defense. Todos los derechos reservados.</td></tr>";
								$message .= "</table>";
								$message .= "</body></html>";
				
								$mail->Body = $message;						

								if($mail->send()){
									echo "El correo ha sido enviado\n";
									echo "Nuevos resultados " . $ultimos . " de total " . $totalAlDia . " de palabra ".$fila->name." a email(s): ".substr($mails,2)."\n";

									$query1 = "update mailsSent set dateSent= now(), email1Sent= '".$emailUser."', email2Sent='".$emailKeyword."', status='T' where idSent=".$fila->idSent;
									$result = $conn->query($query1);

								}else{
									echo "El correo no pudo ser enviado a email(s) ".substr($mails,2)."\n";
								}
								unset($mail);
							}
						} catch (Exception $e) {
							// echo $e->errorMessage(); //Pretty error messages from PHPMailer
							// // $r = array("title" => "Error", "msg" => "No fue posible enviar el Correo " . ($e->errorMessage()), "type" => "error");
							// var_dump('1'.$e);
							echo $e->errorMessage();
						} catch (\Exception $e) { //The leading slash means the Global PHP Exception class will be caught
							// echo $e->getMessage(); //Boring error messages from anything else!
							// // $r = array("title" => "Error", "msg" => "No fue posible enviar el Correo " . ($e->getMessage()), "type" => "error");
							// var_dump('2'.$e);
							echo $e->getMessage();
						}
					}else{
						echo "La palabra clave \"".$fila->name."\" no cuenta con correos electronicos para su envio.\n";
					}
				// }
			}
		}
	}

	function isEmailValid($email){
		// Remove all illegal characters from email
		$email = filter_var($email, FILTER_SANITIZE_EMAIL);

		// Validate e-mail
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			// echo("Email is a valid email address\n");
			return true;
		} else {
			// echo("Oppps! Email is not a valid email address\n");
			return false;
		}
	}

	function actualizaEmailsSent(){
		$servername = "localhost";
		$username = "userdb";
		$database = "darkDB";
		$password = getEncryptedPass(1);
				
		$conn = new mysqli($servername, $username, $password, $database);
		if ($conn->connect_error){ 
			
			  die("Error de Conexion\n"); 
		}else{
			#$query1 = "SELECT notify, id_empresa FROM M_USER"; 
			
		} 

		// primero escaneamos todas las keywords.. y si son diferentes del maximo reportado 
		//$query1 = "select idKeyword,  result from keyword where deleted = 0 and idKeyword=1 ";
		$query1 = "select idKeyword,  result from keyword where deleted = 0  ";
		$result = $conn->query($query1);
		$numfilasTotales = $result->num_rows;

		// echo ('qqq'.$numfilas); die();

		if(isset($result) && $result && $numfilasTotales>0){ 
			//for ($x=0; $x < $numfilasTotales; $x++) {
			while($result && $fila = $result->fetch_object()){

				// var_dump($result);
				// $fila = $result->fetch_object();
				// var_dump($fila);
				
				if($fila->result && $fila->idKeyword){
					//$query1 = "select idKeyword,  MAX(dateSent) from mailsSent where status='T' GROUP BY idKeyword  ; ";
					$query1 = "select idKeyword,   totalAlDia, dateSent from mailsSent where status='P' and idKeyword=".$fila->idKeyword." order BY dateSent desc  ";
					$result2x = $conn->query($query1);
					$numfilas = $result2x->num_rows;

					if(isset($result2x) && $result2x && $numfilas>0){ 
						//for ($x=0; $x < $numfilas; $x++) {
							// no hacemos nada.. debe finalizar su envio.. 							
						//}
					}else{
						// si no se encontro ahora buscamos como en pendiente de envio
						$query1 = "select idKeyword,   totalAlDia, dateSent from mailsSent where status='T' and idKeyword=".$fila->idKeyword." order BY dateSent desc  ";
						$result2x = $conn->query($query1);
						$numfilas = $result2x->num_rows;
	
						if(isset($result2x) && $result2x && $numfilas>0){
							// solo la primera fila obtenemos, que es la fecha mas reciente de envio..
							$fila2x = $result2x->fetch_object();
							if($fila2x->totalAlDia && $fila2x->totalAlDia!=$fila->result){
								$ultimos = $fila->result - $fila2x->totalAlDia;
								$nuevototalaldia=$fila->result;
								$query1 = "insert into mailsSent (idKeyword, status, ultimosReportados, totalAlDia, dateCaptura) values (".$fila->idKeyword.",'P',".$ultimos.",".$nuevototalaldia.", now())";
								$resultInsert = $conn->query($query1);
							}

						}else{
							// insertamos nuevo registro ... 
							$ultimos = $fila->result;
							$nuevototalaldia=$fila->result;
							$query1 = "insert into mailsSent (idKeyword, status, ultimosReportados, totalAlDia, dateCaptura) values (".$fila->idKeyword.",'T',".$ultimos.",".$nuevototalaldia.", now())";
							$resultInsert = $conn->query($query1);
						}

					}



				}
			}
		}		
		
	}

	function getEncryptedPass($type){
		if ($type==1)
			return "Cru54D45#.M3d1c45";
		else
			// return "1nf0rm3-2020";
			return "4l3rt4m13nt02021";
	}

	actualizaEmailsSent();

	sendMails()


?>
 
	
	