<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MiPerfilController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("MiPerfilModel");
        $this->load->model("HomeModel");
    }

    function index() {
        //$m = $this->MisionVisionModel->getMisionVision();
        $datos = $this->HomeModel->obtenerDatosCliente($_SESSION['id_cliente']);
        // // var_dump($datos); die();
        // $params = array("menu_expandido"=>"0", "pantalla"=>"2", "cliente" => $datos);        
        $params = array("menu_expandido"=>"0", "pantalla"=>"0", "cliente" => $datos);
        makeDefaultLayout(
            "miPerfilView", 
            $params,
            array(
                "assets/template/plugins/dropzone/min/dropzone.min.js",
                "assets/js/global.js",
                "assets/js/miPerfil.js"
            ),
            array(
                'assets/template/plugins/dropzone/min/dropzone.min.css'
            )
        );
    }

    function subirAvatar() {
        $r = $this->MiPerfilModel->subirAvatar();
        print json_encode($r);
    }

}