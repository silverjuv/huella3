<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MonitorOsintOsrframework  extends CI_Controller {

    function getUsufyPlatforms() {
        $data = AifRequest::getPost('search');
        $r = MonitorModelOsintOsrframework::getUsufyPlatforms($data);
        
        Aif::$render = "json";
        Monitor::out($r);
    }

    function getSearchfyPlatforms() {
        $data = AifRequest::getPost('search');
        $r = MonitorModelOsintOsrframework::getSearchfyPlatforms($data);
        
        Aif::$render = "json";
        Monitor::out($r);
    }

    function ejecutarUsufy() {
        $data = AifRequest::getPost('data');
        $plataformas = AifRequest::getPost('plataformas');
        $osr = new MonitorClassOsrframework();
        
        $r = $osr->ejecutarUsufy($data, $plataformas);
        
        Aif::$render = "json";
        Monitor::out($r);
    }

    function ejecutarSearchfy() {
        $data = AifRequest::getPost('data');
        $plataformas = AifRequest::getPost('plataformas');
        $osr = new MonitorClassOsrframework();
        
        $r = $osr->ejecutarSearchfy($data, $plataformas);
    
        Aif::$render = "json";
        Monitor::out($r);
    }
}