<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/OSCalls.php';
require APPPATH . 'libraries/RedesSociales.php';

class ResultadosOdometroController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("ResultadosOdometroModel");
    }

    function index()
    {
        $params = array("menu_expandido" => "17", "pantalla" => "30");
        makeDefaultLayout(
            "resultadosOdometroView",
            $params,
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                'assets/speedometer/js/speedometer.js',
                'assets/js/global.js',
                'assets/js/resultadosOdometro.js'
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css',
                'assets/speedometer/css/speedometer.css'
            )
        );
    }



    function verResultados()
    {
        $data = $this->ResultadosOdometroModel->verResultados();
        print json_encode($data);
    }

    function verRiesgosAsociados()
    {
        $data = $this->ResultadosOdometroModel->verRiesgosAsociados();
        print json_encode($data);
    }

    function listarAPIconsultas()
    {
        // $data = $this->ResultadosOdometroModel->listarAPIconsultas();
        $data = $this->ResultadosOdometroModel->listarAnalisis();
        print json_encode($data);
    }

    function listarPersonas()
    {
        $data = $this->ResultadosOdometroModel->listarPersonas();
        print json_encode($data);
    }

    function listarPuestos()
    {
        $data = $this->ResultadosOdometroModel->listarPuestos();
        print json_encode($data);
    }

    function enviarAnalisis()
    {
        $data = $this->ResultadosOdometroModel->enviarAnalisis();
        print json_encode($data);
    }


    function sendMail()
    {


        $rFinal = $this->ResultadosOdometroModel->getResultadosPyP($_POST['id_api']);
        $rCliente = $this->ResultadosOdometroModel->getEmailCliente($rFinal->id_cliente);
        //$rDetalles = $this->ResultadosOdometroModel->getResultadosDetalles(3, $_POST['id_api']);
        $rDetalles = $this->getResultadosIndividuales($rFinal->ponderacion);
        // var_dump($rDetalles);

        // Load PHPMailer library
        $this->load->library('phpmailer_lib');


        // PHPMailer object
        $mail = $this->phpmailer_lib->load();

        $this->load->model("ParametrosModel");

        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = $this->ParametrosModel->getParametroById('smtp_host')->valor;
        $mail->SMTPAuth = true;
        $mail->Username = $this->ParametrosModel->getParametroById('smtp_user')->valor;
        $mail->Password = $this->ParametrosModel->getParametroById('smtp_pass')->valor;
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = $this->ParametrosModel->getParametroById('smtp_port')->valor;

        $mail->setFrom($mail->Username, 'Informes Tic Defense');
        //$mail->addReplyTo('info@example.com', 'Programacion.net');

        // Add a recipient
        $mail->addAddress($rCliente->email_contacto);

        // Add cc or bcc 
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        // Email subject
        $mail->Subject = ('Se envía resultado detallado del análisis de la persona ' . strip_tags(($rFinal->nombre)) . ' para el puesto ' . strip_tags(($rFinal->puesto)));

        // Set email format to HTML
        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';

        $color = 'green';
        $tipoRiesgo = 'Bajo';
        if ($rFinal->ponderacion > 1 && $rFinal->ponderacion <= 4) {
            $color = 'yellow';
            $tipoRiesgo = 'Poco Riesgoso';
        }
        if ($rFinal->ponderacion > 4 && $rFinal->ponderacion <= 7) {
            //$color ='#FF9326';
            $color = 'orange';
            $tipoRiesgo = 'Riesgoso';
        }
        if ($rFinal->ponderacion > 7 && $rFinal->ponderacion <= 10) {
            $color = 'red';
            $tipoRiesgo = 'Muy Riesgoso';
        }

        $chartConfigArr = array(
            'type' => 'radialGauge',
            'data' => array(
                //   'labels' => array(2012, 2013, 2014, 2015, 2016),
                'datasets' => array(
                    array(
                        //   'label' => 'Users',
                        //   'data' => array(120, 60, 50, 180, 120),
                        'data' => array($rFinal->ponderacion),
                        'backgroundColor' => $color
                    )
                )
            ),
            'options' => array(
                'domain' => array(0, 10),
            )
        );
        $chartConfig = json_encode($chartConfigArr);
        $chartUrl = 'https://quickchart.io/chart?w=200&h=70&c=' . urlencode($chartConfig);
        // $encoded_config="{type:'radialGauge',data:{datasets:[{data:[".($rFinal->ponderacion*10)."],backgroundColor:'".$color."'}]}}";
        // $chart_url = 'https://quickchart.io/chart?c='.$encoded_config;
        //echo $chart_url; die();


        $message = '<html><body style="font-color:#000000">';
        $message .= '<img src="https://www.ticdefense.com/assets/imgs/ticdefense.png" alt="TicDefense" />';
        $message .= '<h2>Se env&iacute;a an&aacute;lisis de la siguiente persona:</h2></>';
        $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
        $message .= "<tr style='background: #eee;'><td><strong>Nombre:</strong> </td><td>" . strip_tags(htmlentities($rFinal->nombre)) . "<br />" . $rFinal->curp . "</td><td style=\"text-align:center\">Nivel de riesgo:  " . ($tipoRiesgo) . "<br /><b>Riesgo mayor localizado:</b> " . htmlentities($rFinal->riesgo) . "<br />Ponderación: " . $rFinal->ponderacion . "</td></tr>";
        $message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($rFinal->email) . "</td><td rowspan='4'  style=\"text-align:center\"><img src=\"$chartUrl\"></td></tr>";
        // $message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($rFinal->email) . "</td></tr>";
        $message .= "<tr><td><strong>Telefono:</strong> </td><td>" . strip_tags($rFinal->celular) . "</td></tr>";
        $message .= "<tr><td><strong>Puesto requerido:</strong> </td><td>" . strip_tags(htmlentities($rFinal->puesto)) . "</td></tr>";
        // $message .= "<tr><td><strong>Resultado General:</strong> </td><td>" . $rFinal->ponderacion . "</td></tr>";
        $message .= "<tr><td colspan='3'><strong>Detalles de Resultados:</strong></td></tr>";

        // $resultados = '<tr><td colspan="3"><table rules="all" style="border-color: #666;" cellpadding="10">';

        // foreach ($rDetalles as $row) {
        //     $colorx = '';
        //     if ($rFinal->ponderacion == $row['ponderacion']) {
        //         $colorx = ' style="background-color:#ff9999"';
        //     }
        //     $resultados .= "<tr " . $colorx . "><td><strong>Fuente:</strong> </td><td>" . strip_tags($row['url']) . "</td></tr>";
        //     $resultados .= "<tr><td><strong>Riesgo:</strong> </td><td>" . htmlentities($row['riesgo']) . "</td></tr>";
        //     $resultados .= "<tr><td><strong>Ponderaci&oacute;n</strong></td><td>" . $row['ponderacion'] . "</td></tr>";
        //     $resultados .= "<tr style='background: #eee;'><td colspan='2'>&nbsp;</td></tr>";
        // }
        // $resultados .= '</table></td></tr></table>';
        // $rDetalles

        $message .= '<tr><td colspan="3">'.$rDetalles['msg'].'</td></tr>';

        $message .= "<tr><td colspan='3'>Copyright &copy; 2020 | Tic Defense. Todos los derechos reservados.</td></tr>";
        $message .= "</table>";
        $message .= "</body></html>";

        $mail->Body = $message;


        // echo $message; die();
        // Send email
        $r = array("title" => "Excelente", "msg" => "Correo enviado al cliente", "type" => "success");

        if (!$mail->send()) {
            $r = array("title" => "Error", "msg" => "No fue posible enviar el Correo", "type" => "error");
        }
        print json_encode($r);
    }

    function armaTablaEmailAPI($idapi)
    {
        $rFinal = $this->ResultadosOdometroModel->getResultadosPyP($idapi);

        $color = 'green';
        $tipoRiesgo = 'Bajo';
        if ($rFinal->ponderacion > 1 && $rFinal->ponderacion <= 4) {
            $color = 'yellow';
            $tipoRiesgo = 'Poco Riesgoso';
        }
        if ($rFinal->ponderacion > 4 && $rFinal->ponderacion <= 7) {
            //$color ='#FF9326';
            $color = 'orange';
            $tipoRiesgo = 'Riesgoso';
        }
        if ($rFinal->ponderacion > 7 && $rFinal->ponderacion <= 10) {
            $color = 'red';
            $tipoRiesgo = 'Muy Riesgoso';
        }

        $chartConfigArr = array(
            'type' => 'radialGauge',
            'data' => array(
                //   'labels' => array(2012, 2013, 2014, 2015, 2016),
                'datasets' => array(
                    array(
                        //   'label' => 'Users',
                        //   'data' => array(120, 60, 50, 180, 120),
                        'data' => array($rFinal->ponderacion),
                        'backgroundColor' => $color
                    )
                )
            ),
            'options' => array(
                'domain' => array(0, 10),
            )
        );
        $chartConfig = json_encode($chartConfigArr);
        $chartUrl = 'https://quickchart.io/chart?w=200&h=70&c=' . urlencode($chartConfig);


        $message = '<table rules="all" style="border-color: #666;" cellpadding="10">';
        $message .= "<tr style='background: #eee;'><td style=\"text-align:center\"><strong>Persona</strong> </td><td style=\"text-align:center\"><strong>Puesto</strong></td><td style=\"text-align:center\"><strong>Resultado</strong></td><td style=\"text-align:center\"><strong></strong></td></tr>";
        $message .= "<tr><td>" . strip_tags(htmlentities($rFinal->nombre)) . "</td><td>" . strip_tags(htmlentities($rFinal->puesto)) . "</td><td style=\"text-align:center\"> Nivel de riesgo:  " . ($tipoRiesgo) . "<br />Riesgo mayor localizado:  " . htmlentities($rFinal->riesgo) . "<br />Resultado:  " . $rFinal->ponderacion . "</td><td  style=\"text-align:center\" ><img src=\"$chartUrl\"></td></tr>";

        $message .= "<tr><td colspan='4'><strong>Detalles del Resultado:</strong></td></tr>";
        

        // noticias
        $rDetalles = $this->ResultadosOdometroModel->getResultadosDetalles(3, $idapi);
        $resultados = '';
        if (count($rDetalles) > 0) {
            $message .= "<tr><td colspan='4'>&gt;&gt; En Sitios de Noticias:</td></tr>";
            $resultados = '<tr><td colspan="4"><table rules="all" style="border-color: #666; width:100%" cellpadding="10">';
            $resultados .= "<tr><td style=\"text-align:center\"><strong>Fuente</strong> </td><td style=\"text-align:center\"> <strong>Riesgo</strong> </td><td style=\"text-align:center\"><strong>Ponderación</strong> </td></tr>";

            foreach ($rDetalles as $row) {
                $colorx = '';
                if ($rFinal->ponderacion == $row['ponderacion']) {
                    $colorx = ' style="background-color:#ff9999"';
                }
                $resultados .= "<tr " . $colorx . "><td>" . strip_tags($row['url']) . "</td><td style=\"text-align:center\">" . htmlentities($row['riesgo']) . "</td><td style=\"text-align:center\">" . $row['ponderacion'] . "</td></tr>";
            }
            $resultados .= '</table></td></tr>';
        }
        $message .= $resultados;

        

        // darkweb
        $rDetalles = $this->ResultadosOdometroModel->getResultadosDetalles(11, $idapi);
        $resultados = '';
        if (count($rDetalles) > 0) {
            $message .= "<tr><td colspan='4'>&gt;&gt; En la DarkWeb:</td></tr>";
            $resultados = '<tr><td colspan="4"><table rules="all" style="border-color: #666; width:100%" cellpadding="10">';
            $resultados .= "<tr><td style=\"text-align:center\"><strong>Fuente</strong> </td><td style=\"text-align:center\"> <strong>Riesgo</strong> </td><td style=\"text-align:center\"><strong>Ponderación</strong> </td></tr>";

            foreach ($rDetalles as $row) {
                $colorx = '';
                if ($rFinal->ponderacion == $row['ponderacion']) {
                    $colorx = ' style="background-color:#ff9999"';
                }
                $resultados .= "<tr " . $colorx . "><td>" . strip_tags($row['url']) . "</td><td style=\"text-align:center\">" . htmlentities($row['riesgo']) . "</td><td style=\"text-align:center\">" . $row['ponderacion'] . "</td></tr>";
            }
            $resultados .= '</table></td></tr>';
        }
        $message .= $resultados;

        // buscadores
        
        $rDetalles = $this->ResultadosOdometroModel->getResultadosDetalles("'12','13','14'", $idapi);
        $resultados = '';
        if (count($rDetalles) > 0) {
            $message .= "<tr><td colspan='4'>&gt;&gt; En Buscadores:</td></tr>";
            $resultados = '<tr><td colspan="4"><table rules="all" style="border-color: #666; width:100%" cellpadding="10">';
            $resultados .= "<tr><td style=\"text-align:center\"><strong>Fuente</strong> </td><td style=\"text-align:center\"> <strong>Riesgo</strong> </td><td style=\"text-align:center\"><strong>Ponderación</strong> </td></tr>";

            foreach ($rDetalles as $row) {
                $colorx = '';
                if ($rFinal->ponderacion == $row['ponderacion']) {
                    $colorx = ' style="background-color:#ff9999"';
                }
                $resultados .= "<tr " . $colorx . "><td>" . strip_tags($row['url_asociado']) . "</td><td style=\"text-align:center\">" . htmlentities($row['riesgo']) . "</td><td style=\"text-align:center\">" . $row['ponderacion'] . "</td></tr>";
            }
            $resultados .= '</table></td></tr>';
        }
        $message .= $resultados;

        // en osint
        
        $rDetalles = $this->ResultadosOdometroModel->getResultadosDetalles("'6','7','8','9','10'", $idapi);
        $resultados = '';
        if (count($rDetalles) > 0) {
            $message .= "<tr><td colspan='4'>&gt;&gt; En OSINT:</td></tr>";
            $resultados = '<tr><td colspan="4"><table rules="all" style="border-color: #666; width:100%" cellpadding="10">';
            $resultados .= "<tr><td style=\"text-align:center\"><strong>Fuente</strong> </td><td style=\"text-align:center\"> <strong>Información recabada</strong> </td><td style=\"text-align:center\"><strong>Ponderación</strong> </td></tr>";

            foreach ($rDetalles as $row) {
                $colorx = '';
                if ($rFinal->ponderacion == $row['ponderacion']) {
                    $colorx = ' style="background-color:#ff9999"';
                }
                $resultados .= "<tr " . $colorx . "><td>" . strip_tags($row['anexo']) . "</td><td style=\"text-align:center\">" . htmlentities($row['json_data']) . "</td><td style=\"text-align:center\">" . $row['ponderacion'] . "</td></tr>";
            }
            $resultados .= '</table></td></tr>';
        }
        $message .= $resultados;

        // en redes sociales
        
        $rDetalles = $this->ResultadosOdometroModel->getResultadosDetalles("'2','5'", $idapi);
        $resultados = '';
        if (count($rDetalles) > 0) {
            $message .= "<tr><td colspan='4'>&gt;&gt; En Redes Sociales:</td></tr>";
            $resultados = '<tr><td colspan="4"><table rules="all" style="border-color: #666; width:100%" cellpadding="10">';
            $resultados .= "<tr><td style=\"text-align:center\"><strong>Fuente</strong> </td><td style=\"text-align:center\"> <strong>Riesgo</strong> </td><td style=\"text-align:center\"><strong>Ponderación</strong> </td></tr>";

            foreach ($rDetalles as $row) {
                $colorx = '';
                if ($rFinal->ponderacion == $row['ponderacion']) {
                    $colorx = ' style="background-color:#ff9999"';
                }
                $resultados .= "<tr " . $colorx . "><td>" . strip_tags($row['url_asociado']) . "</td><td style=\"text-align:center\">" . htmlentities($row['riesgo']) . "</td><td style=\"text-align:center\">" . $row['ponderacion'] . "</td></tr>";
            }
            $resultados .= '</table></td></tr>';
        }
        $message .= $resultados;

        return $message;'</table><hr />';
    }

    function getResultadosInforme(){
        print json_encode($this->getResultadosIndividuales());
    }
    
    function getResultadosIndividuales($ponderacion = 0){
        $message = "";

        $ponderacion = isset($_POST['ponderacion']) ? $_POST['ponderacion'] : $ponderacion;

        // noticias
        // $notics=false;
        $notics= $this->ResultadosOdometroModel->getResultadosDetalles('3', $_POST['id_api']);
        $resultados = '';
        if (count($notics) > 0) {
            $message .= "<strong>&gt;&gt; En sitios de Noticias (".count($notics)."): </strong><br />";
            $resultados = '<table rules="all" style="border-color: #666; width:100%" cellpadding="10">';
            $resultados .= "<tr><td style=\"text-align:center\"><strong>Fuente</strong> </td><td style=\"text-align:center\"> <strong>Riesgo</strong> </td><td style=\"text-align:center\"><strong>Ponderación</strong> </td></tr>";

            foreach ($notics as $row) {
                $colorx = '';
                if ($_POST['ponderacion'] == $row['ponderacion']) {
                    $colorx = ' style="background-color:#ff9999"';
                }
                $resultados .= "<tr " . $colorx . "><td>" . strip_tags($row['url']) . "</td><td style=\"text-align:center\">" . htmlentities($row['riesgo']) . "</td><td style=\"text-align:center\">" . $row['ponderacion'] . "</td></tr>";
            }
            $resultados .= '</table><hr />';
        }
        $message .= $resultados;
        
        $dark= $this->ResultadosOdometroModel->getResultadosDetalles('11', $_POST['id_api']);
        $resultados = '';
        if (count($dark) > 0) {
            $message .= "<strong>&gt;&gt; En la DarkWeb (".count($dark)."): </strong><br />";
            $resultados = '<table rules="all" style="border-color: #666; width:100%" cellpadding="10">';
            $resultados .= "<tr><td style=\"text-align:center\"><strong>Fuente</strong> </td><td style=\"text-align:center\"> <strong>Riesgo</strong> </td><td style=\"text-align:center\"><strong>Ponderación</strong> </td></tr>";

            foreach ($dark as $row) {
                $colorx = '';
                if ($_POST['ponderacion'] == $row['ponderacion']) {
                    $colorx = ' style="background-color:#ff9999"';
                }
                $resultados .= "<tr " . $colorx . "><td>" . strip_tags($row['url']) . "</td><td style=\"text-align:center\">" . htmlentities($row['riesgo']) . "</td><td style=\"text-align:center\">" . $row['ponderacion'] . "</td></tr>";
            }
            $resultados .= '</table><hr />';
        }
        $message .= $resultados;

        $buscadores= $this->ResultadosOdometroModel->getResultadosDetalles("'12','13','14'", $_POST['id_api']);
        $resultados = '';
        if (count($buscadores) > 0) {
            $message .= "<strong>&gt;&gt; En Buscadores (".count($buscadores)."):</strong> <br />";
            $resultados = '<table rules="all" style="border-color: #666; width:100%" cellpadding="10">';
            $resultados .= "<tr><td style=\"text-align:center\"><strong>Fuente</strong> </td><td style=\"text-align:center\"> <strong>Riesgo</strong> </td><td style=\"text-align:center\"><strong>Ponderación</strong> </td></tr>";

            foreach ($buscadores as $row) {
                $colorx = '';
                if ($_POST['ponderacion'] == $row['ponderacion']) {
                    $colorx = ' style="background-color:#ff9999"';
                }
                $resultados .= "<tr " . $colorx . "><td>" . strip_tags($row['url_asociado']) . "</td><td style=\"text-align:center\">" . htmlentities($row['riesgo']) . "</td><td style=\"text-align:center\">" . $row['ponderacion'] . "</td></tr>";
            }
            $resultados .= '</table><hr />';
        }
        $message .= $resultados;

        $osint= $this->ResultadosOdometroModel->getResultadosDetalles("'6','7','8','9','10'", $_POST['id_api']);
        $resultados = '';
        if (count($osint) > 0) {
            $message .= "<strong>&gt;&gt; En OSINT (".count($osint)."):</strong> <br />";
            $resultados = '<table rules="all" style="border-color: #666; width:100%" cellpadding="10">';
            $resultados .= "<tr><td style=\"text-align:center\"><strong>Fuente</strong> </td><td style=\"text-align:center\"> <strong>Información recabada</strong> </td><td style=\"text-align:center\"><strong>Ponderación</strong> </td></tr>";

            foreach ($osint as $row) {
                $colorx = '';
                if ($_POST['ponderacion'] == $row['ponderacion']) {
                    $colorx = ' style="background-color:#ff9999"';
                }
                $resultados .= "<tr " . $colorx . "><td>" . strip_tags($row['anexo']) . "</td><td style=\"text-align:center\">" . htmlentities($row['json_data']) . "</td><td style=\"text-align:center\">" . $row['ponderacion'] . "</td></tr>";
            }
            $resultados .= '</table><hr />';
        }
        $message .= $resultados;

        $redes= $this->ResultadosOdometroModel->getResultadosDetalles("'2','5'", $_POST['id_api']);
        $resultados = '';
        if (count($redes) > 0) {
            $message .= "<strong>&gt;&gt; En Redes Sociales(".count($redes)."):</strong> <br />";
            $resultados = '<table rules="all" style="border-color: #666; width:100%" cellpadding="10">';
            $resultados .= "<tr><td style=\"text-align:center\"><strong>Fuente</strong> </td><td style=\"text-align:center\"> <strong>Riesgo</strong> </td><td style=\"text-align:center\"><strong>Ponderación</strong> </td></tr>";

            foreach ($redes as $row) {
                $colorx = '';
                if ($_POST['ponderacion'] == $row['ponderacion']) {
                    $colorx = ' style="background-color:#ff9999"';
                }
                $resultados .= "<tr " . $colorx . "><td>" . strip_tags($row['url_asociado']) . "</td><td style=\"text-align:center\">" . htmlentities($row['riesgo']) . "</td><td style=\"text-align:center\">" . $row['ponderacion'] . "</td></tr>";
            }
            $resultados .= '</table><hr />';
        }
        $message .= $resultados;

        // 
        $this->load->model("CatPersonasModel");
        $person = $this->CatPersonasModel->getPersonaById($_POST['id_persona']);

        return array("error"=>"0","persona"=> $person, "osint"=>$osint, "redes"=>$redes, "noticias"=>count($notics),"dark"=>count($dark),"buscadores"=>count($buscadores),"msg"=>$message);
    }

    function sendMailLote()
    {

        /// obtenemos las personas del lote, ciclamos para armar el resultado
        //   PERSONA (Datos personales) |  PUESTO | resultado | Grafica
        //   Detalles:
        //   >> Noticias
        //   Fuente      |   Riesgo localizado    |  Ponderacion
        //   >> OSINT
        //   Fuente      |   Riesgo localizado    |  Ponderacion
        //   -----------------------------------------------------------------
        $idsapi = $this->ResultadosOdometroModel->getIDsApis($_POST['lote']);


        if (count($idsapi) > 0) {

            $resultados = '';
            foreach ($idsapi as $row) {
                $resultados .= $this->armaTablaEmailAPI($row['id']);
            }

            $rFinal = $this->ResultadosOdometroModel->getResultadosPyP($idsapi[0]['id']);
            $rCliente = $this->ResultadosOdometroModel->getEmailCliente($rFinal->id_cliente);
            // $rDetalles = $this->ResultadosOdometroModel->getResultadosDetalles(3);
            // var_dump($idsapi);
            // Load PHPMailer library
            $this->load->library('phpmailer_lib');


            // PHPMailer object
            try {
                $mail = $this->phpmailer_lib->load();

                $this->load->model("ParametrosModel");

                // SMTP configuration
                $mail->isSMTP();
                $mail->Host     = $this->ParametrosModel->getParametroById('smtp_host')->valor;
                $mail->SMTPAuth = true;
                $mail->Username = $this->ParametrosModel->getParametroById('smtp_user')->valor;
                $mail->Password = $this->ParametrosModel->getParametroById('smtp_pass')->valor;
                $mail->SMTPSecure = 'ssl';
                $mail->Port     = $this->ParametrosModel->getParametroById('smtp_port')->valor;

                $mail->setFrom($mail->Username, 'Informes Tic Defense');

                //$mail->addReplyTo('info@example.com', 'Programacion.net');

                // Add a recipient
                $mail->addAddress($rCliente->email_contacto);

                // Add cc or bcc 
                //$mail->addCC('cc@example.com');
                //$mail->addBCC('bcc@example.com');

                // Email subject
                $mail->Subject = 'Se envía resultado detallado del lote ' . $_POST['lote'];

                // Set email format to HTML
                $mail->isHTML(true);
                $mail->CharSet = 'utf-8';


                // $encoded_config="{type:'radialGauge',data:{datasets:[{data:[".($rFinal->ponderacion*10)."],backgroundColor:'".$color."'}]}}";
                // $chart_url = 'https://quickchart.io/chart?c='.$encoded_config;
                //echo $chart_url; die();


                $message = '<html><body style="font-color:#000000">';
                $message .= '<img src="https://www.ticdefense.com/assets/imgs/ticdefense.png" alt="TicDefense" />';
                $message .= '<h2>Se env&iacute;a an&aacute;lisis detallado del lote:' . $_POST['lote'] . '</h2>';
                $message  .= $resultados;
                $message .= "<table><tr><td colspan='3'>Copyright &copy; 2020 | Tic Defense. Todos los derechos reservados.</td></tr>";
                $message .= "</table>";
                $message .= "</body></html>";

                $mail->Body = $message;


                // echo $message; die();
                // Send email
                $r = array("title" => "Excelente", "msg" => "Correo enviado al cliente", "type" => "success");

                // try {
                $result = $mail->send();
                if (!$result) {
                    $r = array("title" => "Error", "msg" => "No fue posible enviar el Correo ", "type" => "error");
                }
            } catch (Exception $e) {
                // echo $e->errorMessage(); //Pretty error messages from PHPMailer
                $r = array("title" => "Error", "msg" => "No fue posible enviar el Correo " . ($e->errorMessage()), "type" => "error");
            } catch (\Exception $e) { //The leading slash means the Global PHP Exception class will be caught
                // echo $e->getMessage(); //Boring error messages from anything else!
                $r = array("title" => "Error", "msg" => "No fue posible enviar el Correo " . ($e->getMessage()), "type" => "error");
            }
        } else {
            $r = array("title" => "Error", "msg" => "No fue posible enviar el Correo, no hay elementos que enviar", "type" => "error");
        }
        print json_encode($r);
    }


    function exportData()
    {
        if (trim($_GET['format'])=='CSV'){
            $this->exportCSV();
            return;
        }

        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel.php';
        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel/IOFactory.php';
        // Import classes.
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\Spreadsheet';
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\IOFactory';

        $filename = 'resultados';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $data = $this->ResultadosOdometroModel->verResultados(true);

        $rowCount = 1;
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, 'Tipo Entrada');
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, 'Cliente');
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, 'Fecha Resultado');
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, 'Curp');
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, 'Nombre');
        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, 'Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, 'Celular');
        $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, 'INE');
        $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, 'Direccion');
        $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, 'Ponderacion');
        $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, 'Puesto');
        $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, 'Riesgo');

        //var_dump($data["data"]); die();

        //while($row = $data["data"]){ 
        foreach ($data["data"] as $row) {
            //var_dump($row); die();
            $rowCount++;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row['tipo_entrada']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['empresa']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['fecha_resultado']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['curp']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['nombre']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['email']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row['celular']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row['ine']);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $row['direccion']);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $row['ponderacion']);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $row['puesto']);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $row['riesgo']);
        }

        // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        // $objWriter->save('some_excel_file.xlsx');   
        header("Pragma: public");
        header("Expires: 0");
        // header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');

        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        // $objWriter->setSheetIndex(0);   // Select which sheet.
        // $objWriter->setDelimiter(';');  // Define delimiter
        // $objWriter->save('my-excel-file.csv');        
    }

    private function exportCSV()
    {
        // filename for download
        $filename = "resultadosanalisis_" . date('Ymd') . ".csv";

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv");

        $out = fopen("php://output", 'w');

        $flag = false;
        $data = $this->ResultadosOdometroModel->verResultados(true);
        //while (false !== ($row = pg_fetch_assoc($result))) {
        foreach ($data["data"] as $row) {
            $datax['tipo_entrada']=($row['tipo_entrada']);
            $datax['cliente']=($row['empresa']);
            $datax['fecha_resultado']=($row['fecha_resultado']);
            $datax['curp']=($row['curp']);
            $datax['nombre']=($row['nombre']);
            $datax['email']=($row['email']);
            $datax['celular']=($row['celular']);
            $datax['ine']=($row['ine']);
            $datax['direccion']=($row['direccion']);
            $datax['ponderacion']=($row['ponderacion']);
            $datax['puesto']=($row['puesto']);
            $datax['riesgo']=($row['riesgo']);
        
            if (!$flag) {
                // display field/column names as first row
                fputcsv($out, array_keys($datax), ',', '"');
                $flag = true;
            }
            array_walk($datax, __NAMESPACE__ . '\cleanData');
            fputcsv($out, array_values($datax), ',', '"');
        }

        fclose($out);
    }
}

function cleanData(&$str)
{
    if ($str == 't') $str = 'TRUE';
    if ($str == 'f') $str = 'FALSE';
    if (preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
        $str = "'$str";
    }
    if (strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}