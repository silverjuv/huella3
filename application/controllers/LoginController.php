<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	private $error;
	function __construct(){
		//header("Access-Control-Allow-Origin: *");
		parent::__construct();
		$this->load->model("LoginModel");
	}

	public function index($params = array()) {
		$this->load->view('loginView', $params);
	}

	function validarCaptcha($token) {
	
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$data = array(
			'secret' => '6Le1ROYUAAAAABbgx9ODcamrYXoJR38YaiYqxpwL',
			'response' => $token
		);
		$options = array(
			'http' => array (
				'method' => 'POST',
				'content' => http_build_query($data)
			)
		);
		
		$context  = stream_context_create($options);
		$verify = file_get_contents($url, false, $context);
		$captcha_success = json_decode($verify);
		
		return ($captcha_success->success) ? true : false;
	}

	function login() {
		$email = trim($this->input->post("email", TRUE));
		$pass = trim($this->input->post("pass", TRUE));
		$hashes = $this->LoginModel->getUserPass($email);

		$captcha_success = NULL;
		if(ENVIRONMENT == "production") { 
			$captcha_success = $this->validarCaptcha($_POST["token"]);
			if(!$captcha_success) {
				return $this->index(array("msg"=>"Captcha incorrecto, la respuesta del captcha ya no es valida, tiene un tiempo de vida de 2 minutos... por favor intente de nuevo"));
			}
		}

		$u = null;
		//se puede tener mas de un usuario con el mismo correo pero debe tener diferente password
		//de lo contrario es muy probable que se retorne el perfil incorrecto
		foreach($hashes as $hash) {
			if(password_verify($pass, $hash["pass"])) {
				$u = $this->LoginModel->getUserByEmailAndHash($email, $hash["pass"]);
				break;		
			}
		}
		if($u != null) {

			$data['id_usuario'] =  $u->id_usuario;
            $data['cve_usuar'] = substr(trim($this->input->post("email", TRUE)),0,60);
			$data['logueado'] = 1;
			$data['modo_acceso'] = 'W';
			$data['id_session'] = $this->session->session_id;
            $data['ip_publica'] = $this->getRealIP();
            $data['fecha_logueo'] = date('Y-m-d H:i:s');
			$this->LoginModel->insertarLogAcceso($data);
                
            // $logAccesos_model = new \App\Models\LogAccesos_model();
            // $logAccesos_model->set('fecha_logueo', 'getdate()', FALSE);
            // $logAccesos_model->insert($data);   

			$this->session->set_userdata('id_usuario', $u->id_usuario);
			$this->session->set_userdata('id_session', $this->session->session_id);
			$this->session->set_userdata('id_cliente', $u->id_cliente);
			$this->session->set_userdata('cliente', $u->cliente);
			$this->session->set_userdata('id_perfil', $u->id_perfil);
			$this->session->set_userdata('api_key', $u->api_key);
			$this->session->set_userdata('email', $u->correo);
			$this->session->set_userdata('perfil', $u->perfil);
			$this->session->set_userdata('nombre', ($u->paterno." ".$u->materno." ".$u->nombre));
			$this->session->set_userdata('avatar', $u->avatar);

			redirect('/HomeController/index/', 'refresh');
		} else {

			// $data['id_usuario'] =  NULL;
			$data['cve_usuar'] = substr(trim($this->input->post("email", TRUE)),0,60);
            $data['logueado'] = 0;
			$data['modo_acceso'] = 'W';
            $data['ip_publica'] = $this->getRealIP();
            $data['fecha_logueo'] = date('Y-m-d H:i:s');
			$u = $this->LoginModel->insertarLogAcceso($data);
			// var_dump($u); die();

			return $this->index(array("msg"=>"Usuario y/o contraseña invalidos"));
		}
	}

	function exit(){
		$this->session->sess_destroy();
		redirect('/LoginController/index/', 'refresh');
	}


    private function getRealIP(){

        if (isset($_SERVER["HTTP_CLIENT_IP"])){

            return $_SERVER["HTTP_CLIENT_IP"];

        }elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){

            return $_SERVER["HTTP_X_FORWARDED_FOR"];

        }elseif (isset($_SERVER["HTTP_X_FORWARDED"])){

            return $_SERVER["HTTP_X_FORWARDED"];

        }elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){

            return $_SERVER["HTTP_FORWARDED_FOR"];

        }elseif (isset($_SERVER["HTTP_FORWARDED"])){

            return $_SERVER["HTTP_FORWARDED"];

        }else{

            return $_SERVER["REMOTE_ADDR"];

        }
    }    	
}