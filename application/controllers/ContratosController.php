<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContratosController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("ContratosModel");
    }

    function index() {
        $params = array("menu_expandido"=>"3", "pantalla"=>"34");
        makeDefaultLayout(
            "contratosView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                'assets/template/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                'assets/template/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js',
                "assets/js/global.js",
                "assets/js/contratos.js"
            ),
               array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css'
            )
        );
    }

    function listar() {
        $r = $this->ContratosModel->listar();
        print json_encode($r);
    }

    function getClientesForCombo() {
        $this->load->model("ClientesModel");
        $r = $this->ClientesModel->getClientesForCombo();
        print json_encode($r);
    }

    function getDatos($insertar_nuevo) {
        $d = $this->input->post("data");
        $r["id_cliente"] = intval($d["cliente"]);
        //$r["tipo_servicio"] = intval($d["servicio"]);
        $r["tipo_servicio"] = ($d["servicio"]);
        $r["fuentes"] = $d["fuentes"];
        $r["status"] = $d["status"];

        if($r["tipo_servicio"] == 'T') {
            $r["fecha_inicio"] = $d["finicio"];
            $r["fecha_fin"] = $d["ffin"];
            $r["num_procesamientos"] = "0";
        } else if($r["tipo_servicio"] == 'P'){
            $r["num_procesamientos"] = intval($d["procesamientos"]);
            $r["fecha_inicio"] = null;
            $r["fecha_fin"] = null;
        }

        if(empty($r["id_cliente"])) {
            return array("title"=>"Error", "msg"=>"Especifique el cliente", "type"=>"error");
        }
        if(empty($r["tipo_servicio"])) {
            return array("title"=>"Error", "msg"=>"Especifique el tipo de servicio", "type"=>"error");
        }
        if($r["tipo_servicio"] == "1") {
            if(empty($r["fecha_inicio"])) {
                return array("title"=>"Error", "msg"=>"Especifique la fecha de inicio", "type"=>"error");
            }
            if(empty($r["fecha_fin"])) {
                return array("title"=>"Error", "msg"=>"Especifique la fecha fin", "type"=>"error");
            }
        } else if($r["tipo_servicio"] == "2") {
            if(empty($r["num_procesamientos"])) {
                return array("title"=>"Error", "msg"=>"Especifique el número de procesamientos", "type"=>"error");
            }
        }
        if(empty($r["fuentes"])) {
            return array("title"=>"Error", "msg"=>"Especifique al menos una fuente de datos", "type"=>"error");
        }
        if(empty($r["status"])) {
            return array("title"=>"Error", "msg"=>"Especifique el status del contrato", "type"=>"error");
        }

        // Esta validacion se eliminara, y automaticamente, si el status='A' se desactivaran los anteriores.. del cliente.
        // // $contratoActivo = $this->ContratosModel->tieneContratoActivoCliente($r["id_cliente"]);
        // // //if($contratoActivo && strtolower($r["status"]) == 'a') {
        // // if($contratoActivo && $r["status"] == 'A' && $insertar_nuevo==true) {
        // //     return array("title"=>"Error", "msg"=>"El cliente seleccionado ya tiene un contrato activo; no se permite tener mas de un contrato activo por cliente", "type"=>"error");
        // // }else if ($insertar_nuevo==false && $r["status"] == 'A'){
        // //     // checamos que se este actualizando el mismo contrato en Activo...
        // //     if ($this->ContratosModel->hayOtroActivo($this->input->post("id_contrato"), $r["id_cliente"])){
        // //         return array("title"=>"Error", "msg"=>"El cliente seleccionado ya tiene un contrato activo; no se permite tener mas de un contrato activo por cliente", "type"=>"error");
        // //     }
        // // }

        return $r;
    }

    function guardarContrato() {
        $r = $this->getDatos(true);
        if(isset($r["type"]) && $r["type"]=="error") {
            print json_encode($r);
            die();
        }
        $r["api_key"] = $this->crearApikey();
        
        // if ($r['status']=='A'){
        //     $this->ContratosModel->cancelaContratos($r['cliente'],$d['status']='C');
        // }

        $res = $this->ContratosModel->insertarContrato($r);
        print json_encode($res);
    }

    function editarContrato() {
        $r = $this->getDatos(false);
        if(isset($r["type"]) && $r["type"]=="error") {
            print json_encode($r);
            die();
        }

        $id_contrato = intval($this->input->post("id_contrato"));

        // if ($r['status']=='A'){
        //     $this->ContratosModel->cancelaContratos($r['cliente'],$d['status']='C');
        // }

        $res = $this->ContratosModel->actualizarContrato($id_contrato, $r);
        print json_encode($res);
    }

    function crearApikey() {
        $this->load->helper('string');
        $micro = str_replace(".", "", microtime()); 
        $micro = str_replace(" ", "", $micro)."_".random_string("alnum", 10);
        return $micro;
    }

    function generarApiKey(){
        $id_contrato = intval($this->input->post("id_contrato"));
        $apiKey = $this->crearApikey();
        $r = $this->ContratosModel->guardarApiKey($id_contrato, $apiKey);
        print json_encode($r);
    }

    function getContratoById() {
        $id_contrato = intval($this->input->post("id_contrato"));
        $r = $this->ContratosModel->getContratoById($id_contrato);
        print json_encode($r);
    }

    function deleteContratoById() {
        $id_contrato = intval($this->input->post("id_contrato"));
        $r = $this->ContratosModel->deleteContratoById($id_contrato);
        print json_encode($r);
    }

    function getFuentes() {
        $r = $this->ContratosModel->getFuentes();
        print json_encode($r);
    }

    function exportData()
    {
        if (trim($_GET['format'])=='CSV'){
            $this->exportCSV();
            return;
        }

        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel.php';
        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel/IOFactory.php';
        // Import classes.
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\Spreadsheet';
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\IOFactory';

        $filename = 'contratos';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $data = $this->ContratosModel->listar(true);

        $rowCount = 1;
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, 'Cliente');
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, 'Tipo de servicio');
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, 'Fecha inicio');
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, 'Fecha finaliza');
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, '# de procesamientos contratados');
        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, '# de consumos');        
        $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, 'Status');

        //var_dump($data["data"]); die();

        //while($row = $data["data"]){ 
        foreach ($data["data"] as $row) {
            //var_dump($row); die();
            $rowCount++;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row['cliente']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['servicio']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['fecha_inicio']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['fecha_fin']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['procesamientos']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['consumos']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row['status']);
        }

        // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        // $objWriter->save('some_excel_file.xlsx');   
        header("Pragma: public");
        header("Expires: 0");
        // header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');

        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        // $objWriter->setSheetIndex(0);   // Select which sheet.
        // $objWriter->setDelimiter(';');  // Define delimiter
        // $objWriter->save('my-excel-file.csv');        
    }

    private function exportCSV()
    {
        // filename for download
        $filename = "contratos_" . date('Ymd') . ".csv";

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv");

        $out = fopen("php://output", 'w');

        $flag = false;
        $data = $this->ContratosModel->listar(true);
        //while (false !== ($row = pg_fetch_assoc($result))) {
        foreach ($data["data"] as $row) {
            $datax['cliente']=($row['cliente']);
            $datax['tipo_del_servicio']=($row['servicio']);
            $datax['fecha_de_inicio']=($row['fecha_inicio']);
            $datax['fecha_de_finalizacion']=($row['fecha_fin']);
            $datax['procesamientos']=($row['procesamientos']);
            $datax['consumos']=($row['consumos']);
            $datax['habilitado']=($row['status']);
        
            if (!$flag) {
                // display field/column names as first row
                fputcsv($out, array_keys($datax), ',', '"');
                $flag = true;
            }
            array_walk($datax, __NAMESPACE__ . '\cleanData');
            fputcsv($out, array_values($datax), ',', '"');
        }

        fclose($out);
    }
}

function cleanData(&$str)
{
    if ($str == 't') $str = 'TRUE';
    if ($str == 'f') $str = 'FALSE';
    if (preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
        $str = "'$str";
    }
    if (strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}
    