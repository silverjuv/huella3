<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LayoutsUploaderController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("LayoutsUploaderModel");
    }

    function index() {
        $params = array("menu_expandido"=>"13", "pantalla"=>"33");
        makeDefaultLayout(
            "layoutsUploaderView", 
            $params,
            array(
                "assets/js/global.js",
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                'assets/template/plugins/dropzone/min/dropzone.min.js',
                "assets/js/layoutsUploader.js"
            ),
            array(
                'assets/template/plugins/select2/dist/css/select2.min.css',
                'assets/template/plugins/dropzone/min/dropzone.min.css',
            )
        );
    }

    function subirLayout() {
        $r = $this->LayoutsUploaderModel->subirLayout();
        print json_encode($r);
    }
}    