<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CambiarClienteController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("ConfigAdminModel");
    }

    function index() {

        // obtenemos configuracion de cliente.. 
        //$data = $this->ConfigClienteModel->getConfiguracionCliente($_SESSION['id_cliente']);

        $params = array("menu_expandido"=>"3", "pantalla"=>"43");
        makeDefaultLayout(
            "cambiaClienteView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                // 'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                // 'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.full.min.js',                
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/cambiarCliente.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css'                
            )
        );
    }


    function actualizarConfiguracion() {
        $data = $this->input->post("data");

        // borramos
        //$this->ConfigClienteModel->borrar($_SESSION['id_cliente']);
        

        // insertamos
        $r = null;
        
        foreach ($data as $clave => $valor) {
            if ($clave=='client'){
                $_SESSION['id_cliente']=$valor ;
                $r = $this->ConfigAdminModel->getRowById($valor);
                // var_dump($r ); die();
                $_SESSION['cliente']= $r->descripcion ;
                // $r=$this->ConfigAdminModel->actualizaDefaultCliente($valor, $_SESSION['id_usuario']);
            }
            // $row["id_cliente"]=$_SESSION['id_cliente'];
            // $row["id_config_cliente"]=intval(substr($clave,4));
            // $row["valor"]=$valor;
            // // var_dump($data); die();
            // $r=$this->ConfigClienteModel->insertar($row);
        }
        
        print json_encode(array("title" => "Info", "msg" => "Registro actualizado", "type" => "success"));
    }
}