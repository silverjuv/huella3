<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NoticiasController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("NoticiasModel");
    }

    function index() {
        $params = array("menu_expandido"=>"0", "pantalla"=>"11");
        makeDefaultLayout(
            "noticias/noticiasView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/noticias/noticias.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            )
        );
    }

    function listar() { 
        $this->load->library('pagination');
        $page = empty($this->input->get("per_page")) ? 1 : intval($this->input->get("per_page"));
        $search = urldecode($this->input->get("search"));
        $limit_per_page = 10;
        
        // Agregamos otros parametros posibles:
        $urlx = urldecode($this->input->get("url"));

        //$page = intval($page);
        $offset = (($page-1)*$limit_per_page);
        //$params["model"] = $this->NoticiasModel->listar($limit_per_page, $offset, $search);
        $params["model"] = $this->NoticiasModel->listar($limit_per_page, $offset, $search, $urlx);

        $total_records = $params["model"]["total"];

        $config['base_url'] = base_url() . 'NoticiasController/listar/';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 10;
        // $config['use_page_numbers'] = TRUE;
        // $config['page_query_string'] = TRUE;
        // $config['reuse_query_string'] = TRUE;
        // $config['first_link'] = 'Primer Pg ';
        // $config['first_tag_open'] = '<span class="firstlink btn btn-default m-2">';
        // $config['first_tag_close'] = '</span>';
        // $config['last_link'] = ' Ultima Pg';
        // $config['last_tag_open'] = '<span class="lastlink btn btn-default m-2">';
        // $config['last_tag_close'] = '</span>';
        // $config['next_link'] = ' > ';
        // $config['next_tag_open'] = '<span class="nextlink btn btn-default m-2">';
        // $config['next_tag_close'] = '</span>';
        // $config['prev_link'] = ' < ';
        // $config['prev_tag_open'] = '<span class="prevlink btn btn-default m-2">';
        // $config['prev_tag_close'] = '</span>';
        // $config['cur_tag_open'] = '<span class="curlink btn btn-default m-2">';
        // $config['cur_tag_close'] = '</span>';
        // $config['num_tag_open'] = '<span class="btn btn-default m-2">';
        // $config['num_tag_close'] = '</span>';
        $config['page_query_string'] = TRUE;
        $config['reuse_query_string'] = TRUE;        
        $config['use_page_numbers'] = TRUE;
        $config['cur_page'] = $page;
        $config['full_tag_open'] = '<ul class="pagination" >';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li >';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';        
        

        $this->pagination->initialize($config);
            
        // build paging links
        $params["paginador"] = $this->pagination->create_links();
        //var_dump($paramsPag); die();
        $params["menu_expandido"] = "0";
        $params["pantalla"] ="11";
        makeDefaultLayout(
            "noticias/resultadosNoticiasView",
            $params,
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/noticias/noticias.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            )
        );
    }

    function getSearchAutocomplete()
    {

        $rs = $this->NoticiasModel->getSearchAutocompleteELK($_GET['keyw']);
        // var_dump ($rs); die();
        $arr['results'] = [];

        foreach($rs as $row){
            // var_dump ($val);
            // var_dump($row); die();
            $arr['results'][]= $row;
        }
 
        header("Content-Type: application/json");
        echo json_encode($arr); die();
        
    }

    function listarElk() { 
        $this->load->library('pagination');
        $page = empty($this->input->get("per_page")) ? 1 : intval($this->input->get("per_page"));
        $search = urldecode($this->input->get("search"));
        $limit_per_page = 10;
        
        // Agregamos otros parametros posibles:
        $urlx = urldecode($this->input->get("url"));

        //$page = intval($page);
        $offset = (($page-1)*$limit_per_page);
        //$params["model"] = $this->NoticiasModel->listar($limit_per_page, $offset, $search);
        //$params["model"] = $this->NoticiasModel->listar($limit_per_page, $offset, $search, $urlx);
        $params["model"] = $this->NoticiasModel->listarElk($limit_per_page, $offset, $search, $urlx);

        $total_records = $params["model"]["total"];

        if ($total_records>0) {
            /// Actualizamos indice elasticsearch autocomplete...
            $this->NoticiasModel->insertAutocompleteIndex($search);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////
        // $client = $this->elasticclient;
        // $result = array();

        // $i = 0;

        // $params = [
        //     'index' => 'huelladbidx_paginas_x',
        //     'type'  => 'noticias',
        //     'body'  => [
        //         'query' => [
        //             'match' => ['body_dom' => $search],
        //         ],
        //     ],
        // ];
        // $query                 = $client->search($params);
        // $total_records = $hits                  = sizeof($query['hits']['hits']);
        // $hit                   = $query['hits']['hits'];
        // $result['searchfound'] = $hits;
        // while ($i < $hits) {

        //     $result['result'][$i] = $query['hits']['hits'][$i]['_source'];

        //     $i++;
        // }

        // //return  $result;
        //////////////////////////////////////////////////////////////////////////////////////////////////

        $config['base_url'] = base_url() . 'NoticiasController/listarElk/';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 10;
        // $config['use_page_numbers'] = TRUE;
        // $config['page_query_string'] = TRUE;
        // $config['reuse_query_string'] = TRUE;
        // $config['first_link'] = 'Primer Pg ';
        // $config['first_tag_open'] = '<span class="firstlink btn btn-default m-2">';
        // $config['first_tag_close'] = '</span>';
        // $config['last_link'] = ' Ultima Pg';
        // $config['last_tag_open'] = '<span class="lastlink btn btn-default m-2">';
        // $config['last_tag_close'] = '</span>';
        // $config['next_link'] = ' > ';
        // $config['next_tag_open'] = '<span class="nextlink btn btn-default m-2">';
        // $config['next_tag_close'] = '</span>';
        // $config['prev_link'] = ' < ';
        // $config['prev_tag_open'] = '<span class="prevlink btn btn-default m-2">';
        // $config['prev_tag_close'] = '</span>';
        // $config['cur_tag_open'] = '<span class="curlink btn btn-default m-2">';
        // $config['cur_tag_close'] = '</span>';
        // $config['num_tag_open'] = '<span class="btn btn-default m-2">';
        // $config['num_tag_close'] = '</span>';
        $config['page_query_string'] = TRUE;
        $config['reuse_query_string'] = TRUE;        
        $config['use_page_numbers'] = TRUE;
        $config['cur_page'] = $page;
        $config['full_tag_open'] = '<ul class="pagination" >';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li >';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';        
        

        $this->pagination->initialize($config);
            
        // build paging links
        $params["paginador"] = $this->pagination->create_links();
        //var_dump($paramsPag); die();
        $params["menu_expandido"] = "0";
        $params["pantalla"] ="11";
        $params["totalFound"] = $total_records;
        makeDefaultLayout(
            "noticias/resultadosNoticiasView",
            $params,
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/noticias/noticias.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            )
        );
    }
}