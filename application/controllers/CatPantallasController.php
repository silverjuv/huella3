<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatPantallasController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("CatPantallasModel");
    }

    function index() {
        $params = array("menu_expandido"=>"46", "pantalla"=>"7");
        makeDefaultLayout(
            "catPantallasView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                "assets/js/global.js",
                "assets/js/catPantallas.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css'
            )
        );
    }

    function listar() {
        $data = $this->CatPantallasModel->listar();
        print json_encode($data);
    }

    function getMenusPadresForCombo() {
        $data =$this->CatPantallasModel->getMenusPadresForCombo();

        print json_encode($data);
    }

    function validar(&$data) {
        if(empty($data["descripcion"])) {
            return array("error"=>"1", "title"=>"Error", "msg"=>"El campo Descripción es obligatorio", "type"=>"error");
        }
        
        return array("error"=>"0");
    }

    function guardar() {
        $data = $this->input->post("data");
        $r = $this->validar($data);
        
        if($r["error"] === "0") {
            
            $id = intval($data["id_edicion"]);
            unset($data["id_edicion"]);
            
            if($data["accion"] == "editar") {
                unset($data["accion"]);
                $r = $this->CatPantallasModel->actualizar($id, $data);
            } else if($data["accion"] == "insertar") {
                unset($data["accion"]);
                $r = $this->CatPantallasModel->insertar($data);
            }
            
        }
        
        print json_encode($r);
    }

    function getPantallaById() {
        $id = intval($this->input->post("id_pantalla"));
        if(!empty($id)) {
            $r = $this->CatPantallasModel->getPantallaById($id);
        } else {
            $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
        }

        print json_encode($r);
    }

    function deletePantallaById() {
        $id = intval($this->input->post("id_pantalla"));
        if(!empty($id)) {
            $r = $this->CatPantallasModel->deletePantallaById($id);
        } else {
            $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
        }

        print json_encode($r);
    }
    
}