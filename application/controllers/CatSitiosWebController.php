<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatSitiosWebController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("CatSitiosWebModel");
    }

    function index() {
        $params = array("menu_expandido"=>"46", "pantalla"=>"28");
        makeDefaultLayout(
            "catSitioswebView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                "assets/js/global.js",
                "assets/js/catSitiosweb.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css'
            )
        );
    }

    function listar() {
        $data = $this->CatSitiosWebModel->listar();
        print json_encode($data);
    }

    function getPerfilesForCombo() {
        $this->load->model("CatSitiosWebModel");
        $data =$this->CatPerfilesModel->getPerfilesForCombo();

        print json_encode($data);
    }

    function validar(&$data) {
        //$data["cve_pais"] = intval($data["id_perfil"]);
        $data["id_categoria"] = intval($data["id_categoria"]);

        if(empty($data["target"])) {
            return array("error"=>"1", "title"=>"Error", "msg"=>"El campo Sitio web es obligatorio", "type"=>"error");
        }
        // if(empty($data["entidad"])) {
        //     return array("error"=>"1", "title"=>"Error", "msg"=>"El campo Nombre es obligatorio", "type"=>"error");
        // }
        
        // if(empty($data["cve_pais"])) {
        //     return array("error"=>"1", "title"=>"Error", "msg"=>"El campo Pais es obligatorio", "type"=>"error");
        // }
        // if(empty($data["id_categoria"])) {
        //     return array("error"=>"1", "title"=>"Error", "msg"=>"Es necesario que seleccione la categoria asociada al sitio web", "type"=>"error");
        // }
        
        return array("error"=>"0");
    }

    function guardar() {
        $data = $this->input->post("data");
        $r = $this->validar($data);
        $data["id_usuario"]=$_SESSION['id_usuario'];
        
        if($r["error"] === "0") {
            
            $id = intval($data["id_edicion"]);
            unset($data["id_edicion"]);
            
            if($data["accion"] == "editar") {
                unset($data["accion"]);
                $r = $this->CatSitiosWebModel->actualizar($id, $data);
            } else if($data["accion"] == "insertar") {
                unset($data["accion"]);
                $r = $this->CatSitiosWebModel->insertar($data);
            }
            
        }
        
        print json_encode($r);
    }

    function getSitioWebById() {
        $id = intval($this->input->post("id_site"));
        if(!empty($id)) {
            $r = $this->CatSitiosWebModel->getSitioWebById($id);
        } else {
            $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
        }

        print json_encode($r);
    }

    // function deleteUsuarioById() {
    //     $id = intval($this->input->post("id_usuario"));
    //     if(!empty($id)) {
    //         $r = $this->CatUsuariosModel->deleteUsuarioById($id);
    //     } else {
    //         $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
    //     }

    //     print json_encode($r);
    // }

    function deshabilitarSitioWebById() {
        $id = intval($this->input->post("id_site"));
        if(!empty($id)) {
            $r = $this->CatSitiosWebModel->deshabilitarSitioWebById($id);
        } else {
            $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
        }

        print json_encode($r);
    }

    // function listarUsuariosForCombo() {
    //     $data = $this->CatUsuariosModel->listarUsuariosForCombo();
    //     print json_encode($data);
    // }

    function getPaisesForCombo() {
        //$this->load->model("SitiosWebModel");
        $data =$this->CatSitiosWebModel->getPaisesForCombo();

        print json_encode($data);
    }

    function getCategoriasForCombo() {
        //$this->load->model("SitiosWebModel");
        $data =$this->CatSitiosWebModel->getCategoriasForCombo();

        print json_encode($data);
    }    

    function exportData()
    {
        if (trim($_GET['format'])=='CSV'){
            $this->exportCSV();
            return;
        }

        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel.php';
        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel/IOFactory.php';
        // Import classes.
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\Spreadsheet';
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\IOFactory';

        $filename = 'sitioswebobjetivo';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $data = $this->CatSitiosWebModel->listar(true);

        $rowCount = 1;
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, 'SitioWeb');
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, 'Categoria');
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, 'Ponderación');
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, 'País');
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, 'Entidad');
        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, 'Habilitado');

        //var_dump($data["data"]); die();

        //while($row = $data["data"]){ 
        foreach ($data["data"] as $row) {
            //var_dump($row); die();
            $rowCount++;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row['target']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['categoria']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['peso']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['pais']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['entidad']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['activo']);
        }

        // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        // $objWriter->save('some_excel_file.xlsx');   
        header("Pragma: public");
        header("Expires: 0");
        // header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');

        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        // $objWriter->setSheetIndex(0);   // Select which sheet.
        // $objWriter->setDelimiter(';');  // Define delimiter
        // $objWriter->save('my-excel-file.csv');        
    }

    private function exportCSV()
    {
        // filename for download
        $filename = "sitioswebobjetivo_" . date('Ymd') . ".csv";

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv");

        $out = fopen("php://output", 'w');

        $flag = false;
        $data = $this->CatSitiosWebModel->listar(true);
        //while (false !== ($row = pg_fetch_assoc($result))) {
        foreach ($data["data"] as $row) {
            $datax['sitioweb']=($row['target']);
            $datax['categoria']=($row['categoria']);
            $datax['ponderacion']=($row['peso']);
            $datax['pais']=($row['pais']);
            $datax['entidad']=($row['entidad']);
            $datax['habilitado']=($row['activo']);
        
            if (!$flag) {
                // display field/column names as first row
                fputcsv($out, array_keys($datax), ',', '"');
                $flag = true;
            }
            array_walk($datax, __NAMESPACE__ . '\cleanData');
            fputcsv($out, array_values($datax), ',', '"');
        }

        fclose($out);
    }
}

function cleanData(&$str)
{
    if ($str == 't') $str = 'TRUE';
    if ($str == 'f') $str = 'FALSE';
    if (preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
        $str = "'$str";
    }
    if (strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}
