<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/OSCalls.php';
require APPPATH . 'libraries/RedesSociales.php';

class ExpedientesController extends CI_Controller {
    private $objOScalls;

	function __construct(){
        parent::__construct();
        $this->load->model("ExpedientesModel");
        $this->objOSCalls = new OSCalls();
    }
 
    function index() {
        $params = array("menu_expandido"=>"17", "pantalla"=>"18");
        makeDefaultLayout(
            "expedientesView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                'assets/js/global.js',
                'assets/js/expedientes.js'
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css',
            )
        );
    }

    // function buscarRedesSociales() {
    //     $id_perfil=intval($this->session->userdata("id_perfil"));
    //     $fuente = strval(trim($this->input->post("fuente")));
    //     if($id_perfil==2){
    //         $datos["noErrores"]=1;
    //         $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
    //         print json_encode(array(
    //             "name"=>intval($this->input->post("id_persona")), 
    //             "cmd"=>"x",
    //             "json"=>$datos
    //         ));
    //         die();
    //     }
    //     $where["cp.id_persona"]=intval($this->input->post("id_persona"));
    //     $where["cc.status"]=strval("A");
    //     $where["cc.borrado"]=intval(0);
    //     $where["cf.fuente"]=$fuente;
    //     $this->db->select("cc.id_contrato");
    //     $this->db->from("clientes_contratos cc");
    //     $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
    //     $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
    //     $this->db->where($where);
    //     $rC = $this->db->get();
    //     if($rC->num_rows()==0) {
    //         //$data=Array();
    //         $datos["noErrores"]=1;
    //         $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
    //         //$data["json"]=$datos;
    //         //array_push($data["json"],$datos);
    //         print json_encode(array(
    //             "name"=>intval($this->input->post("id_persona")), 
    //             "cmd"=>"x",
    //             "json"=>$datos
    //         ));
    //         die();
    //     }  
    //     $id = intval($this->input->post("id_persona"));
    //     $data=[];
    //     $objR = new RedesSociales(); 
    //     $resultYoutube=$objR->ejecutarScrapRS($id,'youtube');
    //     array_push($data,json_encode($resultYoutube));
    //     //$resultLinkedin=$objR->ejecutarScrapRS($id,'linkedin');
    //     //array_push($data,$resultLinkedin);
    //     $resultFacebook=$objR->ejecutarScrapRS($id,'facebook');
    //     array_push($data,json_encode($resultFacebook));
    //     print json_encode(array(
    //         "name"=>$id,
    //         "json"=>$data
    //     ));

    // }

    function buscarRedesSociales(){
        // if($id_perfil!=1){
        //     $datos["noErrores"]=1;
        //     $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
        //     print json_encode(array(
        //         "name"=>intval($data), 
        //         "cmd"=>"x",
        //         "json"=>$datos
        //     ));
        //     die();
        // }        

        $this->load->model("ProcesosModel");
        $data=$_POST['data'];
        if ($this->ProcesosModel->existeProcesoPersona($data,2,'0') && $this->ProcesosModel->existeProcesoPersona($data,5,'0')){
            $msg = array("error"=>"1","msg"=>"Ya se encuentra registrada la operación solicitada para esa persona", "status" => "0");
            print json_encode(
                // "result"=>$msg
                $msg
                // "json"=>$results
            ); 
            return;
        }

        $comandoEnv = "cd ".PATH_SCRIPTS_PYHUELLA." ; source env3/bin/activate ; python ";
        $comandoEnvFinal = " ; deactivate ";

        // $d['fecha_programacion'] = mdate("%Y-%m-%d %H:%i:%s");
        $now = date('Y-m-d H:i:s');
        $d["fecha_programacion"]=$now;
        // $d['window']='O';
        $d['id_session'] = $_SESSION['id_session'];
        $d['id_persona']=intval($data);   
        
        // se crean los n procesos OSINT 
        $folderFileName = $this->objOSCalls->getUniqueName().".json"; 
        // OUTPUT_RESULTS_OSINT_W."/".
        // $cmd = $comandoEnv." scrap_facebook.py ".($data)." ".$p." ".$folderFileName.$comandoEnvFinal;  
        $cmd = $comandoEnv." scrap_facebook.py ".($data)."  ".$comandoEnvFinal;  
        $d['id_anexo']=2;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = $folderFileName;            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);

        $folderFileName = $this->objOSCalls->getUniqueName().".json"; 
        $cmd = $comandoEnv." scrap_youtube.py ".($data)."  ".$comandoEnvFinal;  
        $d['id_anexo']=5;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = $folderFileName;            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);

        print json_encode(
            // "result"=>$msg
            $msg
            // "json"=>$results
        );        
        
    }    

    // // function buscarWeb() {
    // //     $id_perfil=intval($this->session->userdata("id_perfil"));
    // //     $fuente = strval(trim($this->input->post("fuente")));
    // //     if($id_perfil==2){
    // //         $datos["noErrores"]=1;
    // //         $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
    // //         print json_encode(array(
    // //             "name"=>$_POST['data'], 
    // //             "cmd"=>"x",
    // //             "json"=>$datos
    // //         ));
    // //         die();
    // //     }
    // //     $where["cp.id_persona"]=intval($_POST['data']);
    // //     $where["cc.status"]=strval("A");
    // //     $where["cc.borrado"]=intval(0);
    // //     $where["cf.fuente"]=$fuente;
    // //     $this->db->select("cc.id_contrato");
    // //     $this->db->from("clientes_contratos cc");
    // //     $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
    // //     $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
    // //     $this->db->where($where);
    // //     $rC = $this->db->get();
    // //     if($rC->num_rows()==0) {
    // //         //$data=Array();
    // //         $datos["noErrores"]=1;
    // //         $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
    // //         //$data["json"]=$datos;
    // //         //array_push($data["json"],$datos);
    // //         print json_encode(array(
    // //             "name"=>$_POST['data'], 
    // //             "cmd"=>"x",
    // //             "json"=>$datos
    // //         ));
    // //         die();
    // //     }    
    // //     $id_persona=$_POST['data'];
    // //     $data=[];
    // //     $fileName = $this->objOSCalls->getUniqueName().".json";
    // //     $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/scrap_anypagegoogle.py ".$id_persona." ".$fileName;
    // //     //$fileName = $this->objOSCalls->OUTPUT_RESULTS."/".$fileName;
    // //     $resultGoogle = $this->objOSCalls->ejecutarCmd($cmd, $fileName);
    // //     array_push($data,json_encode($resultGoogle));
    // //     //$fileName = $this->objOSCalls->getUniqueName().".json";
    // //     //$cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/scrap_anypageyahoo.py ".$fileName;
    // //     //$fileName = $this->objOSCalls->OUTPUT_RESULTS."/".$fileName;
    // //     //$resultYahoo = $this->objOSCalls->ejecutarCmd($cmd, $fileName);
    // //     //array_push($data,json_encode($resultYahoo));
    // //     $fileName = $this->objOSCalls->getUniqueName().".json";
    // //     $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/scrap_anypagebing.py ".$id_persona." ".$fileName;
    // //     //$fileName = $this->objOSCalls->OUTPUT_RESULTS."/".$fileName;
    // //     $resultBing = $this->objOSCalls->ejecutarCmd($cmd, $fileName);
    // //     array_push($data,json_encode($resultBing));
    // //     print json_encode(array(
    // //         "name"=>$id_persona, 
    // //         "cmd"=>$cmd,
    // //         "json"=>$data
    // //     ));
    // // } 
    function buscarWeb(){
        // if($id_perfil!=1){
        //     $datos["noErrores"]=1;
        //     $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
        //     print json_encode(array(
        //         "name"=>intval($data), 
        //         "cmd"=>"x",
        //         "json"=>$datos
        //     ));
        //     die();
        // }        

        $this->load->model("ProcesosModel");
        $data=$_POST['data'];
        if ($this->ProcesosModel->existeProcesoPersona($data,12,'0') && $this->ProcesosModel->existeProcesoPersona($data,13,'0')
            && $this->ProcesosModel->existeProcesoPersona($data,14,'0') && $this->ProcesosModel->existeProcesoPersona($data,16,'0')){
            $msg = array("error"=>"1","msg"=>"Ya se encuentra registrada la operación solicitada para esa persona", "status" => "0");
            print json_encode(
                // "result"=>$msg
                $msg
                // "json"=>$results
            ); 
            return;
        }

        $comandoEnv = "cd ".PATH_SCRIPTS_PYHUELLA." ; source env3/bin/activate ; python ";
        $comandoEnvFinal = " ; deactivate ";

        // $d['fecha_programacion'] = mdate("%Y-%m-%d %H:%i:%s");
        $now = date('Y-m-d H:i:s');
        $d["fecha_programacion"]=$now;
        // $d['window']='O';
        $d['id_session'] = $_SESSION['id_session'];
        $d['id_persona']=intval($data);   
        
        $folderFileName = $this->objOSCalls->getUniqueName().".json"; 
        // OUTPUT_RESULTS_OSINT_W."/".
        $cmd = $comandoEnv." scrap_anypagegoogle.py ".($data)."  ".$comandoEnvFinal;  
        $d['id_anexo']=12;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = $folderFileName;            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);

        // // $folderFileName = $this->objOSCalls->getUniqueName().".json"; 
        // // $cmd = $comandoEnv." scrap_anypagebing.py ".($data)." ".$p." ".$folderFileName.$comandoEnvFinal;  
        // // $d['id_anexo']=13;    // pantalla de OSINT tools
        // // $d['comando']=$cmd; 
        // // $d['outputDirFile'] = $folderFileName;            
        // // $msg = $this->ProcesosModel->insertarProcesaPersonas($d);

        $folderFileName =  $this->objOSCalls->getUniqueName().".json"; 
        $cmd = $comandoEnv." scrap_anypageyahoo.py ".($data)."  ".$comandoEnvFinal;  
        $d['id_anexo']=14;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = $folderFileName;            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);

        $folderFileName =  $this->objOSCalls->getUniqueName().".json"; 
        $cmd = $comandoEnv." scrap_anypageduckduckgo.py ".($data)."  ".$comandoEnvFinal;  
        $d['id_anexo']=16;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = $folderFileName;            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);

        print json_encode(
            // "result"=>$msg
            $msg
            // "json"=>$results
        );        
        
    }      

    function listarFuentes(){
        $data = $this->ExpedientesModel->listarFuentes();
        print json_encode($data);
    }

    function listarPersonas(){
        $data = $this->ExpedientesModel->listarPersonas();
        print json_encode($data);
    }

    function verExpediente(){
        $data = $this->ExpedientesModel->verExpediente();
        print json_encode($data);
    }

    function abrirExpediente(){
        $this->load->library('pagination');
        $page = empty($this->input->post("pagina")) ? 1 : intval($this->input->post("pagina"));
        $limit_per_page = 5;
        $page = intval($page);
        $offset = (($page-1)*$limit_per_page);
        $datos = $this->ExpedientesModel->abrirExpediente($limit_per_page, $offset);
        $total_records = $datos["total"];
        $config['base_url'] = base_url() . 'ExpedientesController/abrirExpediente/';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['cur_page'] = $page;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data["data"]=$datos;
        $data["paginador"]= $this->pagination->create_links();
        print json_encode($data);
    }

    function abrirExpedienteExtra(){

        $datos = $this->ExpedientesModel->abrirExpedienteExtra($this->input->post("id_persona"), $this->input->post("fuente"));
       
        print json_encode($datos);
    }

    // // function buscarNoticias() {
    // //     $id_perfil=intval($this->session->userdata("id_perfil"));
    // //     if($id_perfil!=1){
    // //         $data["error"] = "0";
    // //         $data["msg"] = "No tiene acceso a esta acción";
    // //         print json_encode($data);
    // //         die();
    // //     }
    // //     $data = $this->ExpedientesModel->buscarNoticias();
    // //     print json_encode($data);
    // // }

    // // function buscarDarkWeb() {
    // //     $id_perfil=intval($this->session->userdata("id_perfil"));
    // //     if($id_perfil==2){
    // //         $data["error"] = "0";
    // //         $data["msg"] = "No tiene acceso a esta acción";
    // //         print json_encode($data);
    // //         die();
    // //     }
    // //     $data = $this->ExpedientesModel->buscarDarkWeb();
    // //     print json_encode($data);
    // // }

    function buscarNoticias(){
        // if($id_perfil!=1){
        //     $datos["noErrores"]=1;
        //     $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
        //     print json_encode(array(
        //         "name"=>intval($data), 
        //         "cmd"=>"x",
        //         "json"=>$datos
        //     ));
        //     die();
        // }        

        // primero consultamos que no exista ya el proceso para evitar duplicidad en momentos continuos
        // si puede ejecutarse nuevamente pero no 2 registros al mismo tiempo con status = 0
        $this->load->model("ProcesosModel");
        $data=$_POST['data'];
        if ($this->ProcesosModel->existeProcesoPersona($data,3,'0')){
            $msg = array("error"=>"1","msg"=>"Ya se encuentra registrada la operación solicitada para esa persona", "status" => "0");
            print json_encode(
                // "result"=>$msg
                $msg
                // "json"=>$results
            ); 
            return;
        }

        $comandoEnv = "cd ".PATH_SCRIPTS_PYHUELLA." ; source env3/bin/activate ; python ";
        $comandoEnvFinal = " ; deactivate ";

        // $d['fecha_programacion'] = mdate("%Y-%m-%d %H:%i:%s");
        $now = date('Y-m-d H:i:s');
        $d["fecha_programacion"]=$now;
        // $d['window']='O';
        $d['id_session'] = $_SESSION['id_session'];
        $d['id_persona']=intval($data);   
        

        
        // se crean los n procesos OSINT 
        // $folderFileName = OUTPUT_RESULTS_OSINT_W."/".$this->objOSCalls->getUniqueName().".json"; 
        $cmd = $comandoEnv." ejecutarNoticiasELK.py ".($data)." ".$comandoEnvFinal;  
        $d['id_anexo']=3;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = '';            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);


        print json_encode(
            // "result"=>$msg
            $msg
            // "json"=>$results
        );        
        
    }       

    function buscarDarkWeb(){
        // if($id_perfil!=1){
        //     $datos["noErrores"]=1;
        //     $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
        //     print json_encode(array(
        //         "name"=>intval($data), 
        //         "cmd"=>"x",
        //         "json"=>$datos
        //     ));
        //     die();
        // }        

        $this->load->model("ProcesosModel");
        $data=$_POST['data'];
        if ($this->ProcesosModel->existeProcesoPersona($data,11,'0')  ){
            $msg = array("error"=>"1","msg"=>"Ya se encuentra registrada la operación solicitada para esa persona", "status" => "0");
            print json_encode(
                // "result"=>$msg
                $msg
                // "json"=>$results
            ); 
            return;
        }

        $comandoEnv = "cd ".PATH_SCRIPTS_PYHUELLA." ; source env3/bin/activate ; python ";
        $comandoEnvFinal = " ; deactivate ";

        // $d['fecha_programacion'] = mdate("%Y-%m-%d %H:%i:%s");
        $now = date('Y-m-d H:i:s');
        $d["fecha_programacion"]=$now;
        // $d['window']='O';
        $d['id_session'] = $_SESSION['id_session'];
        $d['id_persona']=intval($data);   
        
        // $folderFileName = OUTPUT_RESULTS_OSINT_W."/".$this->objOSCalls->getUniqueName().".json"; 
        $cmd = $comandoEnv." ejecutarDarkwebELK.py ".($data)." ".$comandoEnvFinal;  
        $d['id_anexo']=11;    // pantalla de OSINT tools
        $d['comando']=$cmd; 
        $d['outputDirFile'] = '';            
        $msg = $this->ProcesosModel->insertarProcesaPersonas($d);


        print json_encode(
            // "result"=>$msg
            $msg
            // "json"=>$results
        );        
        
    }           
    
}