<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DarkWebController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("DarkWebModel");
    }

    function index() {
        $params = array("menu_expandido"=>"0", "pantalla"=>"32");
        makeDefaultLayout(
            "noticias/darkwebView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/noticias/darkweb.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            )
        );
    }

    function listar() { 
        $this->load->library('pagination');
        $page = empty($this->input->get("per_page")) ? 1 : intval($this->input->get("per_page"));
        $search = urldecode($this->input->get("search"));
        $limit_per_page = 10;
        
        // Agregamos otros parametros posibles:
        $urlx = urldecode($this->input->get("url"));

        //$page = intval($page);
        $offset = (($page-1)*$limit_per_page);
        //$params["model"] = $this->NoticiasModel->listar($limit_per_page, $offset, $search);
        $params["model"] = $this->DarkWebModel->listar($limit_per_page, $offset, $search, $urlx);

        $total_records = $params["model"]["total"];

        $config['base_url'] = base_url() . 'DarkWebController/listar/';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['first_link'] = 'Primer Pg ';
        $config['first_tag_open'] = '<span class="firstlink btn btn-default m-2">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = ' Ultima Pg';
        $config['last_tag_open'] = '<span class="lastlink btn btn-default m-2">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = ' > ';
        $config['next_tag_open'] = '<span class="nextlink btn btn-default m-2">';
        $config['next_tag_close'] = '</span>';
        $config['prev_link'] = ' < ';
        $config['prev_tag_open'] = '<span class="prevlink btn btn-default m-2">';
        $config['prev_tag_close'] = '</span>';
        $config['cur_tag_open'] = '<span class="curlink btn btn-default m-2">';
        $config['cur_tag_close'] = '</span>';
        $config['num_tag_open'] = '<span class="btn btn-default m-2">';
        $config['num_tag_close'] = '</span>';
        

        $this->pagination->initialize($config);
            
        // build paging links
        $params["paginador"] = $this->pagination->create_links();
        //var_dump($paramsPag); die();
        $params["menu_expandido"] = "0";
        $params["pantalla"] ="32";
        makeDefaultLayout(
            "noticias/resultadosDarkwebView",
            $params,
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/noticias/darkweb.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            )
        );
    }


    function getSearchAutocomplete()
    {

        $rs = $this->DarkWebModel->getSearchAutocompleteELK($_GET['keyw']);
        // var_dump ($rs); die();
        $arr['results'] = [];

        foreach($rs as $row){
            // var_dump ($val);
            // var_dump($row); die();
            $arr['results'][]= $row;
        }
 
        header("Content-Type: application/json");
        echo json_encode($arr); die();
        
    }

    function listarElk() { 
        $this->load->library('pagination');
        $page = empty($this->input->get("per_page")) ? 1 : intval($this->input->get("per_page"));
        $search = urldecode($this->input->get("search"));
        $limit_per_page = 10;
        
        // Agregamos otros parametros posibles:
        $urlx = urldecode($this->input->get("url"));

        //$page = intval($page);
        $offset = (($page-1)*$limit_per_page);
        //$params["model"] = $this->NoticiasModel->listar($limit_per_page, $offset, $search);
        //$params["model"] = $this->DarkWebModel->listar($limit_per_page, $offset, $search, $urlx);
        $params["model"] = $this->DarkWebModel->listarElk($limit_per_page, $offset, $search, $urlx);

        $total_records = $params["model"]["total"];
        if ($total_records>0) {
            /// Actualizamos indice elasticsearch autocomplete...
            $this->DarkWebModel->insertAutocompleteIndex($search);
        }

        $config['base_url'] = base_url() . 'DarkWebController/listarElk/';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['first_link'] = 'Primer Pg ';
        $config['first_tag_open'] = '<span class="firstlink btn btn-default m-2">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = ' Ultima Pg';
        $config['last_tag_open'] = '<span class="lastlink btn btn-default m-2">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = ' > ';
        $config['next_tag_open'] = '<span class="nextlink btn btn-default m-2">';
        $config['next_tag_close'] = '</span>';
        $config['prev_link'] = ' < ';
        $config['prev_tag_open'] = '<span class="prevlink btn btn-default m-2">';
        $config['prev_tag_close'] = '</span>';
        $config['cur_tag_open'] = '<span class="curlink btn btn-default m-2">';
        $config['cur_tag_close'] = '</span>';
        $config['num_tag_open'] = '<span class="btn btn-default m-2">';
        $config['num_tag_close'] = '</span>';
        

        $this->pagination->initialize($config);
            
        // build paging links
        $params["paginador"] = $this->pagination->create_links();
        //var_dump($paramsPag); die();
        $params["menu_expandido"] = "0";
        $params["pantalla"] ="32";
        makeDefaultLayout(
            "noticias/resultadosDarkwebView",
            $params,
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/noticias/darkweb.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            )
        );
    }

}