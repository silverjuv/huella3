<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatUsuariosController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("CatUsuariosModel");
    }

    function index() {
        $params = array("menu_expandido"=>"46", "pantalla"=>"4");
        makeDefaultLayout(
            "catUsuariosView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                "assets/js/global.js",
                "assets/js/catUsuarios.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css',
                "assets/css/catUsuarios.css"
            )
        );
    }

    function listar() {
        $data = $this->CatUsuariosModel->listar();
        print json_encode($data);
    }

    function getPerfilesForCombo() {
        $this->load->model("CatPerfilesModel");
        $data =$this->CatPerfilesModel->getPerfilesForCombo();

        print json_encode($data);
    }

    function validar(&$data) {
        $data["id_perfil"] = intval($data["id_perfil"]);
        $data["id_cliente"] = intval($data["id_cliente"]);

        if(empty($data["paterno"])) {
            return array("error"=>"1", "title"=>"Error", "msg"=>"El campo Apellido Paterno es obligatorio", "type"=>"error");
        }
        if(empty($data["nombre"])) {
            return array("error"=>"1", "title"=>"Error", "msg"=>"El campo Nombre es obligatorio", "type"=>"error");
        }
        
        if(empty($data["id_perfil"])) {
            return array("error"=>"1", "title"=>"Error", "msg"=>"El campo Perfil es obligatorio", "type"=>"error");
        }
        if(empty($data["id_cliente"])) {
            return array("error"=>"1", "title"=>"Error", "msg"=>"Es necesario que seleccione el cliente asociado al usuario", "type"=>"error");
        }
        
        if($data["accion"] === "insertar") {
            if(empty($data["pass"])) {
                return array("error"=>"1", "title"=>"Error", "msg"=>"El campo Contraseña es obligatorio", "type"=>"error");
            }
            if($data["pass"] !== $data["pass2"]) {
                return array("error"=>"1", "title"=>"Error", "msg"=>"Las contraseñas no coinciden", "type"=>"error");
            }
            $data["pass"] = password_hash($data["pass"], PASSWORD_DEFAULT);;
            unset($data["pass2"]);
        } else {
            if($data["pass"] !== $data["pass2"]) {
                return array("error"=>"1", "title"=>"Error", "msg"=>"Las contraseñas no coinciden", "type"=>"error");
            }
            if($data["pass"] !== "") {
                $data["pass"] = password_hash($data["pass"],PASSWORD_DEFAULT);
                unset($data["pass2"]);
            } else {
                unset($data["pass"], $data["pass2"]);
            }
        }
        
        return array("error"=>"0");
    }

    function guardar() {
        $data = $this->input->post("data");
        $r = $this->validar($data);
        
        if($r["error"] === "0") {
            
            $id = intval($data["id_edicion"]);
            unset($data["id_edicion"]);
            
            if($data["accion"] == "editar") {
                unset($data["accion"]);
                $r = $this->CatUsuariosModel->actualizar($id, $data);
            } else if($data["accion"] == "insertar") {
                unset($data["accion"]);
                $r = $this->CatUsuariosModel->insertar($data);
            }
            
        }
        
        print json_encode($r);
    }

    function getUsuarioById() {
        $id = intval($this->input->post("id_usuario"));
        if(!empty($id)) {
            $r = $this->CatUsuariosModel->getUsuarioById($id);
        } else {
            $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
        }

        print json_encode($r);
    }

    function deleteUsuarioById() {
        $id = intval($this->input->post("id_usuario"));
        if(!empty($id)) {
            $r = $this->CatUsuariosModel->deleteUsuarioById($id);
        } else {
            $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
        }

        print json_encode($r);
    }

    function deshabilitarUsuarioById() {
        $id = intval($this->input->post("id_usuario"));
        if(!empty($id)) {
            $r = $this->CatUsuariosModel->deshabilitarUsuarioById($id);
        } else {
            $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
        }

        print json_encode($r);
    }

    function listarUsuariosForCombo() {
        $data = $this->CatUsuariosModel->listarUsuariosForCombo();
        print json_encode($data);
    }

    function getClientesForCombo() {
        $this->load->model("ClientesModel");
        $data =$this->ClientesModel->getClientesForCombo();

        print json_encode($data);
    }

    // function crearApikey() {
    //     $id = intval($this->input->post("id_usuario"));
    //     $micro = str_replace(".", "", microtime()); 
    //     $micro = str_replace(" ", "", $micro)."_".random_string("alnum", 10);
    //     if(!empty($id)) {
    //     $datos["id_usuario"]=$id;
    //     $datos["api_key"]=$micro;  
    //     $datos["activa"]=1;
    //     $datos["fecha_creacion"]=date("Ymd H:i:s");
    //     $activa["id_usuario"]=$id;
    //     $activa["activa"]=1;   
    //     $data = $this->CatUsuariosModel->crearApikey($datos,$activa);
    //     }else{
    //         $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");  
    //     }
    //     print json_encode($data);
    // }
    

    function exportData()
    {
        if (trim($_GET['format'])=='CSV'){
            $this->exportCSV();
            return;
        }

        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel.php';
        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel/IOFactory.php';
        // Import classes.
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\Spreadsheet';
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\IOFactory';

        $filename = 'usuarios';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $data = $this->CatUsuariosModel->listar(true);

        $rowCount = 1;
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, 'Cliente');
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, 'Nombre usuario');
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, 'Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, 'Perfil');
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, 'Deshabilitado');

        //var_dump($data["data"]); die();

        //while($row = $data["data"]){ 
        foreach ($data["data"] as $row) {
            //var_dump($row); die();
            $rowCount++;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row['empresa']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['nombre']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['correo']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['perfil']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['inactivo']);
        }

        // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        // $objWriter->save('some_excel_file.xlsx');   
        header("Pragma: public");
        header("Expires: 0");
        // header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');

        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        // $objWriter->setSheetIndex(0);   // Select which sheet.
        // $objWriter->setDelimiter(';');  // Define delimiter
        // $objWriter->save('my-excel-file.csv');        
    }

    private function exportCSV()
    {
        // filename for download
        $filename = "usuarios_" . date('Ymd') . ".csv";

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv");

        $out = fopen("php://output", 'w');

        $flag = false;
        $data = $this->CatUsuariosModel->listar(true);
        //while (false !== ($row = pg_fetch_assoc($result))) {
        foreach ($data["data"] as $row) {
            $datax['cliente']=($row['empresa']);
            $datax['nombre_usuario']=($row['nombre']);
            $datax['email']=($row['correo']);
            $datax['perfil']=($row['perfil']);
            $datax['deshabilitado']=($row['inactivo']);
        
            if (!$flag) {
                // display field/column names as first row
                fputcsv($out, array_keys($datax), ',', '"');
                $flag = true;
            }
            array_walk($datax, __NAMESPACE__ . '\cleanData');
            fputcsv($out, array_values($datax), ',', '"');
        }

        fclose($out);
    }
}

function cleanData(&$str)
{
    if ($str == 't') $str = 'TRUE';
    if ($str == 'f') $str = 'FALSE';
    if (preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
        $str = "'$str";
    }
    if (strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}
