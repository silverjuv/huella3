<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClientesController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("ClientesModel");
    }

    function index() {
        $params = array("menu_expandido"=>"3", "pantalla"=>"38");
        makeDefaultLayout(
            "clientesView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                "assets/js/global.js",
                "assets/js/clientes.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            )
        );
    }

    function listar() {
        $data = $this->ClientesModel->listar();
        print json_encode($data);
    }

    function getRowById() {
        $id = intval($this->input->post("id"));
        if(!empty($id)) {
            $r = $this->ClientesModel->getRowById($id);
        } else {
            $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
        }

        print json_encode($r);
    }

    function deleteRowById() {
        $id = intval($this->input->post("id"));
        if(!empty($id)) {
            $r = $this->ClientesModel->deleteRowById($id);
        } else {
            $r = array("error"=>"1", "title"=>"Error", "msg"=>"No se recibieron los parametros esperados", "type"=>"error");
        }

        print json_encode($r);
    }

    function validar(&$data) {
        if(empty($data["descripcion"])) {
            return array("error"=>"1", "title"=>"Error", "msg"=>"El campo Proyectos es obligatorio", "type"=>"error");
        }
        
        return array("error"=>"0");
    }

    function guardar() {
        $data = $this->input->post("data");
        $r = $this->validar($data);
        
        if($r["error"] === "0") {
            
            $id = intval($data["id_edicion"]);
            unset($data["id_edicion"]);
            
            if($data["accion"] == "editar") {
                unset($data["accion"]);
                $r = $this->ClientesModel->actualizar($id, $data);
            } else if($data["accion"] == "insertar") {
                unset($data["accion"]);
                $r = $this->ClientesModel->insertar($data);

                if ($r['error']==0){
                    $this->load->model("ConfigClienteModel");
                    $data2["id_cliente"]=$r['lastid'];
                    $data2["id_config_cliente"]=1;
                    $data2["valor"]=1; // valor por default 1 - Reprocesar persona-puesto aunque ya tenga resultados..      
                    $this->ConfigClienteModel->insertar($data2);

                    $data2["id_cliente"]=$r['lastid'];
                    $data2["id_config_cliente"]=2;
                    $data2["valor"]=0; // valor por default 0 - NO Regenerar expediente persona aunque ya tenga resultados..      
                    $this->ConfigClienteModel->insertar($data2);

                    $data2["id_cliente"]=$r['lastid'];
                    $data2["id_config_cliente"]=3;
                    $data2["valor"]=1; // valor por default 1 - Recibir email de resultados cuando peticion analisis persona-puesto sea por API-REST      
                    $this->ConfigClienteModel->insertar($data2);                    
                }
            }
            
        }
        
        print json_encode($r);
    }


    function exportData()
    {
        if (trim($_GET['format'])=='CSV'){
            $this->exportCSV();
            return;
        }

        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel.php';
        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel/IOFactory.php';
        // Import classes.
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\Spreadsheet';
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\IOFactory';

        $filename = 'clientes';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $data = $this->ClientesModel->listar(true);

        $rowCount = 1;
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, 'Cliente');
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, 'Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, 'Telefono');
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, 'Dirección');

        //var_dump($data["data"]); die();

        //while($row = $data["data"]){ 
        foreach ($data["data"] as $row) {
            //var_dump($row); die();
            $rowCount++;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row['descripcion']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['email_contacto']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['telefono']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['direccion']);
        }

        // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        // $objWriter->save('some_excel_file.xlsx');   
        header("Pragma: public");
        header("Expires: 0");
        // header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');

        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        // $objWriter->setSheetIndex(0);   // Select which sheet.
        // $objWriter->setDelimiter(';');  // Define delimiter
        // $objWriter->save('my-excel-file.csv');        
    }

    private function exportCSV()
    {
        // filename for download
        $filename = "clientes_" . date('Ymd') . ".csv";

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv");

        $out = fopen("php://output", 'w');

        $flag = false;
        $data = $this->ClientesModel->listar(true);
        //while (false !== ($row = pg_fetch_assoc($result))) {
        foreach ($data["data"] as $row) {
            $datax['cliente']=($row['descripcion']);
            $datax['email']=($row['email_contacto']);
            $datax['telefono']=($row['telefono']);
            $datax['direccion']=($row['direccion']);
        
            if (!$flag) {
                // display field/column names as first row
                fputcsv($out, array_keys($datax), ',', '"');
                $flag = true;
            }
            array_walk($datax, __NAMESPACE__ . '\cleanData');
            fputcsv($out, array_values($datax), ',', '"');
        }

        fclose($out);
    }
}

function cleanData(&$str)
{
    if ($str == 't') $str = 'TRUE';
    if ($str == 'f') $str = 'FALSE';
    if (preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
        $str = "'$str";
    }
    if (strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}
