<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("HomeModel");
    }

    function index() {
        $datos = $this->HomeModel->obtenerDatosCliente($_SESSION['id_cliente']);
        // var_dump($datos); die();
        $params = array("menu_expandido"=>"0", "pantalla"=>"2", "cliente" => $datos);

        if ($_SESSION['id_perfil']==2){
            makeDefaultLayout(
                "homeView",
                $params,
                array(
                    "assets/template/plugins/charts/Chart.min.js",
                    "assets/template/plugins/jquery-animateNumber-0.0.14/jquery.animateNumber.min.js",
                    // "assets/template/plugins/chart-js/Chart.min.js",
                    // "assets/template/plugins/chart-js/chartjs-plugin-datalabels.min.js",
                    "assets/template/plugins/randomColors/randomColor.js",
                    "assets/js/global.js",
                    "assets/js/home.js",
                    'assets/speedometer/js/speedometer.js',
                    // "assets/js/graficas.js"

                ),
                array(
                    'assets/speedometer/css/speedometer.css',
                    "assets/template/plugins/charts/Chart.min.css",
                )
            );
        }else{
            // si el perfil es administrador = 1
            makeDefaultLayout(
                "homeViewAdministrativo",
                $params,
                array(
                    "assets/template/plugins/charts/Chart.min.js",
                    "assets/template/plugins/jquery-animateNumber-0.0.14/jquery.animateNumber.min.js",
                    // "assets/template/plugins/chart-js/Chart.min.js",
                    // "assets/template/plugins/chart-js/chartjs-plugin-datalabels.min.js",
                    "assets/template/plugins/randomColors/randomColor.js",
                    "assets/js/global.js",
                    "assets/js/homeAdministrativo.js",
                    'assets/speedometer/js/speedometer.js',
                    // "assets/js/graficas.js"

                ),
                array(
                    'assets/speedometer/css/speedometer.css',
                    "assets/template/plugins/charts/Chart.min.css",
                )
            );
        }

        // // $this->elasticsearch->client
        // $params = [
        //     'index' => 'my_index',
        //     'id'    => 'my_id',
        //     'body'  => ['testField' => 'abc']
        // ];

        // $response = $this->elasticsearch->client->index($params);
        // print_r($response);


    }

    function cambiarContrasena() {
        $params = array("menu_expandido"=>"0", "pantalla"=>"2");
        makeDefaultLayout(
            "cambiarContrasenaView",
            $params,
            array(
                "assets/js/global.js",
                "assets/js/cambiarPass.js"
            )
        );
    }

    function validarPassData(&$d) {
        if(empty($d)) {
            return array("title"=>"Atención", "msg"=>"No se recibió la información esperada... (1)", "type"=>"error");
        }
        if(empty($d["pass"])) {
            return array("title"=>"Atención", "msg"=>"No se recibió la información esperada... (2)", "type"=>"error");
        }
        if(empty($d["passn"])) {
            return array("title"=>"Atención", "msg"=>"No se recibió la información esperada... (3)", "type"=>"error");
        }
        if($d["passn"] != $d["passnc"]) {
            return array("title"=>"Atención", "msg"=>"Las contraseñas no coinciden... (4)", "type"=>"error");
        }

        return true;
    }

    function actualizarPass() {
        $d = $this->input->post("data");
        $r = $this->validarPassData($d);
        if($r === true) {
            $r = $this->HomeModel->actualizarPass($this->session->id_usuario, $d["pass"], $d["passn"]);
        }

        print json_encode($r);
    }

    // function getMiStatusSemanal() {
    //     $id_usuario = $this->session->id_usuario;
    //     $r = $this->HomeModel->getMiStatusSemanal($id_usuario);
    //     print json_encode($r);
    // }

    // function getMisStatus() {
    //     $id_usuario = $this->session->id_usuario;
    //     $r = $this->HomeModel->getMisStatus($id_usuario);
    //     print json_encode($r);
    // }

    // function getEquipoStatusSemanal() {
    //     $id_usuario = $this->session->id_usuario;
    //     $r = $this->HomeModel->getObjetivosEquipoStatus($id_usuario, true);
    //     print json_encode($r);
    // }

    // function getEquipoStatus() {
    //     $id_usuario = $this->session->id_usuario;
    //     $r = $this->HomeModel->getObjetivosEquipoStatus($id_usuario);
    //     print json_encode($r);
    // }

    function cargaNumerosTop () {
        $tPersonas = $this->HomeModel->obtenerTotalPersonasCliente($_SESSION['id_cliente']);
        $tPuestos = $this->HomeModel->obtenerTotalPuestosCliente($_SESSION['id_cliente']);
        $tExpedientes = $this->HomeModel->obtenerTotalExpedientesCliente($_SESSION['id_cliente']);
        $tConsumos = $this->HomeModel->obtenerTotalConsumosCliente($_SESSION['id_cliente']);
        $tPromedio = $this->HomeModel->obtenerTotalPromedioResultados($_SESSION['id_cliente']);
        $tListaMeses = $this->HomeModel->obtenerTotalProcesosMensuales($_SESSION['id_cliente']);
        $tRangosColors = $this->HomeModel->obtenerTotalColoresRiesgos($_SESSION['id_cliente']);

        $tListaRiesgos = $this->HomeModel->obtenerTotalRiesgosEncontrados($_SESSION['id_cliente']);

        $riesgos = [];
        $riesgostotales = [];
        $colorsBack = [];
        if (count($tListaRiesgos)>0){
            $riesgos = $tListaRiesgos['riesgos'];
            $riesgostotales = $tListaRiesgos['totales'];
            $colorsBack = $tListaRiesgos['colorsBack'];
        }

        $r = array("error"=>"0", "title"=>"ok", "msg"=>"Estadisticos generados", "type"=>"success",
                    "totalPersonas"=>$tPersonas,"totalPuestos"=>$tPuestos,"totalExpedientes"=>$tExpedientes,"totalConsumos"=>$tConsumos,
                    "totalPromedio"=>$tPromedio, "totalMeses"=>$tListaMeses, "riesgos"=>$riesgos, "totalRiesgos"=>$riesgostotales,
                    "colorsBack"=>$colorsBack,"rangosColors"=>$tRangosColors);
        print json_encode($r);
    }


    function cargaNumerosAdministrativos () {
        // $tPersonas = $this->HomeModel->obtenerTotalPersonasCliente($_SESSION['id_cliente']);
        // $tPuestos = $this->HomeModel->obtenerTotalPuestosCliente($_SESSION['id_cliente']);
        // $tExpedientes = $this->HomeModel->obtenerTotalExpedientesCliente($_SESSION['id_cliente']);
        
        // $tPromedio = $this->HomeModel->obtenerTotalPromedioResultados($_SESSION['id_cliente']);
        $tListaMeses = $this->HomeModel->obtenerTotalProcesosPorMes();
        // $tRangosColors = $this->HomeModel->obtenerTotalColoresRiesgos($_SESSION['id_cliente']);

        $tListaRiesgos = $this->HomeModel->obtenerTotalRiesgosMasLocalizados();
        $riesgos = [];
        $riesgostotales = [];
        $colorsBack = [];
        if (count($tListaRiesgos)>0){
            $riesgos = $tListaRiesgos['riesgos'];
            $riesgostotales = $tListaRiesgos['totales'];
            $colorsBack = $tListaRiesgos['colorsBack'];
        }

        $tContratos = $this->HomeModel->obtenerTotalContratos();
        $tUsuarios = $this->HomeModel->obtenerTotalUsuarios();
        $tClientes = $this->HomeModel->obtenerTotalClientes();
        $tConsumos = $this->HomeModel->obtenerTotalConsumos();
        $tRiesgos = $this->HomeModel->obtenerTotalRiesgos();
        $tNoticias = $this->HomeModel->obtenerTotalNoticias();


        $r = array("error"=>"0", "title"=>"ok", "msg"=>"Estadisticos generados", "type"=>"success",
                    "totalUsuarios"=>$tUsuarios,"totalClientes"=>$tClientes,"totalContratos"=>$tContratos,"totalConsumos"=>$tConsumos,
                    "totalMeses"=>$tListaMeses, "riesgos"=>$riesgos, "totalRiesgos"=>$riesgostotales, "totalRiesgosRegistrados"=>$tRiesgos,
                    "colorsBack"=>$colorsBack ,  "totalNoticias"=>$tNoticias);
        print json_encode($r);
    }

}