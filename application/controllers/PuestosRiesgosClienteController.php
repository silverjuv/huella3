<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PuestosRiesgosClienteController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("PuestosRiesgosClienteModel");
    }

    function index() {
        $params = array("menu_expandido"=>"0", "pantalla"=>"26");
        makeDefaultLayout(
            "puestosRiesgosClienteView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.min.js',
                "assets/js/global.js",
                "assets/js/puestosRiesgosCliente.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css'
            )
        );
    }


    function listarPuestos() {
        $data =$this->PuestosRiesgosClienteModel->listarPuestos();
        print json_encode($data);
    }

    function listarRiesgosSubriesgos() {
        $id_riesgo = intval($this->input->post("id_riesgo"));
        $data = $this->PuestosRiesgosClienteModel->listarRiesgosSubriesgos($id_riesgo);
        print json_encode($data);
    }

    function listarPuestosRiesgos() {
        $id_puesto = intval($this->input->post("id_puesto"));
        $data = $this->PuestosRiesgosClienteModel->listarPuestosRiesgos($id_puesto);
        print json_encode($data);
    }

}