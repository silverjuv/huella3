<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PuestosRiesgosController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("PuestosRiesgosModel");
    }

    function index() {
        $params = array("menu_expandido"=>"3", "pantalla"=>"25", "idc" => intval($this->input->get("idc")), 
                    "idp" => intval($this->input->get("idp")));
        makeDefaultLayout(
            "puestosRiesgosView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.min.js',
                "assets/js/global.js",
                "assets/js/puestosRiesgos.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css'
            )
        );
    }

    function listarClientes() {
        $data =$this->PuestosRiesgosModel->listarClientes();
        print json_encode($data);
    }

    function listarPuestos() {
        $data =$this->PuestosRiesgosModel->listarPuestos();
        print json_encode($data);
    }

    function listarRiesgos() {
        $data = $this->PuestosRiesgosModel->listarRiesgos();
        print json_encode($data);
    }

    function listarRiesgosSubriesgos() {
        $id_riesgo = intval($this->input->post("id_riesgo"));
        $data = $this->PuestosRiesgosModel->listarRiesgosSubriesgos($id_riesgo);
        print json_encode($data);
    }

    function listarPuestosRiesgos() {
        $id_puesto = intval($this->input->post("id_puesto"));
        $id_cliente = intval($this->input->post("id_cliente"));
        $data = $this->PuestosRiesgosModel->listarPuestosRiesgos($id_puesto,$id_cliente);
        print json_encode($data);
    }

    function insertar() {
        $id_puesto = intval($this->input->post("id_puesto"));
        $id_riesgo = intval($this->input->post("id_riesgo"));
        $id_cliente = intval($this->input->post("id_cliente"));
        $data = $this->PuestosRiesgosModel->insertar($id_puesto, $id_riesgo,$id_cliente);
        print json_encode($data);
    }

    function quitarRiesgoPuesto() {
        $id_puesto = intval($this->input->post("id_puesto"));
        $id_riesgo = intval($this->input->post("id_riesgo"));
        $id_cliente = intval($this->input->post("id_cliente"));
        $data = $this->PuestosRiesgosModel->quitarRiesgoPuesto($id_puesto, $id_riesgo,$id_cliente);
        print json_encode($data);
    }

}