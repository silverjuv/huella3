<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SpiderFootController extends CI_Controller {

    //const AIF_PATH = "/var/www/threats/www/huella";
    //const OUTPUT_RESULTS_OSINT_W = "/var/www/threats/www/huella/assets/documents";
    const MAX_TIME = 0;
	/**
	 * Identificador de sesion activa
	 *
	 * @var string
	 */
	public static $id = NULL;    

	function __construct(){
        parent::__construct();
        $this->load->model("OSRFrameworkModel");

        $this->load->model("ParametrosModel");
        $this->load->model("ProcesosModel");
        // $this->load->library("Aif/Aif");
        // $this->load->library("Aif/AifRequest");
        // $this->load->library("Aif/MonitorModelOsintOsrframework");
        // $this->load->library("Aif/AifDb");
    }

    function index() {
        $params = array("menu_expandido"=>"15", "pantalla"=>"20");
        makeDefaultLayout(
            "esociales/spiderFootView",
            $params, 
            array(
                'assets/template/plugins/select2/dist/js/select2.js',
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                'assets/template/plugins/waitingfor.js',
                "assets/js/global.js",
                "assets/js/logos.js",
                "assets/js/esociales/spiderFoot.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.css',
            )
        );
    }

    // function getUsufyPlatforms() {
        
    //     // $data = AifRequest::getPost('search');
    //     // $r = MonitorModelOsintOsrframework::getUsufyPlatforms($data);
        
    //     // Aif::$render = "json";
    //     // Monitor::out($r);

    //     $data = $this->OSRFrameworkModel->getUsufyPlatforms();
    //     print json_encode($data);        
    // }

    // function getSearchfyPlatforms() {
    //     $data = $this->OSRFrameworkModel->getSearchfyPlatforms();
    //     print json_encode($data);        
    // }

    // function getMailfyPlatforms() {
    //     $data = $this->OSRFrameworkModel->getMailfyPlatforms();
    //     print json_encode($data);        
    // }

    // function getPhonefyPlatforms() {
    //     $data = $this->OSRFrameworkModel->getPhonefyPlatforms();
    //     print json_encode($data);        
    // }

    function ejecutarNumverify() {

        $row = $this->ParametrosModel->getParametroById('API_KEY_numverify.com');
        $access_key = $row->valor;

        // set API Access Key
        // $access_key = '9bbb9754aebaad5825fbefd8f4669312';

        // set phone number
        // $data=$_POST['data'];
        $phone_number = $_POST['data'];

        // Initialize CURL:
        $ch = curl_init('http://apilayer.net/api/validate?access_key='.$access_key.'&number='.$phone_number.'');  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);

        // Decode JSON response:
        $validationResult = json_decode($json, true);

        // // Access and use your preferred validation result objects
        // $validationResult['valid'];
        // $validationResult['country_code'];
        // $validationResult['carrier'];
        //$cmd='curl numverify (http://apilayer.net)';
        $cmd = '';

        print json_encode(array(
            "name"=>$phone_number, 
            "cmd"=>$cmd,
            "json"=>$json
        ));
    }

    // function ejecutarSearchfy() {
    //     $data = AifRequest::getPost('data');
    //     $plataformas = AifRequest::getPost('plataformas');
    //     $osr = new MonitorClassOsrframework();
        
    //     $r = $osr->ejecutarSearchfy($data, $plataformas);
    
    //     Aif::$render = "json";
    //     Monitor::out($r);
    // }

    public function getUniqueName() {
        $micro = str_replace(".","",microtime()); 
		return $this->get_idsession('id')."_".str_replace(" ","",$micro);	
    }

    // private function plataformasToStr($plataformas, $modulo) {
    //     $lenPlataformas = is_array($plataformas)?count($plataformas):0;
    //     $p="";

    //     if($lenPlataformas>0) {
    //         $p = "-p ".implode(" ", $plataformas);
    //     } else {
    //         //si el usuario especificó plataformas entonces excluimos algunas (solo en usufy)
    //         if($modulo == "usufy") {
    //             $p = " -x ".implode(" ", $this->usufyExcludes);
    //         }
    //     }
    //     return $p;
    // }

    public function ejecutarMailrep() {
        $data=$_POST['data'];
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        $fileName = OUTPUT_RESULTS_OSINT_W."/".self::getUniqueName().".json";
        //python3 sf.py -m sfp_emailrep -s daescrux@gmail.com -o json > mrepx.json 2>&1

        $comandoEnv = "cd ".PATH_SCRIPTS_PYHUELLA." ; source env1/bin/activate ; ";
        $comandoEnvFinal = " ; deactivate ";
        // $rutaEnv = "";
        $cmd = $comandoEnv.PYTHON_VER." emailrep_query.py ".escapeshellcmd($data)." ".$fileName." ".$comandoEnvFinal;
        //$cmd = "/usr/local/bin/searchfy  -q ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        // $results = $this->ejecutarCmd($cmd, $folderName);
        $d['proceso']='E';   // emailrep_query script personalizado
        $d['window']='O';   // pantalla publica de OSINT
        $d['comando']=$cmd;
        // $d['fecha_programacion'] = mdate("%Y-%m-%d %H:%i:%s");
        $now = date('Y-m-d H:i:s');
        $d["fecha_programacion"]=$now;
        // $d['window']='O';
        $d['id_session'] = $_SESSION['id_session'];
        $d['outputDirFile'] = $fileName;

        $msg = $this->ProcesosModel->insertar($d);
        //echo $results.' ok ';

        print json_encode(
            // "result"=>$msg
            $msg
            // "json"=>$results
        );        
        // // // //$cmd = "python3.6 /var/www/threats/tools/spiderfoot/sf.py -m sfp_emailrep -s ".escapeshellcmd($data)." -q  -o json > ".$fileName;
        // // // //$cmd = "/usr/local/bin/searchfy  -q ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        // // // $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/emailrep_query.py ".$data." ".$fileName;
        // // // $results = $this->ejecutarCmd($cmd, $fileName);
        // // // //echo $results.' ok ';

        // // // print json_encode(array(
        // // //     "name"=>$data, 
        // // //     "cmd"=>$cmd,
        // // //     "json"=>$results
        // // // ));
        // return ( );
    }


    public function verResultadosEmailrep(){
        $idproc=isset($_POST['idProc']) ? $_POST['idProc']: "-1";
        $data = $this->ProcesosModel->getResultadoProceso($idproc);
        // print($idproc);
        // var_dump($data);
        // print($data->status);
        $msg = '';
        if ($data->status!=1){
            $msg = 'No fue posible realizar el proceso.';
        }

        switch($data->proceso){
            case 'E':
                $jsonFile = $data->outputDirFile;
                // echo $jsonFile;
                $results = $this->getResultsAndCleanPath($jsonFile);
        }
        // $results =  $this->getResultsAndCleanPath($resultsFolder, $jsonFile);

        // $status = ($msg['status']==0)
        print json_encode(array(
            "status"=>intVal($data->status), 
            // "name"=>$data, 
            "msg"=>$msg,
            "json"=>$results
        ));        

        // return ( );
    }

    private function ejecutarCmd($cmd, $jsonFile) {
        $completeCmd = $cmd;//self::PYTHON_PATH." ". self::LAUNCHER ." ".$cmd;
        set_time_limit(self::MAX_TIME);
        
        $o = shell_exec ($completeCmd." 2>&1");
        //var_dump($o);

        return $this->getResultsAndCleanPath($jsonFile);
    }
    
    private function getResultsAndCleanPath($file) {
        $result = "";
        try {
            if(file_exists($file)) {
                $result = file_get_contents($file);
                // echo $file."<br />".$result."<br />";
                // die();
                @unlink($file);
            }else{
                $result="[]";
            }
            
            return $result;    
        } catch(Exception $e){ 
            echo $e."<br />";
             die();
            return $result; 
        }
    }    

// //   /**
// //    * Elimina un directorio de forma recursiva
// //    * @param string $dir
// //    * @return boolean
// //    */
// //   public function removeDir($dir){
// //     if (!file_exists($dir)) return TRUE;
// //     if (!is_dir($dir) || is_link($dir)) return unlink($dir);
// //     $ar = scandir($dir);
// //     foreach ($ar as $item){
// //       if ($item == '.' || $item == '..') continue;
// //       if (!self::removeDir($dir . "/" . $item)) return FALSE;
// //     }
// //     return rmdir($dir);
// //   }    

/**
	 * Comprueba si existe una variable en la sesion
	 *
	 * @param string $var
	 *        	Nombre de la variable
	 * @return boolean
	 */
	public  function isVar($var) {
		return isset ( $_SESSION [self::$id] ) && isset ( $_SESSION [self::$id] [$var] ) ? true : false;
	}
	/**
	 * Regresa un valor de una variable en la sesion
	 *
	 * @param string $var
	 *        	Nombre de la variable de session
	 * @return mixed Regresa el valor de la variable de session
	 */
	public  function get_idsession($var) {
		return  $this->isVar ( $var ) ? $_SESSION [self::$id] [$var] : false;
	}
}