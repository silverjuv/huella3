<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PermissionDeniedController extends CI_Controller {

	public function index() {
		$params = array("menu_expandido"=>"0", "pantalla"=>"0");
        makeDefaultLayout(
            "permissionDeniedView",
            $params
        );
	}
}