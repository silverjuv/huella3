<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PerfilesPantallasController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("PerfilesPantallasModel");
    }

    function index() {
        $params = array("menu_expandido"=>"46", "pantalla"=>"9");
        makeDefaultLayout(
            "perfilesPantallasView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                "assets/js/global.js",
                "assets/js/perfilesPantallas.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css'
            )
        );
    }

    function listarPantallas() {
        $this->load->model("CatPantallasModel");
        $data = $this->CatPantallasModel->listarPantallas();
        print json_encode($data);
    }

    function getPerfilesForCombo() {
        $this->load->model("CatPerfilesModel");
        $data =$this->CatPerfilesModel->getPerfilesForCombo();

        print json_encode($data);
    }

    function listarPantallasDePerfil() {
        $id_perfil = intval($this->input->post("id_perfil"));
        $data = $this->PerfilesPantallasModel->listarPantallasDePerfil($id_perfil);
        print json_encode($data);
    }

    function insertar() {
        $id_perfil = intval($this->input->post("id_perfil"));
        $id_pantalla = intval($this->input->post("id_pantalla"));

        $data = $this->PerfilesPantallasModel->insertar($id_perfil, $id_pantalla);
        print json_encode($data);
    }

    function quitarPantallaDePerfil() {
        $id_perfil = intval($this->input->post("id_perfil"));
        $id_pantalla = intval($this->input->post("id_pantalla"));

        $data = $this->PerfilesPantallasModel->quitarPantallaDePerfil($id_perfil, $id_pantalla);
        print json_encode($data);
    }

}