<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfigClienteController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("ConfigClienteModel");
    }

    function index() {

        // obtenemos configuracion de cliente.. 
        // $data = $this->ConfigClienteModel->getConfiguracionCliente($_SESSION['id_cliente']);

        // $params = array("menu_expandido"=>"3", "pantalla"=>"40","datos"=>$data);
        $params = array("menu_expandido"=>"3", "pantalla"=>"40");
        makeDefaultLayout(
            "configClienteView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                // 'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                // 'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/configCliente.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
            )
        );
    }


    function actualizarConfiguracion() {
        $data = $this->input->post("data");

        // borramos
        $this->ConfigClienteModel->borrar($_SESSION['id_cliente']);

        // insertamos
        $r = null;
        // var_dump($data);
        foreach ($data as $clave => $valor) {
            $row["id_cliente"]=$_SESSION['id_cliente'];
            $row["id_config_cliente"]=intval(substr($clave,4));
            $row["valor"]=$valor;
            // var_dump($data); die();
            $r=$this->ConfigClienteModel->insertar($row);
        }

        
        print json_encode($r);
    }

    function obtenerConfiguracion() {
        $data = $this->ConfigClienteModel->getConfiguracionCliente($_SESSION['id_cliente']);

        print json_encode($data);
    }
}