<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HarvesterController extends CI_Controller {

    //const AIF_PATH = "/var/www/threats/www/huella";
    const OUTPUT_RESULTS = "/var/www/threats/www/huella/assets/documents";
    const MAX_TIME = 0;
	/**
	 * Identificador de sesion activa
	 *
	 * @var string
	 */
	public static $id = NULL;    

	function __construct(){
        parent::__construct();
        $this->load->model("OSRFrameworkModel");
        // $this->load->library("Aif/Aif");
        // $this->load->library("Aif/AifRequest");
        // $this->load->library("Aif/MonitorModelOsintOsrframework");
        // $this->load->library("Aif/AifDb");
    }

    function index() {
        $params = array("menu_expandido"=>"15", "pantalla"=>"21");
        makeDefaultLayout(
            "esociales/harvesterView",
            $params, 
            array(
                'assets/template/plugins/select2/dist/js/select2.js',
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/logos.js",
                "assets/js/esociales/harvester.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.css',
            )
        );
    }

    // function getUsufyPlatforms() {
        
    //     // $data = AifRequest::getPost('search');
    //     // $r = MonitorModelOsintOsrframework::getUsufyPlatforms($data);
        
    //     // Aif::$render = "json";
    //     // Monitor::out($r);

    //     $data = $this->OSRFrameworkModel->getUsufyPlatforms();
    //     print json_encode($data);        
    // }

    // function getSearchfyPlatforms() {
    //     $data = $this->OSRFrameworkModel->getSearchfyPlatforms();
    //     print json_encode($data);        
    // }

    // function getMailfyPlatforms() {
    //     $data = $this->OSRFrameworkModel->getMailfyPlatforms();
    //     print json_encode($data);        
    // }

    // function getPhonefyPlatforms() {
    //     $data = $this->OSRFrameworkModel->getPhonefyPlatforms();
    //     print json_encode($data);        
    // }

    function ejecutarUsufy() {
        //$p = $this->plataformasToStr($plataformas, "usufy");
        $data=$_POST['data'];
        $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        // echo $cmd; die();
        
        $r = $this->ejecutarCmd($cmd, $folderName);

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>$r
        ));
    }

    // function ejecutarSearchfy() {
    //     $data = AifRequest::getPost('data');
    //     $plataformas = AifRequest::getPost('plataformas');
    //     $osr = new MonitorClassOsrframework();
        
    //     $r = $osr->ejecutarSearchfy($data, $plataformas);
    
    //     Aif::$render = "json";
    //     Monitor::out($r);
    // }

    public function getUniqueName() {
        $micro = str_replace(".","",microtime()); 
		return $this->get_idsession('id')."_".str_replace(" ","",$micro);	
    }

    private function plataformasToStr($plataformas, $modulo) {
        $lenPlataformas = is_array($plataformas)?count($plataformas):0;
        $p="";

        if($lenPlataformas>0) {
            $p = "-p ".implode(" ", $plataformas);
        } else {
            //si el usuario especificó plataformas entonces excluimos algunas (solo en usufy)
            if($modulo == "usufy") {
                $p = " -x ".implode(" ", $this->usufyExcludes);
            }
        }
        return $p;
    }

    public function ejecutarSearchfy() {
        $data=$_POST['data'];
        $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();

        $cmd = "searchfy -q ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        //$cmd = "/usr/local/bin/searchfy  -q ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $results = $this->ejecutarCmd($cmd, $folderName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>$results
        ));
        // return ( );
    }

    public function ejecutarMailfy() {
        $data=$_POST['data'];
        $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "mailfy") : "";
        $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();

        $cmd = "mailfy -m ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        //$cmd = "/usr/local/bin/searchfy  -q ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $results = $this->ejecutarCmd($cmd, $folderName);
        // echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>$results
        ));
        // return ( );
    }    

    public function ejecutarPhonefy() {
        $data=$_POST['data'];
        $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "mailfy") : "";
        $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();

        $cmd = "phonefy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        //$cmd = "/usr/local/bin/searchfy  -q ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $results = $this->ejecutarCmd($cmd, $folderName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>$results
        ));
        // return ( );
    }  

    private function ejecutarCmd($cmd, $resultsFolder) {
        $completeCmd = $cmd;//self::PYTHON_PATH." ". self::LAUNCHER ." ".$cmd;
        set_time_limit(self::MAX_TIME);

        // echo $cmd.'<br />';
        // echo $resultsFolder;
        // die();
        
        $o = shell_exec ($completeCmd." 2>&1");
        //var_dump($o);
        $jsonFile = $resultsFolder."/profiles.json";
        // echo $completeCmd.'<br />';
        // echo $resultsFolder;
        // echo '<br />'.$jsonFile;
        // die();
        return $this->getResultsAndCleanPath($resultsFolder, $jsonFile);
    }
    
    private function getResultsAndCleanPath($folder, $file) {
        $result = "";
        try {
            if(file_exists($file)) {
                $result = file_get_contents($file);
                // echo $file."<br />".$result."<br />";
                // die();
                unlink($file);
            }else{
                $result="[]";
            }
            if(is_dir($folder)) {
                //AifLibFile::removeDir($folder);
                $this->removeDir($folder);
            }
            // echo $result;
            // die();
            return $result;    
        } catch(Exception $e){ 
            echo $e."<br />";
             die();
            return $result; 
        }
    }    

  /**
   * Elimina un directorio de forma recursiva
   * @param string $dir
   * @return boolean
   */
  public function removeDir($dir){
    if (!file_exists($dir)) return TRUE;
    if (!is_dir($dir) || is_link($dir)) return unlink($dir);
    $ar = scandir($dir);
    foreach ($ar as $item){
      if ($item == '.' || $item == '..') continue;
      if (!self::removeDir($dir . "/" . $item)) return FALSE;
    }
    return rmdir($dir);
  }    

/**
	 * Comprueba si existe una variable en la sesion
	 *
	 * @param string $var
	 *        	Nombre de la variable
	 * @return boolean
	 */
	public  function isVar($var) {
		return isset ( $_SESSION [self::$id] ) && isset ( $_SESSION [self::$id] [$var] ) ? true : false;
	}
	/**
	 * Regresa un valor de una variable en la sesion
	 *
	 * @param string $var
	 *        	Nombre de la variable de session
	 * @return mixed Regresa el valor de la variable de session
	 */
	public  function get_idsession($var) {
		return  $this->isVar ( $var ) ? $_SESSION [self::$id] [$var] : false;
	}
}