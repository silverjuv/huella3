<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/OSCalls.php';
class TicDefenseToolsController extends CI_Controller {

    private $objOScalls;

    function __construct(){
        parent::__construct();
        $this->load->model("ExpedientesModel");
        $this->objOSCalls = new OSCalls();

        $this->load->model("CatPersonasModel");
        $this->load->model("ParametrosModel");
    }

    function ejecutarOSINTNumVerify() {
        //$p = $this->plataformasToStr($plataformas, "usufy");
        $id_persona=$_POST['data'];
        $fuente = strval(trim($this->input->post("fuente")));
        $id_perfil=intval($this->session->userdata("id_perfil"));
        $fuente = strval(trim($this->input->post("fuente")));
        if($id_perfil==2){
            $datos["noErrores"]=1;
            $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
            print json_encode(array(
                "name"=>intval($id_persona), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        $where["cp.id_persona"]=intval($id_persona);
        $where["cc.status"]=strval("A");
        $where["cc.borrado"]=intval(0);
        $where["cf.fuente"]=$fuente;
        $this->db->select("cc.id_contrato");
        $this->db->from("clientes_contratos cc");
        $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
        $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
        $this->db->where($where);
        $rC = $this->db->get();
        if($rC->num_rows()==0) {
            //$data=Array();
            $datos["noErrores"]=1;
            $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
            //$data["json"]=$datos;
            //array_push($data["json"],$datos);
            print json_encode(array(
                "name"=>intval($id_persona), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";
        $person = null;
        if($id_persona > 0) {
            $person = $this->CatPersonasModel->getPersonaById($id_persona);
            //var_dump($person);die();
        }

        $nombreP='';
        $errores=0;
        $msg='';
        $url="numverify.com_ByCel";

        if ($person!=null){
            // set API Access Key
            if (strlen($person->celular)==10){
                $row = $this->ParametrosModel->getParametroById('API_KEY_numverify.com');
                $access_key = $row->valor;

                // set phone number
                // $data=$_POST['data'];
                $phone_number = "+52".$person->celular;
                //$url="emailrep.io_ByEmail";

                // Initialize CURL:
                $ch = curl_init('http://apilayer.net/api/validate?access_key='.$access_key.'&number='.$phone_number.'');  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // Store the data:
                $json = curl_exec($ch);
                curl_close($ch);
                
                // Decode JSON response:
                $validationResult = json_decode($json, true);
                //echo $results.' ok ';

                //Guardamos en Base de datos
                $row = $this->ExpedientesModel->consultaUrlSocialIdPersona($url,$id_persona);
                if (count($row)>0){
                    // si hay persona ligada al url_asociado... solo actualizamos URLS_sociales
                    $data["url_asociado"]=trim($url);
                    $data["contenido"]="";
                    $data["json_data"]=$json;
                    $data["id_anexo"]=7;
                    $now = date('Y-m-d H:i:s');
                    $data["fecha_captura"]=$now;
                    $this->ExpedientesModel->actualizarUrlSocial("urls_sociales",$row[0]['id_anexado'],$data);
                }else{
                    $data["url_asociado"]=trim($url);
                    $data["contenido"]="";
                    $data["json_data"]=$json;
                    $data["id_anexo"]=7;
                    $now = date('Y-m-d H:i:s');
                    $data["fecha_captura"]=$now;
                    $idURlAsociado = $this->ExpedientesModel->insertarUrlSocial("urls_sociales",$data);

                    if ($idURlAsociado>0){
                        // INSERTAMOS enlace en expedientes_anexos..
                        $datax["id_persona"]=$id_persona;
                        $datax["id_anexado"]=$idURlAsociado;
                        $datax["fuente"]='R';
                        $datax["usuario"]=1;
                        //$datax["calificacion"]=0;
                        $this->ExpedientesModel->insertarExpedientesAnexos("expedientes_anexos",$datax);
                    }

                }

            }else{
                $msg = 'El celular de la persona no es correcto!';
                $errores+=1;
            }
        }else{
            $msg = 'Persona no localizada en base de datos.. ';
            $errores+=1;
        }

        $results['Id_Persona'] = $id_persona;    
        $results['Persona'] = $nombreP;
        $results['Proceso'] = 'numverify.com_ByCel';
        $results['NoComentarios'] = 0;
        $results['NoUrls'] = 0;
        $results['NoErrores'] = $errores;
        $results['Mensaje'] = $msg;

        print json_encode(array(
            "name"=>$id_persona, 
            "cmd"=>'php numverify',
            "json"=>json_encode($results)
        ));
        // return ( );
    }

    function ejecutarOSINTEmailrep() {
        $data=$_POST['data'];
        $fuente = strval(trim($this->input->post("fuente")));
        $id_perfil=intval($this->session->userdata("id_perfil"));
        $fuente = strval(trim($this->input->post("fuente")));
        if($id_perfil==2){
            $datos["noErrores"]=1;
            $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        $where["cp.id_persona"]=intval($data);
        $where["cc.status"]=strval("A");
        $where["cc.borrado"]=intval(0);
        $where["cf.fuente"]=$fuente;
        $this->db->select("cc.id_contrato");
        $this->db->from("clientes_contratos cc");
        $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
        $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
        $this->db->where($where);
        $rC = $this->db->get();
        if($rC->num_rows()==0) {
            //$data=Array();
            $datos["noErrores"]=1;
            $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
            //$data["json"]=$datos;
            //array_push($data["json"],$datos);
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        //$fileName = self::OUTPUT_RESULTS."/".self::getUniqueName().".json";
        $fileName = $this->objOSCalls->getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/fromEmailrep.py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = $this->objOSCalls->OUTPUT_RESULTS."/".$fileName;
        
        $results = $this->objOSCalls->ejecutarCmd($cmd, $fileName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>json_encode($results)
        ));
        // return ( );
    }

    function ejecutarOSINTFullcontact() {
        $data=$_POST['data'];
        $fuente = strval(trim($this->input->post("fuente")));
        $id_perfil=intval($this->session->userdata("id_perfil"));
        $fuente = strval(trim($this->input->post("fuente")));
        if($id_perfil==2){
            $datos["noErrores"]=1;
            $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        $where["cp.id_persona"]=intval($data);
        $where["cc.status"]=strval("A");
        $where["cc.borrado"]=intval(0);
        $where["cf.fuente"]=$fuente;
        $this->db->select("cc.id_contrato");
        $this->db->from("clientes_contratos cc");
        $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
        $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
        $this->db->where($where);
        $rC = $this->db->get();
        if($rC->num_rows()==0) {
            //$data=Array();
            $datos["noErrores"]=1;
            $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
            //$data["json"]=$datos;
            //array_push($data["json"],$datos);
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        //$fileName = self::OUTPUT_RESULTS."/".self::getUniqueName().".json";
        $fileName = $this->objOSCalls->getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/fromFullcontact.py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = $this->objOSCalls->OUTPUT_RESULTS."/".$fileName;
        
        $results = $this->objOSCalls->ejecutarCmd($cmd, $fileName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>json_encode($results)
        ));
        // return ( );
    }  


    function ejecutarOSINTClearbit() {
        $data=$_POST['data'];
        $fuente = strval(trim($this->input->post("fuente")));
        $id_perfil=intval($this->session->userdata("id_perfil"));
        $fuente = strval(trim($this->input->post("fuente")));
        if($id_perfil==2){
            $datos["noErrores"]=1;
            $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        $where["cp.id_persona"]=intval($data);
        $where["cc.status"]=strval("A");
        $where["cc.borrado"]=intval(0);
        $where["cf.fuente"]=$fuente;
        $this->db->select("cc.id_contrato");
        $this->db->from("clientes_contratos cc");
        $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
        $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
        $this->db->where($where);
        $rC = $this->db->get();
        if($rC->num_rows()==0) {
            //$data=Array();
            $datos["noErrores"]=1;
            $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
            //$data["json"]=$datos;
            //array_push($data["json"],$datos);
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        //$fileName = self::OUTPUT_RESULTS."/".self::getUniqueName().".json";
        $fileName = $this->objOSCalls->getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/fromClearbit.py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = $this->objOSCalls->OUTPUT_RESULTS."/".$fileName;
        
        $results = $this->objOSCalls->ejecutarCmd($cmd, $fileName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>json_encode($results)
        ));
        // return ( );
    }    

    function ejecutarOSINTHunter() {
        $data=$_POST['data'];
        $fuente = strval(trim($this->input->post("fuente")));
        $id_perfil=intval($this->session->userdata("id_perfil"));
        $fuente = strval(trim($this->input->post("fuente")));
        if($id_perfil==2){
            $datos["noErrores"]=1;
            $datos["mensaje"] = " No tiene acceso a esta acción por medio del cliente";
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        $where["cp.id_persona"]=intval($data);
        $where["cc.status"]=strval("A");
        $where["cc.borrado"]=intval(0);
        $where["cf.fuente"]=$fuente;
        $this->db->select("cc.id_contrato");
        $this->db->from("clientes_contratos cc");
        $this->db->join("contratos_fuentes cf","cc.id_contrato=cf.id_contrato");
        $this->db->join("cat_personas cp","cc.id_cliente=cp.id_cliente");
        $this->db->where($where);
        $rC = $this->db->get();
        if($rC->num_rows()==0) {
            //$data=Array();
            $datos["noErrores"]=1;
            $datos["mensaje"]=" no tiene acceso a esta fuente en su contrato activo";
            //$data["json"]=$datos;
            //array_push($data["json"],$datos);
            print json_encode(array(
                "name"=>intval($data), 
                "cmd"=>"x",
                "json"=>$datos
            ));
            die();
        }
        // $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

        // $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
        // $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();
        //$fileName = self::OUTPUT_RESULTS."/".self::getUniqueName().".json";
        $fileName = $this->objOSCalls->getUniqueName().".json";

        //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
        $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/fromHunter.py ".$data." ".$fileName;
        //echo $cmd; die();
        $fileName = $this->objOSCalls->OUTPUT_RESULTS."/".$fileName;
        
        $results = $this->objOSCalls->ejecutarCmd($cmd, $fileName);
        //echo $results.' ok ';

        print json_encode(array(
            "name"=>$data, 
            "cmd"=>$cmd,
            "json"=>json_encode($results)
        ));
        // return ( );
    }   

}

// class TicDefenseToolsController extends CI_Controller {

//     //const AIF_PATH = "/var/www/threats/www/huella";
//     const OUTPUT_RESULTS = "/var/www/threats/www/huella/assets/documents";
//     const MAX_TIME = 0;
// 	/**
// 	 * Identificador de sesion activa
// 	 *
// 	 * @var string
// 	 */
// 	public static $id = NULL;    

// 	function __construct(){
//         parent::__construct();
//         $this->load->model("OSRFrameworkModel");
//         // $this->load->library("Aif/Aif");
//         // $this->load->library("Aif/AifRequest");
//         // $this->load->library("Aif/MonitorModelOsintOsrframework");
//         // $this->load->library("Aif/AifDb");
//     }

//     function index() {
//         $params = array("menu_expandido"=>"15", "pantalla"=>"22");
//         makeDefaultLayout(
//             "esociales/ticdefensetoolsView",
//             $params, 
//             array(
//                 'assets/template/plugins/select2/dist/js/select2.js',
//                 'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
//                 'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
//                 'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
//                 'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
//                 'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
//                 "assets/js/global.js",
//                 "assets/js/logos.js",
//                 "assets/js/esociales/ticdefensetools.js"
//             ),
//             array(
//                 'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
//                 'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
//                 'assets/template/plugins/select2/dist/css/select2.css',
//             )
//         );
//     }

//     // function getUsufyPlatforms() {
        
//     //     // $data = AifRequest::getPost('search');
//     //     // $r = MonitorModelOsintOsrframework::getUsufyPlatforms($data);
        
//     //     // Aif::$render = "json";
//     //     // Monitor::out($r);

//     //     $data = $this->OSRFrameworkModel->getUsufyPlatforms();
//     //     print json_encode($data);        
//     // }

//     // function getSearchfyPlatforms() {
//     //     $data = $this->OSRFrameworkModel->getSearchfyPlatforms();
//     //     print json_encode($data);        
//     // }

//     // function getMailfyPlatforms() {
//     //     $data = $this->OSRFrameworkModel->getMailfyPlatforms();
//     //     print json_encode($data);        
//     // }

//     // function getPhonefyPlatforms() {
//     //     $data = $this->OSRFrameworkModel->getPhonefyPlatforms();
//     //     print json_encode($data);        
//     // }

//     function ejecutarUsufy() {
//         //$p = $this->plataformasToStr($plataformas, "usufy");
//         $data=$_POST['data'];
//         $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

//         $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
//         $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();

//         //$cmd = $this->osr."usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
//         $cmd = "usufy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
//         // echo $cmd; die();
        
//         $r = $this->ejecutarCmd($cmd, $folderName);

//         print json_encode(array(
//             "name"=>$data, 
//             "cmd"=>$cmd,
//             "json"=>$r
//         ));
//     }

//     // function ejecutarSearchfy() {
//     //     $data = AifRequest::getPost('data');
//     //     $plataformas = AifRequest::getPost('plataformas');
//     //     $osr = new MonitorClassOsrframework();
        
//     //     $r = $osr->ejecutarSearchfy($data, $plataformas);
    
//     //     Aif::$render = "json";
//     //     Monitor::out($r);
//     // }

//     public function getUniqueName() {
//         $micro = str_replace(".","",microtime()); 
// 		return $this->get_idsession('id')."_".str_replace(" ","",$micro);	
//     }

//     private function plataformasToStr($plataformas, $modulo) {
//         $lenPlataformas = is_array($plataformas)?count($plataformas):0;
//         $p="";

//         if($lenPlataformas>0) {
//             $p = "-p ".implode(" ", $plataformas);
//         } else {
//             //si el usuario especificÃ³ plataformas entonces excluimos algunas (solo en usufy)
//             if($modulo == "usufy") {
//                 $p = " -x ".implode(" ", $this->usufyExcludes);
//             }
//         }
//         return $p;
//     }

//     public function ejecutarSearchfy() {
//         $data=$_POST['data'];
//         $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

//         $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "searchfy") : "";
//         $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();

//         $cmd = "searchfy -q ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
//         //$cmd = "/usr/local/bin/searchfy  -q ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
//         $results = $this->ejecutarCmd($cmd, $folderName);
//         //echo $results.' ok ';

//         print json_encode(array(
//             "name"=>$data, 
//             "cmd"=>$cmd,
//             "json"=>$results
//         ));
//         // return ( );
//     }

//     public function ejecutarMailfy() {
//         $data=$_POST['data'];
//         $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

//         $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "mailfy") : "";
//         $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();

//         $cmd = "mailfy -m ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
//         //$cmd = "/usr/local/bin/searchfy  -q ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
//         $results = $this->ejecutarCmd($cmd, $folderName);
//         // echo $results.' ok ';

//         print json_encode(array(
//             "name"=>$data, 
//             "cmd"=>$cmd,
//             "json"=>$results
//         ));
//         // return ( );
//     }    

//     public function ejecutarPhonefy() {
//         $data=$_POST['data'];
//         $plataformas=isset($_POST['plataformas']) ? $_POST['plataformas']: "";

//         $p = (is_array($plataformas) && count($plataformas)>0) ? $this->plataformasToStr($plataformas, "mailfy") : "";
//         $folderName = self::OUTPUT_RESULTS."/".self::getUniqueName();

//         $cmd = "phonefy -n ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
//         //$cmd = "/usr/local/bin/searchfy  -q ".escapeshellcmd($data)." ".$p." -e json -o ".$folderName;
//         $results = $this->ejecutarCmd($cmd, $folderName);
//         //echo $results.' ok ';

//         print json_encode(array(
//             "name"=>$data, 
//             "cmd"=>$cmd,
//             "json"=>$results
//         ));
//         // return ( );
//     }  

//     private function ejecutarCmd($cmd, $resultsFolder) {
//         $completeCmd = $cmd;//self::PYTHON_PATH." ". self::LAUNCHER ." ".$cmd;
//         set_time_limit(self::MAX_TIME);

//         // echo $cmd.'<br />';
//         // echo $resultsFolder;
//         // die();
        
//         $o = shell_exec ($completeCmd." 2>&1");
//         //var_dump($o);
//         $jsonFile = $resultsFolder."/profiles.json";
//         // echo $completeCmd.'<br />';
//         // echo $resultsFolder;
//         // echo '<br />'.$jsonFile;
//         // die();
//         return $this->getResultsAndCleanPath($resultsFolder, $jsonFile);
//     }
    
//     private function getResultsAndCleanPath($folder, $file) {
//         $result = "";
//         try {
//             if(file_exists($file)) {
//                 $result = file_get_contents($file);
//                 // echo $file."<br />".$result."<br />";
//                 // die();
//                 unlink($file);
//             }else{
//                 $result="[]";
//             }
//             if(is_dir($folder)) {
//                 //AifLibFile::removeDir($folder);
//                 $this->removeDir($folder);
//             }
//             // echo $result;
//             // die();
//             return $result;    
//         } catch(Exception $e){ 
//             echo $e."<br />";
//              die();
//             return $result; 
//         }
//     }    

//   /**
//    * Elimina un directorio de forma recursiva
//    * @param string $dir
//    * @return boolean
//    */
//   public function removeDir($dir){
//     if (!file_exists($dir)) return TRUE;
//     if (!is_dir($dir) || is_link($dir)) return unlink($dir);
//     $ar = scandir($dir);
//     foreach ($ar as $item){
//       if ($item == '.' || $item == '..') continue;
//       if (!self::removeDir($dir . "/" . $item)) return FALSE;
//     }
//     return rmdir($dir);
//   }    

// /**
// 	 * Comprueba si existe una variable en la sesion
// 	 *
// 	 * @param string $var
// 	 *        	Nombre de la variable
// 	 * @return boolean
// 	 */
// 	public  function isVar($var) {
// 		return isset ( $_SESSION [self::$id] ) && isset ( $_SESSION [self::$id] [$var] ) ? true : false;
// 	}
// 	/**
// 	 * Regresa un valor de una variable en la sesion
// 	 *
// 	 * @param string $var
// 	 *        	Nombre de la variable de session
// 	 * @return mixed Regresa el valor de la variable de session
// 	 */
// 	public  function get_idsession($var) {
// 		return  $this->isVar ( $var ) ? $_SESSION [self::$id] [$var] : false;
// 	}
// }