<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnalyzerController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("AnalizerModel");
    }

    function index() {
        $params = array("menu_expandido"=>"13", "pantalla"=>"44");
        makeDefaultLayout(
            "personas/catPersonasAnalyzerView", 
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                "assets/js/global.js",
                "assets/js/analyzer/index.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css',
            )
        );
    }

    function listar() {
         $r = $this->AnalizerModel->listarPersonas();
         print json_encode($r);
    }

    function formulario() {
        $id_persona = intval($this->uri->segment(3));
        $person = null;
        if($id_persona > 0) {
            $person = $this->CatPersonasModel->getPersonaById($id_persona);
        }

        $params = array("menu_expandido"=>"13", "pantalla"=>"44", "person"=>$person);
        makeDefaultLayout(
            "personas/formulario",
            $params, 
            array(
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                "assets/js/global.js",
                "assets/js/catPersonas/formulario.js"
            ),
            array(
                'assets/template/plugins/select2/dist/css/select2.min.css'
            )
        );       
    }

    function validarPersona(&$d) {
        if(empty($d)) {
            return array("title"=>"Error", "msg"=>"Parametros incorrectos", "type"=>"error");
        }
        if(empty($d["paterno"])) {
            return array("title"=>"Error", "msg"=>"El apellido paterno es obligatorio", "type"=>"error");
        }
        if(empty($d["nombre"])) {
            return array("title"=>"Error", "msg"=>"El nombre es obligatorio", "type"=>"error");
        }
        if(empty($d["email"])) {
            return array("title"=>"Error", "msg"=>"El email es obligatorio", "type"=>"error");
        }
        return array("error"=>"0");
    }

    function iniciarAnalisis() {
        $d = $this->input->post("data");
        // $r = $this->validarPersona($d); // $d["paterno"]
        $keywords = preg_split("/[,]/", $d["ids"]);
        foreach($keywords as $idper){
            // $data['subriesgo'] = $riesgox;
            // $r = $this->validarSubriesgo($data);
            // if($r["error"] === "0") {
            // $data['fecha_programacion'] = $riesgox;
            // $this->AnalizerModel->set('fecha_programacion', 'now()', TRUE);
            // $this->db->set('user_registered', mdate("%Y-%m-%d %H:%i:%s"));
            // $data['fecha_programacion'] = mdate("%Y-%m-%d %H:%i:%s");
            $data['status'] = 'E';
            $data['tipo_entrada'] = 'W';
            // $data['escenarios'] = 'O,W,RD,DW,N';
            $data['escenarios'] = $this->AnalizerModel->getFuentes();
            $data['resultado'] = -1;
            $data['id_persona'] = $idper;
            $data['id_puesto'] = $d["idpuesto"];
            $data['id_session'] = $_SESSION['id_session'];
            // $data['id_api'] = '';
            $this->AnalizerModel->generaAnalisis($data);
            // }
        }
        $r=array("title"=>"Programacion Realizada", "msg"=>"Para dar seguimiento a analisis, ir a pantalla Personas / Resultados", "type"=>"info");
        
        print json_encode($r);
    }

    function getEmpresas() {
        $this->load->model("ClientesModel");
        print json_encode($this->ClientesModel->getClientesForCombo());
    }

    function borrarPersona() {
        $id = intval($this->input->post("data"));
        print json_encode($this->CatPersonasModel->borrarPersona($id));
    }

    // function uploadLayout(){
    //     include APPPATH . 'third_party/phpExcel/Classes/PHPExcel.php';
    //     include APPPATH . 'third_party/phpExcel/Classes/PHPExcel/IOFactory.php';

    //     $this->load->model("ApiRestModel");

    //     $ruta = BASEPATH.'../assets/layouts_temp/';
    //     //echo $ruta; die();
    //     $file = 'layoutpersonas.xlsx';
    //     //require_once dirname(__FILE__) . '/../Classes/PHPExcel/IOFactory.php';    

    //     $objReader = PHPExcel_IOFactory::createReader('Excel2007');
    //     $objReader->setReadDataOnly(true); //optional
        
    //     $objPHPExcel = $objReader->load($ruta.$file);
    //     $objWorksheet = $objPHPExcel->getActiveSheet();
        
    //     $i=1;
    //     foreach ($objWorksheet->getRowIterator() as $row) {
        
    //         // $column_A_Value = $objPHPExcel->getActiveSheet()->getCell("A$i")->getValue();//column A
    //         //you can add your own columns B, C, D etc.
    //         if ($i>1){
    //             $obj['ine'] = trim($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue());
    //             $obj['curp'] = trim($objPHPExcel->getActiveSheet()->getCell("B$i")->getValue());
    //             $obj['paterno'] = trim($objPHPExcel->getActiveSheet()->getCell("C$i")->getValue());
    //             $obj['materno'] = trim($objPHPExcel->getActiveSheet()->getCell("D$i")->getValue());
    //             $obj['nombre'] = trim($objPHPExcel->getActiveSheet()->getCell("E$i")->getValue());
    //             $obj['email'] = trim($objPHPExcel->getActiveSheet()->getCell("F$i")->getValue());
    //             $obj['celular'] = trim($objPHPExcel->getActiveSheet()->getCell("G$i")->getValue());
    //             $obj['alias'] = trim($objPHPExcel->getActiveSheet()->getCell("H$i")->getValue());
    //             $obj['direccion'] = trim($objPHPExcel->getActiveSheet()->getCell("I$i")->getValue());
    //             $obj['twitter'] = trim($objPHPExcel->getActiveSheet()->getCell("J$i")->getValue());
    //             $obj['linkedin'] = trim($objPHPExcel->getActiveSheet()->getCell("K$i")->getValue());
    //             $obj['id_cliente']=$_SESSION['id_usuario'];
 
    //             $actualizado = false;
    //             // primero checamos que no exista CURP o INE, en caso de que existan actualizamos registro pero para EL CLIENTE...
    //             if (!empty($obj['ine']) && strlen(trim($obj['ine']))>0){
    //                 $rPersona = $this->ApiRestModel->getPersonaByField('ine',trim($obj['ine']),$obj['id_cliente']);
    //                 if ($rPersona){
    //                     //var_dump($obj); die();
    //                     $actualizado = $this->ApiRestModel->actualizarPersonaByField('ine',$obj['ine'],$obj);
    //                 }else if (!empty($obj['curp'])  && strlen(trim($obj['curp']))>0){
    //                     $rPersona = $this->ApiRestModel->getPersonaByField('curp',trim($obj['curp']),$obj['id_cliente']);
    //                     //var_dump($rPersona); die();
    //                     if ($rPersona)
    //                         $actualizado = $this->ApiRestModel->actualizarPersonaByField('curp',$obj['curp'],$obj);
    //                 }
    //             }else if (!empty($obj['curp'])  && strlen(trim($obj['curp']))>0){
    //                 $rPersona = $this->ApiRestModel->getPersonaByField('curp',trim($obj['curp']),$obj['id_cliente']);
    //                 if ($rPersona)
    //                 $actualizado = $this->ApiRestModel->actualizarPersonaByField('curp',$obj['curp'],$obj);
    //             }
                
    //             // En caso de que no se haya actualizado el registro de la persona y el cliente, insertamos
    //             if (!$actualizado)
    //                 $insertado = $this->ApiRestModel->insertarPersona($obj);
    //         }
        
    //         $i++;
    //     }        
       


    // }
    function exportData()
    {
        if (trim($_GET['format'])=='CSV'){
            $this->exportCSV();
            return;
        }

        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel.php';
        include APPPATH . 'third_party/phpExcel/Classes/PHPExcel/IOFactory.php';
        // Import classes.
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\Spreadsheet';
        // include APPPATH . 'PhpOffice\PhpSpreadsheet\IOFactory';

        $filename = 'personas';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $data = $this->CatPersonasModel->listar(true);

        $rowCount = 1;
        // $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, 'Tipo Entrada');
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, 'Cliente');
        // $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, 'Fecha Resultado');
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, 'Curp');
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, 'Nombre');
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, 'Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, 'Celular');
        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, 'INE');
        $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, 'Direccion');
        // $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, 'Ponderacion');
        // $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, 'Puesto');
        // $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, 'Riesgo');

        //var_dump($data["data"]); die();

        //while($row = $data["data"]){ 
        foreach ($data["data"] as $row) {
            //var_dump($row); die();
            $rowCount++;
            // $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row['tipo_entrada']);
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row['empresa']);
            // $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['fecha_resultado']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['curp']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['nombre']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['email']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['celular']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['ine']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row['direccion']);
            // $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $row['ponderacion']);
            // $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $row['puesto']);
            // $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $row['riesgo']);
        }

        // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        // $objWriter->save('some_excel_file.xlsx');   
        header("Pragma: public");
        header("Expires: 0");
        // header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');

        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        // $objWriter->setSheetIndex(0);   // Select which sheet.
        // $objWriter->setDelimiter(';');  // Define delimiter
        // $objWriter->save('my-excel-file.csv');        
    }

    private function exportCSV()
    {
        // filename for download
        $filename = "catpersonas_" . date('Ymd') . ".csv";

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv");

        $out = fopen("php://output", 'w');

        $flag = false;
        //$data = $this->ResultadosOdometroModel->verResultados(true);
        $data = $this->CatPersonasModel->listar(true);
        // var_dump($data["data"]); die();
        //while (false !== ($row = pg_fetch_assoc($result))) {
        foreach ($data["data"] as $row) {
            // $datax['tipo_entrada']=($row['tipo_entrada']);
            $datax['cliente']=($row['empresa']);
            // $datax['fecha_resultado']=($row['fecha_resultado']);
            $datax['curp']=($row['curp']);
            $datax['nombre']=($row['nombre']);
            $datax['email']=($row['email']);
            $datax['celular']=($row['celular']);
            $datax['ine']=($row['ine']);
            $datax['direccion']=($row['direccion']);
            // $datax['ponderacion']=($row['ponderacion']);
            // $datax['puesto']=($row['puesto']);
            // $datax['riesgo']=($row['riesgo']);
        
            if (!$flag) {
                // display field/column names as first row
                fputcsv($out, array_keys($datax), ',', '"');
                $flag = true;
            }
            array_walk($datax, __NAMESPACE__ . '\cleanData');
            fputcsv($out, array_values($datax), ',', '"');
        }

        fclose($out);
    }
}

function cleanData(&$str)
{
    if ($str == 't') $str = 'TRUE';
    if ($str == 'f') $str = 'FALSE';
    if (preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
        $str = "'$str";
    }
    if (strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}