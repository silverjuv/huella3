<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MonitorController extends CI_Controller {

	function __construct(){
        parent::__construct();
        //$this->load->model("MonitorModel");
    }

    function index() {
        $params = array("menu_expandido"=>"13", "pantalla"=>"29");
        makeDefaultLayout(
            "procesos/monitorView",
            $params, 
            array(
                'assets/template/plugins/loadingPlugin/jquery.preloaders.min.js',
                "assets/js/global.js",
                "assets/js/procesos/monitor.js",
                "assets/template/js/jquery.highlight.js"
            ),
            array(
                'assets/template/css/jquery.highlight.css',
                
            )
        );
    }    

    function reloadLog(){
        // $data['log'] = join(PHP_EOL,array_slice(explode("\n",file_get_contents('../scripts_huella/pyhuella/logs/huellamon.log')), -1*$_POST['size']-1));
        $data['log'] = join(PHP_EOL,array_slice(explode("\n",file_get_contents('/home/threats/demon/crontab_proccess/huella_proccess/scripts_analys/logs/huellapp.log')), -1*$_POST['size']-1));
        print json_encode($data);
    }
    
    function startHuellamon() {
        $statuscode = "off-on\n";
        // Escribir los contenidos en el fichero,
        // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
        file_put_contents('../scripts_huella/pyhuella/statusCodeHuellamon.txt', $statuscode, LOCK_EX);
        $data['status']=$statuscode;
        print json_encode($data);
    }

    function stopHuellamon() {
        $statuscode = "on-off\n";
        // Escribir los contenidos en el fichero,
        // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
        file_put_contents('../scripts_huella/pyhuella/statusCodeHuellamon.txt', $statuscode, LOCK_EX);
        $data['status']=$statuscode;
        print json_encode($data);
    }

    function restartHuellamon() {
        $statuscode = "restart\n";
        // Escribir los contenidos en el fichero,
        // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
        file_put_contents('../scripts_huella/pyhuella/statusCodeHuellamon.txt', $statuscode, LOCK_EX);
        $data['status']=$statuscode;
        print json_encode($data);
    }

    function checaDemonio(){
        set_time_limit(0);
        // $path_output = "/var/www/threats/www/huella/assets/documents";

        // if ($_POST['dem']=='hg'){
        //     $demon = 'huellamongen.py';
        //     $file = $path_output.'/statusDemonio.txt';
        // }
        // if ($_POST['dem']=='hs'){
        //     $demon = 'hilos.py';
        //     $file = $path_output.'/statusDemonioHilos.txt';
        // }
        // if ($_POST['dem']=='notics'){
        //     $demon = 'Main_v4.js';
        //     $file = $path_output.'/statusDemonioNoticias.txt';
        // }
        if ($_POST['dem']=='procs_osint'){
            $demon = PROCESOS_OSINT;
            $file = OUTPUT_RESULTS_PPERSONAS.'/statusDemonioProcsOSINT.txt';
        }
        if ($_POST['dem']=='procs_pers'){
            $demon = PROCESOS_PERSONAS;
            $file = OUTPUT_RESULTS_PPERSONAS.'/statusDemonioProcsPersonas.txt';
        }
        if ($_POST['dem']=='procs_ppers'){
            $demon = PROCESA_PUESTOPERSONAS;
            $file = OUTPUT_RESULTS_PPERSONAS.'/statusDemonioPuestosPersonas.txt';
        }  
        if ($_POST['dem']=='proc_nots'){
            $demon = CAPTURA_NOTICIAS;
            $file = OUTPUT_RESULTS_PPERSONAS.'/statusDemonioNoticias.txt';
        } 
        if ($_POST['dem']=='proc_darkw'){
            $demon = CAPTURA_DARKWEB;
            $file = OUTPUT_RESULTS_PPERSONAS.'/statusDemonioDarkWeb.txt';
        }        

        //$demon = 'quepedoconesto';
        $comandoEnv = "cd ".PATH_SCRIPTS_PYHUELLA." ;   python3 ";
        // $comandoEnvFinal = " ; deactivate ";

        //$cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/script_status_demonios.py ".$demon;
        // se requiere que carpeta de salida ./documents sea chmod 775
        $cmd = $comandoEnv." script_status_demonios.py ".$demon." ".$file;
        // echo $cmd;

        $o = shell_exec ($cmd." 2>&1");
        // var_dump($o);
        // exec( $cmd, $o, $return_var );
        // var_dump($o);
        // leemos status
        $result='';
        if(file_exists($file))
            $result = file_get_contents($file);

        print json_encode($result);
    }

    // // IMPOSIBLEW NO FUNCIONA SE QUEDA BLOQUEADO EL APACHE... 
    // function iniciaDemonioGeneral(){
    //     set_time_limit(0);
    //     // $path_output = "/var/www/threats/www/huella/assets/documents";

    //     // if ($_POST['dem']=='hg'){
    //     //     $demon = 'huellamongen.py';
    //     //     $file = $path_output.'/statusDemonio.txt';
    //     // }
    //     // if ($_POST['dem']=='hs'){
    //     //     $demon = 'hilos.py';
    //     //     $file = $path_output.'/statusDemonioHilos.txt';
    //     // }

    //     //$demon = 'quepedoconesto';

        

    //     $cmd = "/var/www/threats/www/scripts_huella/pyhuella/huellamongen.sh &";
    //     $o = shell_exec ($cmd." ");
    //     // exec( $cmd, $o, $return_var );
    //     // $cmd = "/usr/bin/python3.6 /var/www/threats/www/scripts_huella/pyhuella/huellamongen.py ";
    //     // $o = shell_exec ($cmd." 2>&1");
    //     // leemos status
    //     var_dump($o);
    //     $result='Demonio general iniciado';

    //     print json_encode($result);
    // }
}