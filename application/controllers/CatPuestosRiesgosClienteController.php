<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatPuestosRiesgosClienteController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("CatPuestosRiesgosModel");
    }

    function index() {
        $params = array("menu_expandido"=>"0", "pantalla"=>"45", "idc" => intval($this->input->get("idc")), 
                    "idp" => intval($this->input->get("idp")));
        makeDefaultLayout(
            "catPuestosRiesgosClienteView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.min.js',
                'assets/template/plugins/bootstrapValidator/bootstrapValidator.js',
                'assets/template/plugins/jqueryform/jquery.form.js',
                'assets/template/plugins/jqueryform/jquery.parser.js',                
                "assets/js/global.js",
                "assets/js/catPuestosRiesgosCliente.js"
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/bootstrapValidator/bootstrapValidator.css',
                'assets/template/plugins/select2/dist/css/select2.min.css'
            )
        );
    }

    function validar(&$data) {
        if(empty($data["puesto"])) {
            return array("error"=>"1","msg"=>"El campo puesto es obligatorio");
        }
        return array("error"=>"0");
    }

    function guardar() {
        $data["puesto"]=strval($this->input->post("descripcion"));      
        $r = $this->validar($data);
        if($r["error"] === "0") {
            $id_puesto = intval($this->input->post("id_puesto"));
            $id_cliente = $_SESSION['id_cliente'];
            if(strval($this->input->post("action")) == "0") {
                $r = $this->CatPuestosRiesgosModel->actualizarPuesto($id_puesto,$id_cliente,$data);
            } else if(strval($this->input->post("action")) == "1") {
                $r = $this->CatPuestosRiesgosModel->insertarPuesto($id_cliente,$data);
            }
            
        }
        print json_encode($r);
    }

    function eliminar(){
        $id_puesto = intval($this->input->post("id_puesto"));
        $id_cliente = $_SESSION['id_cliente'];
        $data = $this->CatPuestosRiesgosModel->eliminar($id_puesto,$id_cliente);
        print json_encode($data);
    }

    function listarClientes() {
        $data =$this->CatPuestosRiesgosModel->listarClientes();
        print json_encode($data);
    }

    function listarPuestos() {
        $data =$this->CatPuestosRiesgosModel->listarPuestos();
        print json_encode($data);
    }

    function listarRiesgos() {
        $data = $this->CatPuestosRiesgosModel->listarRiesgos();
        print json_encode($data);
    }

    function listarRiesgosSubriesgos() {
        $id_riesgo = intval($this->input->post("id_riesgo"));
        $data = $this->CatPuestosRiesgosModel->listarRiesgosSubriesgos($id_riesgo);
        print json_encode($data);
    }

    function listarPuestosRiesgos() {
        $id_puesto = intval($this->input->post("id_puesto"));
        $id_cliente = $_SESSION['id_cliente'];
        $data = $this->CatPuestosRiesgosModel->listarPuestosRiesgos($id_puesto,$id_cliente);
        print json_encode($data);
    }

    function insertar() {
        $id_puesto = intval($this->input->post("id_puesto"));
        $id_riesgo = intval($this->input->post("id_riesgo"));
        // $id_cliente = intval($this->input->post("id_cliente"));
        $id_cliente = $_SESSION['id_cliente'];
        $data = $this->CatPuestosRiesgosModel->insertar($id_puesto, $id_riesgo,$id_cliente);
        print json_encode($data);
    }

    function quitarRiesgoPuesto() {
        $id_puesto = intval($this->input->post("id_puesto"));
        $id_riesgo = intval($this->input->post("id_riesgo"));
        // $id_cliente = intval($this->input->post("id_cliente"));
        $id_cliente = $_SESSION['id_cliente'];
        $data = $this->CatPuestosRiesgosModel->quitarRiesgoPuesto($id_puesto, $id_riesgo,$id_cliente);
        print json_encode($data);
    }

}