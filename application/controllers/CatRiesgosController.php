<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatRiesgosController extends CI_Controller { 
    
	function __construct(){
        parent::__construct();
        $this->load->model("CatRiesgosModel");
    }
 
    function index() {
        $params = array("menu_expandido"=>"3", "pantalla"=>"24");
        makeDefaultLayout(
            "catRiesgosView",
            $params, 
            array(
                'assets/template/plugins/DataTables/media/js/jquery.dataTables.js',
                'assets/template/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
                'assets/template/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
                'assets/template/plugins/DataTables/extensions/Select/js/dataTables.select.min.js',
                'assets/template/plugins/select2/dist/js/select2.full.min.js',
                'assets/template/plugins/bootstrapValidator/bootstrapValidator.js',
                'assets/template/plugins/jqueryform/jquery.form.js',
                'assets/template/plugins/jqueryform/jquery.parser.js',
                'assets/template/plugins/ion.rangeSlider/js/ion.rangeSlider.min.js',
                'assets/js/global.js',
                'assets/js/catRiesgos.js'
            ),
            array(
                'assets/template/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
                'assets/template/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
                'assets/template/plugins/select2/dist/css/select2.min.css',
                'assets/template/plugins/bootstrapValidator/bootstrapValidator.css',
                'assets/template/plugins/ion.rangeSlider/css/ion.rangeSlider.min.css',
            )
        );
    }

    function listarRiesgos(){
        $data = $this->CatRiesgosModel->listarRiesgos();
        print json_encode($data);
    }
    
    function listarSubriesgos(){
        $data = $this->CatRiesgosModel->listarSubriesgos();
        print json_encode($data);
    }

    function listarAreaRiesgos(){
        $data = $this->CatRiesgosModel->listarAreaRiesgos();
        print json_encode($data);
    }

    private function validar(&$data) {
        if(empty($data["riesgo"])) {
            return array("error"=>"1","msg"=>"El campo riesgo es obligatorio");
        }
        if(empty($data["ponderacion"])){
            return array("error"=>"1","msg"=>"El campo ponderacion es obligatorio");
        }
        return array("error"=>"0");
    }

    private function validarSubriesgo(&$data){
        if(empty($data["id_riesgo"])||intval($data["id_riesgo"] == 0)) {
            return array("error"=>"1","msg"=>"El campo id_riesgo es obligatorio");
        }
        if(empty($data["subriesgo"])||strval($data["subriesgo"] == "")){
            return array("error"=>"1","msg"=>"El campo subriesgo es obligatorio");
        }
        return array("error"=>"0");
    }

    private function Quitar_Espacios($Frase){
        $array = explode(' ',$Frase);  // convierte en array separa por espacios;
        $salida ='';
        for ($i=0; $i < count($array); $i++) { 
            if(strlen($array[$i])>0) {
                $salida.= ' ' . $array[$i];
            }
        }
        return  trim($salida);
    }

    function guardar() {
        $verbo = ($this->input->post('verbo') !== null) ? $this->input->post('verbo') :false;
        $riesgo = $this->Quitar_Espacios(strtolower(strval($this->input->post("descripcion"))));
        $data["ponderacion"] = intval($this->input->post("ponderacion"));
        $data["id_area_riesgo"] = intval($this->input->post("id_area"));
        $data["riesgo"] = $riesgo;      
        $data["verbo"] =  intVal(boolval($verbo));
        
        $r = $this->validar($data);
        if($r["error"] === "0") {
            $id = intval($this->input->post("id_riesgo"));
            if(strval($this->input->post("action")) == "0") {
                $r = $this->CatRiesgosModel->actualizar($id, $data);
            } else if(strval($this->input->post("action")) == "1") {
                // $data["verbo"] =  intVal(boolval($verbo));
                $r = $this->CatRiesgosModel->insertar($data);
            }
            
        }
        print json_encode($r);
    }

    function eliminar(){
        $id = intval($this->input->post("id_riesgo"));
        if(empty($id)||intval($id == 0)) {
            return array("error"=>"1","msg"=>"El campo id_riesgo es obligatorio");
        }
        $data = $this->CatRiesgosModel->eliminar($id);
        print json_encode($data);
    }

    function guardarSubriesgo(){
        $verbo = ($this->input->post('verbo') !== null) ? $this->input->post('verbo') :false;
        $subriesgo = $this->Quitar_Espacios(strtolower(strval($this->input->post("subriesgo"))));
        $data["verbo"] =  intVal(boolval($verbo));
        $data["id_riesgo"] = intval($this->input->post("id_riesgo"));

        $keywords = preg_split("/[,]/", $subriesgo);
        foreach($keywords as $riesgox){
            $data['subriesgo'] = $riesgox;
            $r = $this->validarSubriesgo($data);
            if($r["error"] === "0") {
                $r = $this->CatRiesgosModel->guardarSubriesgo($data);
            }
        }

        
        print json_encode($r);
    }

    function eliminarSubriesgo(){
        $data["id_riesgo"] = intval($this->input->post("id_riesgo"));
        $data['subriesgo'] = strval(trim($this->input->post("subriesgo")));
        $r = $this->validarSubriesgo($data);
        if($r["error"] === "0") {
            $data = $this->CatRiesgosModel->eliminarSubriesgo($data);
        }    
        print json_encode($data);
    }
}    