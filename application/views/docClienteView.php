<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Doc Cliente</a></li>
    <li class="breadcrumb-item active">Documentacion</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Uso del Aplicativo Web</h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Uso del Aplicativo Web</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 id="Una_Ejemplo_de_un_Sitio_Web_con_PHP">1. ¿Que podemos hacer dentro de este aplicativo?</h3>
                <p>El Aplicativo de Digital Risk Human (DRH) fue realizado para analizar las personas que van a ocupar
                    un puesto en alguna empresa o centro de trabajo. Analizando por riesgos especificados que tan
                    confiable es una persona para ocupar un cargo o puesto dentro de la dependencia de trabajo.</p>
                <p>Al ingresar al aplicativo web, se tiene un resumen por cliente de los procesamientos realizados cada mes, un promedio
                    de resultados generados, cuales son los riesgos más encontrados al momento de analizar su personal, y cuales son los promeidos de los
                    rangos de resultados obtenidos en el análisis de personas-puesto.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/dashboard.png"; ?>"> -->
                        <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/dashboard.png"; ?>" alt="Dahsboard" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>En la parte inferior de la página de inicio se encuentra información del último contrato registrado por el cliente y que tipo
                    de contrato tiene.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg19.png"; ?>"> -->
                        <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg19.png"; ?>" alt="Dahsboard" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>En lo que se refiere al análisis de personas-puesto, para analizar una persona se tiene acceso a las siguientes acciones dentro del aplicativo web:</p>
                <ul>
                    <li>Cargar un catalogo de personas, para capturar los datos personales de las personas a analizar
                    </li>
                    <li>Buscar en noticias, para buscar la persona por algún dato personal y checar si ha aparecido en
                        alguna noticia y que haya cometido algún delito. </li>
                    <li>Buscar en OSINT, usando unas herramientas OSINT se puede checar por email, o alias o teléfono
                        algunos datos de referencia de esa persona.</li>
                    <li>Subir layouts, donde se pueden subir archivos de Excel para asociar una persona - puesto para su
                        análisis, o bien, subir un
                        catálogo completo de personas, y no capturar manualmente.</li>
                    <li>Pantalla de Resultados, donde se puede asociar una persona - puesto para su análisis, y observar
                        la puntuación correspondiente del análisis realizado.</li>
                    <li>Usar el API Rest, que permite desde un lenguaje de programación asociar una persona - puesto
                        para su análisis, este tema se trata más a fondo en el menú Ayuda / Documentación API</li>
                </ul>
                <p>En los puntos siguientes se explican estas pantallas.</p>
                <br />
                <h3 id="Página_Principal">2. Pantalla de Captura de Personas</h3>
                <p style="text-align:justify">Esta pantalla permite capturar los datos personales de las personas a
                    analizar.
                    Para acceder a ella se debe dar clic en el menú Personas y Expedientes / Catálogo de personas.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg1.png"; ?>"> -->
                        <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg1.png"; ?>" alt="Captura de personas" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <br />
                <p style="text-align:justify">Algunas pantallas contienen la opción de exportar el contenido de toda la tabla que se les muestra a los clientes, en la Ventana
                    que les aparece existe la opción de exportar en Excel (xlsx) o bien en CSV. Eliga la opción que mas le convenga.
                </p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg20.png"; ?>"> -->
                        <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg20.png"; ?>" alt="Captura de personas" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <br />
                <p style="text-align:justify">En varias pantallas existen 3 botones, donde el botón <em style="color:#000000"><b>Nuevo</b></em> abre una ventana de captura de personas,
                    el botón <em style="color:#000000"><b>Editar</b></em> abre la misma ventana pero para edición de una
                    persona seleccionada del listado. Y el botón
                    <em style="color:#000000"><b>Borrar</b></em>
                    permite eliminar lógicamente una persona seleccionada del listado. En el caso del botón Editar y Borrar, es necesario tener
                    seleccionado el registro de la tabla que se desea editar o borrar. El borrado es lógico. La pantalla de captura de datos
                    personales es la siguiente:
                </p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg2.png"; ?>"> -->
                        <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg2.png"; ?>" alt="Captura de personas" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <br />
                <p>Como se puede observar, los campos obligatorios son los marcados con <em style="color:#ff0000">*</em>. Los valores
                    aceptados en la captura de personas son:</p>
                <ul>
                    <li>INE, Numero de identificación nacional electoral.</li>
                    <li>CURP, Cláve Única de Registro de Población </li>
                    <li>Apellido Paterno.</li>
                    <li>Apellido Materno.</li>
                    <li>Nombre de la persona a analizar</li>
                    <li>Email</li>
                    <li>Teléfono Celular</li>
                    <li>Alias en Redes Sociales, si es que tiene.</li>
                    <li>Dirección, la captura de la dirección debe tener una longitud mínima de 20 caracteres</li>
                    <li>Empresa, por omisión se elije el cliente actual.</li>
                    <li>Cuenta twitter, en caso de que tuviera cuenta.</li>
                    <li>Cuenta linkedin, en caso de que tuviera cuenta.</li>
                </ul>
                <h3 id="Página_Principal">3. Pantalla de Búsqueda en Noticias</h3>
                <p style="text-align:justify">Esta pantalla permite buscar alguna persona por algún dato personal y
                    checar si ha aparecido en
                    alguna noticia y que haya cometido algún delito.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg3.png"; ?>"> -->
                        <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg3.png"; ?>" alt="Búsqueda de noticias" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <br />
                <p style="text-align:justify">Una vez que se da clic en el botón Buscar, se lista las posibles noticias
                    donde aparece la coincidencia
                    que se esta buscando. En ella se muestra el dominio o sitio web que emite la noticia, el enlace
                    original de la noticia, y en algunos casos, una posible
                    imagen de captura del sitio de la noticia.
                </p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg4.png"; ?>"> -->
                        <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg4.png"; ?>" alt="Búsqueda de noticias" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <br />
                <h3 id="Página_Principal">4. Pantallas de Búsqueda en OSINT</h3>
                <p style="text-align:justify">Esta pantalla permite buscar detalles adicionales de alguna persona usando
                    unas herramientas OSINT se puede checar por email, o alias o teléfono
                    algunos datos de referencia de esa persona. Se implementaron herramientas de 2 servicios OSINT muy conocidos llamadas
                    OSRFramework y SpiderFoot.</p>
                <h4 id="Página_Principal">4.1. OSRFramework</h4>
                <p>La primera que se muestra a continuación es OSRFramework y 2 herramientas útiles, buscar por searchfy
                    y buscar por usufy. La herramienta
                    <em style="color:#000000"><b>searchfy</b></em> encuentra perfiles vinculados a nombres completos. La
                    herramienta <em style="color:#000000"><b>usufy</b></em> identifica perfiles de redes sociales usando
                    un apodo o alias.
                </p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg5.png"; ?>"> -->
                        <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg5.png"; ?>" alt="Búsqueda de ORSFramework" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <br />
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg6.png"; ?>"> -->
                        <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg6.png"; ?>" alt="Búsqueda de ORSFramework" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <h4 id="Página_Principal">4.2. SpiderFoot</h4>
                <p>De este servicio OSINT las herramientas que se implementaron fueron:
                    emailrep, y numverify. La herramienta
                    <em style="color:#000000"><b>emailrep</b></em> permite saber la reputación de un email dado, de
                    acuerdo a varias observaciones que se han hecho sobre su uso en el internet. La
                    herramienta <em style="color:#000000"><b>numverify</b></em> permite verificar un número telefónico.
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg7.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg7.png"; ?>" alt="Búsqueda de ORSFramework" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
                    <br />
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg8.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg8.png"; ?>" alt="Búsqueda de ORSFramework" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
                    <br />
                    <h3 id="Página_Principal">5. Pantalla para subir Layouts</h3>
                    <p style="text-align:justify">Esta pantalla permite subir archivos de Excel para asociar una persona -
                        puesto para su análisis, o bien, subir un
                        catálogo completo de personas, y no capturarlas manualmente. Para el cliente existen 2 layouts que
                        se descargan y una vez llenados
                        se pueden subir la información por esta misma pantalla. Para acceder a esta pantalla debemos dar
                        clic en el menú Procesos / Subir Layouts.</p>
                    <h4 id="Página_Principal">5.1. Subir layout Personas</h4>
                    <p>Los campos para incluir en el layout de Personas son semejantes a los de la captura de personas, que
                        serían: nombre, apellido paterno, apellido materno,
                        ine, curp, email, teléfono celular, alias, direccion, y alguna cuenta de twitter o linkedin. No son
                        obligatorios apellido materno, alias, cuenta de twitter, ni cuenta de linkedin.</p>
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg9.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg9.png"; ?>" alt="Subir Layouts" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
                    <p>Para subir un layout simplemente se descarga el formato de layout que necesitamos subir, lo llenamos
                        con los datos correspondientes, del
                        directorio donde lo tenemos guardado lo arrastramos al panel de carga o bien, damos clic izquierdo
                        del ratón en el panel de carga y buscamos
                        entre nuestros archivos el layout que vamos a subir.
                    </p>
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg10.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg10.png"; ?>" alt="Subir Layouts, arrastrar archivo" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
                    <h4 id="Página_Principal">5.2. Subir layout Personas y Puestos a analizar</h4>
                    <p>Los campos para incluir en el layout de Personas son semejantes a los del layout anterior. La única
                        diferencia es que este layout tiene adicional el campo
                        id_puesto, el cual es obligatorio y debe coincidir con un identificador único de puesto que tiene
                        asignado un cliente en su contrato o cuenta. Para
                        verificar que puestos tiene asignado un cliente existe la pantalla llamada Puestos y riesgos del
                        cliente que se puede acceder desde el menú
                        <em style="color:#000000"><b>Desglose de puestos</b></em>
                    </p>
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg15.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg15.png"; ?>" alt="Subir Layouts, arrastrar archivo" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
                    <p>En esta pantalla se puede observar que riesgos y subriesgos estan asociados a un puesto, y de ahí
                        depende la calificación de una
                        persona para un puesto. Esto es por que si en alguna noticia donde salió esa persona existe el
                        riesgo asociado, se le dará cierta puntuación
                        de riesgo según nuestro catálogos internos de puntajes.</p>
                    <br />
                    <h3 id="Página_Principal">6. Pantalla de Resultados</h3>
                    <p style="text-align:justify">En esta pantalla es donde se puede asociar una persona - puesto para su
                        análisis, y observar la puntuación que se le da a esa persona para tal puesto.
                        Para acceder a esta pantalla debemos dar clic en el menú Personas y Expedientes / Resultados. </p>
                    <p>En esta pantalla existen 2 pestañas, una llamada <em style="color:#000000"><b>Resultados</b></em> que
                        es donde se iran agregando las personas que ya han sido analizadas para tal puesto. La otra
                        pestaña se llama <em style="color:#000000"><b>Personas en proceso</b></em>, que es donde el cliente
                        debe marcar de manera manual una persona para su análisis para un puesto dado.</p>
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg12.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg12.png"; ?>" alt="Pestaña de Resultados" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
                    <p>Dentro de la pestaña <em style="color:#000000"><b>Resultados</b></em> se puede dar clic en una fila y
                        posteriormente dar clic en el botón <em>Ver Informe del Registro seleccionado</em> para abrir ventana emergente y observar de donde se ha
                        obtenido
                        información para llegar a tal ponderación. De igual manera, una vez cargada esta ventana emergente, hay un botón llamado
                        <em>Recibir en email este informe </em> para que el cliente reciba
                        en su correo este informe individual.</p>
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg21.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg21.png"; ?>" alt="" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
                    <p>Dentro de la pestaña <em style="color:#000000"><b>Resultados</b></em> otro botón interesante es <em>Ver Ponderación de Riesgos del Registro seleccionado</em>
                        para abrir ventana emergente y observar un resumen de riesgos asociados a la persona seleccionada.</p>
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg22.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg22.png"; ?>" alt="" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>

                    <p>En el caso de la pestaña para <em style="color:#000000"><b>Enviar a análisis</b></em> a una persona
                        para un puesto dado, se debe dar clic en el botón <em>Enviar persona a analizar</em>. Con ello se
                        abre una ventana
                        modal para asignar una persona a un puesto dado. Es importante recordar que el puesto tiene asignado una serie de riesgos que se encuentran
                        en el análisis de la persona - puesto, causarán un conteo en su ponderación. </p>
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg14.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg14.png"; ?>" alt="Ventana de asignacion persona-puesto" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
                    <br />
                    <h3 id="Página_Principal">7. Pantalla de Expedientes</h3>
                    <p style="text-align:justify">En esta pantalla es donde se puede ver lo que se ha encontrado de una persona en la internet. Donde
                        pudiera haber resultados en búsquedas OSINT, en búsquedas por NOTICIAS, en búsquedas en la DarkWeb, en búsquedas en redes sociales. </p>
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg17.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg17.png"; ?>" alt="Ventana de asignacion persona-puesto" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
                    <p>Para ver los detalles de lo que se encontró de una persona, se debe dar clic con el botón izquierdo del ratón sobre el registro y luego en el botón
                        <em style="color:#000000"><b>Detalles</b></em> con lo cual aparecerá una pantalla como la siguiente. </p>
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg18.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg18.png"; ?>" alt="Ventana de verificacion de expediente" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
                    <br />
                    <h3 id="Página_Principal">8. Opciones de configuración</h3>
                    <p style="text-align:justify">Existe una pantalla de configuración donde se parametrizan algunas
                        opciones importantes para el cliente. Esta pantalla se encuentra en el menú Administración / configuración de cliente. Entre las opciones disponibles se encuentran:
                    </p>
                    <ul>
                        <li>Reprocesar Persona-Puesto siempre que se requiera análisis semántico aunque ya tenga resultados. Por default viene activada. </li>
                        <li>Regenerar Expediente Persona-Puesto siempre que se requiera análisis semántico. Por default viene desactivada. </li>
                        <li>Recibir email de resultados cuando la petición de analisis persona - puesto sea por petición API REST o Layout (xlsx). Por default viene activada. </li>
                    </ul>
                    <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/docimg16.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/docimg16.png"; ?>" alt="Ventana de opciones de configuracion" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
            </div>
        </div>
    </div><!-- panel body -->
</div><!-- panel -->
<!-- </div> -->