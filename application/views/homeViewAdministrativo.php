<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Inicio</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Inicio</h1>
<!-- end page-header --> 

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="card shadow">
                                <div class="content2">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="icon-big text-center">
                                                <!-- <i class="teal fas fa-shopping-cart"></i> -->
                                                <i class="teal fas fa-users"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="detail">
                                                <p class="detail-subtitle">Usuarios</p>
                                                <!-- <span class="number">6,267</span> -->
                                                <span class="number" id="p_tUsuarios">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer2">
                                        <hr>
                                        <div class="stats">
                                            <i class="fas fa-calendar"></i> Registrados y Activos
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="card shadow">
                                <div class="content2">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="icon-big text-center">
                                            <img width="60%" src="<?php print base_url()."assets/imgs/periodico.jpg";?>" />
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="detail">
                                                <p class="detail-subtitle">Páginas Web</p>
                                                <!-- <span class="number">180,900</span> -->
                                                <span class="number" id="p_tNoticias">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer2">
                                        <hr>
                                        <div class="stats">
                                            <i class="fas fa-stopwatch"></i> Base de datos Páginas Web
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="card shadow">
                                <div class="content2">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="icon-big text-center">
                                                <i class="violet fas fa-eye"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="detail">
                                                <p class="detail-subtitle">Páginas DarkWeb</p>
                                                <!-- <span class="number">28,210</span> -->
                                                <span class="number" id="p_tDarkweb">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer2">
                                        <hr>
                                        <div class="stats">
                                            <i class="fas fa-stopwatch"></i> Base de datos DarkWebDB
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="card shadow">
                                <div class="content2">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="icon-big text-center">
                                                <i class="orange fas fa-users-cog"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="detail">
                                                <p class="detail-subtitle">Analizados</p>
                                                <!-- <span class="number">75</span> -->
                                                <span class="number"  id="p_tAnalisis">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer2">
                                        <hr>
                                        <div class="stats">
                                            <i class="fas fa-envelope-open-text"></i> Personas-Puesto Analizados
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<!--inicia odometro-->

<div class="row">
    <div class="col-xl-12">
        <div class="shadow-lg widget widget-stats">
            <!-- <div class="row"> -->
            <!-- <div class="stats-info"> -->
                <span  style="text-align:center; font-size:14px; font-weight:bold; color:#000; font-family: sans-serif " >Palabras de Riesgos mas usadas</span>
                <!-- <div id="graphWords"  ></div> -->
                <div id="graficaWords" style="width:100%; height: 300px"></div>
            <!-- </div> -->
            <!-- </div> -->
        </div>
    </div> 


    <!-- <div class="col-xl-6">
        <div class="shadow-lg widget widget-stats">
                <div id="chart-line2"></div>
                <div id="chart-line"></div>
        </div>
    </div> -->
</div>
<div class="row">
    <div class="col-xl-6">
        <div class="shadow-lg widget widget-stats">
            <!-- <div class="row"> -->
                <!-- <h3 style="text-align:center; font-size:18px; color:#000">Hallazgos realizados por Herramienta</h3> -->
                <!-- <span style="text-align:center; font-size:14px; font-weight:bold; color:#000; font-family:  sans-serif  " >Hallazgos realizados por Herramienta</span> -->
                <!-- <div id="miDona2"   ></div> -->
                <div id="miDona2" style="width:100%; height: 300px"  ></div>
            <!-- </div> -->
        </div>
    </div>
    <div class="col-xl-6">
        <div class="shadow-lg widget widget-stats">
                <div id="graphSessions"  style="width:100%; height: 350px"></div>
                <br />
        </div>
    </div> 
</div>
<div class="row">
    <div class="col-xl-12">
        <div class="shadow-lg widget widget-stats">
            <!-- <div class="row"> -->
            <!-- <div class="stats-info"> -->
                <!-- <h3 class="text-secondary" style="text-align:center">Número de Procesamientos Mensuales Realizados</h3> -->
                <!-- <div id="myChartProcesamientos"  ></div> -->
                <div id="myChartProcesamientos" style="width:100%; height: 400px" ></div>
            <!-- </div> -->
            <!-- </div> -->
        </div>
    </div>
</div>


<br /><br />

<style>
      
      #mychartXYZ {
    max-width: 650px;
    margin: 35px auto;
  }

  /* #myChartProcesamientos {
    margin:0 auto;
    width: 450px;
    height: 437px;
  } */

  #miDona2 {
    /* max-width: 650px; */
    margin: 35px auto;     
    height:365px; 
  }
    
  </style>
<style>
#graphWords {
  margin:0 auto;
  width: 450px;
  height: 437px;
}

/* #demo-autosize {
  width:80%;
} */
</style>
<!-- 
<script>
      window.Promise ||
        document.write(
          '<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="https://cdn.jsdelivr.net/npm/eligrey-classlist-js-polyfill@1.2.20171210/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="https://cdn.jsdelivr.net/npm/findindex_polyfill_mdn"><\/script>'
        )
</script> -->

    
<!-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> -->
<!-- <link rel="stylesheet" href="http://mistic100.github.io/jQCloud/dist/jqcloud2/dist/jqcloud.min.css">
<script src="http://mistic100.github.io/jQCloud/dist/jqcloud2/dist/jqcloud.min.js"></script> -->