<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Personas y Expedientes</a></li>
    <li class="breadcrumb-item active">Expedientes</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Expedientes de Personas</h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Expedientes de Personas</h4>
    </div>
    <div class="panel-body alert-secondary">
    <div id="ctrls" class="row">
        <div class="col-sm-12 text-center">
            <div class="btn-group btn-group-sm" role="group">
                    
                    
                    <button id="_btnNoticias" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Buscando en noticias..." class="btn btn-secondary btn-sm">
                        <i class="fas fa-newspaper"></i>
                        Buscar de Noticias
                    </button>
                    <button id="_btnOSINT" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Buscando en OSINT..." class="btn btn-info btn-sm">
                        <i class="fas fa-eye"></i>
                        Buscar en OSINT
                    </button>
                    <button id="_btnRedesSociales" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Buscando en R. Sociales ..." type="button" class="btn btn-primary btn-sm">
                        <i class="fas fa-comments"></i>
                        Buscar en Redes Sociales
                    </button>
                    <button id="_btnDarkweb" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Buscando en dark web..." class="btn btn-inverse btn-sm">
                        <i class="fas fa-at"></i>
                        Buscar en Dark Web
                    </button>
                    <button id="_btnWeb" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Buscando en Web..." class="btn btn-default btn-sm">
                        <i class="fas fa-at"></i>
                        Buscar en Web
                    </button>
                    
                </div>
                <div class="btn-group btn-group-sm" role="group">
                <button id="_btnDetalles" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i> ..." class="btn btn-green btn-sm">
                        <i class="fas fa-address-card"></i>
                        Detalles
                    </button><button id="_btnReload" type="button" data-loading-text="<i class='fa fa-table'></i> " class="btn btn-green btn-sm">
                        <i class="fas fa-table"></i>
                        Recargar
                    </button>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="table-responsive">
                <table id="tPersonas" class="responsive table  table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>id_persona</th>
                                <th>id_cliente</th>
                                <th>Cliente</th>
                                <th><input type="checkbox" id="seleccionarTodos" value="" /> Seleccionar/Deseleccionar todos</th>
                                <th>Email</th>
                                <th>Celular</th>
                                <th>INE</th>
                                <th>Direccion</th>
                                <th>Alias</th>
                                <th>Noticias</th>
                                <th>OSINT</th>
                                <th>R. Sociales</th>
                                <th>DarkWeb</th>
                                <th>Web</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                </table>
            </div>
        </div>
    </div><!-- panel body -->
</div><!-- panel -->
<!--dialog agregar a expediente-->
<div class="modal fade " id="verExpedienteDialog" role="dialog">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content">
			<div class="modal-header alert-dark">
				<h4 class="modal-title" id="verExpedienteDialog_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div id="__modal_body" class="modal-body alert-secondary">
                <!--<div class="row mt-5">-->
                Fuente: <select id="fuente" name="fuente"   class="form-control form-control-sm"></select>
                </br></br>
                    <div id="divNoticias" style="height:500px;overflow-y: scroll;">
                    </div>
                    <div id="pagination">
                    </div>
                    <!--<div class="table-responsive">
                        <table id="tnoticias" class="responsive table" width="100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>url</th>
                                    <th>body_dom</th>
                                    <th>target</th>
                                    <th></th>
                                    <th>hash_url</th>
                                    <th>id_persona</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>  -->  
                <!--</div>-->    
            </div>
		</div>
	</div>
</div>
<!--fin dialog agregar expediente-->
    