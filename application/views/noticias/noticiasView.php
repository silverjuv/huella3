
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Noticias</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<br /><br />
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Busqueda de Noticias</h4>
    </div>
    <div class="panel-body">
        <!-- <h1 class="page-header">Noticias</h1> -->
        <!-- end page-header -->
        <div class="row">
            <div class="col-sm-3">
                <label for="busqueda">Escriba su búsqueda</label>
            </div>
            <div class="col-sm-7">
                <input type="text" maxlength="100" class="form-control basicAutoComplete" id="termino">
                <input type="hidden" id="hd_url" value="<?php echo base_url()?>" />
            </div>
            <div class="col-sm-2">
                <button id="btnBuscar" class="btn btn-primary">Buscar</button>
            </div>
        </div>
    </div><!-- panel body -->
</div><!-- panel -->


<div class="toast" role="alert" aria-live="assertive" data-delay="4000" aria-atomic="true">
    <div class="toast-header">
        <!-- <img src="..." class="rounded mr-2" alt="..."> -->
        <strong class="mr-auto">Error de validaci&oacute;n</strong>
        <!-- <small>11 mins ago</small> -->
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body">

    </div>
</div>


