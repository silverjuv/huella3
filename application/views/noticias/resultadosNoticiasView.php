<?php
// pruebas
// echo $_SERVER['REQUEST_URI']; die();
if (strpos($_SERVER['REQUEST_URI'],'index.php')>0){
    $ruta_noticias = "../../../scripts_huella/nhuella/capturas/";
}else{
    $ruta_noticias = "../../scripts_huella/nhuella/capturas/";
}
// produccion
// if (file_exists($ruta_noticias."00e29f5c6dad718aeb7e99738e62a7cd910c35dffc0997cd7d9126190020e582ab9f1e852052d03553ed98f7498cd4c8cd55b5dcb22eb0f6e3fe7794c50bee90.jpg")){
//     die("ok");
// }
?>
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Noticias</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->

<br /><br />
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Busqueda de Noticias</h4>        
    </div>
    <div class="panel-body">

        <!-- <h1 class="page-header">Noticias</h1> -->

        <div class="row">
            <div class="col-sm-3">
                <label for="busqueda">Escriba su búsqueda</label>
            </div>
            <div class="col-sm-7">
                <input type="text" maxlength="100" class="form-control basicAutoComplete" id="termino"
                    value="<?php print $this->input->get("search"); ?>">
                    <input type="hidden" id="hd_url" value="<?php echo base_url()?>" />
            </div>
            <div class="col-sm-2">
                <button id="btnBuscar" class="btn btn-primary">Buscar</button>
            </div>
        </div>
        <br />
        <span>Total de Registros localizados: <?php echo $totalFound; ?></span>
        <hr /><hr /><br />
        <!-- <div class="row">
    <div class="col-sm-12">
    <small>Su búsqueda fue: </small>
    <em><h4><?php print $this->input->get("search"); ?></h4></em>
    </div>
</div> -->

        <div class="row">
            <div class="col-sm-12">

                <?php 
            foreach($model["data"] as $row) {
        ?>

                <div class="row ">
                    <div class="col-sm-12">
                        Fuente: <a target="_blank"  href="<?php print ($row["target"]); ?>"><?php print ($row["target"]); ?></a>
                        <!-- Fuente: <a target="_blank" 
                            href="<?php print parse_url($row["url"], PHP_URL_HOST) ?>"><?php print parse_url($row["url"], PHP_URL_HOST) ?></a> -->
                    </div>
                    <?php if ($row["fecha_articulo"]!=null && $row["fecha_articulo"]!='0000-00-00'){ ?>
                    <div class="col-sm-8">
                        Enlace: <a target="_blank"  href="<?php print ($row["url"]); ?>"><?php print ($row["url"]); ?></a>
                    </div>
                    <div class="col-sm-4">
                        Fecha del articulo: <?php print ($row["fecha_articulo"]); ?>
                    </div>
                    <?php } else { ?>
                    <div class="col-sm-12">
                        Enlace: <a target="_blank"  href="<?php print ($row["url"]); ?>"><?php print ($row["url"]); ?></a>
                    </div>
                    <?php }  ?>
                    <div class="col-sm-12">
                        <h2><a target="_blank"  href="<?php print ($row["url"]); ?>" style="color:black"><?php print ($row["titulo_articulo"]); ?></a></h2>
                    </div>
                    <?php
            // echo $ruta_noticias."".$row['hash_body'].".jpg"; die();
                    if (file_exists(BASEPATH.$ruta_noticias."".$row['hash_body'].".jpg")){
                ?>
                    <div class="row">
                        <div class="col-sm-10">
                            <p style="margin:10px; text-align:justify">
                            <?php print $row["pg"]; ?>
                            </p>
                        </div>
                        <div class="col-sm-2">
                            <a target="_blank" href="<?php print "../".$ruta_noticias."".$row['hash_body'].".jpg"; ?>"><img
                                    src="<?php print "../".$ruta_noticias."".$row['hash_body'].".jpg"; ?>"
                                    style=" width:80px; border:4px solid #f0f0f0" /></a>
                        </div>
                    </div>
                    <?php }else{ 
?>
                    <div class="row">
                        <div class="col-sm-12">
                            <p style="margin:10px">
                            <?php print $row["pg"]; ?>
                            </p>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                        <hr />
                <?php } ?>

                <div class="row ">
                    <div class="col-sm-12" style="text-align:center; left: 40%;">
                        <?php if (isset($paginador)) { ?>
                        <?php echo $paginador ?>
                        <?php } ?>
                    </div>
                </div>


            </div>
        </div>

    </div><!-- panel body -->
</div><!-- panel -->


<div class="toast" role="alert" aria-live="assertive" data-delay="4000" aria-atomic="true">
    <div class="toast-header">
        <!-- <img src="..." class="rounded mr-2" alt="..."> -->
        <strong class="mr-auto">Error de validaci&oacute;n</strong>
        <!-- <small>11 mins ago</small> -->
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body">

    </div>
</div>