<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Subir Layouts</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Subir Layouts</h1>
<!-- end page-header -->
<!-- <div class="row mb-2">
    <div class="col-sm-12 text-right" title="Ir a inicio">
        <a href="" id="linkIrInicio" class="btn btn-info">
            <i class="fas fa-home"></i> Inicio
        </a>
    </div>
</div> -->

<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Subir Layouts</h4>
    </div>
    <div id="panelMision" class="panel-body">
    <div class="row mt-2">
            <div class="col-2">

            </div>
            <div class="col-8 text-center">
                <div class="form-group">
                    * Layouts de ejemplo para su llenado y carga posterior al sistema.
                </div>
            </div>
        </div>    
        <div class="row">
            <div class="col-4">
            </div>
            <div class="col-4">
                <div class="btn-group">
                    <a class="btn btn-success" href="<?php print base_url(); ?>/assets/layouts_temp/layout_personas.xlsx" target="_blank" class="texto">
                        Agregar Personas
                        <img style="width:40px" src="<?php print base_url(); ?>/assets/imgs/Excel-icon.png"></img>
                    </a>
                    <a class="btn btn-success" href="<?php print base_url(); ?>/assets/layouts_temp/layout_personas_puestos.xlsx" target="_blank" class="texto">
                        Programar Personas y Puestos para analisis
                        <img style="width:40px" src="<?php print base_url(); ?>/assets/imgs/Excel-icon.png"></img>
                    </a>
                    <?php print ($_SESSION['id_perfil'] == '1') ? '<a class="btn btn-success" href="' . base_url() . '/assets/layouts_temp/layout_sitios.xlsx" target="_blank" class="texto">
                        sitios
                        <img style="width:40px" src="' . base_url() . '/assets/imgs/Excel-icon.png"></img>
                    </a>' : ''; ?>
                </div>
            </div>
            <div class="col-4">
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-12">
                * En el caso del layout_personas_puestos.xlsx, existe un identificador lote, el cual se genera automaticamente con la fecha actual por layout que se sube.
            </div>
        </div> -->
        <div class="row mt-2">
            <div class="col-2">

            </div>
            <div class="col-8 text-center">
            <br />
                <div class="row">
                    <div class="col-sm-4"><label for="nivel" class="control-label">Seleccione el Layout que desea cargar al sistema:</label></div>
                    
                    <div class="col-sm-8">
                    <select name="layout" id="layout">
                        <option value="P">Layout solo de Personas</option>
                        <?php print ($_SESSION['id_perfil'] == '1') ? '<option value="S">Catalogo de Sitios Web</option>' : ''; ?>
                        <option value="A">Layout de Personas y Puestos a Analizar</option>
                    </select>
                    </div>
                </div>
                <br />
                <div class="dropzone" id="layoutFile"></div>
                </br>
                <div class="form-group">
                    <button id="procesar" type="button" data-loading-text="<i class='fas fa-cog fa-spin'></i>Procesando..." class="btn btn-green">
                        <i class="fas fa-cog"></i>
                        Procesar
                    </button>
                </div>
                <div class="form-group">
                    * En el caso del layout_personas_puestos.xlsx, existe un identificador lote (no incluido en el layout), el cual se genera automaticamente con la fecha actual por layout que se sube.
                </div>                
            </div>
        </div>
    </div>
</div>
<!--fin del panel-->