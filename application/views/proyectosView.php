<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Administrar</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Proyectos</h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Proyectos</h4>
    </div>
    <div class="panel-body">

    <div id="ctrls" class="row">
        <div class="col-sm-12 text-center">
            <div class="btn-group btn-group-sm" role="group">
                    
                    <button id="_agregar" type="button" class="btn btn-lime btn-sm">
                        <i class="fas fa-asterisk"></i>
                        Nuevo
                    </button>
                    <button id="_editar" type="button" class="btn btn-warning btn-sm">
                        <i class="fas fa-edit"></i>
                        Editar
                    </button>
                    <button id="_borrar" type="button" class="btn btn-danger btn-sm">
                        <i class="fas fa-trash-alt"></i>
                        Borrar
                    </button>
                    
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-12">
                <table id="tabla" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Proyecto</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                </table>
            </div>
        </div>

    </div><!-- panel body -->

</div><!-- panel -->

<div class="modal fade" id="__modal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="__modal_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			
			<div id="__modal_body" class="modal-body">
                
                <input type="hidden" id="id_edicion" value="">
                <div class="row">
                    <div class="col-sm-4">
                         <label for="proyecto">Nombre del proyecto</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="text" id="proyecto" class="form-control">
                    </div>
                </div>       
                
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary" id="btnGuardar">Guardar</button>
			</div>
				
		</div>
	</div>
</div>