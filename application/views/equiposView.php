<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Administrar</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Equipos</h1>
<!-- end page-header -->

<!-- begin panel -->
<div id="panelListado" class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Equipos</h4>
    </div>
    <div class="panel-body">

    <div id="ctrls" class="row">
        <div class="col-sm-12 text-center">
            <div class="btn-group btn-group-sm" role="group">
                    
                    <button id="_agregar" type="button" class="btn btn-lime btn-sm">
                        <i class="fas fa-asterisk"></i>
                        Nuevo
                    </button>
                    <button id="_editar" type="button" class="btn btn-warning btn-sm">
                        <i class="fas fa-edit"></i>
                        Editar
                    </button>
                    <button id="_borrar" type="button" class="btn btn-danger btn-sm">
                        <i class="fas fa-trash-alt"></i>
                        Borrar
                    </button>
                    
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-12">
                <table id="tabla" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre del equipo</th>
                                <th>Proyecto</th>
                                <th>Cliente</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                </table>
            </div>
        </div>

    </div><!-- panel body -->

</div><!-- panel listado -->

<input type="hidden" id="id_equipo" value="">
<div id="panelFormulario" class="panel panel-inverse" style="display:none;">
    <div class="panel-heading">
        <h4 class="panel-title" id="tituloAccion">Equipos</h4>
    </div>
    <div class="panel-body">
        <div class="col-sm-12 justify-content-start">
            <button id="btnRegresar" type="button" class="btn btn-outline-info">
                <i class="fas fa-long-arrow-alt-left"></i> Regresar al listado
            </button>
        </div>
        <fieldset class="mt-3">
            <legend>1. Nombre su equipo</legend>
            <div class="form-group row">
                <label for="equipo" class="col-sm-3 col-form-label">Nombre del equipo</label>
                
                <div class="col-sm-9">
                    <input type="text" id="descripcion" class="form-control">
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-3">
                    <label for="proyecto">Seleccione el proyecto</label>
                </div>
                <div class="col-sm-9">
                    <select name="proyecto" id="proyecto" style="width:100%"></select>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-3">
                    <label for="proyecto">Seleccione el cliente</label>
                </div>
                <div class="col-sm-9">
                    <select name="cliente" id="cliente" style="width:100%"></select>
                </div>
            </div>
            <div class="form-group row mt-3">
                <div class="col-sm-12 text-right">
                    <button id="btnGuardarEquipo" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend>2. Seleccione los miembros del equipo para cada proyecto</legend>
            <div class="row mt-3">
                <div class="col-sm-5">
                    <table id="tusuarios" class="responsive table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                    </table>
                </div>
                <div class="col-sm-2 text-center">
                    <button id="btnAddMember" class="btn btn-lime" title="Agregar elemento seleccionado al equipo">
                        <i class="fas fa-plus-circle"></i>
                    </button>
                    <br>
                    <button id="btnDelMember" class="btn btn-danger m-3" title="Quitar elemento seleccionado del equipo">
                        <i class="fas fa-minus-circle"></i>
                    </button>
                    <br>
                    <button id="btnMarkLeader" class="btn btn-primary m-3" title="Marcar como lider de proyecto">
                        <i class="fas fa-award"></i>
                    </button>
                </div>
                <div class="col-sm-5">
                    <table id="tequipo" class="responsive table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>es_lider</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                    </table>
                </div>
            </div>
        </fieldset>

        <div class="col-sm-12 text-right mt-5">
            <button id="btnRegresar2" type="button" class="btn btn-outline-info">
                <i class="fas fa-long-arrow-alt-left"></i> Regresar al listado
            </button>
        </div>

    </div>

</div><!-- panel formulario -->