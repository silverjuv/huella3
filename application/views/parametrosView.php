<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Catálogos</a></li>
    <li class="breadcrumb-item active">Parametros</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Parametros <small>Gestión de parametros...</small></h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Parametros</h4>
    </div>
    <div class="panel-body">
    <div id="ctrls" class="row">
        <div class="col-sm-12 text-center">
            <div class="btn-group btn-group-sm" role="group">
                    
                    <button id="_editar" type="button" class="btn btn-warning btn-sm">
                        <i class="fas fa-edit"></i>
                        Editar
                    </button>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-12">
                <table id="tparametros" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Cve_param</th>
                                <th>Valor</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                </table>
            </div>
        </div>

    </div><!-- panel body -->
</div><!-- panel -->

<div class="modal fade" id="__modal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="__modal_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			
			<div id="__modal_body" class="modal-body">
                
                <input type="hidden" id="id_edicion" value="">
                <div class="row">
                    <div class="col-sm-4">
                         <label for="cve_param">Cve_param</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="text" id="cve_param" class="form-control" readonly="readonly">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <label for="valor">Valor</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="text" id="valor" class="form-control">
                    </div>
                </div>                               
                
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary" id="btnGuardar">Guardar</button>
			</div>
				
		</div>
	</div>
</div>