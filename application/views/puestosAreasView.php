<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Administrar</a></li>
    <li class="breadcrumb-item active">Puestos - Áreas</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Puestos - Áreas <small>Asignación de puestos a áreas...</small></h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Puestos - Áreas</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <select name="id_area" id="id_area" style="width:100%"></select>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-sm-6">
                <table id="tasignadas" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Puesto asignado</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                </table>
            </div>
            <div class="col-sm-6">
                <table id="tcatalogo" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Puesto</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 text-center">
                <button id="btnDel" class="btn btn-danger">Quitar</button>
            </div>
            <div class="col-sm-6 text-center">
                <button id="btnAdd" class="btn btn-success">Agregar</button>
            </div>
        </div>

    </div><!-- panel body -->
</div><!-- panel -->