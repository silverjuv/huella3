<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Cambiar contraseña</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Cambiar contraseña</h1>
<!-- end page-header -->

<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Cambio de mi contraseña</h4>
    </div>
    
    <div class="panel-body">

        <div class="form-group">
            <label for="pass">Contraseña actual</label>
            <input id="pass" name="pass" type="password" class="form-control">
        </div>

        <div class="form-group">
            <label for="passn">Nueva contraseña</label>
            <input id="passn" type="password" class="form-control">
        </div>

        <div class="form-group">
            <label for="passnc" >Confirmar contraseña</label>
            <input id="passnc" name="passnc" type="password" class="form-control">
        </div>

        <div class="row">
            <div class="col-sm-12 text-right">
                <button id="btnCambiar" class="btn btn-primary">Cambiar</button>    
            </div>
        </div>
        
    </div>

</div>