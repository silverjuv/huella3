<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Inicio</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Inicio</h1>
<!-- end page-header -->

<div class="row">
                        <div class="col-sm-3">
                            <div class="card shadow">
                                <div class="content2">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="icon-big text-center">
                                                <!-- <i class="teal fas fa-shopping-cart"></i> -->
                                                <i class="teal fas fa-users"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="detail">
                                                <p class="detail-subtitle">Personas</p>
                                                <!-- <span class="number">6,267</span> -->
                                                <span class="number" id="p_tPersonas">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer2">
                                        <hr>
                                        <div class="stats">
                                            <i class="fas fa-calendar"></i> Personas registradas
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="card shadow">
                                <div class="content2">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="icon-big text-center">
                                                <i class="teal fas fa-user-tie"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="detail">
                                                <p class="detail-subtitle">Puestos </p>
                                                <!-- <span class="number">180,900</span> -->
                                                <span class="number" id="p_tPuestos">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer2">
                                        <hr>
                                        <div class="stats">
                                            <i class="fas fa-stopwatch"></i> Puestos registrados
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                        <div class="col-sm-3">
                            <div class="card shadow">
                                <div class="content2">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="icon-big text-center">
                                                <i class="orange fas fa-users-cog"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="detail">
                                                <p class="detail-subtitle">Analizados</p>
                                                <!-- <span class="number">75</span> -->
                                                <span class="number"  id="p_tAnalisis">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer2">
                                        <hr>
                                        <div class="stats">
                                            <i class="fas fa-envelope-open-text"></i> Personas-Puesto Analizados
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="card shadow">
                                <div class="content2">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="icon-big text-center">
                                                <div id="chart-3" style="min-height: 38.95px;"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="detail">
                                                <p class="detail-subtitle" style="font-size:11px">Resultado Promedio</p>
                                                <span class="number"  id="p_tResultados">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer2">
                                        <hr>
                                        <div class="stats">
                                            <i class="fas fa-stopwatch"></i> 0-No Riesgo a 10 Riesgo Crítico
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>


<!--inicia odometro-->
<div class="row">
    <div class="col-xl-12">
        <div class="shadow-lg widget widget-stats">
            <!-- <div class="row"> -->
            <!-- <div class="stats-info"> -->
                <!-- <h3 class="text-secondary" style="text-align:center">Número de Procesamientos Mensuales Realizados</h3> -->
                <div id="myChartProcesamientos" style="width:100%; height: 400px" ></div>
            <!-- </div> -->
            <!-- </div> -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-6">
        <div class="shadow-lg widget widget-stats">
            <!-- <div class="row"> -->
                <!-- <h3 style="text-align:center; font-size:18px; color:#000">Hallazgos realizados por Herramienta</h3> -->
                <!-- <span style="text-align:center; font-size:14px; font-weight:bold; color:#000; font-family:  sans-serif  " >Hallazgos realizados por Herramienta</span> -->
                <div id="miDona2" style="width:100%; height: 300px"  ></div>
            <!-- </div> -->
        </div>
    </div>
    <div class="col-xl-6">
        <div class="shadow-lg widget widget-stats">
            <!-- <div class="row"> -->
            <!-- <div class="stats-info"> -->
                <span  style="text-align:center; font-size:14px; font-weight:bold; color:#000; font-family: sans-serif " >Analisis Puesto-personas Realizados por Mes en últimos 3 años</span>
                <div id="barras2"  style="width:100%; height: 300px"   ></div>
            <!-- </div> -->
            <!-- </div> -->
        </div>
    </div>    
</div>

<!-- <div class="modal" id="popup" style="display: none;">

            <div class="modal-body">
                
            <div class="spinner-border text-success" role="status">
                <span class="sr-only">Loading...</span>
            </div>

            </div>
</div> -->
<div class="modal fade bd-example-modal-sm" tabindex="-1" id="popup" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" style="text-align:center">
        <br /><br />
        <div class="d-flex justify-content-center">
            <!-- <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div> -->
            <div class="spinner-grow" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <br /><br />
        <label>Cargando graficas, espere un momento....</label>        
    </div>
  </div>
</div>

<br /><br />
<!-- 
<script>
      window.Promise ||
        document.write(
          '<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="https://cdn.jsdelivr.net/npm/eligrey-classlist-js-polyfill@1.2.20171210/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="https://cdn.jsdelivr.net/npm/findindex_polyfill_mdn"><\/script>'
        )
    </script>

    
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> -->
    