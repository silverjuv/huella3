<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Procesos</a></li>
    <li class="breadcrumb-item active">Programar Analisis de Personas - Puestos</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Programar Analisis de  personas y puestos </h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Seleccionar Persona(s) y Puesto</h4>
    </div>
    <div class="panel-body">
        <div id="ctrls" class="row">
            <div class="col-sm-12  " style="text-align:right">
                <div class="btn-group btn-group-sm" role="group">

                    <button id="_analizar" type="button" class="btn btn-lime btn-sm">
                        <!-- <i class="fas fa-asterisk"></i> -->
                        Programar Análisis
                    </button>                    
                </div>
                <br />
                <span style="text-align:right; width:100%">* Los resultados de estos analisis se pueden ver en el menu Personas / Resultados</span>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-2">
                <label for="pass">Puesto requerido:</label>
            </div>
            <div class="col-sm-10">
                <select name="id_puesto" id="id_puesto" style="width:100%"></select>
                <span style="text-align:right; width:100%">&nbsp;</span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <label for="pass">Seleccione una o mas personas a analizar para un puesto:</label>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <table id="tpersonas" class="responsive table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th>id_persona</th>
                            <th>Seleccionar</th>
                            <th>CURP</th>
                            <!-- <th>INE</th> -->
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Celular</th>
                            <th>Analisis aplicados</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>

    </div><!-- panel body -->
</div><!-- panel -->



<!--dialog exportar a-->
<div class="modal fade " id="exportarDialog" role="dialog">
    <div class="modal-dialog modal-sm " role="document">
        <div class="modal-content">
            <div class="modal-header alert-dark">
                <h4 class="modal-title" id="exportarDialog_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div id="__modal_body" class="modal-body alert-secondary">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="radio">
                            <label><input type="radio" name="optradio" value="XLS" checked> &nbsp;Excel (xlsx)</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="radio">
                            <label><input type="radio" name="optradio" value="CSV"> &nbsp;CSV</label>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="btn-group btn-group-sm" role="group">
                        <button id="_exportar" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i>Exportando..." class="btn btn-success btn-sm">
                            <i class="fas fa-filter"></i> Exportar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin dialog enviar a analizar-->
