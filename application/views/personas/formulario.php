<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Catálogos</a></li>
    <li class="breadcrumb-item active">Personas</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Personas <small>Gestión de personas...</small></h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Personas</h4>
    </div>
    <div class="panel-body">
    
        <input type="hidden" id="id_edicion" value="<?php print empty($person->id_persona)?"": $person->id_persona; ?>">
        <div class="row">
            <div class="col-sm-3">
                <!-- <span style="color:red"> * </span> -->
                <label for="ine">INE</label>
            </div>
            <div class="col-sm-3">
                <input id="ine" type="text" maxlength="18" class="form-control" value="<?php print empty($person->ine)?"": $person->ine; ?>">
            </div>
            <div class="col-sm-3 text-right">
                <span style="color:red"> * </span><label for="curp">CURP</label>
            </div>
            <div class="col-sm-3">
                <input id="curp" type="text" maxlength="18" class="form-control" value="<?php print empty($person->curp)?"": $person->curp; ?>">
            </div>
        </div>
        <!-- <div class="row mt-3">
        </div> -->
        <div class="row mt-3">
            <div class="col-sm-3">
                <span style="color:red"> * </span><label for="nombre">Nombre</label>
            </div>
            <div class="col-sm-3">
                <input id="nombre" type="text" class="form-control" value="<?php print empty($person->nombre)?"": $person->nombre; ?>">
            </div>
            <div class="col-sm-3 text-right">
                <label for="alias">Alias</label>
            </div>
            <div class="col-sm-3">
                <input id="alias" placeholder=""  maxlength="60"  type="text" class="form-control" value="<?php print empty($person->alias)?"": $person->alias; ?>">
            </div>
        </div>    
        <div class="row mt-3">
            <div class="col-sm-3">
                <span style="color:red"> * </span><label for="paterno">A. Paterno</label>
            </div>
            <div class="col-sm-3">
                <input id="paterno" type="text" maxlength="50" class="form-control" value="<?php print empty($person->paterno)?"": $person->paterno; ?>">
            </div>
            <div class="col-sm-3 text-right">
                <label for="materno">A. Materno</label>
            </div>
            <div class="col-sm-3">
                <input id="materno" type="text" maxlength="50" class="form-control" value="<?php print empty($person->materno)?"": $person->materno; ?>">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3">
                <span style="color:red"> * </span><label for="email">Email</label>
            </div>
            <div class="col-sm-3">
                <input id="email" type="text" maxlength="60" class="form-control" value="<?php print empty($person->email)?"": $person->email; ?>">
            </div>
            <div class="col-sm-3 text-right">
                <span style="color:red"> * </span><label for="celular">Teléfono</label>
            </div>
            <div class="col-sm-3">
                <input id="celular" type="text" maxlength="10" placeholder="a 10 posiciones seguidas" class="form-control" value="<?php print empty($person->celular)?"": $person->celular; ?>">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3">
                <label for="twitter">Cuenta Twitter sin @:</label>
            </div>
            <div class="col-sm-3">
                <input id="twitter" placeholder=""  maxlength="60"  type="text" class="form-control" value="<?php print empty($person->twitter)?"": $person->twitter; ?>">
            </div>
            <div class="col-sm-3 text-right">
                <label for="linkedin">Cuenta Linkedin:</label>
            </div>
            <div class="col-sm-3">
                <input id="linkedin" placeholder=""  maxlength="60"  type="text" class="form-control" value="<?php print empty($person->linkedin)?"": $person->linkedin; ?>">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3">
                <!-- <span style="color:red"> * </span> -->
                <label for="direccion">Dirección</label>
            </div>
            <div class="col-sm-9">
                <input id="direccion" maxlength="250"  placeholder="av. guadalupe #134, col. san jose, cd. juarez, chihuahua" type="text" class="form-control" value="<?php print empty($person->direccion)?"": $person->direccion; ?>">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3">
                <span style="color:red"> * </span><label for="empresa">Empresa</label>
            </div>
            <div class="col-sm-9">
                <select id="id_cliente" style="width:100%"></select>
                <input type="hidden" id="id_empresa" value="<?php print empty($person->id_cliente) ? ($_SESSION['id_cliente']) : $person->id_cliente; ?>">
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-6 text-left">
                <button type="button" class="btn btn-outline-info btnRegresar">
                    <i class="fas fa-long-arrow-alt-left"></i> Regresar al listado
                </button>
            </div>
            <div class="col-sm-6 text-right">
                <button id="btnLlimpiar" class="btn btn-default">Nueva Captura</button>
                <button id="btnGuardar" class="btn btn-lime">Guardar</button>
            </div>
        </div>

    </div><!-- panel body -->
</div><!-- panel -->