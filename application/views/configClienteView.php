<!-- begin breadcrumb -->
<?php
// var_dump($datos); die();
?>
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Opciones de Configuración</li>
</ol>
<br /><br /><br />
<!-- end breadcrumb -->
<!-- begin page-header -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Opciones de Configuración</h4>
    </div>
    <div class="panel-body">
        <!-- <h1 class="page-header">Noticias</h1> -->
        <!-- end page-header -->
        <div class="row">
            <div class="col-sm-3">
                <label for="busqueda"></label>
            </div>
            <div class="col-sm-7">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="reprocesar">                    
                    <label class="custom-control-label" for="reprocesar">Reprocesar Persona-Puesto siempre que se requiera análisis semántico aunque ya tenga resultados.</label>
                </div>
                <!-- <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="regenerarexpediente">                    
                    <label class="custom-control-label" for="regenerarexpediente">Regenerar Expediente Persona-Puesto siempre que se requiera análisis semántico.</label>
                </div> -->
                <!-- <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="recibiremail">                    
                    <label class="custom-control-label" for="recibiremail">Recibir email de resultados cuando la petición de analisis persona - puesto sea por petición API REST o Layout (xlsx).</label>
                </div>                 -->
            </div>
            <div class="col-sm-2">
                <label for="busqueda"></label>
            </div>
            <!-- <div class="col-sm-2"><label class="custom-control-label" for="reprocesar">
                <button id="btnBuscar" class="btn btn-primary">Guardar Configuración</button>
            </div> -->
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-7">
                &nbsp;
            </div>
            <div class="col-sm-5" style="text-align:right">
                <button class="btn btn-primary" id="btnGuardar"  >Guardar Configuración</button>
            </div>
        </div>
    </div><!-- panel body -->
</div><!-- panel -->


<div class="toast" role="alert" aria-live="assertive" data-delay="4000" aria-atomic="true">
    <div class="toast-header">
        <!-- <img src="..." class="rounded mr-2" alt="..."> -->
        <strong class="mr-auto">Error de validaci&oacute;n</strong>
        <!-- <small>11 mins ago</small> -->
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body">

    </div>
</div>