<!-- begin page-header -->
<h1 class="page-header">Acceso denegado <small>No se tienen los permisos apropiados o la pantalla no existe...</small></h1>
<!-- end page-header -->
<div class="row text-center">
    <div class="col-sm-12 text-center">
        <img class="img-fluid" src="<?php print base_url('assets/imgs/stop.png');?>" alt="alto">
    </div>
</div>