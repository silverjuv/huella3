<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Doc Administrativo</a></li>
    <li class="breadcrumb-item active"></li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Documentacion Administrativa</h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Documentacion Administrativa</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 id="Una_Ejemplo_de_un_Sitio_Web_con_PHP"></h3>
                <p>La documentación administrativa abarca 3 áreas importantes: </p>
                <ul>
                    <li>Documentación de opciones administrativas dentro del aplicativo web</li>
                    <li>Documentación de procesos internos importantes realizados en el backend</li>
                    <li>Documentación de la base de datos</li>
                </ul>
                <p>Una explicación general de lo que sucede y los casos de uso tanto del Administrador como del Cliente
                    es muestra en la siguiente imagen: </p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/procesos_en_huella.jpg";?>"> -->
                        <img style="width:70%"
                            src="<?php print base_url()."assets/imgs/doc-admin/procesos_en_huella.jpg";?>"
                            alt="Proceso General" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <br />
                <h3 id="Una_Ejemplo_de_un_Sitio_Web_con_PHP">Resultados</h3>
                <p>El proceso de ponderación es un tema algo complejo ya que entra en su cálculo varias fuentes, varios
                    categorias de sitios web, y la importancia del
                    o relevancia del sitio web. Las tablas de base de datos que se usan y contienen ponderación son las
                    siguientes:</p>
                <table class="table table-responsive table-responsive-sm table-sm">
                    <tr>
                        <td>
                            <!-- <span class="image-wrapper"> -->
                            <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/cat_anexos.png";?>"> -->
                            <img style="width:100%" class="img img-responsive"
                                src="<?php print base_url()."assets/imgs/doc-admin/cat_anexos.png";?>"
                                alt="Tabla cat_anexos" />
                            <!-- </a> -->
                            <!-- <span class="image-caption"></span> -->
                        </td>
                        <td>
                            <!-- <span class="image-wrapper"> -->
                            <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/cat_categorias_dominios.png";?>"> -->
                            <img style="width:100%" class="img img-responsive"
                                src="<?php print base_url()."assets/imgs/doc-admin/cat_categorias_dominios.png";?>"
                                alt="Tabla cat_categorias_dominios" />
                            <!-- </a> -->
                            <!-- <span class="image-caption"></span> -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- <span class="image-wrapper"> -->
                            <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/cat_sitiosweb.png";?>"> -->
                            <img style="width:100%" class="img img-responsive"
                                src="<?php print base_url()."assets/imgs/doc-admin/cat_sitiosweb.png";?>"
                                alt="Tabla cat_anexos" />
                            <!-- </a> -->
                            <!-- <span class="image-caption"></span> -->
                        </td>
                        <td>
                            <!-- <span class="image-wrapper"> -->
                            <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/cat_riesgos.png";?>"> -->
                            <img style="width:100%" class="img img-responsive"
                                src="<?php print base_url()."assets/imgs/doc-admin/cat_riesgos.png";?>"
                                alt="Tabla cat_riesgos" />
                            <!-- </a> -->
                            <!-- <span class="image-caption"></span> -->
                        </td>
                    </tr>
                </table>
                <p>Como se observa los valores de ponderación o peso van de un rango de 1 al 10. El proceso de cálculo
                    del resultado es el siguiente:</p>
                <ul>
                    <li>Se recorre el expediente de la persona buscando posibles riesgos en las notas donde aparece
                        (nArchivos)</li>
                    <li>Existen varias fuentes, cada una con un peso específico (F), estan estan en la tabla cat_anexos. En todos los catalogos la ponderación es de 0 a 10.</li>
                    <li>De los riesgos localizados se obtiene el de ponderación más alta (R) en una noticia</li>
                    <li>Se verifica la reputación del email en OSINT, el cual puede tener 3 posibles resultados (Omax):
                        no riesgoso, riesgoso y muy riesgoso, donde la ponderación sería 0, 5, y 10 respectivamente.
                    </li>
                    <li>El riesgo localizado se obtuvo de una noticia, si es que se localizo la persona. Esa noticia
                        esta en un sitio web el cual tiene una ponderación (S) y una ponderación en su categoria (C),
                        por lo tanto,
                        la fórmula del resultado sería la siguiente: <br /> <b>Resultado</b> = (si Omax!= 0 : ( Rmax (
                        (R+S+C+F)/4 de nArchivos ) + Omax)/2 ; si Omax=0 : Rmax ( (R+S+C+F)/4 de nArchivos ) ) </li>
                    <li>En el caso de los contenidos obtenidos por Darkweb, la categoria tiene por default ponderación (C) = 5, y la fuente (F) tiene el mismo valor.</li>
                </ul>
                <p>Es muy importante señalar que el proceso se realiza localizando en el internet, medios sociales,
                    buscadores web, noticias web, parte de los datos personales proporcionados,
                    asi es que si algo coincide tendrá un grado de ponderación, sin embargo, no es 100% seguro que sea
                    esa la persona buscada. Por lo tanto, en los resultados hay una sección de detalles para
                    que el cliente vea las fuentes donde se localizó la persona y revise y verifique que es o no es la
                    persona buscada.</p>
            </div>
        </div>
    </div><!-- panel body -->
</div><!-- panel -->
<!-- </div> -->

<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Opciones Administrativas en Aplicativo Web</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 id="Una_Ejemplo_de_un_Sitio_Web_con_PHP">1. Captura de Clientes</h3>
                <p>Para capturar clientes debemos ir a la opción Catálogo de Clientes ubicado en el menú Administración.
                </p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg1.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg1.png";?>"
                            alt="Pantalla de Clientes" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p style="text-align:justify">En varias pantallas existen 3 botones, donde el botón <em
                        style="color:#000000"><b>Nuevo</b></em> abre una ventana de captura de clientes,
                    el botón <em style="color:#000000"><b>Editar</b></em> abre la misma ventana pero para edición de un
                    cliente seleccionado del listado. Y el botón
                    <em style="color:#000000"><b>Borrar</b></em>
                    permite eliminar lógicamente un cliente seleccionado del listado. En el caso del botón Editar y
                    Borrar, es necesario tener
                    seleccionado el registro de la tabla que se desea editar o borrar. El borrado es lógico. La pantalla
                    de captura de datos
                    del cliente es la siguiente:
                </p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg4.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg4.png";?>"
                            alt="Edición de clientes" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>Como se puede observar, los campos obligatorios son los marcados con <em
                        style="color:#ff0000">*</em>. Los valores
                    aceptados en la captura de clientes son:</p>
                <ul>
                    <li>Nombre del cliente, Numero de identificación nacional electoral.</li>
                    <li>Email principal, donde recibirá notificaciones el cliente, Cláve Única de Registro de Población
                    </li>
                    <li>Teléfono de contacto.</li>
                    <li>La dirección física, simplemente campo no vacío.</li>
                </ul>
                <p>En la base de datos la tabla a la que se hace referencia es: <b>cat_clientes</b>.</p>
                <br />
                <h3 id="Página_Principal">2. Pantalla de Captura de Contratos de Clientes</h3>
                <p>Para capturar contratos de clientes debemos ir a la opción Contratos de Clientes ubicado en el menú
                    Administración. </p>
                <p>Esta pantalla permite como su nombre lo indica asociar contratos a clientes. Los contratos pueden ser
                    de 2 formas: por número
                    de procesamientos (análisis realizados a personas-puesto), o bien, por tiempo limitado (por un
                    periodo dado) con número de
                    procesamientos ilimitados.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg5.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg5.png";?>"
                            alt="Edición de clientes" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>Los campos requeridos para la captura de contratos son:</p>
                <ul>
                    <li>Cliente, debemos seleccionar el cliente.</li>
                    <li>Servicio, que se refiere al tipo de servicio que quiere contratar el cliente, ya se por número
                        de procesamientos
                        o por periodo.</li>
                    <li>Fecha de Inicio, Fecha de Fin, opciones para validar el periodo que el cliente va a tener
                        permitido realizar procesamientos.</li>
                    <li># procesamientos, se refiere al número de procesamientos contratados por el cliente, siempre y
                        cuando el tipo de servicio sea por número de procesamientos.</li>
                    <li>Fuentes, se refiere a las fuentes de información contratadas por el cliente: redes sociales,
                        busquedas en motores de búsqueda, osint, noticias, darkweb.</li>
                    <li>Status, se refiere al status del contrato que puede ser: Activo, Cancelado, Suspendido,
                        Terminado. Si se llega al limite establecido por el tipo de servicio,
                        se termina automáticamente el contrato.</li>
                    <li>Api key. Se crea automáticamente. Si se desea cambiar, una vez guardado el registro del
                        contrato, puede editarse y dar clic en el botón <em style="color:#000000"><b>Api Key</b></em>.
                    </li>
                </ul>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg7.png";?>"> -->
                        <img src="<?php print base_url()."assets/imgs/doc-admin/docimg7.png";?>"
                            alt="Edición de clientes" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg6.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg6.png";?>"
                            alt="Edición de clientes" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>En la base de datos la tabla a la que se hace referencia es: <b>clientes_contratos</b>.</p>
                <br />
                <h3 id="Página_Principal">3. Pantalla de Puestos de Clientes</h3>
                <p>Para capturar puestos de clientes debemos ir a la opción Puestos de Clientes ubicado en el menú
                    Administración. </p>
                <p>Esta pantalla permite asociar puestos a clientes. Para ello existen 3 botones: Agregar, para captura
                    de nueva asociación, Quitar, para
                    quitar el puesto asociado al cliente, y Editar registro seleccionado.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg8.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg8.png";?>"
                            alt="Pantalla puestos - clientes" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>Simplemente se captura o edita un solo registro, previamente debemos seleccionar el campo cliente y
                    posteriormente el botón Agregar para
                    nuevo registro, y si se seleccionó un registro de la tabla dar clic el el botón Editar o Quitar.</p>
                <p>En la base de datos la tabla a la que se hace referencia es: <b>cat_puestos</b>.</p>
                <br />
                <h3 id="Página_Principal">4. Pantalla Catálogo de Riesgos</h3>
                <p>Para acceder a esta pantalla debemos ir a la opción Catálogo de riesgos ubicado en el menú
                    Administración. </p>
                <p>Esta pantalla permite generar riesgos para posteriormente asociar a puestos de clientes. Para ello
                    existen 3 botones: Agregar, para captura de nuevo riesgo, Quitar, para
                    quitar el riesgo, y Editar registro seleccionado.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg9.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg9.png";?>"
                            alt="Pantalla catalogo de riesgos" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>Es algo compleja esta pantalla, ya que al crear nuevo riesgo no aparecen otras opciones que aparecen
                    al editar un riesgo que no es VERBO. Las opciones
                    que se tienen para agregar un riesgo son:</p>
                <ul>
                    <li>Verbo?, ya que si es verbo la palabra marcada como riesgo, no permitirá agregar subriesgos.</li>
                    <li>Nombre del riesgo, que debe ingresarse a un lado de la palabra Verbo?.</li>
                    <li>Ponderación, que se refiere al valor que tiene ese riesgo al momento de hacer los calculos de
                        resultados que se le presentarán al cliente, 0 significa no hay riesgo, 10 es el riesgo máximo.
                    </li>
                    <li>Area de riesgo, es un catálogo que se tiene para clasificar ese riesgo.</li>
                </ul>
                <p>Una vez que el riesgo asociado no es VERBO, y se da en el botón editar, aparece la siguiente
                    pantalla.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg10.png";?>"> -->
                        <img src="<?php print base_url()."assets/imgs/doc-admin/docimg10.png";?>"
                            alt="Pantalla catalogo de riesgos" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>En esta pantalla los campos adicionales son:</p>
                <ul>
                    <li>Agregar verbo. Al final lo importante es que el riesgo este asociado a Verbos.</li>
                    <li>Quitar el verbo. Elimina el verbo asociado al riesgo.</li>
                </ul>
                <p>Si se desea Borrar un riesgo de la tabla principal, se da en el botón Borrar y automáticamente se
                    elimina el riesgo y los
                    verbos asociados. </p>
                <p>En la base de datos la tabla a la que se hace referencia es: <b>cat_riesgos</b>.</p>
                <br />
                <h3 id="Página_Principal">5. Pantalla de Asociación de Puestos a Riesgos</h3>
                <p>Para acceder a esta pantalla debemos ir a la opción Clientes Puestos - Riesgos ubicado en el menú
                    Administración. Esta pantalla permite asociar los riesgos a puestos de clientes.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg11.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg11.png";?>"
                            alt="Pantalla Clientes puestos - riesgos" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>En esta pantalla simplemente seleccionamos el cliente, el puesto y en la tabla Catalogo de riesgos,
                    se selecciona el riesgo a agregar
                    y se da clic en el botón Agregar. En el caso de que un riesgo no sea VERBO, si se selecciona en
                    cualquiera de las 2 tablas el riesgo y se da
                    clic en el botón Detalles, aparecerá una ventana modal con los subriesgos asociados a ese riesgo,
                    como se muestra en la siguiente imagen.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg12.png";?>"> -->
                        <img src="<?php print base_url()."assets/imgs/doc-admin/docimg12.png";?>"
                            alt="Pantalla riesgos asociados" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>En la base de datos la tabla a la que se hace referencia es: <b>puestos_riesgos</b>.</p>
                <br />
                <h3 id="Página_Principal">6. Pantalla de Parámetros del sistema</h3>
                <p>Para acceder a esta pantalla debemos ir a la opción Parámetros del sistema ubicado en el menú
                    Administración. </p>
                <p>Esta pantalla permite actualizar los parámetros usados en todo el sistema.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg13.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg13.png";?>"
                            alt="Pantalla parametros del sistema" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>Simplemente se selecciona un registro, se da clic en el botón Editar, se edita el valor y se da clic
                    en el botón Guardar para actualizar
                    ese parámetro.</p>
                <p>Adicionalmente dentro del proyecto scripts_huella/py_huella (scripts en python), existe un archivo de parametros llamado parametros.py, 
                donde vienen opciones de configuracion como conexion a las bases de datos que no se pueden poner en la tabla de base de datos.</p>
                <p>En la base de datos la tabla a la que se hace referencia es: <b>parametros</b>.</p>
                <br />
                <h3 id="Página_Principal">7. Pantalla de Sitios a escanear</h3>
                <p>Para acceder a esta pantalla debemos ir a la opción Sitios a escanear ubicado en el menú
                    Administración. </p>
                <p>Esta pantalla permite crear y editar registros de posibles sitios web o dominios que podremos
                    escanear para obtener Noticias. El botón llamaod Deshabilitar, deshabilita o habilita un
                    registro seleccionado para no escanearlo.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg14.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg14.png";?>"
                            alt="Pantalla sitios a escanear" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>Los campos que estan disponibles para llenar en esta pantalla son:</p>
                <ul>
                    <li>Dominio, se captura toda la URL del dominio, incluyendo el protocolo y sin diagonal al final del
                        .com o .com.mx</li>
                    <li>Ponderación o Peso, valor que se le dará al dominio, según la importancia del sitio web. Valor
                        usado para los calculos de resultados que se le presentarán al cliente, 0 significa no hay
                        riesgo, 10 es el riesgo máximo.</li>
                    <li>Categoría, cada dominio entra en una categoría, pudiendo ser: Periódico, Foro, Red Social,
                        Página de gobierno, Portal web.</li>
                    <li>País, para saber de que país es un sitio web.</li>
                    <li>Estado, para saber de que estado del país es un sitio web.</li>
                </ul>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg15.png";?>"> -->
                        <img src="<?php print base_url()."assets/imgs/doc-admin/docimg15.png";?>"
                            alt="Cajas de texto para sitio a escanear" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>En la base de datos la tabla a la que se hace referencia es: <b>cat_sitiosweb</b>.</p>
                <br />
                <h3 id="Página_Principal">8. Pantalla de Usuarios</h3>
                <p>Para acceder a esta pantalla debemos ir a la opción Usuarios ubicado en el menú Administración. </p>
                <p>Esta pantalla permite crear, editar, borrar y deshabilitar usuarios de los clientes.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg16.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg16.png";?>"
                            alt="Pantalla sitios a escanear" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>Hay campos obligatorios a llenar, y estan marcados con asterísco Rojo. Los campos que estan
                    disponibles para llenar en esta pantalla son:</p>
                <ul>
                    <li>Paterno, apellido paterno del usuario.</li>
                    <li>Materno, apellido materno del usuario.</li>
                    <li>Nombre, nombre del usuario.</li>
                    <li>Correo, correo electrónico del usuario.</li>
                    <li>Perfil, tipo de perfil. Para los clientes siempre debe ser perfil clientes.</li>
                    <li>Cliente, se refiere al cliente para el cual se esta creando el usuario.</li>
                    <li>Contraseña y confirmar contraseña, solo cuando es nuevo registro es obligatorio su llenado, si
                        es actualización no es obligatorio.
                        La contraseña en base de datos se guarda en formato BCRYPT con un tamaño de 60 caracteres.</li>
                </ul>
                <p>En la base de datos la tabla a la que se hace referencia es: <b>cat_usuarios</b>.</p>
                <br />
                <h3 id="Página_Principal">9. Pantallas y Perfiles</h3>
                <p>Este tema abarca 3 opciones del menú Administración que son: Pantallas, Perfiles y
                    Perfiles-Pantallas. En lo que se refiere a las opciones Pantallas y Perfiles la creación, edición y
                    borrado de registros es semejante
                    a las pantallas anteriores. </p>
                <p>En el caso de la pantalla Perfiles simplemente es un catálogo de 2 perfiles actualmente:
                    Administrador y Clientes.</p>
                <p>En la base de datos la tabla a la que se hace referencia es: <b>cat_perfiles</b>.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg18.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg18.png";?>"
                            alt="Pantalla sitios a escanear" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>En el caso de la pantalla Pantallas, aquí si hay varios campos por agregar los cuales se explican a
                    continuación:
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg19.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg19.png";?>"
                            alt="Pantalla sitios a escanear" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <ul>
                    <li>Nombre del menú, nombre que tendrá la pantalla en el menú.</li>
                    <li>Descripción, a que se refiere la pantalla.</li>
                    <li>Controlador, qué controlador va a capturar los eventos de la pantalla.</li>
                    <li>Orden, que órden respecto a las demas pantallas va a tener la pantalla.</li>
                    <li>Icono, icono que tendrá en el menú la pantalla. Tiene que ser iconos awesome incluidos en la
                        librería font-awesome 5.3 (assets/template/plugins/font-awesome/5.3)</li>
                    <li>Menu padre, en caso de que sea hijo de algún menú padre.</li>
                    <li>Tiene submenú?, se debe marcar en caso de que vaya a tener menus hijos.</li>
                    <li>Es pantalla pública?, se marca para indicar que esa pantalla es accedida sin necesidad de
                        loguearse en el sistema.</li>
                    <li>No tiene menú?, se marca en caso de que no deba ser incluida en el menú principal.</li>
                </ul>
                <p>En la base de datos la tabla a la que se hace referencia es: <b>cat_pantallas</b>.</p>
                <br />
                <p>Finalmente la pantalla que integra la relación entre pantallas y perfiles sería Perfiles - Pantallas.
                    En esta pantalla
                    simplemente se selecciona el perfil y en la parte de abajo se selecciona un registro de una de las 2
                    tablas ya sea para agregar
                    o quitar pantallas.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg20.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg20.png";?>"
                            alt="Pantalla sitios a escanear" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>En la base de datos la tabla a la que se hace referencia es: <b>perfiles_pantallas</b>.</p>
                <br />
                <h3 id="Página_Principal">10. Monitor de Demonios</h3>
                <p>Para acceder a esta pantalla debemos ir a la opción Monitor de Demonios ubicado en el menú Procesos.
                </p>
                <p>Esta pantalla permite observar como se comportan algunos demonios usados en el backend, hechos en
                    Python y NodeJS. Por el momento
                    hay 2 demonios en posible ejecución: uno se llama huellamongen.py, el cual esta activo si cualquiera
                    de los 2 botones Iniciar servicio o Detener
                    servicio estan habilitados. En caso de que no este ningúno debemos activarlo desde consola Linux con
                    la instrucción: <br /># python3 /var/www/scripts_huella/pyhuella/huellamongen.py </p>
                <p>Una vez encendido en demonio huellamongen.py, se habilita uno u otro botón para Iniciar o Detener el
                    demonio hilos.py. Este demonio es el encargado de
                    observar las peticiones de análisis persona-puesto que un cliente quiere realizar, checando la tabla
                    <b>api_consultas</b>. Por cada persona-puesto desencadena
                    un hilo de ejecución. Lo que esta sucediendo en este demonio se muestra en esta pantalla.
                </p>
                <p>Esta pantalla muestra los últimos 20, 50 o 100 registros del Log del demonio hilos.py, el log se esta actualizando en esta pantalla cada 10 segundos, a menos que se de clic en el botón
                    <em>Actualizar log</em>.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg21.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg21.png";?>"
                            alt="Pantalla monitor de demonios" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>Otro demonio que se ejecuta cada cierto tiempo según lo especificado en el servidor, es el demonio capturador de noticias. Se programón con el crontab del sistema operativo. 
                Para ello debemos ejecutar como usuario correspondiente <em>crontab -e</em>. Este demonio ha sido programado usando el lenguaje NODEjs.</p>
                <br />
                <h3 id="Página_Principal">11. Subir Layouts</h3>
                <p style="text-align:justify">Esta pantalla permite del lado del perfil Cliente subir archivos de Excel
                    para asociar una persona -
                    puesto para su análisis, o bien, subir un
                    catálogo completo de personas, y no capturarlas manualmente. Adicionalmente del lado del perfil
                    Administrador
                    hay un layout para subir nuevos dominios a analizar. Para acceder a esta pantalla debemos dar
                    clic en el menú Procesos / Subir Layouts.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg22.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg22.png";?>"
                            alt="Pantalla Subir Layouts" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>En lo que respecta al layout de sitios web para subirlo requerimos, descargar el layout
                    correspondiente de dominios o sitios web, lo llemanos en archivo en excel, y lo arrastramos a la
                    zona donde se
                    adjunta el archivo, previamente seleccionamos que vamos a subir un layout de tipo Catalogo de sitios
                    web. Finalmente se da clic en el botón Procesar</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg23.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg23.png";?>"
                            alt="Layout sitios web" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>Los campos que se piden en este Layout son los mismos que se piden en la pantalla de sitios web. La
                    ponderación o peso debe ser un valor entre 1 y 10, tambien debemos tener cuidado con el valor
                    de la categoria, ya que esta debe coincidir con un identificador de categoria valida, los cuales se
                    muestran en la siguiente imagen:</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg24.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg24.png";?>"
                            alt="Categorias para sitios web" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>En caso de que ya existiera un dominio, se actualiza con los nuevos datos dados en el layout. Esta
                    pantalla afecta las tablas de base de datos: <b>cat_personas</b>, <b>api_consultas</b> y
                    <b>cat_sitiosweb</b>.
                </p>
                <br />
                <h3 id="Página_Principal">12. Opciones Administrativas</h3>
                <p style="text-align:justify">Esta pantalla permite modificar ciertas opciones administrativas como el cambio de 
                cliente para ver los estadisticos y resultados de sus procesos y observar que todo este en buena operación. Para acceder a esta pantalla debemos dar
                    clic en el menú Administración / Opciones administrativas.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <!-- <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg25.png";?>"> -->
                        <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg25.png";?>"
                            alt="Pantalla Subir Layouts" />
                        <!-- </a> -->
                        <span class="image-caption"></span>
                    </span>
                </p>
                <br />
            </div>
        </div>
    </div><!-- panel body -->
</div><!-- panel -->

<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Procesos Internos en el Backend</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 id="Una_Ejemplo_de_un_Sitio_Web_con_PHP"></h3>
                <p>En lo que se refiere a este tema de procesos que se realizan en el backend, existen los siguientes
                    scripts que se implementaron en Python y nodeJS:</p>
                <ul>
                    <li><b>Crawler Web</b>, que captura en la tabla paginas de la base de datos huelladb el contenido de
                        los sitios web registrados en la tabla cat_sitiosweb. Esta programado
                        en nodeJS, y se ejecuta indefinidamente capturando cada 5 segundos una pagina URL que se
                        encuentran en la tabla urls_procesadas. Se inicializa con: <br /># nodejs Main_v4.js start -a
                        <br />Donde la opción -a significa que guardara nuevos links en la tabla urls_procesadas.
                        <br />Adicionalmente puede aceptar el parametro -t 3, que significa que capturará contenido de un cierto dominio cada 3 segundos. Los segundos son configurables.
                    </li>
                    <li><b>ScreenCapture</b>, se encarga de capturar la pantalla del sitio web que ha sido guardado en
                        la tabla paginas. Esta hecho en nodeJS y se ejecuta cada dia a las
                        7 de la mañana, usando crontab. La instrucción de ejecución es: <br /># nodejs ScreenCapture.js
                        start</li>
                    <li><b>huellamongen</b>, se encarga de iniciar o detener el demonio hilos.py. Esta observando cada
                        10 segundos el valor del archivo
                        statusCodeHuellamon.txt que actualizamos desde los botones (Iniciar/Detener) de la pantalla
                        Monitor de Demonios, este archivo esta ubicado en la ruta
                        <em>/var/www/threats/www/scripts_huella/pyhuella</em>. Este script esta realizado en lenguaje
                        Python3.
                    </li>
                    <li><b>dumpsqlhuelladb</b>, se encarga de respaldar la base de datos huelladb y generar un zip. Esta
                        realizado en lenguaje Python3.</li>
                    <li><b>script_actualiza_status_contratos</b>, se encarga de actualizar en la primera hora del día el
                        status de los contratos, principalmente contratos que son de tipo Periodo. Poniendo el status en
                        'T'-Terminado. Esta realizado en lenguaje Python3.</li>
                    <li><b>hilos</b>, este demonio es el encargado de
                        observar las peticiones de análisis persona-puesto que un cliente quiere realizar, checando la
                        tabla <b>api_consultas</b>. Por cada persona-puesto desencadena un hilo de ejecución. Esta
                        realizado en lenguaje Python3.
                    </li>
                    <li><b>Analizador Semántico</b>, este script es todo un script complejo el cual es el encargado de
                        analizar las notas de las noticias y otras fuentes para darle ponderación a una persona-puesto
                        según los riesgos
                        localizados en esas notas. Se ejecuta desde el script <em>hilos.py</em>. Esta realizado en
                        lenguaje Python3 con
                        el uso de la herramienta NLP Spacy.
                    </li>
                </ul>
                <p>Los siguientes scripts realizados en python 3 fueron realizados para el análisis de redes sociales,
                    búsquedas en motores web,
                    y búsquedas en el mundo OSINT. Todos guardan de manera temporal el resultado en la ruta
                    <em>/var/www/threats/www/huella/assets/documents</em>.
                    Todos estos scripts reciben como parámetros: el identificador de la persona de la tabla
                    cat_personas, y el nombre del archivo temporal donde se
                    escribirá el resultado de las operaciones realizadas en el script. Los resultados adicionales de
                    información localizada sobre la pesona, se guardan en
                    las tablas <b>urls_sociales</b> y <b>expedientes_anexos</b> de la base de datos huelladb.</p>
                <h5>Scripts de análisis de redes sociales</h5>
                <ul>
                    <li><b>scrap_facebook.py</b>, se encarga de capturar comentarios en la red social Facebook. Sin
                        embargo, si no encuentra comentarios donde
                        aparezca la persona como tal, no se guardan.
                    </li>
                    <li><b>scrap_youtube.py</b>, se encarga de capturar comentarios en la red social Youtube. Sin
                        embargo, si no encuentra comentarios donde
                        aparezca la persona como tal, no se guardan.
                    </li>
                </ul>
                <h5>Scripts para búsquedas en motores web</h5>
                <ul>
                    <li><b>scrap_anypagebing.py</b>, desde el motor de búsqueda bing.com, se encarga de capturar
                        contenido web de páginas donde aparece mencionada la persona que se busca. La busqueda se hace por el nombre completo de la persona,
                        el usuario del email antes del arroba (@), y por el Alias.
                    </li>
                    <li><b>scrap_anypagegoogle.py</b>, desde el motor de búsqueda google.com.mx, se encarga de capturar
                        contenido web de páginas donde aparece mencionada la persona que se busca. La busqueda se hace por el nombre completo de la persona,
                        el usuario del email antes del arroba (@), y por el Alias.
                    </li>
                    <!-- <li><b>scrap_anypageyahoo.py</b>, desde el motor de búsqueda mx.search.yahoo.com, se encarga de
                        capturar contenido web de páginas donde aparece mencionada la persona que se busca.
                    </li> -->
                </ul>
                <h5>Scripts para búsquedas en el mundo OSINT</h5>
                <pa>Para la ejecución de estos scripts, es necesario tener una api key del sitio. Esto se configura en
                    la tabla parametros de la base de datos huelladb.
                    <ul>
                        <li><b>fromClearbit.py</b>, permite saber la reputación de un email desde el sitio clearbit.com
                        </li>
                        <li><b>fromEmailrep.py</b>, permite saber la reputación de un email desde el sitio emailrep.io
                        </li>
                        <li><b>fromFullcontact.py</b>, obtiene información personal como lugar de trabajo, etc, que ha
                            recabado el sitio web fullcontact.com
                        </li>
                        <li><b>fromHunter.py</b>, obtiene información personal si es que existe en el almacen del sitio
                            web hunterio.io usando el email del trabajo de la persona.
                        </li>
                        <li><b>fromNumverify.py</b>, permite verificar que un número telefónico con clave del país sea
                            válido y de cierto lugar numverify.com
                        </li>
                    </ul>
                    <br />
            </div>
        </div>
    </div><!-- panel body -->
</div><!-- panel -->

<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Documentación de la Base de Datos</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 id="Una_Ejemplo_de_un_Sitio_Web_con_PHP">1. Diagramas Entidad-Relación</h3>
                <p>Para poder entender bien como se ha compuesto la base de datos, es indispensable ver graficamente
                    como se componen las tablas. Para empezar
                    tenemos un diagrama de ingreso principal al sistema, y de la configuración que un cliente puede
                    tener dentro del mismo.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg2.png";?>">
                            <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg2.png";?>"
                                alt="Diagrama E-R principal" />
                        </a>
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>Como se puede observar de manera general tenemos lo siguiente:</p>
                <ul>
                    <li>Un cliente puede tener n números de contratos. Donde cada contrato tiene es por periodo o por
                        numero de procesamientos. Cada contrato tiene asociado un API_KEY unico para peticiones API
                        REST. </li>
                    <li>Un cliente puede tener n usuarios de tipo Cliente.</li>
                    <li>Un cliente puede tener asociado n puestos. Donde cada puesto puede tener asociado n riesgos por
                        cliente.</li>
                    <li>Un cliente puede tener asociado n personas. Donde cada persona puede ser capturada desde API
                        REST, de un layout en excel o desde la pantalla de personas.</li>
                    <li>Existen 40 pantallas en base de datos de las cuales solo 12 pantallas puede tener acceso a un
                        perfil cliente.</li>
                </ul>
                <br />
                <p>Enseguida tenemos otro diagrama entidad relación complementario, donde se procesan las
                    personas-puesto por cliente.</p>
                <p class=" text-center"><span class="image-wrapper">
                        <a target="_blank" href="<?php print base_url()."assets/imgs/doc-admin/docimg3.png";?>">
                            <img style="width:70%" src="<?php print base_url()."assets/imgs/doc-admin/docimg3.png";?>"
                                alt="Diagrama E-R principal" />
                        </a>
                        <span class="image-caption"></span>
                    </span>
                </p>
                <p>De manera general sucede lo siguiente:</p>
                <ul>
                    <li>Un cliente tiene derecho a n peticiones de procesamientos, el cual puede ser ilimitado si el
                        contrato es por periodo, y limitado si el
                        contrato es por tiempo.</li>
                    <li>Un cliente puede pedir o no que se vuelva a procesar una persona-puesto, esto se guarda en la
                        tabla config_clientes.</li>
                    <li>Un cliente puede pedir análisis de personas-puesto desde 3 métodos, ya sea desde el API REST,
                        desde la pantalla Resultados del aplicativo web, o bien desde
                        un layout en excel. Las peticiones de análisis se guardan en la tabla api_consultas</li>
                    <li>Antes de realizarle en análisis a una persona-puesto, debe existir un expediente por persona,
                        donde cada expediente
                        tiene asociadas n noticas, darweb, redes sociales, consultas osint, en caso de que aplique al
                        cliente por las FUENTES contratadas, y
                        en caso de que la persona sea localizada en esas fuentes. El expediente por persona se guarda en
                        la tabla Expedientes_Anexos.</li>
                    <li>Las tablas intermedias entre lo localizado en las fuentes anteriores son urls_sociales, paginas
                        y darkweb.pages y de ahí hay relación con tabla expedientes_anexos. La
                        tabla urls_sociales guarda consultas de: redes sociales, busquedas en motores web y búsquedas en
                        osint-tools.</li>
                    <li>La tabla darkweb.pages existe en la base de datos darkweb.</li>
                    <li>Una vez finalizado el análisis los resultados se guardan en 2 tablas: Resultados y
                        Resultados_detalles. Sin embargo, al cliente
                        en la pantalla de Resultados se le muestra primeramente lo de la tabla Resultados.</li>
                </ul>
                <br />
                <h3 id="Una_Ejemplo_de_un_Sitio_Web_con_PHP">2. Diccionario de datos</h3>
                <p>Existen 2 bases de datos que usamos para desarrollar esta aplicación: Huelladb y Darkdb.</p>
                <p>Las tablas que usamos de la base de datos principal llamada Huelladb son las siguientes:</p>
                <ul>
                    <li><b>cat_usuarios</b>, contiene el catalogo de usuarios que ingresaran al sistema.</li>
                    <li><b>cat_perfiles</b>, contiene el catalogo de perfiles de usuarios que ingresaran al sistema.
                    </li>
                    <li><b>cat_pantallas</b>, contiene el catalogo de pantallas que estan disponibles en el sistema.
                    </li>
                    <li><b>perfiles_pantallas</b>, contiene la relación pantallas y perfiles a los cuales tiene acceso
                        un usuario en el sistema.</li>
                    <li><b>cat_anexos</b>, contiene el catalogo de tipos de fuentes de información.</li>
                    <li><b>cat_areas_riesgos</b>, contiene el catalogo de posible clasificacion de los riesgos, usada en
                        la tabla cat_riesgos.</li>
                    <li><b>cat_categorias_dominios</b>, contiene el catalogo de categorias disponibles para clasificar
                        los sitios web en la tabla cat_sitiosweb.</li>
                    <li><b>cat_clientes</b>, contiene el catalogo de clientes o empresas que requieren el servicio.</li>
                    <li><b>cat_configuraciones_clientes</b>, contiene el catalogo de posibles configuraciones a las que
                        un cliente tiene acceso en el sistema.</li>
                    <li><b>cat_osintools</b>, contiene el catalogo de herramientas OSINT que usamos al sistema</li>
                    <li><b>cat_paises</b>, contiene el catalogo de paises del mundo usado en la tabla cat_sitiosweb para
                        saber de que pais es un dominio.</li>
                    <li><b>cat_personas</b>, contiene el catalogo de las personas que un cliente ha capturado.</li>
                    <li><b>cat_plataformas</b>, contiene el catalogo de redes sociales y su relación con las
                        herramientas OSINT que usamos en el sistema.</li>
                    <li><b>cat_puestos</b>, contiene el catalogo de puestos que podrían existir en una empresa o
                        negocio.</li>
                    <li><b>cat_riesgos</b>, contiene el catalogo de posibles riesgos a analizar.</li>
                    <li><b>cat_sitiosweb</b>, contiene el catalogo de dominios a escanear para obtener noticias.</li>
                    <li><b>api_consultas</b>, contiene todas las peticiones de consulta que hace el cliente para
                        procesar una persona-puesto.</li>
                    <li><b>api_eventos</b>, contiene el catalogo de eventos que se aceptan en el API REST.</li>
                    <li><b>clientes_contratos</b>, contiene el catalogo de contratos realizados a un cliente.</li>
                    <li><b>clientes_puestos</b>, contiene la relación entre puestos contratados por el cliente.
                        Independientemente del contrato, el cliente tiene acceso
                        a los puestos y a datos estadísticos asociados.</li>
                    <li><b>config_clientes</b>, contiene las opciones de configuración de un cliente dentro del sistema.
                    </li>
                    <li><b>contratos_fuentes</b>, contiene la relación entre las fuentes contratadas por el cliente y el
                        contrato.</li>
                    <li><b>expedientes_anexos</b>, contiene la relación persona-puesto y las distintas fuentes de donde
                        se le ha localizado: redes sociales, noticias, darkweb, osint.</li>
                    <li><b>log_clientes</b>, contiene el log de un cliente, actividad que realiza un cliente en el
                        sistema.</li>
                    <li><b>paginas</b>, contiene el contenido de páginas de noticias.</li>
                    <li><b>parametros</b>, contiene parámetros de configuración usados en el sistema.</li>
                    <li><b>puestos_riesgos</b>, contiene los riesgos asociados a un puesto.</li>
                    <li><b>resultados</b>, contiene el riesgo mayor asociado a una persona-puesto.</li>
                    <li><b>resultados_detalles</b>, contiene un desgloce de todos los riesgos asociados a una
                        persona-puesto.</li>
                    <li><b>riesgos_subriesgos</b>, contiene riesgos asociados a otro riesgo. Como por ejemplo el riesgo
                        crimen organizado se asocia con verbos como: escalar, estar, pertenecer, relacionar, ser,
                        vincular.</li>
                    <li><b>urls_procesadas</b>, contiene el catalogo de urls que de las cuales se consultará contenido
                        web, si es que cumple con los requisitos como son que tenga un tamaño de cuerpo mayor a 500
                        caracteres y sea una paigna web con contenido válido para análisis.</li>
                    <li><b>urls_sociales</b>, guarda consultas de: redes sociales, busquedas en motores web y búsquedas
                        en osint-tools</li>
                </ul>
                <br />
                <h3 id="Una_Ejemplo_de_un_Sitio_Web_con_PHP">3. Creación y respaldo de la base de datos huelladb</h3>
                <p>El script mencionado anteriormente en la sección procesos internos en el backend, dumpsqlhuelladb.py,
                    permite
                    realizar un respaldo automático de la base de datos usanod el usuario userdb. El archivo de salida
                    se llama huelladb-fecha(actual-YYYY-MM-DD).sql.gz</p>
                <p>Para realizar la restauración de la base de datos, debemos crearla de la siguiente manera dentro de
                    la consola MARIADB:
                    <br /># CREATE DATABASE huelladb character set = 'utf8mb4' collate = 'utf8mb4_general_ci'; <br />
                    y posteriormente ejecutar el siguiente comando en la consola linux: <br />
                    # mariadb -u userdb -p -D huelladb < huella-2020-07-30.sql --default-character-set=utf8mb4</p>

            </div>
        </div>
    </div><!-- panel body -->
</div><!-- panel -->