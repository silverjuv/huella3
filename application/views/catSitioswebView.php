<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Catálogos</a></li>
    <li class="breadcrumb-item active">Sitios Web</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Sitios Web <small>Gestión de sitios web...</small></h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Sitios Web</h4>
    </div>
    <div class="panel-body">
        <div id="ctrls" class="row">
            <div class="col-sm-12 text-center">
                <div class="btn-group btn-group-sm" role="group">

                    <button id="_agregar" type="button" class="btn btn-lime btn-sm">
                        <i class="fas fa-asterisk"></i>
                        Nuevo
                    </button>
                    <button id="_editar" type="button" class="btn btn-warning btn-sm">
                        <i class="fas fa-edit"></i>
                        Editar
                    </button>
                    <!-- <button id="_borrar" type="button" class="btn btn-danger btn-sm">
                        <i class="fas fa-trash-alt"></i>
                        Borrar
                    </button> -->
                    <button id="_activar" type="button" class="btn btn-gray btn-sm"
                        title="Habilitar/Deshabilitar sitio web">
                        <i class="fas fa-user-slash"></i>
                        Deshabilitar
                    </button>
                    &nbsp;<button type="button" class="btn btn-warning btn-sm" id="exportData">
                        <i class="fa fa-print" aria-hidden="true"></i> Exportar tabla
                    </button>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-12">
                <table id="tsitiosweb" class="responsive table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th>id_sitioweb</th>
                            <th>Sitio</th>
                            <th>Categoria</th>
                            <th>Peso</th>
                            <th>Paginas capturadas</th>
                            <th>Pais</th>
                            <th>Entidad</th>
                            <th>Habilitado</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>

    </div><!-- panel body -->
</div><!-- panel -->

<div class="modal fade" id="__modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="__modal_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>

            <div id="__modal_body" class="modal-body">

                <input type="hidden" id="id_edicion" value="">
                <div class="row">
                    <div class="col-sm-4">
                        <label for="dominio">Sitio web (dominio):</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" id="dominio" class="form-control" maxlength="100">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                        <label for="peso">Peso:</label>
                    </div>
                    <div class="col-sm-8">
                        <select name="peso" id="peso" class="form-control" style="width: 100%">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                        <label for="categoria">Categoria</label>
                    </div>
                    <div class="col-sm-8">
                        <select name="id_categoria" id="id_categoria" class="form-control" style="width: 100%">
                        </select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                        <label for="pais">Pais</label>
                    </div>
                    <div class="col-sm-8">
                        <select name="cve_pais" id="cve_pais" class="form-control" style="width: 100%">
                        </select>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-sm-4">
                        <label for="entidad">Entidad</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" id="entidad" class="form-control" maxlength="40">
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary" id="btnGuardar">Guardar</button>
            </div>

        </div>
    </div>
</div>

<!--dialog exportar a-->
<div class="modal fade " id="exportarDialog" role="dialog">
    <div class="modal-dialog modal-sm " role="document">
        <div class="modal-content">
            <div class="modal-header alert-dark">
                <h4 class="modal-title" id="exportarDialog_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div id="__modal_body" class="modal-body alert-secondary">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="radio">
                            <label><input type="radio" name="optradio" value="XLS" checked> &nbsp;Excel (xlsx)</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="radio">
                            <label><input type="radio" name="optradio" value="CSV"> &nbsp;CSV</label>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="btn-group btn-group-sm" role="group">
                        <button id="_exportar" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i>Exportando..." class="btn btn-success btn-sm">
                            <i class="fas fa-filter"></i> Exportar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin dialog enviar a analizar-->