<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Personas-Puesto y Resultados</a></li>
    <li class="breadcrumb-item active">Resultados</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Personas-Puesto en Analisis y Analizadas <small>(Resultados)</small></h1>
<!-- end page-header -->


<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Personas-Puesto en Analisis y Analizadas</h4>
    </div>
    <div class="panel-body alert-secondary">
        <div class="row">
            <div class="col-xl-12">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a href="#default-tab-1" data-toggle="tab" class="nav-link active">
                            <!-- <span class="d-sm-none">Tab 2</span> -->
                            <span class="d-sm-block d-none">Personas-Puesto en análisis</span>
                        </a>
                    </li>                    
                    <li class="nav-item">
                        <a href="#default-tab-2" data-toggle="tab" class="nav-link ">
                            <!-- <span class="d-sm-none">Tab 1</span> -->
                            <span class="d-sm-block d-none">Personas-Puesto analizadas</span>
                        </a>
                    </li>                    
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="default-tab-1">
                        <!-- <div id="ctrls" class="row">
                            <div class="col-sm-12 text-center">
                                <div class="btn-group btn-group-sm" role="group">
                                    <button id="_enviarAproceso" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i>Enviando..." class="btn btn-info btn-sm">
                                        <i class="fas fa-filter"></i>
                                        Seleccionar y Enviar persona - puesto a analizar
                                    </button>
                                </div>
                            </div>
                        </div> -->
                        <div class="row mt-2">
                            <div class="table-responsive">
                                <table id="tConsultas" class="responsive table  table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Acciones</th>
                                            <!-- <th>Empresa</th> -->
                                            <th>id_persona</th>
                                            <th>Persona</th>
                                            <th>Puesto</th>
                                            <th>Fecha de Registro</th>                                            
                                            <th>Analisis por OSINT</th>
                                            <th>Analisis por Buscadores Web</th>
                                            <th>Analisis por Redes Sociales</th>
                                            <th>Analisis por DarkWeb</th>
                                            <th>Analisis por Noticias</th>
                                            <th>Programó Análisis</th>
                                            <th>Medio de registro</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="default-tab-2">
                        <div id="ctrls" class="row">
                            <div class="col-sm-12 text-center">
                                <div class="btn-group btn-group-sm" role="group">
                                    <button type="button" class="btn btn-danger btn-sm" id="_btnInforme">
                                        <i class="fa fa-person" aria-hidden="true"></i> Ver Informe del Registro seleccionado
                                    </button>
                                    &nbsp;<button id="_btnDetalles" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Buscando en OSINT..." class="btn btn-green btn-sm">
                                        <i class="fas fa-address-card"></i>
                                        Ver Ponderación de Riesgos del Registro seleccionado
                                    </button>
                                    &nbsp;<button type="button" class="btn btn-primary btn-sm" id="sendmailLote">
                                        <i class="fa fa-envelope" aria-hidden="true"></i> Recibir en email detalles del Lote del registro seleccionado
                                    </button>
                                    &nbsp;<button type="button" class="btn btn-warning btn-sm" id="exportData">
                                        <i class="fa fa-print" aria-hidden="true"></i> Exportar tabla
                                    </button>
                                </div>
                            </div>
                            <!-- <br /><br /> -->
                        </div>
                        <div class="row">
                            <div class="col-sm-12" style="font-size:11px"><br /> * Los resultados cuando son por petición Layout o API Rest se envían al correo electrónico del cliente,
                                siempre y cuando el cliente tenga habilitada esta opción en la pantalla <b>Configuración de cuenta</b>.
                                En caso de que no se enviara un correo, existe el botón para enviar email de resultados ya sea por Lote o por registro seleccionado.
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="table-responsive">
                                <table id="tResultados" class="responsive table  table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Acciones</th>
                                            <th>id_persona</th>
                                            <th>Medio de petición</th>
                                            <th>Fecha-Hora</th>
                                            <th>CURP</th>
                                            <th>Nombre</th>
                                            <th>Email</th>
                                            <th>Celular</th>
                                            <th>INE</th>
                                            <th>Dirección</th>
                                            <th>Empresa</th>
                                            <th>Puesto</th>
                                            <th>Riesgo</th>
                                            <th>Ponderación</th>
                                            <th>Lote o Grupo</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div><!-- panel body -->
</div><!-- panel -->
<!--dialog agregar a expediente-->
<div class="modal fade " id="verResultadosDialog" role="dialog">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header alert-dark">
                <h4 class="modal-title" id="verResultadosDialog_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div id="__modal_body" class="modal-body alert-secondary">
                <div class="demo">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="row">
                                <div class="col-sm-6">
                                    <span class="label label-info">Riesgos encontrados</span>
                                </div>
                                <div class="col-sm-6">
                                    <!-- <button type="button" class="btn btn-primary btn-sm" onclick="sendMail()" id="sendmail">
                                        <i class="fa fa-envelope" aria-hidden="true"></i> Recibir en email m&aacute;s detalles
                                    </button> -->
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="tRiesgosAsociados" class="responsive table  table-bordered " width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th>Acciones</th> -->
                                            <!-- <th>Puesto</th> -->
                                            <th>Riesgos</th>
                                            <th>Ponderación</th>
                                            <th>Fuente</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <span id="spamMayorRiesgo" class="label label-danger"></span>
                            <input id="demo">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin dialog agregar expediente-->

<!--dialog enviar a analizar-->
<div class="modal fade " id="enviarAnalizarDialog" role="dialog">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header alert-dark">
                <h4 class="modal-title" id="enviarAnalizarDialog_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div id="__modal_body" class="modal-body alert-secondary">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table id="tpersonas" class="responsive table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>Acciones</ht>
                                        <th>id_persona</th>
                                        <th>NOMBRE</th>
                                        <th>CURP</th>
                                        <th>INE</th>
                                        <th>ALIAS</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-1">
                        <label for="pass">Puesto:</label>
                    </div>
                    <div class="col-sm-11">
                        <select name="id_puesto" id="id_puesto" style="width:100%"></select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="btn-group btn-group-sm" role="group">
                        <button id="_enviar" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i>Enviando..." class="btn btn-success btn-sm">
                            <i class="fas fa-filter"></i> Enviar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin dialog enviar a analizar-->


<!--dialog exportar a-->
<div class="modal fade " id="exportarDialog" role="dialog">
    <div class="modal-dialog modal-sm " role="document">
        <div class="modal-content">
            <div class="modal-header alert-dark">
                <h4 class="modal-title" id="exportarDialog_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div id="__modal_body" class="modal-body alert-secondary">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="radio">
                            <label><input type="radio" name="optradio" value="XLS" checked> &nbsp;Excel (xlsx)</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="radio">
                            <label><input type="radio" name="optradio" value="CSV"> &nbsp;CSV</label>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="btn-group btn-group-sm" role="group">
                        <button id="_exportar" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i>Exportando..." class="btn btn-success btn-sm">
                            <i class="fas fa-filter"></i> Exportar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin dialog enviar a analizar-->



<!--dialog informe-->
<div class="modal fade " id="informeDialog" role="dialog">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header alert-dark">
                <h4 class="modal-title" id="informeDialog_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div id="__modal_body" class="modal-body alert-secondary">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <button type="button" class="btn btn-primary btn-sm" onclick="sendMail()" id="sendmail">
                                    <i class="fa fa-envelope" aria-hidden="true"></i> Recibir en email este informe
                                </button>
                            </div>
                        </div>
                        <br />
                        <div class="row mt-3">
                            <div class="col-sm-3">
                                <span style="color:red"> * </span><label for="nombre">Nombre</label>
                            </div>
                            <div class="col-sm-9">
                                <input id="nombre" type="text" class="form-control" value="" readonly="readonly" style="font-weight: bold; color: #000">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-3">
                                <span style="color:red"> * </span><label for="ine">INE</label>
                            </div>
                            <div class="col-sm-4">
                                <input id="ine" type="text" maxlength="18" class="form-control" value="" readonly="readonly" style="font-weight: bold; color: #000">
                            </div>
                            <div class="col-sm-2 text-right">
                                <span style="color:red"> * </span><label for="curp">CURP</label>
                            </div>
                            <div class="col-sm-3">
                                <input id="curp" type="text" maxlength="18" class="form-control" value="" readonly="readonly" style="font-weight: bold; color: #000">
                            </div>
                        </div>
                        <!-- <div class="row mt-3">
        </div> -->
                        <div class="row mt-3">
                            <div class="col-sm-3">
                                <span style="color:red"> * </span><label for="email">Email</label>
                            </div>
                            <div class="col-sm-3">
                                <input id="email" type="text" maxlength="60" class="form-control" value="" readonly="readonly" style="font-weight: bold; color: #000">
                            </div>
                            <div class="col-sm-3 text-right">
                                <span style="color:red"> * </span><label for="celular">Celular</label>
                            </div>
                            <div class="col-sm-3">
                                <input id="celular" type="text" maxlength="10" class="form-control" value="" readonly="readonly" style="font-weight: bold; color: #000">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-3">
                                <label for="twitter">Cuenta Twitter sin @:</label>
                            </div>
                            <div class="col-sm-3">
                                <input id="twitter" placeholder="" maxlength="60" type="text" class="form-control" value="" readonly="readonly" style="font-weight: bold; color: #000">
                            </div>
                            <div class="col-sm-3 text-right">
                                <label for="linkedin">Cuenta Linkedin:</label>
                            </div>
                            <div class="col-sm-3">
                                <input id="linkedin" placeholder="" maxlength="60" type="text" class="form-control" value="" readonly="readonly" style="font-weight: bold; color: #000">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-3">
                                <label for="alias">Alias</label>
                            </div>
                            <div class="col-sm-3">
                                <input id="alias" placeholder="" maxlength="60" type="text" class="form-control" value="" readonly="readonly" style="font-weight: bold; color: #000">
                            </div>
                            <div class="col-sm-3 text-right">
                                <label for="ponderacion">Ponderación</label>
                            </div>
                            <div class="col-sm-3">
                                <input id="ponderacion" placeholder="" maxlength="60" type="text" class="form-control" value="" readonly="readonly" style="font-weight: bold; color: #000">
                            </div>
                        </div>                        
                        <div class="row mt-3">
                            <div class="col-sm-3">
                                <span style="color:red"> * </span><label for="direccion">Dirección</label>
                            </div>
                            <div class="col-sm-9">
                                <input id="direccion" maxlength="250" type="text" class="form-control" value="" readonly="readonly"  style="font-weight: bold; color: #000">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <hr />
                        <table rules="all" style="border-color: #666;" cellpadding="10">
                            <tr>
                                <td>
                                    <strong>Resumen de Riesgos localizados:</strong><br />

                                </td>
                            </tr>
                            <tr>
                                <td id="td_resultados">
                                    &gt;&gt; Noticias:
                                    <table rules="all" style="border-color: #666;" cellpadding="10">
                                        <tr>
                                            <td style="text-align:center"><strong>Fuente</strong> </td>
                                            <td style="text-align:center"> <strong>Riesgo</strong> </td>
                                            <td style="text-align:center"><strong>Ponderación</strong> </td>
                                        </tr>
                                        <tr>
                                            <td>urlx...</td>
                                            <td style="text-align:center">riesgoxxx</td>
                                            <td style="text-align:center">10</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <hr />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin dialog enviar a analizar-->