<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <!-- <li class="breadcrumb-item"><a href="javascript:;">Catáogos</a></li> -->
    <li class="breadcrumb-item active">Puestos - riesgos</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Puestos - Riesgos <small>Asignación de riesgos y creación de puestos</small></h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Puestos - Riesgos</h4>
    </div>
    <div class="panel-body">
        <!-- <div class="row mt-5">
            <div class="col-sm-1">
                <label for="pass">Cliente:</label>
            </div>
            <div class="col-sm-11">
                <select name="id_cliente" id="id_cliente" style="width:100%"></select>
            </div>
        </div> -->
        <div id="ctrls" class="row">
            <div class="col-sm-12 text-center">
                <div class="btn-group btn-group-sm" role="group">    
                    <button id="_agregar" type="button" class="btn btn-lime btn-sm">
                        <i class="fas fa-asterisk"></i>
                        Crear Puesto
                    </button>
                    <button id="_editar" type="button" class="btn btn-warning btn-sm">
                        <i class="fas fa-edit"></i>
                        Editar Puesto
                    </button>
                    <!-- <button id="_borrar" type="button" class="btn btn-danger btn-sm">
                        <i class="fas fa-trash-alt"></i>
                        Borrar Puesto
                    </button> -->
                </div>
            </div>
        </div>
        <div class="col sm-12">&nbsp;</div>
        <div class="row">
            <div class="col-sm-1">
                <label for="pass">Puesto:</label>
            </div>
            <div class="col-sm-11">
                <select name="id_puesto" id="id_puesto" style="width:100%"></select>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-5">
                <div class="table-responsive">    
                    <table id="trasignados" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>Id</th>
                                <th>Riesgo asignado</th>
                                <th>Frases asociadas</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>    
            </div>
            <div class="col-sm-2">
                <div class="col sm-12">&nbsp;</div>
                <div class="col sm-12">&nbsp;</div>
                <div class="col sm-12">&nbsp;</div>
                <div class="col sm-12">&nbsp;</div>
                <button id="btnQuitar" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Quitando ..." class="btn btn-danger btn-sm">Quitar   <i class="fa fa-arrow-right"></i> </button>
                <div class="col sm-12">&nbsp;</div>
                <!-- <button id="btnDetalles" class="btn btn-warning btn-sm">Detalles <i class="fas fa-edit"></i> </button>
                <div class="col sm-12">&nbsp;</div> -->
                <button id="btnPoner" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Agregando ..." class="btn btn-success btn-sm"> <i class="fa fa-arrow-left"></i> Agregar</button>
            </div>    
            <div class="col-sm-5">
                <div class="table-responsive">
                    <table id="tcriesgos" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>Id</th>
                                <th>Riesgos</th>
                                <th>Frases asociadas</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>    
            </div>
        </div>

        <div class="row">
            <input type="hidden" id="idc" value="<?php echo $idc; ?>" />
            <input type="hidden" id="idp" value="<?php echo $idp; ?>" />
        </div>

    </div><!-- panel body -->
</div><!-- panel -->


<div class="modal fade" id="__modal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="__modal_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<form id="formPuesto" method="post">
                <div id="__modal_body" class="modal-body">
                    <input type="hidden" id="action" name="action" >
                    <input type="hidden" id="id_puesto" name="id_puesto" >
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon ">
                                <i class="fas fa-edit"></i>
                            </span>
                            <input type="text" id="descripcion"   name="descripcion" class="form-control" placeholder="Nombre" >
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnGuardar" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Guardando ..." class="btn btn-primary">
                        <i class="fas fa-save"></i>
                        Guardar
                    </button>
                </div>
			</form>	
		</div>
	</div>
</div>

