<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Objetivos</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Mis objetivos</h1>
<!-- end page-header -->

<input type="hidden" id="id_objetivo" value="">
<input type="hidden" id="id_perfil" value="<?php print $this->session->id_perfil; ?>">

<div id="panelListado" class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Objetivos/Hitos</h4>
    </div>
    
    <div class="panel-body">
                
        <div id="ctrls" class="row">
            <div class="col-sm-12 text-center">
                <div class="btn-group btn-group-sm" role="group">
                    
                    <button id="_agregar" type="button" class="btn btn-lime btn-sm" title="Agregar nuevo objetivo">
                        <i class="fas fa-asterisk"></i>
                        Nuevo
                    </button>
                    <button id="_editar" type="button" class="btn btn-warning btn-sm" title="Editar un objetivo">
                        <i class="fas fa-edit"></i>
                        Editar
                    </button>
                    <button id="_borrar" type="button" class="btn btn-danger btn-sm" title="Borrar un objetivo">
                        <i class="fas fa-trash-alt"></i>
                        Borrar
                    </button>
                    <button id="_publicar" type="button" class="btn btn-primary btn-sm" title="Hacer público mi objetivo">
                        <i class="fas fa-share-alt"></i>
                        Publicar
                    </button>
                    
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-3">
                <label for="campo">Elija los filtros</label>
            </div>
            <div class="col-sm-6">
                <select id="campo" style="width:100%;">
                    <option value="equipo">Cliente y Proyecto</option>
                    <option value="objetivo">Objetivo</option>
                    <option value="fecha_inicio">Fecha de inicio planeada</option>
                    <option value="fecha_fin">Fecha de fin planeada</option>
                    <option value="avance">Avance</option>
                    <option value="status">Status</option>
                    <option value="publicado">Publicados</option>
                </select>
            </div>
            <div class="col-sm-3 text-right">
                <button id="btnFiltrar"class="btn btn-purple m-3" title="Aplicar filtros">
                    <i class="fas fa-filter"></i> Filtrar
                </button>
                <button id="btnExcel" class="btn btn-green m-3">
                    <i class="fas fa-file-excel"></i> Exportar
                </button>
            </div>
        </div>

        <div id="filtros" class="row">
            <div class="col-sm-12">
                <div id="filtros_equipo" class="row d-none mt-2">
                    <div class="col-sm-3">
                        <label for="f_equipo">Cliente y Proyecto</label>
                    </div>
                    <div class="col-sm-9">
                        <select id="f_equipo" style="width:100%;" class="form-control"></select>
                    </div>
                </div>
                <div id="filtros_objetivo" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_objetivo">Objetivo</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" id="f_objetivo" class="form-control" placeholder="Escriba el texto a buscar en el objetivo">
                    </div>
                </div>
                <div id="filtros_fecha_inicio" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_fecha_inicio">Fecha de inicio</label>
                    </div>
                    <div class="col-sm-2">
                        <select id="f_fecha_inicio_oper" class="form-control" style="width:100%;">
                            <option value="=">Igual que</option>
                            <option value=">">Mayor que</option>
                            <option value="<">Menor que</option>
                            <option value=">=">Mayor o igual que</option>
                            <option value="<=">Menor o igual que</option>
                        </select>
                    </div>
                    <div class="col-sm-7">
                        <input type="text" id="f_fecha_inicio" class="form-control">
                    </div>
                </div>
                <div id="filtros_fecha_fin" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_fecha_fin">Fecha de Fin</label>
                    </div>
                    <div class="col-sm-2">
                        <select id="f_fecha_fin_oper" class="form-control" style="width:100%;">
                            <option value="=">Igual que</option>
                            <option value=">">Mayor que</option>
                            <option value="<">Menor que</option>
                            <option value=">=">Mayor o igual que</option>
                            <option value="<=">Menor o igual que</option>
                        </select>
                    </div>
                    <div class="col-sm-7">
                        <input type="text" id="f_fecha_fin" class="form-control">
                    </div>
                </div>
                <div id="filtros_avance" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_avance">Avance</label>
                    </div>
                    <div class="col-sm-2">
                        <select id="f_avance_oper" class="form-control" style="width:100%;">
                            <option value="=">Igual que</option>
                            <option value=">">Mayor que</option>
                            <option value="<">Menor que</option>
                            <option value=">=">Mayor o igual que</option>
                            <option value="<=">Menor o igual que</option>
                        </select>
                    </div>
                    <div class="col-sm-7">
                        <input type="number" min="0" max="100" step="1" id="f_avance" value="0" class="form-control">
                    </div>
                </div>
                <div id="filtros_status" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_status">Status</label>
                    </div>
                    <div class="col-sm-9">
                        <select id="f_status" style="width:100%;" class="form-control"></select>
                    </div>
                </div>
                <div id="filtros_publicado" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_publicado">Publicado</label>
                    </div>
                    <div class="col-sm-9">
                        <select id="f_publicado" style="width:100%;" class="form-control">
                            <option value="1">Si</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-sm-12">
                <table id="tabla" class="responsive table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>id_objetivo</th>
                            <th>id_equipo</th>
                            <th>Cliente</th>
                            <th>Proyecto</th>
                            <th>Objetivo</th>
                            <th>Fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th>% de Avance</th>
                            <th>status</th>
                            <th>Status</th>
                            <th>publicado</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>

    </div><!-- panelBody -->

</div><!-- panel -->


<div id="panelFormulario" class="panel panel-inverse" style="display:none;">
    <div class="panel-heading">
        <h4 class="panel-title" id="tituloAccion">Captura de Objetivos/Hitos</h4>
    </div>
    
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12 justify-content-start">
                <button id="btnRegresar" type="button" class="btn btn-outline-info">
                    <i class="fas fa-long-arrow-alt-left"></i> Regresar al listado
                </button>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-sm-3">
                <label for="equipo">Cliente y Proyecto</label>
            </div>
            <div class="col-sm-9">
                <select id="equipo" class="form-control" style="width:100%;"></select>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-sm-3">
                <label for="objetivo">Objetivo</label>
            </div>
            <div class="col-sm-9">
                <textarea class="form-control" id="objetivo" cols="30" rows="10"></textarea>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3">
                <label for="tipo">Tipo de objetivo</label>
            </div>
            <div class="col-sm-9">
                <select id="tipo" class="form-control" style="width:100%;">
                    <option value="E">Estrategico</option>
                    <option value="O">Operativo</option>
                </select>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3">
                <label for="fecha_inicio">Fecha de inicio</label>
            </div>
            <div class="col-sm-9">
                <input type="text" id="fecha_inicio" class="form-control">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3">
                <label for="fecha_fin">Fecha fin</label>
            </div>
            <div class="col-sm-9">
                <input type="text" id="fecha_fin" class="form-control">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3">
                <label for="hrs_planeadas">Hrs planeadas</label>
            </div>
            <div class="col-sm-9">
                <input type="number" id="hrs_planeadas" step="1" min="0" max="50" value="0" class="form-control">
            </div>
        </div>
        <div id="hrsRealesRow" class="row mt-3">
            <div class="col-sm-3">
                <label for="hrs_reales">Hrs reales</label>
            </div>
            <div class="col-sm-9">
                <input type="number" id="hrs_reales" step="1" min="0" max="100" value="0" class="form-control">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3">
                <label for="avance">% de avance</label>
            </div>
            <div class="col-sm-9">
                <input type="text" class="js-range-slider" id="avance" name="avance" value="" />
            </div>
        </div>
        <div id="statusRow" class="row mt-3">
            <div class="col-sm-3">
                <label for="status">Status</label>
            </div>
            <div class="col-sm-9">
                <select id="status" class="form-control" style="width:100%;"></select>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-6">
                <button id="btnRegresar2" type="button" class="btn btn-outline-info">
                    <i class="fas fa-long-arrow-alt-left"></i> Regresar al listado
                </button>
            </div>
            <div class="col-sm-6 text-right">
                <button id="btnSaveObjective" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div><!-- panel body -->
</div><!-- panelFormulario -->