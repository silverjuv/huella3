<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;"></a></li>
    <li class="breadcrumb-item active">Puestos - riesgos</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Puestos laborales y riesgos asignados
<!-- <small>Desglose de puestos y riesgos del clientes</small> -->
</h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Riesgos Asignados a Puestos</h4>
    </div>
    <div class="panel-body">
        <div class="row mt-3">
            <div class="col-sm-6">
                <div class="table-responsive">
                    <table id="tpuestos" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>Id</th>
                                <th>Puestos</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>    
            </div>
            <div class="col-sm-6">
                <div class="table-responsive">    
                    <table id="trasignados" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>Id</th>
                                <th>Riesgo asignado</th>
                                <th>Frases asociadas</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><!-- panel body -->
</div><!-- panel -->

<div class="modal fade" id="__modal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header alert-dark">
				<h4 class="modal-title" id="__modal_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div id="__modal_body" class="modal-body alert-secondary">      	
                <div class="table-responsive">
                    <table id="tSubriesgos" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>Id</th>
                                <th>Subriesgo</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>  
			</div>
		</div>
	</div>
</div>