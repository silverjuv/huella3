<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Administrar</a></li>
    <li class="breadcrumb-item active">Riesgos</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Riesgos <small>Gestión de Riesgos...</small></h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Riesgos</h4>
    </div>
    <div class="panel-body alert-secondary">
    <div id="ctrls" class="row">
        <div class="col-sm-12 text-center">
            <div class="btn-group btn-group-sm" role="group">
                    
                    <button id="_agregar" type="button" class="btn btn-lime btn-sm">
                        <i class="fas fa-asterisk"></i>
                        Nuevo
                    </button>
                    <button id="_editar" type="button" class="btn btn-warning btn-sm">
                        <i class="fas fa-edit"></i>
                        Editar
                    </button>
                    <button id="_borrar" type="button" class="btn btn-danger btn-sm">
                        <i class="fas fa-trash-alt"></i>
                        Borrar
                    </button>
                    <button id="_reload" type="button" class="btn  btn-sm">
                        <i class="fa fa-refresh"></i>
                         listado
                    </button>
                    
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table id="tRiesgos" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>Id</th>
                                <th>Riesgo</th>
                                <th>Verbo</th>
                                <th>ponderacion</th>
                                <th>id area de riesgo</th>
                                <th>Frases asociadas</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>    
            </div>
        </div>

    </div><!-- panel body -->
</div><!-- panel -->

<div class="modal fade" id="__modal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header alert-dark">
				<h4 class="modal-title" id="__modal_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div id="__modal_body" class="modal-body alert-secondary">
                <div class="row">   
                        <div class="col-sm-12">
                        
                    <form id="formRiesgo" method="post">
                            <input type="hidden" id="action" name="action" >
                            <input type="hidden" id="id_riesgo" name="id_riesgo" >
				            <div class="form-group">
					            <div class="input-group">
                                    <span class="input-group-addon">¿Verbo?
                                        <input type="checkbox" id="verbo" name="verbo" value="true"/>  
						            </span>
                                    <input type="text" id="descripcion"   name="descripcion" class="form-control" placeholder="Riesgo" >
                                    <button type="submit" id="btnGuardar" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Guardando ..." class="btn btn-primary">
                                        <i class="fas fa-save"></i> Guardar
				                    </button>
                                </div>
				            </div>
                            <div class="form-group row">
                                <label for="ponderacion" class="col-sm-2 col-form-label">Ponderación:</label>
                                <div class="col-sm-10">
                                    <input type="number" id="ponderacion" name="ponderacion" value="0" class="form-control">
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label for="id_area" class="col-sm-2 col-form-label">Area de riesgo:</label>
                                <div class="col-sm-10">
                                    <select name="id_area" id="id_area" style="width:100%"></select>
                                </div>
                            </div>       
                            </form>  
                        </div>  
                </div>        	
                <div class="row" id="contenedorTabla">
                    <div class="col-sm-12">
                        <form id="formSubriesgo" method="post">
                            <div class="form-group">
					            <div class="input-group">
                                    <!--<span class="input-group-addon">¿Verbo?
                                        <input type="checkbox" id="verboSubriesgo" name="verbo" value="true"/>
                                    </span>-->        
                                    <input type="text" id="subriesgo"   name="subriesgo" class="form-control" placeholder="Verbo" >
                                    <button id="_agregarSubriesgo" type=submit data-loading-text="<i class='fa fa-spinner fa-spin'></i> Agregando..." class="btn btn-lime btn-sm">
                                        <i class="fas fa-asterisk"></i> Agregar
                                    </button>  
                                </div>
                            </div>
                        </form>      
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table id="tSubriesgos" class="table responsive table-striped table-bordered table-hover" width="100%">
                                <thead style="width:100%">
                                    <tr>
                                        <th>Acciones</th>
                                        <th>Id</th>
                                        <th>Verbo</th>
                                        <th></th>
                                        <!-- <th>
                                            <div class="row">
                                                <div class="col-sm-12">Verbo</div> -->
                                                <!-- <div class="col-sm-4">
                                                    <button id="_borrarSubriesgo" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Borrando..." class="btn btn-danger btn-sm">
                                                        <i class="fas fa-trash-alt"></i> Borrar
                                                    </button>
                                                </div> -->
                                            </div>
                                        <!-- </th> -->
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>    
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>