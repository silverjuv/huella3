<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Reportes</a></li>
    <li class="breadcrumb-item active">Gráficas</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Gráficas</h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Parametrice el reporte</h4>
    </div>
    <div class="panel-body">

        <div class="row mt-5">
            <div class="col-sm-3">
                <label for="campo">Elija los parametros</label>
            </div>
            <div class="col-sm-6">
                <select id="campo" style="width:100%;">
                    <option value="colaborador">Colaborador</option>
                    <option value="fecha_inicio">Fecha de inicio planeada</option>
                    <option value="fecha_fin">Fecha de fin planeada</option>
                    <!--
                    <option value="status">Status</option>
                    <option value="area">Área</option>
                    <option value="equipo">Cliente y Proyecto</option>
                    <option value="avance">Avance</option>
                    
                    <option value="publicado">Publicados</option> -->
                </select>
            </div>
            <div class="col-sm-3 text-right">
                <button id="btnGraficar"class="btn btn-default ml-3" title="Aplicar filtros">
                <i class="fas fa-chart-line"></i> Graficar
                </button>
            </div>
        </div>

        <div id="filtros" class="row">
            <div class="col-sm-12">
                <div id="filtros_colaborador" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_colaborador">Colaborador</label>
                    </div>
                    <div class="col-sm-9">
                        <select id="f_colaborador" class="form-control" style="width:100%;">
                    </div>
                </div>
                <div id="filtros_area" class="row d-none mt-2">
                    <div class="col-sm-3">
                        <label for="f_area">Área</label>
                    </div>
                    <div class="col-sm-9">
                        <select id="f_area" style="width:100%;" class="form-control"></select>
                    </div>
                </div>
                <div id="filtros_equipo" class="row d-none mt-2">
                    <div class="col-sm-3">
                        <label for="f_equipo">Cliente y Proyecto</label>
                    </div>
                    <div class="col-sm-9">
                        <select id="f_equipo" style="width:100%;" class="form-control"></select>
                    </div>
                </div>
                <div id="filtros_fecha_inicio" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_fecha_inicio">Fecha de inicio</label>
                    </div>
                    <div class="col-sm-2">
                        <select id="f_fecha_inicio_oper" class="form-control" style="width:100%;">
                            <option value="=">Igual que</option>
                            <option value=">">Mayor que</option>
                            <option value="<">Menor que</option>
                            <option value=">=">Mayor o igual que</option>
                            <option value="<=">Menor o igual que</option>
                        </select>
                    </div>
                    <div class="col-sm-7">
                        <input type="text" id="f_fecha_inicio" class="form-control">
                    </div>
                </div>
                <div id="filtros_fecha_fin" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_fecha_fin">Fecha de Fin</label>
                    </div>
                    <div class="col-sm-2">
                        <select id="f_fecha_fin_oper" class="form-control" style="width:100%;">
                            <option value="=">Igual que</option>
                            <option value=">">Mayor que</option>
                            <option value="<">Menor que</option>
                            <option value=">=">Mayor o igual que</option>
                            <option value="<=">Menor o igual que</option>
                        </select>
                    </div>
                    <div class="col-sm-7">
                        <input type="text" id="f_fecha_fin" class="form-control">
                    </div>
                </div>
                <div id="filtros_avance" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_avance">Avance</label>
                    </div>
                    <div class="col-sm-2">
                        <select id="f_avance_oper" class="form-control" style="width:100%;">
                            <option value="=">Igual que</option>
                            <option value=">">Mayor que</option>
                            <option value="<">Menor que</option>
                            <option value=">=">Mayor o igual que</option>
                            <option value="<=">Menor o igual que</option>
                        </select>
                    </div>
                    <div class="col-sm-7">
                        <input type="number" min="0" max="100" step="1" id="f_avance" value="0" class="form-control">
                    </div>
                </div>
                <div id="filtros_status" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_status">Status</label>
                    </div>
                    <div class="col-sm-9">
                        <select id="f_status" style="width:100%;" class="form-control"></select>
                    </div>
                </div>
                <div id="filtros_publicado" class="row mt-2 d-none">
                    <div class="col-sm-3">
                        <label for="f_publicado">Publicado</label>
                    </div>
                    <div class="col-sm-9">
                        <select id="f_publicado" style="width:100%;" class="form-control">
                            <option value="1">Si</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- panel body -->
</div><!-- panel -->

<div class="panel panel-inverse" id="colaboradorGeneralPanel">
    <div class="panel-heading">
        <h4 class="panel-title">Resultado</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12" id="miCanvasRow">
                <canvas id="miCanvas" width="500px"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <a id="downloadImage" data-canvas="miCanvas"
                    download="ChartImage.jpg" href="" class="btn btn-primary" title="Descargar Gráfico">
                    <i class="fa fa-download"></i>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- 
<div class="panel panel-inverse" id="colaboradorDetallePanel">
    <div class="panel-heading">
        <h4 class="panel-title">Detalle</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12" id="colaboradorDetalleRow">
                <canvas id="colaboradorDetalle" width="500px" height="600px"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <a id="downloadColaboradorDetalle" data-canvas="colaboradorDetalle"
                    download="ChartImage.jpg" href="" class="btn btn-primary" title="Descargar Gráfico">
                    
                    <i class="fa fa-download"></i>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-inverse" id="colaboradorEspecificoPanel">
    <div class="panel-heading">
        <h4 class="panel-title">Detalle</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12" id="colaboradorEspecificoRow">
                <canvas id="colaboradorEspecifico" width="500px" height="300px"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <a id="downloadColaboradorDetalle" data-canvas="colaboradorDetalle"
                    download="ChartImage.jpg" href="" class="btn btn-primary" title="Descargar Gráfico">
                    <i class="fa fa-download"></i>
                </a>
            </div>
        </div>
    </div>
</div>

 -->