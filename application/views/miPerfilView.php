<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Mi perfil</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Mi Perfil</h1>
<!-- end page-header -->

<div class="row mb-2">
    <div class="col-sm-12 text-right" title="Ir a inicio">
        <a href="" id="linkIrInicio" class="btn btn-info">
            <i class="fas fa-home"></i> Inicio
        </a>
    </div>
</div>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Mi perfil</h4>
    </div>
    <div id="panelMision" class="panel-body">

        <div class="row">
            <div class="col-sm-3">
                <img src="<?php print base_url("assets/imgs/avatars/".$this->session->avatar); ?>" class="imgRedonda img-fluid" >
            </div>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dropzone" id="avatarFile"></div>
                    </div>
                </div>
            </div>
        </div>

        <div  class="row ">
            <div class="col-sm-3 offset-sm-3">
                <label>ID de Usuario:</label>
            </div>
            <div class="col-sm-6">
                <label><?php print $_SESSION['id_usuario']; ?></label>
            </div>
        </div>

        <div  class="row ">
            <div class="col-sm-3 offset-sm-3">
                <label>Nombre:</label>
            </div>
            <div class="col-sm-6">
                <label><?php print strtoupper($this->session->paterno." ".$this->session->materno." ".$this->session->nombre); ?></label>
            </div>
        </div>
        
        <div  class="row">
            <div class="col-sm-3 offset-sm-3">
                <label>Email:</label>
            </div>
            <div class="col-sm-6">
                <label><?php print strtoupper($this->session->email); ?></label>
            </div>
        </div>
        <div  class="row">
            <div class="col-sm-3 offset-sm-3">
                <label>Cliente:</label>
            </div>
            <div class="col-sm-6">
                <label><?php print strtoupper($this->session->cliente); ?></label>
            </div>
        </div>
        <div  class="row">
            <div class="col-sm-3 offset-sm-3">
                <label>Perfil:</label>
            </div>
            <div class="col-sm-6">
                <label><?php print strtoupper($this->session->perfil); ?></label>
            </div>
        </div>
                
        <div class="row">
            <div class="col-sm-12 text-right">
                <a class="btn btn-outline-info" href="<?php print base_url("HomeController/cambiarContrasena"); ?>">
                    Cambiar mi contraseña
                </a>
            </div>
        </div>

    </div>
</div>


<br /><br />
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <!-- <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> -->
            <a href="javascript:;"  class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse" ><i class="fa fa-plus"></i></a>
        </div>
        <h4 class="panel-title">Detalles del Contrato del Cliente</h4>
    </div>
    <div id="panelMision" class="panel-body">

        <div  class="row">
            <div class="col-sm-4 offset-sm-2">
                <label>Cliente</label>
            </div>
            <div class="col-sm-6">
                <label><?php print strtoupper($cliente['cliente']); ?></label>
            </div>
        </div>
        <?php if (!isset($cliente['tipo_servicio'])){
            // Tiene contrato vencido
            ?>
            <div  class="row">
                <div class="col-sm-4 offset-sm-2">
                    <label>Status de Contrato: </label>
                </div>
                <div class="col-sm-6">
                    Su contrato se encuentra suspendido o cancelado. Contacte al administrador del sitio. 
                </div>
            </div>
        <?php }else{ ?>
        <div  class="row">
            <div class="col-sm-4 offset-sm-2">
                <label>Tipo de Contrato: </label>
            </div>
            <div class="col-sm-6">
                <?php print ($cliente['tipo_servicio'] =='T' ? 'Por Tiempo Determinado.' : 'Por Número de Procesamientos.'); ?>
            </div>
        </div>
        <?php if ($cliente['tipo_servicio'] =='T') { ?>
        <div  class="row">
            <div class="col-sm-4 offset-sm-2">
                <label>Periodo del Contrato</label>
            </div>
            <div class="col-sm-6">
                <?php print  'Del '.$cliente['fecha_inicio'].' al '.$cliente['fecha_fin']; ?>
            </div>
        </div>
        <?php } ?>
        <div  class="row">
            <div class="col-sm-4 offset-sm-2">
                <label>Num. Procesamientos autorizados:</label>
            </div>
            <div class="col-sm-6">
                <?php print  ($cliente['tipo_servicio'] =='P' ? number_format($cliente['num_procesamientos'],0) : ' Los que ocurran en el periodo.'); ?>
            </div>
        </div>
        <?php if ($cliente['tipo_servicio'] =='P') { ?>
        <div  class="row">
            <div class="col-sm-4 offset-sm-2">
                <label>Num. Procesamientos realizados</label>
            </div>
            <div class="col-sm-6">
                <?php print  number_format($cliente['num_consumos'],0); ?>
            </div>
        </div>
        <div  class="row">
            <div class="col-sm-4 offset-sm-2">
                <label>Num. Procesamientos restantes</label>
            </div>
            <div class="col-sm-6">
                <?php print  number_format($cliente['num_procesamientos']-$cliente['num_consumos'],0); ?>
            </div>
        </div>
        <?php } ?>
        <div  class="row">
            <div class="col-sm-4 offset-sm-2">
                <label>Status del Contrato:</label>
            </div>
            <div class="col-sm-6">
                 <?php print ($cliente['status'] =='A' ? 'Activo' : 'Otro');  ?> 
            </div>
        </div>
        <div  class="row">
            <div class="col-sm-4 offset-sm-2">
                <label>Fuentes de análisis:</label>
            </div>
            <div class="col-sm-6">
                 <?php print implode(", ",$cliente['fuentes']);  ?> 
            </div>
        </div>
        <div  class="row">
            <div class="col-sm-4 offset-sm-2">
                <label>Api Key:</label>
            </div>
            <div class="col-sm-6">
                 <?php print ($cliente['api_key']);  ?> 
            </div>
        </div>

        <?php } ?>
<!--                 
        <div class="row">
            <div class="col-sm-12 text-right">
                <a class="btn btn-outline-info" href="<?php print base_url("HomeController/cambiarContrasena"); ?>">
                    Cambiar mi contraseña
                </a>
            </div>
        </div> -->

    </div>
</div>