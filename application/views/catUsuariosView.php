<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Administrar</a></li>
    <li class="breadcrumb-item active">Usuarios</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Usuarios <small>Gestión de usuarios...</small></h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Usuarios</h4>
    </div>
    <div class="panel-body">
    <div id="ctrls" class="row">
        <div class="col-sm-12 text-center">
            <div class="btn-group btn-group-sm" role="group">
                    
                    <button id="_agregar" type="button" class="btn btn-lime btn-sm">
                        <i class="fas fa-asterisk"></i>
                        Nuevo
                    </button>
                    <button id="_editar" type="button" class="btn btn-warning btn-sm">
                        <i class="fas fa-edit"></i>
                        Editar
                    </button>
                    <button id="_borrar" type="button" class="btn btn-danger btn-sm">
                        <i class="fas fa-trash-alt"></i>
                        Borrar
                    </button>
                    <!-- <button id="_apikey" type="button" class="btn btn-dark btn-sm">
                        <i class="fas fa-key"></i>
                        Crear API key
                    </button> -->
                    <button id="_activar" type="button" class="btn btn-gray btn-sm" title="Habilitar/Deshabilitar usuario">
                        <i class="fas fa-user-slash"></i>
                        Deshabilitar
                    </button>
                    &nbsp;<button type="button" class="btn btn-warning btn-sm" id="exportData">
                        <i class="fa fa-print" aria-hidden="true"></i> Exportar tabla
                    </button>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-12">
                <table id="tusuarios" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>id_usuario</th>
                                <th>Empresa</th>
                                <th>Nombre usuario</th>
                                <th>Correo</th>                                
                                <th>Perfil</th>
                                <!-- <th>API key</th> -->
                                <th>Deshabilitado</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                </table>
            </div>
        </div>

    </div><!-- panel body -->
</div><!-- panel -->

<div class="modal fade" id="__modal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="__modal_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			
			<div id="__modal_body" class="modal-body">
                
                <input type="hidden" id="id_edicion" value="">
                <div class="row">
                    <div class="col-sm-4">
                    <span style="color:red"> * </span><label for="paterno">Paterno</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="text" id="paterno" class="form-control">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <label for="materno">Materno</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="text" id="materno" class="form-control">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <span style="color:red"> * </span><label for="nombre">Nombre</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="text" id="nombre" class="form-control">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <label for="correo">Correo</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="text" id="correo" class="form-control">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <span style="color:red"> * </span><label for="perfil">Perfil</label>
                    </div>
                    <div class="col-sm-8">
                         <select name="perfil" id="perfil" class="form-control" style="width: 100%">
                         </select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <span style="color:red"> * </span><label for="id_cliente">Cliente</label>
                    </div>
                    <div class="col-sm-8">
                         <select name="id_cliente" id="id_cliente" class="form-control" style="width: 100%">
                         </select>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <span style="color:red"> * </span><label for="pass">Contraseña</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="password" id="pass" class="form-control">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <label for="pass">Confirmar la contraseña</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="password" id="pass2" class="form-control">
                    </div>
                </div>                
                
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary" id="btnGuardar">Guardar</button>
			</div>
				
		</div>
	</div>
</div>

<!--dialog exportar a-->
<div class="modal fade " id="exportarDialog" role="dialog">
    <div class="modal-dialog modal-sm " role="document">
        <div class="modal-content">
            <div class="modal-header alert-dark">
                <h4 class="modal-title" id="exportarDialog_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div id="__modal_body" class="modal-body alert-secondary">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="radio">
                            <label><input type="radio" name="optradio" value="XLS" checked> &nbsp;Excel (xlsx)</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="radio">
                            <label><input type="radio" name="optradio" value="CSV"> &nbsp;CSV</label>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="btn-group btn-group-sm" role="group">
                        <button id="_exportar" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i>Exportando..." class="btn btn-success btn-sm">
                            <i class="fas fa-filter"></i> Exportar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin dialog enviar a analizar-->