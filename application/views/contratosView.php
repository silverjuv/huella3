<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Administrar</a></li>
    <li class="breadcrumb-item active">Contratos</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Contratos <small>Gestión de contratos...</small></h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Contratos</h4>
    </div>
    <div class="panel-body">
    <div id="ctrls" class="row">
        <div class="col-sm-12 text-center">
            <div class="btn-group btn-group-sm" role="group">
                    
                    <button id="_agregar" type="button" class="btn btn-lime btn-sm">
                        <i class="fas fa-asterisk"></i>
                        Nuevo
                    </button>
                    <button id="_editar" type="button" class="btn btn-warning btn-sm">
                        <i class="fas fa-edit"></i>
                        Editar
                    </button>
                    <button id="_borrar" type="button" class="btn btn-danger btn-sm">
                        <i class="fas fa-trash-alt"></i>
                        Borrar
                    </button>
                    &nbsp;<button type="button" class="btn btn-warning btn-sm" id="exportData">
                        <i class="fa fa-print" aria-hidden="true"></i> Exportar tabla
                    </button>                    
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-12">
                <table id="tcontratos" class="responsive table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Cliente</th>
                                <th>Tipo Servicio</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Fin</th>
                                <th># Procesamientos</th>
                                <th># Proc. Realizados</th>
                                <th>Fuentes de análisis</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                </table>
            </div>
        </div>

    </div><!-- panel body -->
</div><!-- panel -->

<div class="modal fade" id="__modal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="__modal_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			
			<div id="__modal_body" class="modal-body">
                
                <input type="hidden" id="id_edicion" value="">
                <div class="row">
                    <div class="col-sm-4">
                         <label for="cliente">Cliente</label>
                    </div>
                    <div class="col-sm-8">
                         <select id="cliente" style="width:100%"></select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <label for="servicio">Servicio</label>
                    </div>
                    <div class="col-sm-8">
                         <select id="servicio" style="width:100%" onchange="actualizaFechas();">
                            <option value="T">Por tiempo</option>
                            <option value="P">Por procesamiento</option>
                         </select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <label for="finicio">F. Inicio</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="text" id="finicio" class="form-control"> 
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <label for="ffin">F. Fin</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="text" id="ffin" class="form-control">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <label for="procesamientos"># Procesamientos</label>
                    </div>
                    <div class="col-sm-8">
                         <input type="text" id="procesamientos" class="form-control">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <label for="fuentes">Fuentes de análisis</label>
                    </div>
                    <div class="col-sm-8">
                         <select id="fuentes" style="width:100%"></select>
                    </div>
                </div>
                <div id="apiRow" class="row mt-3 d-none">
                    <div class="col-sm-4">
                         <button id="btnApiKey" class="btn btn-dark">API Key</button>
                    </div>
                    <div class="col-sm-8">
                         <label id="api_key"></label>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                         <label for="status">Status</label>
                    </div>
                    <div class="col-sm-8">
                         <select id="status" style="width:100%">
                            <option value="A">Activo</option>
                            <option value="C">Cancelado</option>
                            <option value="S">Suspendido</option>
                         </select>
                    </div>
                </div>
                <div class="row mt-3">
                * Si el cliente ya tiene un contrato activo, no se permite guardar uno NUEVO con status ACTIVO.
                </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary" id="btnGuardar">Guardar</button>
			</div>
				
		</div>
	</div>
</div>


<!--dialog exportar a-->
<div class="modal fade " id="exportarDialog" role="dialog">
    <div class="modal-dialog modal-sm " role="document">
        <div class="modal-content">
            <div class="modal-header alert-dark">
                <h4 class="modal-title" id="exportarDialog_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div id="__modal_body" class="modal-body alert-secondary">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="radio">
                            <label><input type="radio" name="optradio" value="XLS" checked> &nbsp;Excel (xlsx)</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="radio">
                            <label><input type="radio" name="optradio" value="CSV"> &nbsp;CSV</label>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="btn-group btn-group-sm" role="group">
                        <button id="_exportar" type="button" data-loading-text="<i class='fa fa-spinner fa-spin'></i>Exportando..." class="btn btn-success btn-sm">
                            <i class="fas fa-filter"></i> Exportar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin dialog enviar a analizar-->