<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">OSINT Tools</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">OSR Framework</h1>
<!-- end page-header -->
<?php 
  // !class_exists('Aif') && die('view not valid');
  //    $web   = AifView::getPath();
  //    $img   = "$web/img";
  //    $url = AifRequest::getUrl('/');
?>

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">OSR Framework</h4>
    </div>
    <div class="panel-body">

        <div class="container">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" id="nusarios" data-toggle="tab" href="#nusuarios_content" role="tab"
                        aria-controls="nusuarios_content" aria-selected="true">
                        Comprobar Nombres Completos (searchfy)
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="bplata" data-toggle="tab" href="#busqueda_plataforma" role="tab"
                        aria-controls="busqueda_plataforma" aria-selected="true">
                        Verificar Usuarios en varias Plataformas (usufy)
                    </a>
                </li>
                <!-- <li class="nav-item">
               <a class="nav-link" id="bmailfy" data-toggle="tab" href="#busqueda_mailfy" role="tab" aria-controls="busqueda_mailfy" aria-selected="true">
                  Checar un Email (mailfy)
               </a>
          </li>
          <li class="nav-item">
               <a class="nav-link" id="bphonefy" data-toggle="tab" href="#busqueda_phonefy" role="tab" aria-controls="busqueda_phonefy" aria-selected="true">
                  Checar Telefonos en eventos maliciosas (phonefy)
               </a>
          </li> -->
            </ul>

            <div class="tab-content  shadow-lg" id="myTabContent">

                <div class="tab-pane fade show active" id="nusuarios_content" role="tabpanel"
                    aria-labelledby="nusuarios_content-tab" >
                    <div class="form-group">

                        <select name="splataforma" id="splataforma" class="form-control" style="width: 100%"></select>

                        <div class="input-group mt-3">
                            <input type="text" class="form-control" name="snick" id="snick"
                                placeholder="Escriba aproximaciones sobre el nombre de usuario">
                            <div class="input-group-append">
                                <button id="btnBuscarAproximaciones" class="btn btn-primary">
                                    <i class="fas fa-search"></i> Buscar
                                </button>
                            </div>
                        </div>


                        <button id="btnAbrirEnlaces" class="btn btn-info">
                            Abrir enlaces encontrados
                        </button>

                        <div id="test">
                            <div id="resSearchfy table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Plataforma</th>
                                            <th>Alias</th>
                                            <th>Uri</th>
                                        </tr>
                                    </thead>
                                    <tbody id="cuerpoResulSearchy">
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div><!-- form-group -->
                </div> <!-- tab-pan -->

                <div class="tab-pane fade" id="busqueda_plataforma" role="tabpanel"
                    aria-labelledby="busqueda_plataforma-tab">

                    <div class="form-group">

                        <select name="plataforma" id="plataforma" class="form-control" style="width: 100%"></select>

                        <div class="input-group mt-3">
                            <input type="text" class="form-control" name="nickUsufy" id="nickUsufy"
                                placeholder="Escriba el usuario a buscar">
                            <div class="input-group-append">
                                <button id="btnBuscarNick" class="btn btn-primary">
                                    <i class="fas fa-search"></i> Buscar
                                </button>
                            </div>
                        </div>

                        <!-- <div id="test_phone">
                         <div id="resPhonefy table-responsive">
                              <table class="table table-hover">
                                   <thead>
                                        <tr>
                                             <th>Plataforma</th>
                                             <th>Alias</th>
                                             <th>Uri</th>
                                        </tr>
                                   </thead>
                                   <tbody id="cuerpoResulPhonefy">
                                   </tbody>
                              </table>
                         </div>
                    </div>                     -->
                    </div><!-- form-group -->

                    <div id="resultados"></div>

                </div><!-- tab pane -->

                <div class="tab-pane fade" id="busqueda_mailfy" role="tabpanel" aria-labelledby="busqueda_mailfy-tab">

                    <div class="form-group">

                        <select name="mailfy" id="mailfy" class="form-control" style="width: 100%"></select>

                        <div class="input-group mt-3">
                            <input type="text" class="form-control" name="nickmailfy" id="nickmailfy"
                                placeholder="Escriba e-mail a buscar">
                            <div class="input-group-append">
                                <button id="btnBuscarNick_mailfy" class="btn btn-primary">
                                    <i class="fas fa-search"></i> Buscar
                                </button>
                            </div>
                        </div>

                        <div id="test_mail">
                            <div id="resMailfy table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Plataforma</th>
                                            <th>Alias</th>
                                            <th>Uri</th>
                                        </tr>
                                    </thead>
                                    <tbody id="cuerpoResulMailfy">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- form-group -->

                    <!-- <div id="resultados_mailfy"></div> -->

                </div>

                <div class="tab-pane fade" id="busqueda_phonefy" role="tabpanel" aria-labelledby="busqueda_phonefy-tab">

                    <div class="form-group">

                        <select name="phonefy" id="phonefy" class="form-control" style="width: 100%"></select>

                        <div class="input-group mt-3">
                            <input type="text" class="form-control" name="nickphonefy" id="nickphonefy"
                                placeholder="Escriba el telefono a buscar">
                            <div class="input-group-append">
                                <button id="btnBuscarNick_phonefy" class="btn btn-primary">
                                    <i class="fas fa-search"></i> Buscar
                                </button>
                            </div>
                        </div>
                    </div><!-- form-group -->

                    <div id="test_phone">
                        <div id="resPhonefy table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Plataforma</th>
                                        <th>Alias</th>
                                        <th>Uri</th>
                                    </tr>
                                </thead>
                                <tbody id="cuerpoResulPhonefy">
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div><!-- tab-content -->


        </div><!-- container principal -->


    </div><!-- panel body -->
</div><!-- panel -->

<!-- <span class="d-none" id="burl"><?php print $url; ?></span> -->