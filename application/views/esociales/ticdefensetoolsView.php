<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">OSINT Tools</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Otras herramientas OSINT</h1>
<!-- end page-header -->
<?php 
  // !class_exists('Aif') && die('view not valid');
  //    $web   = AifView::getPath();
  //    $img   = "$web/img";
  //    $url = AifRequest::getUrl('/');
?>

<div class="container">
     <ul class="nav nav-tabs">
          <li class="nav-item">
               <a class="nav-link active" id="nusarios" data-toggle="tab" href="#nusuarios_content" role="tab" aria-controls="nusuarios_content" aria-selected="true">
                Busquedas en FullContact.com
               </a>
          </li>
          <li class="nav-item">
               <a class="nav-link" id="bplata" data-toggle="tab" href="#busqueda_plataforma" role="tab" aria-controls="busqueda_plataforma" aria-selected="true">
                  Busquedas en DuckDuckGo
               </a>
          </li>          
     </ul>

     <div class="tab-content" id="myTabContent">
          
          <div class="tab-pane fade show active" id="nusuarios_content" role="tabpanel" aria-labelledby="nusuarios_content-tab">
               <div class="form-group">
                    
                    <!-- <select name="splataforma" id="splataforma" class="form-control" style="width: 100%"></select> -->
                    
                    <div class="input-group mt-3">
                         <input type="text" class="form-control" name="snickFull" id="snickFull" placeholder="Escriba un alias, nombre de usuario, email o numero telefonico">  
                         <div class="input-group-append">
                              <button id="btnBuscarFullContact" class="btn btn-primary">
                                   <i class="fas fa-search"></i> Buscar
                              </button>
                         </div>
                    </div>
                    
                    
                    <!-- <button id="btnAbrirEnlaces" class="btn btn-info">
                         Abrir enlaces encontrados
                    </button> -->

                    <div id="test">
                         <!-- <div id="resSearchfy table-responsive">
                              <table class="table table-hover">
                                   <thead>
                                        <tr>
                                             <th>Plataforma</th>
                                             <th>Alias</th>
                                             <th>Uri</th>
                                        </tr>
                                   </thead>
                                   <tbody id="cuerpoResulSearchy">
                                   </tbody>
                              </table>
                         </div> -->
                    </div>
                         
               </div><!-- form-group -->
          </div> <!-- tab-pan -->
          
          <div class="tab-pane fade" id="busqueda_plataforma" role="tabpanel" aria-labelledby="busqueda_plataforma-tab">

               <div class="form-group">
                    
                    <!-- <select name="plataforma" id="plataforma" class="form-control" style="width: 100%"></select> -->
                    
                    <div class="input-group mt-3">
                         <input type="text" class="form-control" name="nickDuck" id="nickDuck" placeholder="Escriba la persona a buscar">  
                         <div class="input-group-append">
                              <button id="btnBuscarDuck" class="btn btn-primary">
                                   <i class="fas fa-search"></i> Buscar
                              </button>
                         </div>
                    </div>

                    <!-- <div id="test_phone">
                         <div id="resPhonefy table-responsive">
                              <table class="table table-hover">
                                   <thead>
                                        <tr>
                                             <th>Plataforma</th>
                                             <th>Alias</th>
                                             <th>Uri</th>
                                        </tr>
                                   </thead>
                                   <tbody id="cuerpoResulPhonefy">
                                   </tbody>
                              </table>
                         </div>
                    </div>                     -->
               </div><!-- form-group -->

               <div id="resultados"></div>

          </div><!-- tab pane -->


     </div><!-- tab-content --> 


</div><!-- container principal -->

<!-- <span class="d-none" id="burl"><?php print $url; ?></span> -->