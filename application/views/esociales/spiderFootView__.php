<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">OSINT Tools</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">SpiderFoot Tools</h1>
<!-- end page-header -->
<?php 
  // !class_exists('Aif') && die('view not valid');
  //    $web   = AifView::getPath();
  //    $img   = "$web/img";
  //    $url = AifRequest::getUrl('/');
?>
<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">SpiderFoot Tools</h4>
    </div>
    <div class="panel-body">

        <div class="container">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" id="bemailrep" data-toggle="tab" href="#busqueda_emailrep" role="tab"
                        aria-controls="busqueda_emailrep" aria-selected="true">
                        Checar reputación de un Email (emailrep)
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="bnumverify" data-toggle="tab" href="#busqueda_numverify" role="tab"
                        aria-controls="busqueda_numverify" aria-selected="true">
                        Checar detalles de Telefonos (numverify)
                    </a>
                </li>
            </ul>

            <div class="tab-content shadow-lg" id="myTabContent">


                <div class="tab-pane fade show active" id="busqueda_emailrep" role="tabpanel"
                    aria-labelledby="busqueda_emailrep-tab">

                    <div class="form-group">

                        <!-- <select name="emailrep" id="emailrep" class="form-control" style="width: 100%"></select> -->

                        <div class="input-group mt-3">
                            <input type="text" class="form-control" name="nickemailrep" id="nickemailrep"
                                placeholder="Escriba e-mail a buscar">
                            <div class="input-group-append">
                                <button id="btnBuscarNick_emailrep" class="btn btn-primary">
                                    <i class="fas fa-search"></i> Buscar
                                </button>
                            </div>
                        </div>

                        <div id="test_mail">
                            <!-- <div id="resemailrep table-responsive">
                              <table class="table table-hover">
                                   <thead>
                                        <tr>
                                             <th>Plataforma</th>
                                             <th>Alias</th>
                                             <th>Uri</th>
                                        </tr>
                                   </thead>
                                   <tbody id="cuerpoResulemailrep">
                                   </tbody>
                              </table>
                         </div> -->
                        </div>
                    </div><!-- form-group -->

                    <!-- <div id="resultados_emailrep"></div> -->

                </div>

                <div class="tab-pane fade" id="busqueda_numverify" role="tabpanel"
                    aria-labelledby="busqueda_numverify-tab">

                    <div class="form-group">

                        <!-- <select name="numverify" id="numverify" class="form-control" style="width: 100%"></select> -->

                        <div class="input-group mt-3">
                            <input type="text" class="form-control" name="nicknumverify" id="nicknumverify"
                                placeholder="Escriba el telefono a buscar +5244332211" maxlength="13">
                            <div class="input-group-append">
                                <button id="btnBuscarNick_numverify" class="btn btn-primary">
                                    <i class="fas fa-search"></i> Buscar
                                </button>
                            </div>
                        </div>
                    </div><!-- form-group -->

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-10">
                            <div id="test_phone"  style="overflow-x: auto;">
                            </di>
                        </div>
                        <div class="col-sm-1">
                        </div>
                        <!-- <div id="resnumverify table-responsive">
                              <table class="table table-hover">
                                   <thead>
                                        <tr>
                                             <th>Plataforma</th>
                                             <th>Alias</th>
                                             <th>Uri</th>
                                        </tr>
                                   </thead>
                                   <tbody id="cuerpoResulnumverify">
                                   </tbody>
                              </table>
                         </div> -->
                    </div>

                </div>

            </div><!-- tab-content -->


        </div><!-- container principal -->


    </div><!-- panel body -->
</div><!-- panel -->
<!-- <span class="d-none" id="burl"><?php print $url; ?></span> -->