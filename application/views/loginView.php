<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Login DRH</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />	

	<link rel="apple-touch-icon" sizes="57x57" href="<?php print base_url("assets/imgs/icos/apple-icon-57x57.png"); ?>" href="">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php print base_url("assets/imgs/icos/apple-icon-60x60.png"); ?>">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php print base_url("assets/imgs/icos/apple-icon-72x72.png"); ?>">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php print base_url("assets/imgs/icos/apple-icon-76x76.png"); ?>">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php print base_url("assets/imgs/icos/apple-icon-114x114.png"); ?>">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php print base_url("assets/imgs/icos/apple-icon-120x120.png"); ?>">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php print base_url("assets/imgs/icos/apple-icon-144x144.png"); ?>">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php print base_url("assets/imgs/icos/apple-icon-152x152.png"); ?>">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php print base_url("assets/imgs/icos/apple-icon-180x180.png"); ?>">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php print base_url("assets/imgs/icos/android-icon-192x192.png"); ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php print base_url("assets/imgs/icos/favicon-32x32.png"); ?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php print base_url("assets/imgs/icos/favicon-96x96.png"); ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php print base_url("assets/imgs/icos/favicon-16x16.png"); ?>">
	<link rel="manifest" href="<?php print base_url("assets/imgs/icos/manifest.json"); ?>">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php print base_url("assets/imgs/icos/ms-icon-144x144.png"); ?>">
	<meta name="theme-color" content="#ffffff">

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/plugins/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/plugins/font-awesome/5.3/css/all.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/css/default/style.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/css/default/style-responsive.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/css/default/theme/default.css" rel="stylesheet" id="theme" />
	<link href="<?php print base_url('assets/template/plugins/pnotify/PNotifyBrightTheme.css'); ?>"  rel="stylesheet" />

	<!-- <script src="<?php print base_url('assets/js/apm-agent/elastic-apm-rum.umd.js') ?>" crossorigin></script>

	<script>

	// When content is loaded
	document.addEventListener('DOMContentLoaded', function() {
		elasticApm.init({
			serviceName: 'miServicioAPMHuella',
			// serverUrl: 'http://93.90.194.109:8200',
			// distributedTracingOrigins: ['http://93.90.194.109:8080']
			serverUrl: 'http://localhost:8200',
			distributedTracingOrigins: ['http://localhost:8080']
		});     
	});

	</script> -->
		
	<?php if(ENVIRONMENT === "production") { ?>
	<script src='https://www.google.com/recaptcha/api.js?render=6Le1ROYUAAAAAIwJeq3dezVVqroQJk92U41y7D56'></script>
	<?php } ?>
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php print base_url(); ?>assets/template/plugins/pace/pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body class="pace-top bg-white">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
		<!-- begin login -->
		<div class="login login-with-news-feed">
			<!-- begin news-feed -->
			<div class="news-feed">
				
				<div class="news-image" style="background-image: url(<?php print base_url(); ?>assets/imgs/fondo.jpg)">
					<div class="news-caption">
						<h4 class="caption-title"><b>Human Digital Behavior</b></h4>
						<p>
							
						</p>
					</div>
				</div>
				
			</div>
			<!-- end news-feed -->
			<!-- begin right-content -->
			<div class="right-content" style="background-image: url('<?php print base_url("assets/imgs/fondo_tic.png"); ?>'); background-repeat: no-repeat; background-position:right center">
				<!-- begin login-header -->
				<div class="login-header">
					<div class="brand">
						<div class="row">
							<div class="col-sm-12">
								<img src="<?php print base_url("assets/imgs/logo.png"); ?>" height="180px">
							</div>
						</div>
						<!-- <h4>Digital Risk Human</h4> -->
						<!-- <br /> -->
						<h6>Bienvenido, por favor regístrese</h6>
					</div>
					<div class="icon">
						<i class="fa fa-sign-in"></i>
					</div>
				</div>
				<!-- end login-header -->
				<!-- begin login-content -->
				<div class="login-content">
					<form action="<?php print base_url("index.php/LoginController/login") ?>" method="POST" class="margin-bottom-0">
						<div class="form-group m-b-15">
							<input id="email" name="email" type="text" class="form-control form-control-lg" placeholder="nombre de usuario" required />
						</div>
						<div class="form-group m-b-15">
							<input type="password" id="pass" name="pass" class="form-control form-control-lg" placeholder="contraseña" required />
						</div>

						<input type="hidden" name="token" id="token" value="" />
						
						<div class="login-buttons">
							<button type="submit" class="btn btn-success btn-block btn-lg">Entrar</button>
						</div>
						
						<hr />
						
					</form>
				</div>
				<!-- end login-content -->
			</div>
			<!-- end right-container -->
		</div>
		<!-- end login -->

	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<!-- <script src="<?php print base_url(); ?>assets/template/plugins/jquery/jquery-3.3.1.min.js"></script> -->
	<script src="<?php print base_url(); ?>assets/template/plugins/jquery/jquery-3.6.0.min.js"></script>
	<script src="<?php print base_url(); ?>assets/template/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php print base_url(); ?>assets/template/plugins/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
	<!--[if lt IE 9]>
		<script src="<?php print base_url(); ?>assets/template/crossbrowserjs/html5shiv.js"></script>
		<script src="<?php print base_url(); ?>assets/template/crossbrowserjs/respond.min.js"></script>
		<script src="<?php print base_url(); ?>assets/template/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php print base_url(); ?>assets/template/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php print base_url(); ?>assets/template/plugins/js-cookie/js.cookie.js"></script>
	<script src="<?php print base_url(); ?>assets/template/js/theme/default.min.js"></script>
	<script src="<?php print base_url(); ?>assets/template/js/apps.min.js"></script>
	<script src='<?php print base_url(); ?>assets/template/plugins/pnotify/iife/PNotify.js'></script>
	<script src='<?php print base_url(); ?>assets/template/plugins/pnotify/iife/PNotifyButtons.js'></script>
	<script src='<?php print base_url(); ?>assets/template/plugins/pnotify/iife/PNotifyConfirm.js'></script>
	<!-- ================== END BASE JS ================== -->

	<script>
		let msg = '';
		PNotify.defaults.styling = 'bootstrap4';
		<?php if(isset($msg)) { ?>
			msg = "<?php print $msg; ?>";
		<?php } ?>

		$(document).ready(function() {
			App.init();
			if(msg != '') {
				PNotify.alert({
					title: 'Error',
					text: msg,
					type: 'error',
					Buttons: { closer: true}
				});
			}

			<?php if(ENVIRONMENT === "production") { ?>
			grecaptcha.ready(function() {
				grecaptcha.execute('6Le1ROYUAAAAAIwJeq3dezVVqroQJk92U41y7D56', {action: 'homepage'}).then(function(token) {
					document.getElementById("token").value=token;
				});
			});

			<?php } ?>

		});
	</script>
</body>
</html>
