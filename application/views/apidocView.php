<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">API Doc</a></li>
    <li class="breadcrumb-item active">Documentacion</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Uso de la API REST</h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                    class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                    class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">API REST</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 id="Una_Ejemplo_de_un_Sitio_Web_con_PHP">1. ¿Por que usar API REST?</h3>
                <p>REST (Representational State Transfer - Transferencia de Estado Representacional) es un estándar
                    lógico, muy
                    eficiente y habitual en la creación de APIs para servicios de Internet.
                    Con REST se pueden generar operaciones sobre datos en todos los formatos posibles. Representa una
                    solución
                    sencilla de manipulación de datos.</p>
                <blockquote class="notquote information" data-type="information">
                    <p style="font-size:14px"> El uso de un API REST es imprescindible en muchos aplicativos que se
                        ofrecen en la actualidad en varios
                        sitios que manejan información como son facebook, twitter, youtube, instagram.</p>
                </blockquote>
                <br />
                <h3 id="Página_Principal">2. ¿Como empezar a usar el API REST?</h3>
                <p style="text-align:justify">Para usar la API REST de este aplicativo web, pueden hacer uso de
                    cualquier lenguaje de programación que soporte llamadas al protocolo HTTP por medio
                    del método POST. Para muestra de ejemplo usaremos 4 lenguajes distintos. Se recomienda tener un
                    listado de personas con campos en letras minúsculas en algún archivo CVS, XLS para
                    por medio del lenguaje de programación cargar los registros uno por uno y realizar la petición HTTP
                    correspondiente. No deben olvidar que
                    por cada bloque de registos de preferencia manejar el valor del <em>lote</em> igual para todos, esto
                    es por que se enviará al correo oficial
                    del cliente un correo electrónico con los resultados correspondientes.</p>
                <p style="text-align:justify">El proceso para usar el API REST de huella consiste en loguearse en la ruta 
                    <a rel="noopener noreferrer" href="https://www.threats.center/huella/ApiRest/login" target="_blank">https://www.threats.center/huella/ApiRest/login</a> 
                    donde debe pasar los datos de acceso <em>username</em> y <em>password</em> por medio de petición POST y si los datos de autenticación 
                    son correctos recibirá un <b>TOKEN</b> con su respectiva fecha de expiración.</p>
                <p class=" text-center"><span class="image-wrapper">
                            <!-- <a target="_blank" href="<?php print base_url() . "assets/imgs/doc/apirest.png"; ?>"> -->
                            <img style="width:70%" src="<?php print base_url() . "assets/imgs/doc/apirest.png"; ?>" alt="Proceso API REST huella" />
                            <!-- </a> -->
                            <span class="image-caption"></span>
                        </span>
                    </p>
                <p style="text-align:justify">Con el token ya podra hacer uso de los recursos a los cuales tiene acceso su usuario. Para ello deben usar la 
                autenticación basada en <b>Bearer Token</b>. Estos son: </p>
                <ul>
                    <li>1 - Consultar la lista de puestos asignados al cliente. Petición GET a 
                        <a rel="noopener noreferrer" href="https://www.threats.center/huella/api/Puestos" target="_blank">https://www.threats.center/huella/api/Puestos</a> </li>
                    <li>2 - Agregar personas - puesto para su analisis. Petición POST a 
                        <a rel="noopener noreferrer" href="https://www.threats.center/huella/api/Personas" target="_blank">https://www.threats.center/huella/api/Personas</a> </li>
                </ul>
                <blockquote class="notquote tip" data-type="tip">
                    <p style="font-size:14px; text-align:justify"> Para entender los códigos de ejemplo de abajo
                        necesitamos tener alguna experiencia
                        con PHP, Python, NodeJs o Javascript. Si no tenemos experiencia sería bueno revisar algún
                        tutorial en <a rel="noopener noreferrer" target="_blank" href="http://www.w3schools.com/php/">w3schools.com</a>.</p>
                </blockquote>
                <br />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <label for="busqueda">Lenguaje de programación: </label>
                <select name="lenguaje" id="lenguaje" class="form-control" style="width: 100%">
                    <option value="php">PHP</option>
                    <option value="python">Python</option>
                    <option value="nodejs">NodeJs</option>
                    <option value="javascript">Javascript (jQuery)</option>
                </select>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9" style="background-color:#ccc">
                <div id="dv_logs"></div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 id="Una_Ejemplo_de_un_Sitio_Web_con_PHP">2.2 Datos de Entrada</h3>
                <p>Los campos obligatorios que debe recibir la petición API REST para el recurso <em>Agregar personas - puesto para su análisis</em>, son los que se muestran enseguida.
                    Aunque algunos parametros no tengan valor deben incluirlos
                    en la petición REST.</p>
                <ol>
                    <!-- <li>
                        <p><b>api_key:</b> Es un token especial generado por cliente.</p>
                    </li>
                    <li>
                        <p><b>id_usuario:</b> Identificador unico del usuario autorizado por el cliente para realizar la
                            petición API REST.
                        </p>
                    </li> -->
                    <li>
                        <p><b>ine:</b> Representa la identifición nacional vigente (INE) de la persona a analizar.
                        </p>
                    </li>
                    <li>
                        <p><b>curp:</b> Representa la CURP de la persona a analizar.
                        </p>
                    </li>
                    <li>
                        <p><b>id_puesto:</b> Representa el identificador del puesto correspondiente al catálogo de
                            puestos asignados al cliente. Identificadores
                            que se pueden obtener en la pantalla <a
                                href="<?php print base_url()."CatPuestosController"; ?>">catálogo de puestos</a>.
                        </p>
                    </li>
                    <li>
                        <p><b>alias:</b> Representa un alias usado por la persona en varios portales web, si no se tiene
                            puede ponerse el texto de su
                            correo electrónico antes del arroba.
                        </p>
                    </li>
                    <li>
                        <p><b>nombre:</b> Se refiere al nombre o nombres de la persona a analizar.
                        </p>
                    </li>
                    <li>
                        <p><b>paterno:</b> Representa el apellido paterno de la persona a analizar.
                        </p>
                    </li>
                    <li>
                        <p><b>materno:</b> Representa el apellido materno de la persona a analizar, si es que tuviese.
                        </p>
                    </li>
                    <li>
                        <p><b>email:</b> Representa el email de la persona a analizar.
                        </p>
                    </li>
                    <li>
                        <p><b>celular:</b> Representa el número telefónico de la persona, preferentemente su número del
                            celular personal.
                        </p>
                    </li>
                    <li>
                        <p><b>direccion:</b> Representa el domicilio de la persona, debe incluir nombre de la calle y
                            número, colonia, codigo postal, lugar, entidad federativa.
                        </p>
                    </li>
                    <li>
                        <p><b>lote:</b> Campo imprescindible ya que es el identificador del lote de personas puesto a
                            analizar. Con este dato
                            se envía un correo electrónico a la cuenta oficial del cliente con el resultado de los
                            análisis generados. Este parametro tiene
                            que llevar como formato YYYMMDD#num_personas (20200929#234), lote de 234 personas con fecha
                            2020-09-29.
                        </p>
                    </li>
                    <li>
                        <p><b>twitter:</b> Representa la cuenta oficial de twitter de la persona..
                        </p>
                    </li>
                    <li>
                        <p><b>linkedin:</b> Representa la cuenta oficial de linkedin de la persona.
                        </p>
                    </li>
                </ol>
                <p>Si usamos la herramienta <code>postman</code>, los datos de entrada serian los siguientes:</p>
                <p><span class="image-wrapper">
                        <a rel="noopener noreferrer" target="_blank" href="<?php print base_url()."assets/imgs/doc/img2.png";?>">
                            <img style="width:70%" src="<?php print base_url()."assets/imgs/doc/img2.png";?>"
                                alt="Petición POSTMAN" /></a>
                        <span class="image-caption"></span>
                    </span>
                </p>
                <br />
                <h3 id="Una_Ejemplo_de_un_Sitio_Web_con_PHP">2.3 Resultados generados</h3>
                <p>Una vez enviada la petición de análisis de Persona y su Puesto, la repsuesta JSON del API REST se
                    compone de los siguientes valores:</p>
                <ol>
                    <li>
                        <p><b>status:</b> Es un valor numérico que puede tener uno de los siguientes valores del 101 al
                            111</p>
                    </li>
                    <li>
                        <p><b>message:</b> Descripción de lo sucedido en la llamada a la API REST. Se corresponde con un
                            código de status.
                        </p>
                    </li>
                </ol>
                <p>Estos valores son los siguientes:</p>
                <ul>
                    <li>
                        <p><b>101</b> - Hay campos obligatorios que no estan incluidos en la URI! ... Explicación</p>
                    </li>
                    <li>
                        <p><b>102</b> - No se pudo procesar la persona solicitada! ... Explicación</p>
                    </li>
                    <li>
                        <p><b>103</b> - Persona en espera de analisis, se enviara un correo de resultados, o bien puede
                            consultarlos en el Aplicativo WEB!</p>
                    </li>
                    <li>
                        <p><b>104</b> - Api Key No valida!, se requiere Autorizacion</p>
                    </li>
                    <li>
                        <p><b>105</b> - Se esta volviendo a solicitar Ponderacion de la misma persona para el mismo
                            puesto en el mismo lote. La Persona se encuentra en espera de analisis.</p>
                    </li>
                    <li>
                        <p><b>106</b> - No es posible insertar mas personas-puestos, ya que el lote solicitado solo era
                            para ... personas-puestos</p>
                    </li>
                    <li>
                        <p><b>107</b> - Ponderacion: ... Numérico 0 a 9. Regresa el valor de la ponderación de la persona y puesto solicitado, siempre y cuando, la opción de re-analizar persona-puesto no esté encendida y la persona-puesto ya haya sido analizada. </p>
                    </li>
                    <li>
                        <p><b>108</b> - Usuario no Valido!, verifique los usuarios asignados a su contrato.</p>
                    </li>
                    <li>
                        <p><b>109</b> - Identificador de Puesto NO Válido para el cliente</p>
                    </li>
                    <li>
                        <p><b>110</b> - La persona solicitada con el puesto indicado ya se encuentran en proceso de
                            análisis!</p>
                    </li>
                    <li>
                        <p><b>111</b> - Hay error en los parámetros enviados, no coinciden con lo requerido, verifique
                            su petición REST.</p>
                    </li>
                    <li>
                        <p><b>112</b> - Solo se permiten peticiones por método POST..</p>
                    </li>
                    <li>
                        <p><b>113</b> - Acceso No Autorizado!</p>
                    </li>
                    <li>
                        <p><b>114</b> - Token ha expirado, se necesita regenerarlo, ingresa login y contraseña!</p>
                    </li>
                    <li>
                        <p><b>115</b> - Token No Valido!</p>
                    </li>
                    <li>
                        <p><b>116</b> - El cliente del cual pertenece el usuario no tiene Contrato Valido!</p>
                    </li>
                </ul>

                <p>Si usamos la herramienta <code>postman</code>, obtendriamos de resultado un JSON como el siguiente:
                </p>
                <p><span class="image-wrapper">
                        <a rel="noopener noreferrer" target="_blank" href="<?php print base_url()."assets/imgs/doc/img1.png";?>">
                            <img style="width:70%" src="<?php print base_url()."assets/imgs/doc/img1.png";?>"
                                alt="Petición POSTMAN" /></a>
                        <span class="image-caption"></span>
                    </span>
                </p>
            </div>
        </div>


    </div><!-- panel body -->
</div><!-- panel -->
<!-- </div> -->