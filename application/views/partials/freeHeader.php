<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="<?php print base_url(); ?>assets/template/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/plugins/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/plugins/font-awesome/5.3/css/all.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/css/material/style.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/css/material/style-responsive.min.css" rel="stylesheet" />
	<link href="<?php print base_url(); ?>assets/template/css/material/theme/default.css" rel="stylesheet" id="theme" />
	<link href="<?php print base_url('assets/template/plugins/pnotify/PNotifyBrightTheme.css'); ?>"  rel="stylesheet" />
	<link href="<?php print base_url('assets/css/global.css'); ?>" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- My own css files -->
	<?php
		if(isset($css)) {
			foreach($css as $path) { ?>
				<link href="<?php print base_url($path); ?>" rel="stylesheet" />
			<?php }
		} 
	?>
	<!-- End my own css files -->

	
</head>
<body>
