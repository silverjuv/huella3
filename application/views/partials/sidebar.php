<!-- begin #sidebar -->
<div id="sidebar" class="sidebar" data-disable-slide-animation="true">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        
        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="nav-header">Menú</li>
            <?php foreach($simpleLinks as $item) { 
                if ($item["controlador"]=='TicDefenseToolsController'){
                    continue;
                }
                ?>
                <li class="<?php if($pantalla == $item["id_pantalla"]){ print 'active'; }?>" title="<?php print $item["nombre_menu"]; ?>">
                    <a href="<?php print base_url('index.php/'.$item["controlador"]); ?>">
                        <?php print $item["icono"]; ?>
                        <span><?php print $item["nombre_menu"]; ?></span>
                    </a>

                </li>
            <?php } ?>

            <?php foreach($parentLinks as $item) { ?>
                <li class="has-sub <?php if($menu_expandido == $item["id_pantalla"]){ print ' expand active'; } ?>"  >
                    <a href="javascript:;">
                        <b class="caret"></b>
                        <?php print $item["icono"]; ?>
                        <span><?php print $item["nombre_menu"]; ?></span>
                    </a>
                    <ul class="sub-menu" style="<?php if($menu_expandido == $item["id_pantalla"]){ print 'display: block;'; }?>">
                    <?php foreach($item["children"] as $child) { ?>    
                        <li class="<?php if($pantalla == $child["id_pantalla"]){ print 'active'; }?>">
                            <a href="<?php print base_url('index.php/'.$child["controlador"]); ?>">
                                <?php print $child["nombre_menu"]; ?>
                            </a>
                        </li>
                    <?php } ?>
                    </ul>

                </li>

            <?php } ?>
            
            <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->

<div id="content" class="content">