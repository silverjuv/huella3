	<!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="material-icons">keyboard_arrow_up</i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->
<span id="burl" class="d-none"><?php print base_url(); ?>index.php/</span>

<!-- ================== BEGIN BASE JS ================== -->
<!-- <script src="<?php print base_url(); ?>assets/template/plugins/jquery/jquery-3.3.1.min.js"></script> -->
<script src="<?php print base_url(); ?>assets/template/plugins/jquery/jquery-3.6.0.min.js"></script>
<script src="<?php print base_url(); ?>assets/template/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php print base_url(); ?>assets/template/plugins/bootstrap/4.5/js/bootstrap.bundle.min.js"></script>
<!--[if lt IE 9]>
    <script src="<?php print base_url(); ?>assets/template/crossbrowserjs/html5shiv.js"></script>
    <script src="<?php print base_url(); ?>assets/template/crossbrowserjs/respond.min.js"></script>
    <script src="<?php print base_url(); ?>assets/template/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php print base_url(); ?>assets/template/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php print base_url(); ?>assets/template/plugins/js-cookie/js.cookie.js"></script>
<script src="<?php print base_url(); ?>assets/template/js/theme/material.min.js"></script>
<script src="<?php print base_url(); ?>assets/template/js/apps.min.js"></script>
<script src='<?php print base_url(); ?>assets/template/plugins/pnotify/iife/PNotify.js'></script>
<script src='<?php print base_url(); ?>assets/template/plugins/pnotify/iife/PNotifyButtons.js'></script>
<script src='<?php print base_url(); ?>assets/template/plugins/pnotify/iife/PNotifyConfirm.js'></script>

<!-- ================== END BASE JS ================== -->

<!-- <script src="https://cdn.jsdelivr.net/gh/xcash/bootstrap-autocomplete@v2.3.7/dist/latest/bootstrap-autocomplete.min.js"></script>  -->

<!-- My own js files -->
<?php
    if(isset($js)) {
        foreach($js as $path) { ?>
            <script src="<?php print base_url($path) . "?seed=" . $seed; ?>"></script>
        <?php }
    } 
?>
<!-- End my own css files -->