<span id="burl" class="d-none"><?php print base_url(); ?></span>

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php print base_url(); ?>assets/template/plugins/jquery/jquery-3.3.1.min.js"></script>
<script src="<?php print base_url(); ?>assets/template/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php print base_url(); ?>assets/template/plugins/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
<!--[if lt IE 9]>
    <script src="<?php print base_url(); ?>assets/template/crossbrowserjs/html5shiv.js"></script>
    <script src="<?php print base_url(); ?>assets/template/crossbrowserjs/respond.min.js"></script>
    <script src="<?php print base_url(); ?>assets/template/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->

<script src="<?php print base_url(); ?>assets/template/js/theme/material.min.js"></script>
<script src="<?php print base_url(); ?>assets/template/js/apps.min.js"></script>
<script src='<?php print base_url(); ?>assets/template/plugins/pnotify/iife/PNotify.js'></script>
<script src='<?php print base_url(); ?>assets/template/plugins/pnotify/iife/PNotifyButtons.js'></script>
<script src='<?php print base_url(); ?>assets/template/plugins/pnotify/iife/PNotifyConfirm.js'></script>

<!-- ================== END BASE JS ================== -->

<!-- My own js files -->
<?php
    if(isset($js)) {
        foreach($js as $path) { ?>
            <script src="<?php print base_url($path); ?>"></script>
        <?php }
    } 
?>
<!-- End my own css files -->


</body>
</html>