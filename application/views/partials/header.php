<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta http-equiv="Content-Type" content="text/html">
	
	<link rel="apple-touch-icon" sizes="57x57" href="<?php print base_url("assets/imgs/icos/apple-icon-57x57.png"); ?>" href="">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php print base_url("assets/imgs/icos/apple-icon-60x60.png"); ?>">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php print base_url("assets/imgs/icos/apple-icon-72x72.png"); ?>">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php print base_url("assets/imgs/icos/apple-icon-76x76.png"); ?>">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php print base_url("assets/imgs/icos/apple-icon-114x114.png"); ?>">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php print base_url("assets/imgs/icos/apple-icon-120x120.png"); ?>">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php print base_url("assets/imgs/icos/apple-icon-144x144.png"); ?>">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php print base_url("assets/imgs/icos/apple-icon-152x152.png"); ?>">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php print base_url("assets/imgs/icos/apple-icon-180x180.png"); ?>">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php print base_url("assets/imgs/icos/android-icon-192x192.png"); ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php print base_url("assets/imgs/icos/favicon-32x32.png"); ?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php print base_url("assets/imgs/icos/favicon-96x96.png"); ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php print base_url("assets/imgs/icos/favicon-16x16.png"); ?>">
	<link rel="manifest" href="<?php print base_url("assets/imgs/icos/manifest.json"); ?>">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php print base_url("assets/imgs/icos/ms-icon-144x144.png"); ?>">
	<meta name="theme-color" content="#ffffff">

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" /> -->
	<link href="<?php print base_url("assets/template/plugins/jquery-ui/jquery-ui.min.css"); ?>" rel="stylesheet" />
	<link href="<?php print base_url("assets/template/plugins/bootstrap/4.5/css/bootstrap.min.css"); ?>" rel="stylesheet" />
	<link href="<?php print base_url("assets/template/plugins/font-awesome/5.3/css/all.min.css"); ?>" rel="stylesheet" />
	<link href="<?php print base_url("assets/template/plugins/animate/animate.min.css"); ?>" rel="stylesheet" />
	<link href="<?php print base_url("assets/template/css/material/style.min.css"); ?>" rel="stylesheet" />
	<link href="<?php print base_url("assets/template/css/material/style-responsive.min.css"); ?>" rel="stylesheet" />
	<link href="<?php print base_url("assets/template/css/material/theme/default.css"); ?>" rel="stylesheet" id="theme" />
	<link href="<?php print base_url('assets/template/plugins/pnotify/PNotifyBrightTheme.css'); ?>"  rel="stylesheet" />
	<link href="<?php print base_url('assets/css/global.css') . "?seed=" . $seed; ?>" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- <script src="<?php print base_url('assets/js/apm-agent/elastic-apm-rum.umd.js') ?>" crossorigin></script> -->

	<script>

	// When content is loaded
	// document.addEventListener('DOMContentLoaded', function() {
	// 	elasticApm.init({
	// 		serviceName: 'miServicioAPMHuella',
	// 		// serverUrl: 'http://93.90.194.109:8200',
	// 		// distributedTracingOrigins: ['http://93.90.194.109:8080']
	// 		serverUrl: 'http://localhost:8200',
	// 		distributedTracingOrigins: ['http://localhost:8080']
	// 	});     
	// });

	</script>

	<!-- My own css files -->
	<?php
		if(isset($css)) {
			foreach($css as $path) { ?>
				<link href="<?php print base_url($path) . "?seed=" . $seed; ?>" type="text/css" rel="stylesheet"   />
			<?php }
		} 
	?>
	<!-- End my own css files -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php print base_url(); ?>assets/template/plugins/pace/pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show">
		<div class="material-loader">
			<svg class="circular" viewBox="25 25 50 50">
				<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
			</svg>
			<div class="message">Cargando...</div>
		</div>
	</div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed page-with-wide-sidebar">
		<!-- begin #header -->
		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed navbar-toggle-left" data-click="sidebar-minify">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="<?php print base_url("HomeController");?>" class="navbar-brand">
					<img src="<?php print base_url("assets/imgs/logo2.png") ?>" alt="Huella">
					Human Digital Behavior 
				</a>
			</div>
			<!-- end navbar-header -->
			
			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				
				<li class="dropdown navbar-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"  style=" line-height: 1.4em; font-size: 0.875em; width:350px; text-align:right;">
						<img src="<?php print base_url("assets/imgs/avatars/").$this->session->avatar; ?>" alt="" />
						<span class="d-none d-md-inline"  ><strong>Usuario:</strong> <?php print $this->session->nombre;?>
						</span><br />
						<span class="d-none d-md-inline"  ><strong>Cliente:</strong> <?php print trim($this->session->cliente);?>
						</span>
						
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="<?php print base_url('index.php/'."MiPerfilController/"); ?>" class="dropdown-item">Mi Perfil</a>
						<a href="<?php print base_url('index.php/'."HomeController/cambiarContrasena");?>" class="dropdown-item">Cambiar contraseña</a>
						
						<div class="dropdown-divider"></div>
						<a href="<?php print base_url('index.php/'."LoginController/exit");?>" class="dropdown-item">Salir</a>
					</div>
				</li>
			</ul>
			<!-- end header navigation right -->
			
			<div class="search-form">
				<button class="search-btn" type="submit"><i class="material-icons">search</i></button>
				<input type="text" class="form-control" placeholder="Buscar..." />
				<a href="#" class="close" data-dismiss="navbar-search"><i class="material-icons">close</i></a>
			</div>
		</div>
		<!-- end #header -->