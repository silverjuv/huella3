<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Procesos</a></li>
    <li class="breadcrumb-item active">Monitor de Demonios</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Monitor de Demonios (Procesos en Background)</h1>
<!-- end page-header -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Status de Demonios</h4>
    </div>
    <div class="panel-body alert-secondary">
        <div id="ctrls" class="row">
            <div class="col-sm-4"><h6>Procesos buscar en OSINT</h6>
            </div>
            <div class="col-sm-8">
                <div class="progress">
                    <div id="progProcsOS" class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                * <span id="sp_status_procs_osi">
                </span>
            </div>            
            <!-- <div class="col-sm-12   ">
                * <span id="sp_status_procs_osi">
                </span>
            </div> -->
        </div>
        <br />
        <div id="ctrls" class="row">
            <div class="col-sm-4"><h6>Proceso Expedientes Personas</h6>
            </div>
            <div class="col-sm-8">
                <div class="progress">
                    <div id="progExpsPers" class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                * <span id="sp_status_procspers">
                </span>
            </div>            
        </div>
        <br />
        <div id="ctrls" class="row">
            <div class="col-sm-4"><h6>Proceso Puestos-Personas</h6>
            </div>
            <div class="col-sm-8">
                <div class="progress">
                    <div id="progPuesPers" class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                * <span id="sp_status_procspuespers">
                </span>
            </div>            
        </div>
        <br />
        <div id="ctrls" class="row">
            <div class="col-sm-4"><h6>Proceso Capturador de Noticias</h6>
            </div>
            <div class="col-sm-8">
                <div class="progress">
                    <div id="progNots" class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                * <span id="sp_status_noticias">
                </span>
            </div>
        </div>
        <br />
        <div id="ctrls" class="row">
            <div class="col-sm-4"><h6>Proceso Capturador de Darkweb</h6>
            </div>
            <div class="col-sm-8">
                <div class="progress">
                    <div id="progDark" class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                * <span id="sp_status_darkw">
                </span>
            </div>
        </div>

    </div>
</div>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Log del Proceso Analizador Puestos - Personas</h4>
    </div>
    <div class="panel-body alert-secondary">
        <div id="ctrls" class="row">
            <div class="col-sm-6 ">
                <div class="row">
                    <div class="col-sm-4 "># Últimos registros:</div>
                    <div class="col-sm-4 "><select class="form-control input-sm" style="height:28px" id="slTamLog" onchange="recargaLogger();">
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select></div>
                    <div class="col-sm-4 "></div>
                </div>
            </div>
            <div class="col-sm-6 ">
                <div class="btn-group btn-group-sm" role="group">

                    <!-- <button id="_iniciar" type="button" class="btn btn-lime btn-sm">
                        <i class="fas fa-play"></i>
                        Iniciar servicio
                    </button>
                    <button id="_detener" type="button" class="btn btn-danger btn-sm">
                        <i class="fas fa-stop"></i>
                        Detener servicio
                    </button> -->
                    <button id="_actualizar" type="button" class="btn btn-warning btn-sm">
                        <i class="fas fa-edit"></i>
                        Actualizar Log
                    </button>
                </div>
            </div>
        </div>
        <br />
        <!-- <div class="row"> -->
        <!-- <div class="col-sm-3">
        <label for="busqueda">Escriba su búsqueda</label>
    </div>
    <div class="col-sm-7">
        <input type="text" class="form-control" id="termino">
    </div>
    <div class="col-sm-2">
        <button id="btnBuscar" class="btn btn-primary">Buscar</button>
    </div> -->

        <!-- <iframe class="embed-responsive-item" id="iMonitor"></iframe> -->
        <div id="dv_logs"></div>
        <!-- <pre class="code" data-language="php">            
            </pre> -->

    </div>
</div>

<!-- <div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Monitor del Demonio Capturador de Noticias</h4>
    </div>
    <div class="panel-body alert-secondary">
        <div id="ctrls" class="row">
            <div class="col-sm-12   ">
                <span id="sp_status_noticias">
                </span>
            </div>
        </div>

    </div>
</div> -->