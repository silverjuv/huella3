<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8" />
	<title>SyntaxHighlighter Build Test Page</title>
	<link rel="stylesheet" media="all" href="assets/template/css/jquery.highlight.css" />
	<style>
		body {
			background:none repeat scroll 0 0 #FFFFFF;
			color:#494949;
			font-family:Verdana,sans-serif;
			font-size:62.5%;
			margin:0;
			padding:0 1.0em;
		}
		p {
			margin:0.5em 0 0;
			padding:0 0 0.5em;
			font-size:1.3em;
			line-height:1.6em;
		}
		div.highlight ul.tabs li {
			font-size: 14px;
		}
  </style>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script src="assets/template/js/jquery.highlight.js"></script>
	<script>
		$(document).ready(function(){
			$('pre.code').highlight({source:0, zebra:0, indent:'space', list:'ol',attribute: 'data-language'});
		});
	</script>
</head>
<body>
<!-- <h1>JQuery syntax Highlight Demo</h1>
<p>This is a test file to insure that everything is working well.</p>
<h2>CSS code</h2>
<pre class="code" data-language="css">
2020-09-07 00:16:05,129 - huellamon - INFO - Email enviado al cliente de persona Horacio Erik Avilés Martínez para el puesto Policia Auxiliar, resultado (1-10): 6
2020-09-07 00:27:29,547 - huellamon - INFO - No hay personas para procesar...
2020-09-07 10:26:00,408 - huellamon - INFO - Email enviado al cliente de persona Horacio Erik Avilés Martínez para el puesto Policia Auxiliar, resultado (1-10): 6
2020-09-07 10:26:06,537 - huellamon - INFO - No hay personas para procesar...
</pre>

<h2>HTML code</h2>
<pre class="code" data-language="html">
2020-09-07 00:16:05,129 - huellamon - INFO - Email enviado al cliente de persona Horacio Erik Avilés Martínez para el puesto Policia Auxiliar, resultado (1-10): 6
2020-09-07 00:27:29,547 - huellamon - INFO - No hay personas para procesar...
2020-09-07 10:26:00,408 - huellamon - INFO - Email enviado al cliente de persona Horacio Erik Avilés Martínez para el puesto Policia Auxiliar, resultado (1-10): 6
2020-09-07 10:26:06,537 - huellamon - INFO - No hay personas para procesar...
</pre> -->

<!-- <h2>PHP code</h2> -->
<pre class="code" data-language="php">
<?php echo $log;?>
</pre>

<!-- <h2>JS code (default)</h2>
<pre class="code" data-language="js">
2020-09-07 00:16:05,129 - huellamon - INFO - Email enviado al cliente de persona Horacio Erik Avilés Martínez para el puesto Policia Auxiliar, resultado (1-10): 6
2020-09-07 00:27:29,547 - huellamon - INFO - No hay personas para procesar...
2020-09-07 10:26:00,408 - huellamon - INFO - Email enviado al cliente de persona Horacio Erik Avilés Martínez para el puesto Policia Auxiliar, resultado (1-10): 6
2020-09-07 10:26:06,537 - huellamon - INFO - No hay personas para procesar...
</pre>

<h2>SQL code</h2>
<pre class="code" data-language="sql">
2020-09-07 00:16:05,129 - huellamon - INFO - Email enviado al cliente de persona Horacio Erik Avilés Martínez para el puesto Policia Auxiliar, resultado (1-10): 6
2020-09-07 00:27:29,547 - huellamon - INFO - No hay personas para procesar...
2020-09-07 10:26:00,408 - huellamon - INFO - Email enviado al cliente de persona Horacio Erik Avilés Martínez para el puesto Policia Auxiliar, resultado (1-10): 6
2020-09-07 10:26:06,537 - huellamon - INFO - No hay personas para procesar...
</pre> -->

<br />

</body>
</html>
