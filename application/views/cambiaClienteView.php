<!-- begin breadcrumb -->
<?php
// var_dump($datos); die();
?>
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item active">Cambio de Cliente</li>
</ol>
<br /><br /><br />
<!-- end breadcrumb -->
<!-- begin page-header -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Cambio de Cliente</h4>
    </div>
    <div class="panel-body">
        <!-- <h1 class="page-header">Noticias</h1> -->
        <!-- end page-header -->
        <div class="row">
            <div class="col-sm-3">
                <!-- <label for="busqueda">Cliente actual: </label> -->
            </div>
            <div class="col-sm-7">
                <div class="row">
                    <div class="col-sm-4">
                        <label >Cliente actual:</label>
                    </div>
                    <div class="col-sm-8">
                        <?php echo $_SESSION['cliente']; ?>
                    </div>
                </div>
                <input type="hidden" id="hd_cliente" value="<?php echo $_SESSION['id_cliente']; ?>" />
                <div class="row">
                    <div class="col-sm-4">
                        <label for="cliente">Cliente nuevo:</label>
                        <!-- <input type="hidden" id="hd_cliente" value="<?php echo $_SESSION['id_cliente']; ?>" /> -->
                    </div>
                    <div class="col-sm-8">
                        <select id="cliente" style="width:100%"></select>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-sm-12">
                        * En el listado se muestra el total de clientes de la base de datos. Sin importar su Status.
                    </div>
                </div> -->
                <br />
                <!-- <br />
                <hr /> -->
                <div class="row">
                    <div class="col-sm-8">
                        <!-- <label for="cliente">Cliente nuevo:</label> -->
                    </div>
                    <div class="col-sm-4">
                        <button class="btn btn-primary" id="btnGuardar">Cambiar de cliente</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <!-- <button class="btn btn-primary" id="btnGuardar">Cambiar de cliente</button> -->
            </div>
        </div>
        <!-- <hr />
        <div class="row">
            <div class="col-sm-3">
                <label for="busqueda"></label>
            </div>
            <div class="col-sm-7">
                &nbsp;
            </div>
            <div class="col-sm-2">
                <button class="btn btn-primary" onclick="guardar()">Guardar Configuración</button>
            </div>
        </div> -->
    </div><!-- panel body -->
</div><!-- panel -->


<div class="toast" role="alert" aria-live="assertive" data-delay="4000" aria-atomic="true">
    <div class="toast-header">
        <!-- <img src="..." class="rounded mr-2" alt="..."> -->
        <strong class="mr-auto">Error de validaci&oacute;n</strong>
        <!-- <small>11 mins ago</small> -->
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body">

    </div>
</div>