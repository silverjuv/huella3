<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function generarString() {
    $micro = str_replace(".","",microtime()); 
    $micro = str_replace(" ","",$micro);
    return $micro;
}

function fileOutput($str, $path="log.txt", $mode="a") {
    $fh = fopen($path, $mode);
    if($fh) {
        fwrite($fh, $str);
        fclose($fh);
    }
}