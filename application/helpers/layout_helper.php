<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dibuja la pantalla usando partials e incluye los scripts js y css necesarios para la vista
 * @param $view Se refiere a la vista que se desea cargar
 * @param $params variables que se quieran mandar a la vista, pero al menos se debieran pasar los valores de los indices menu_expandido (id_pantalla del menu padre) y pantalla (id_pantalla seleccionado en el menu) con la finalidad de renderizar el menu apropiadamente
 * @param $ownScripts arreglo de js a incluir en la vista
 * @param $ownCss arreglo de css a incluir en la vista
 * 
 */
function makeDefaultLayout($view, $params=array(), $ownScripts=array(), $ownCss=array()) {
    $CI =& get_instance();
    $menu = getMenu($CI->session->id_perfil);
    $params["seed"] = random_string("alnum", 10);
    $params["css"] = $ownCss;

    $CI->load->view("partials/header", $params);
    $CI->load->view("partials/sidebar", $menu);
    $CI->load->view($view);

    $ownScripts["js"] = empty($ownScripts)?array():$ownScripts;
    $CI->load->view("partials/footer", $ownScripts);
    $CI->load->view("partials/endTags");
}

function makeFreeLayout($view, $params=array(), $ownScripts=array(), $ownCss=array()) {
    $CI =& get_instance();
    
    $params["css"] = $ownCss;
    $CI->load->view("partials/freeHeader", $params);
    $CI->load->view($view);
    $ownScripts["js"] = empty($ownScripts)?array():$ownScripts;
    $CI->load->view("partials/freeFooter", $ownScripts);
}

/**
 * Obtiene los elementos del menu a dibujar de acuerdo al perfil del usuario
 * @param id_perfil el perfil del cual se obtendran las pantallas a las que se tiene acceso
 * @return array con los elementos del menu 
 */
function getMenu($id_perfil) {
    $CI =& get_instance();
    $id_perfil = intval($id_perfil);

    $sql = "Select id_pantalla, id_padre, nombre_menu, controlador, icono, tiene_hijos, orden 
            From cat_pantallas 
            Where pantalla_publica='1' and sin_menu='0' and tiene_hijos='0' and (id_padre is null or id_padre='0')
            
            UNION distinct
            
            Select p.id_pantalla, p.id_padre, p.nombre_menu, p.controlador, p.icono, p.tiene_hijos, p.orden
            From cat_pantallas as p Join perfiles_pantallas as pp on p.id_pantalla=pp.id_pantalla
            Where pp.id_perfil = $id_perfil and sin_menu='0' and tiene_hijos='0' and (id_padre is null or id_padre='0') order by orden ";
    $r = $CI->db->query($sql);
    
    $CI->db->distinct();
    $r3 = $CI->db->select("pa.id_pantalla, pa.id_padre, pa.nombre_menu, pa.controlador, pa.icono, pa.tiene_hijos, pa.orden")->
        from("cat_usuarios as u")->
        join("cat_perfiles as p", "u.id_perfil = p.id_perfil")->
        join("perfiles_pantallas as pp", "pp.id_perfil = u.id_perfil")->
        join("cat_pantallas as pa", "pa.id_pantalla = pp.id_pantalla")->
        where("u.id_perfil", $id_perfil)->
        where("pa.tiene_hijos = '1'")->
        order_by("pa.orden")->
        get();

    $menu = array();
    //enlaces sin menu padre publicos
    if($r && $r->num_rows()>0) {
        $menu["simpleLinks"] = $r->result_array();
    } else {
        $menu["simpleLinks"] = array();
    }
    

    //menus padres
    if($r3 && $r3->num_rows()>0) {
        $menu["parentLinks"] = $r3->result_array();
        $len = count($menu["parentLinks"]);

        //submenus
        for($i=0; $i<$len; $i++) {
            $CI->db->distinct();
            $r3 = $CI->db->select("pa.id_pantalla, pa.id_padre, pa.nombre_menu, pa.controlador, pa.icono, pa.tiene_hijos, pa.orden")->
                from("cat_usuarios as u")->
                join("cat_perfiles as p", "u.id_perfil = p.id_perfil")->
                join("perfiles_pantallas as pp", "pp.id_perfil = u.id_perfil")->
                join("cat_pantallas as pa", "pa.id_pantalla = pp.id_pantalla")->
                where("u.id_perfil", $id_perfil)->
                where("pa.id_padre", $menu["parentLinks"][$i]["id_pantalla"])->
                order_by("orden")->
                get();
            
            $menu["parentLinks"][$i]["children"] = array();
            if($r3 && $r3->num_rows()>0) {
                $menu["parentLinks"][$i]["children"] = $r3->result_array();
            }
        }
    } else {
        $menu["parentLinks"] = array();
    }

    return $menu;
}