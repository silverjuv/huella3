<?php

class AccessControl 
{
	var $CI;
	function __construct(){
	    $this->CI =& get_instance();
	}

	function verify(){
		$clase = $this->CI->router->class;
		// para evitar el enciclamiento cuando se redirecciona al login
		if($clase == 'LoginController') {
			return true;
		}
		// Para los que tienen acceso directo al REST API
		// echo $clase; die();
		if($clase == 'Puestos' || $clase == 'Personas'  ||  $clase == 'ApiRest'     ) {
			// || $clase == 'apisamples' 
			return true;
		}
		// echo $clase; die();
		//no se tiene session y se redirecciona al index
		if(!isset($this->CI->session->id_perfil) ) {
            redirect('/LoginController/index/', 'refresh');
        }
        else if(isset($this->CI->session->id_perfil)) {
        	//se tiene session y se valida si se tiene permisos a pantalla
        	if($this->hasAccessToController($this->CI->session->id_perfil, $clase)) {
				return true;
			} else {
				//die($clase);
				redirect('/PermissionDeniedController/index/', 'refresh');
			}
        }

        return true;

	}//function

	function hasAccessToController($id_perfil, $clase) {
		$id_perfil = intval($id_perfil);

		$r = $this->CI->db->from("cat_pantallas")->
			where("pantalla_publica='1'")->
			where("controlador", $clase)->count_all_results();
			//die($this->db->last_query());
		if($r>0) {
			return true;
		} else {
			$r = $this->CI->db->from("perfiles_pantallas as pp")->
				join("cat_pantallas as cp", "cp.id_pantalla=pp.id_pantalla")->
				join("cat_perfiles as per", "per.id_perfil=pp.id_perfil")->
				where("pp.id_perfil", $id_perfil)->
				where("cp.controlador", $clase)->
				count_all_results();
			// die($this->CI->db->last_query());
			return ($r>0);
		}
	}

}//class