(function($) {
    $(document).ready(init);

    function init() {
        $("#btnCambiar").on("click", cambiarPass);
    }

    function validar() {
        let obj = {};
        obj.pass = $.trim($("#pass").val());
        obj.passn = $.trim($("#passn").val());
        obj.passnc = $.trim($("#passnc").val());

        if(obj.pass == "") {
            PNotifyAlerta("Atención", "Debe escribir su contraseña actual", "error");
            return false;
        }
        if(obj.passn == "") {
            PNotifyAlerta("Atención", "Debe escribir la nueva contraseña", "error");
            return false;
        }

        if(obj.passn != obj.passnc) {
            PNotifyAlerta("Atención", "La contraseña nueva no coincide con la confirmación, favor de reescribirlas", "error");
            return false;
        }

        return obj;

    }

    function cambiarPass() {
        let obj = validar();

        if(obj === false) {
            return false;
        } else {
            let burl = $("#burl").html() + "HomeController/actualizarPass";
            $.post(burl, {data: obj}, function(result) {
                if(result != undefined) {
                    PNotifyAlerta(result.title, result.msg, result.type);
                }
            }, "json");
        }
    }

})(jQuery)