(function($) {
    $(document).ready(init);

    function init() {
        $("#_agregar").on("click", mostrarAgregar);
        $("#_editar").on("click", mostrarEditar);
        // $("#_borrar").on("click", confirmarBorrado);
        $("#_activar").on("click", deshabilitar);
        $("#btnGuardar").on("click", guardar);
        $("#_exportar").on("click", function () {
            exportData();
        });
        $("#exportData").on("click", function () {
            abreVentanaModal("exportarDialog", "Exportar a...");
        });

        initializeTable();
        iniciarCombos();
    }

    function exportData() {
        
        var radioValue = $("input[name='optradio']:checked").val();
        var burl = $("#burl").html() + "CatSitiosWebController/exportData/?format="+radioValue;
        // var burl = $("#burl").html() + "ResultadosOdometroController/exportCSV";
        window.location=burl;        
    }
    
    function initializeTable() {
        let burl = $("#burl").html() + "CatSitiosWebController/listar";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";
        // alert(lan)

        $("#tsitiosweb").DataTable({
            // "scrollY": '35vh',
            "processing": true,
            "serverSide": false,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function(d) {

                }
            },
            "dataSrc": "data",
            "columns": [
                { "data": "id", "visible": false },
                { "data": "target" },
                { "data": "categoria" },
                { "data": "peso" },
                { "data": "paginas" },
                { "data": "pais" },
                { "data": "entidad" },
                { "data": "activo" }
            ],
            "language": {
                "url": lan
            },
            "select": 'single',
            "lengthChange": false,
            // "pageLength": 10,
            "paging": true,
            "info": false
        }).on('init.dt', function() {

        });
    }

    function limpiaCtrls() {
        $("#dominio").val("");
        $("#peso").val("1");
        $("#entidad").val("");
        $("#cve_pais").val('MEX').trigger("change");
        $("#id_categoria").val(null).trigger("change");
    }

    function mostrarAgregar() {
        limpiaCtrls();
        _idPuesto = 0;
        $("#id_edicion").val("");
        abreVentanaModal("__modal", "Agregar Registro");
    }

    function iniciarCombos() {
        let burl = $("#burl").html() + "CatSitiosWebController/getPaisesForCombo";
        $.post(burl, {}, function(result) {
            if (result != undefined) {
                $("#cve_pais").select2({
                    data: result,
                    placeholder: 'Selecciona un pais',
                    // id: 'MEX',
                    // allowClear: true,
                    // with: 'resolve'
                });
                $("#cve_pais").val(null).trigger("change");
            }
        }, 'json');

        burl = $("#burl").html() + "CatSitiosWebController/getCategoriasForCombo";
        $.post(burl, {}, function(result) {
            if (result != undefined) {
                $("#id_categoria").select2({
                    data: result,
                    placeholder: 'Selecciona una categoria',
                    // allowClear: true,
                    // with: 'resolve'
                }).on("change", function() {

                });
                $("#id_categoria").val(null).trigger("change");
            }
        }, 'json');

    }

    function validar() {
        let obj = {};

        obj.target = $.trim($("#dominio").val());
        obj.peso = $("#peso").val();
        obj.entidad = $.trim($("#entidad").val());
        obj.cve_pais = $("#cve_pais").val();
        obj.id_categoria = $("#id_categoria").val();


        let accion = ($("id_edicion").val() != "") ? "editar" : "insertar";

        if (obj.target === "") {
            PNotifyAlerta("Atención", "El campo Dominio es obligatorio", "error");
            return false;
        }

        if (obj.peso === "") {
            PNotifyAlerta("Atención", "El campo Peso es obligatorio", "error");
            return false;
        }

        return obj;

    }

    function guardar() {
        let obj = validar();
        let burl = $("#burl").html() + "CatSitiosWebController/guardar";

        if (obj === false) {
            return false;
        } else {
            obj.id_edicion = $.trim($("#id_edicion").val());
            if (obj.id_edicion != "") {
                obj.accion = "editar";
            } else {
                obj.accion = "insertar";
            }

            $.post(burl, { data: obj }, function(result) {
                if (result != undefined) {
                    PNotifyAlerta(result.title, result.msg, result.type);
                    if (result.error === "0") {
                        $("#tsitiosweb").DataTable().ajax.reload(null, false);
                        cierraVentanaModal("__modal");
                    }
                }
            }, 'json').fail(function() {
                PNotifyAlerta('Error', 'Algo salio mal, no fue posible guardar, probablemente ya exista ese dominio en su listado.', 'error');
            });
        }
    }

    function mostrarEditar() {
        let burl = $("#burl").html() + "CatSitiosWebController/getSitioWebById";
        let t = $("#tsitiosweb").DataTable();
        let r = t.row({ selected: true }).data();

        if (r != null) {
            $.post(burl, { id_site: r.id }, function(result) {
                if (result != undefined) {
                    if (result.id != null) {
                        setDatosToForm(result);
                        $("#id_edicion").val(result.id);
                        abreVentanaModal("__modal", "Editar Registro");
                    }
                }
            }, 'json');
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar el registro de la tabla a editar", "notice");
        }
    }

    function setDatosToForm(d) {
        limpiaCtrls();
        $("#dominio").val(d.target);
        $("#peso").val(d.peso);
        $("#entidad").val(d.entidad);
        $("#cve_pais").val(d.cve_pais).trigger("change");
        $("#id_categoria").val(d.id_categoria).trigger("change");
    }

    // function confirmarBorrado() {

    //     let t = $("#tsitiosweb").DataTable();
    //     let r = t.row({ selected: true }).data();

    //     if (r != null) {
    //         const notice = PNotifyConfirm("Confirme su acción", "¿Desea borrar el sitio web: " + r.correo + "?", "Si, proceder");

    //         notice.on('pnotify.confirm', (v) => {
    //             borrarSitio(r.id_usuario);
    //         });
    //         notice.on('pnotify.cancel', () => {
    //             console.log("rejected")
    //         });
    //     } else {
    //         PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
    //     }
    // }

    // function borrarSitio(id_usuario) {
    //     let burl = $("#burl").html() + "CatSitiosWebController/deleteSitioWebById";
    //     $.post(burl, { id_usuario: id_usuario }, function(result) {
    //         if (result != undefined) {
    //             PNotifyAlerta(result.title, result.msg, result.type);
    //             if (result.error === "0") {
    //                 $("#tsitiosweb").DataTable().ajax.reload(null, false);
    //             }
    //         }
    //     }, 'json');
    // }

    function deshabilitar() {
        let t = $("#tsitiosweb").DataTable();
        let r = t.row({ selected: true }).data();

        if (r != null) { //TODO
            let burl = $("#burl").html() + "CatSitiosWebController/deshabilitarSitioWebById";
            $.post(burl, { id_site: r.id }, function(result) {
                if (result != undefined) {
                    PNotifyAlerta(result.title, result.msg, result.type);
                    if (result.type == "success") {
                        $("#tsitiosweb").DataTable().ajax.reload(null, false);
                    }
                }
            }, 'json');
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
        }
    }

})(jQuery);



// function checkDomain(nname) {

//     //var url = 'http:www.google.com';
//     url = 'https://google.co.in';
//     var re = /^(http[s]?:\/\/){0,1}(w{3,3}\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/;
//     if (!re.test(url)) {
//         alert("url error");
//         return false;
//     } else {
//         alert('success')
//     }
//     return


//     var arr = new Array('.com', '.net', '.org', '.biz', '.coop', '.info', '.museum', '.name',
//         '.pro', '.edu', '.gov', '.int', '.mil', '.ac', '.ad', '.ae', '.af', '.ag',
//         '.ai', '.al', '.am', '.an', '.ao', '.aq', '.ar', '.as', '.at', '.au', '.aw',
//         '.az', '.ba', '.bb', '.bd', '.be', '.bf', '.bg', '.bh', '.bi', '.bj', '.bm',
//         '.bn', '.bo', '.br', '.bs', '.bt', '.bv', '.bw', '.by', '.bz', '.ca', '.cc',
//         '.cd', '.cf', '.cg', '.ch', '.ci', '.ck', '.cl', '.cm', '.cn', '.co', '.cr',
//         '.cu', '.cv', '.cx', '.cy', '.cz', '.de', '.dj', '.dk', '.dm', '.do', '.dz',
//         '.ec', '.ee', '.eg', '.eh', '.er', '.es', '.et', '.fi', '.fj', '.fk', '.fm',
//         '.fo', '.fr', '.ga', '.gd', '.ge', '.gf', '.gg', '.gh', '.gi', '.gl', '.gm',
//         '.gn', '.gp', '.gq', '.gr', '.gs', '.gt', '.gu', '.gv', '.gy', '.hk', '.hm',
//         '.hn', '.hr', '.ht', '.hu', '.id', '.ie', '.il', '.im', '.in', '.io', '.iq',
//         '.ir', '.is', '.it', '.je', '.jm', '.jo', '.jp', '.ke', '.kg', '.kh', '.ki',
//         '.km', '.kn', '.kp', '.kr', '.kw', '.ky', '.kz', '.la', '.lb', '.lc', '.li',
//         '.lk', '.lr', '.ls', '.lt', '.lu', '.lv', '.ly', '.ma', '.mc', '.md', '.mg',
//         '.mh', '.mk', '.ml', '.mm', '.mn', '.mo', '.mp', '.mq', '.mr', '.ms', '.mt',
//         '.mu', '.mv', '.mw', '.mx', '.my', '.mz', '.na', '.nc', '.ne', '.nf', '.ng',
//         '.ni', '.nl', '.no', '.np', '.nr', '.nu', '.nz', '.om', '.pa', '.pe', '.pf',
//         '.pg', '.ph', '.pk', '.pl', '.pm', '.pn', '.pr', '.ps', '.pt', '.pw', '.py',
//         '.qa', '.re', '.ro', '.rw', '.ru', '.sa', '.sb', '.sc', '.sd', '.se', '.sg',
//         '.sh', '.si', '.sj', '.sk', '.sl', '.sm', '.sn', '.so', '.sr', '.st', '.sv',
//         '.sy', '.sz', '.tc', '.td', '.tf', '.tg', '.th', '.tj', '.tk', '.tm', '.tn',
//         '.to', '.tp', '.tr', '.tt', '.tv', '.tw', '.tz', '.ua', '.ug', '.uk', '.um',
//         '.us', '.uy', '.uz', '.va', '.vc', '.ve', '.vg', '.vi', '.vn', '.vu', '.ws',
//         '.wf', '.ye', '.yt', '.yu', '.za', '.zm', '.zw');
//     var mai = nname;
//     var val = true;
//     var dot = mai.lastIndexOf(".");
//     var dname = mai.substring(0, dot);
//     var ext = mai.substring(dot, mai.length);
//     if (dot > 2 && dot < 57) {
//         for (var i = 0; i < arr.length; i) {
//             if (ext == arr[i]) {
//                 val = true;
//                 break;
//             } else {
//                 val = false;
//             }
//         }
//         if (val == false) {
//             // alert("Su extension de dominio "
//             //     ext " no es correcto");
//             return false;
//         } else {
//             for (var j = 0; j < dname.length; j) {
//                 var dh = dname.charAt(j);
//                 var hh = dh.charCodeAt(0);
//                 if ((hh > 47 && hh < 59) || (hh > 64 && hh < 91) || (hh > 96 && hh < 123) || hh == 45 || hh == 46) {
//                     if ((j == 0 || j == dname.length - 1) && hh == 45) {
//                         // alert("Nombre de dominio no se debe comenzar con el final '-'");
//                         return false;
//                     }
//                 } else {
//                     // alert("Su nombre de dominio no deben tener caracteres especiales");
//                     return false;
//                 }
//             }
//         }
//     } else {
//         // alert("Su nombre de dominio es demasiado corto / largo");
//         return false;
//     }
//     return true;
// }

// // function ctck() {
// //     var sds = document.getElementById("dum");
// //     if (sds == null) { alert("Esta utilizando un paquete gratuito. No esta autorizado para retirar la etiqueta."); }
// //     var sdss = document.getElementById("dumdiv");
// //     if (sdss == null) { alert("Esta utilizando un paquete gratuito. No esta autorizado para retirar la etiqueta."); }
// // }