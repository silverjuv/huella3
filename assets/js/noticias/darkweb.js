(function($) {
    $(document).ready(init);

    function init() {
        $.preloader.stop();



        $("#btnBuscar").on("click", buscarTerminos);
        $("#termino").on("keyup", function(e) {
            // alert('ok');
            if (e.which == 13) {
                buscarTerminos()
            }
        });

        $('.basicAutoComplete').autoComplete({
            resolver: 'custom',
            events: {
                search: function (qry, callback) {
                    // let's do a custom ajax call
                    // https://bootstrap-autocomplete.readthedocs.io/en/latest/#getting-started
                    // https://raw.githack.com/xcash/bootstrap-autocomplete/master/dist/latest/testdata/test-dict.json
                    $.ajax(
                        ''+$("#hd_url").val()+'index.php/DarkWebController/getSearchAutocomplete',
                        {
                            data: { 'keyw': $("#termino").val()}
                        }
                    ).done(function (res) {
                        //alert (res.results)
                        callback(res.results)
                    });
                }
            }
        }); 
        
        $("#termino").focus();
    }

    function buscarTerminos() {
        // Valida longitud de captura no mayor ni menor a ... 4 caracteres y max 50
        if ($.trim($("#termino").val()).length >= 4 && $.trim($("#termino").val()).length <= 50) {
            let search = encodeURIComponent($.trim($("#termino").val()));
            //document.location.href = $("#burl").html() + "DarkWebController/listar/1?search=" + search;
            document.location.href = $("#burl").html() + "DarkWebController/listarElk/1?search=" + search;
        } else {
            $('.toast-body').html("Su busqueda debe ser entre 4 y 50 caracteres...");
            $('.toast').toast('show');
        }
    }

})(jQuery);

$.preloader.start({
    modal: true
});