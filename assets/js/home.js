(function($) {
    $(document).ready(init);

    function init() {

        // loadOdometro();
        // $("body").toggleClass("page-sidebar-minified");
        // $("sidebar-minify").trigger("click");
        setTimeout(cargaDatosEstadisticos, 0);
        // $("#a_contrato").trigger("click");
    }
    

})(jQuery);


function cargaDatosEstadisticos() {
    var burl = $("#burl").html() + "HomeController/cargaNumerosTop";

    // $("#popup").modal({
    //   show: false,
    //   backdrop: 'static'
    // });
    // $("#popup").modal("show"); 
    
    $.post(burl, {}, function(result) {
        // pagina = page;
        // $("#popup").modal("hide"); 
        if (result != undefined) {
            $("#p_tPersonas").animateNumber({ number: result.totalPersonas });
            $("#p_tPuestos").animateNumber({ number: result.totalPuestos });
            $("#p_tResultados").animateNumber({ number: result.totalPromedio });

            // Total de analisis por cliente sin importar el numero de contratos realizados... 
            $("#p_tAnalisis").animateNumber({ number: result.totalAnalisis });
            // loadOdometro(result.totalPromedio);
            setTimeout(function() { $("#p_tAnalisis").html(number_format($("#p_tAnalisis").html())) }, 1000)
            setTimeout(function() { $("#p_tResultados").html(number_format($("#p_tResultados").html())) }, 1000)
            
            // graficaBarras(result.totalMeses);
            // graficaRadar(result.riesgos, result.totalRiesgos, result.colorsBack);
            // graficaDona(result.rangosColors);

            graficaSparkCircleMini(result.promedioRiesgos);

            // // // myChartProcesamientos(result.anios, result.nofinded, result.aptos, result.noaptos);

            // // // barrasProcesamientoMensual(result.aniosBarras, result.anioAct, result.anioAnt, result.anioAntAnt);
            barrasProcesamientoMensual(result.aniosBarras, result.anioAct, result.anioAnt, result.anioAntAnt);

            graficaDona2(result.totalPorTool);

            myChartProcesamientos(result.anios, result.nofinded, result.aptos, result.noaptos);

            
        }

    }, 'json');

}


// function myChartProcesamientos(anios, noencontrados, aptos, noaptos){
//     var options = {
//         series: [{
//         name: 'No se encontró información',
//         data: noencontrados
//       }, {
//         name: 'Persona Apta para Puesto',
//         data: aptos
//       }, {
//         name: 'Persona NO Apta para Puesto',
//         data: noaptos
//       }],
//         chart: {
//         type: 'bar',
//         height: 350,
//         stacked: true,
//         stackType: '100%'
//       },
//       plotOptions: {
//         bar: {
//           horizontal: true,
//         },
//       },
//       stroke: {
//         width: 1,
//         colors: ['#fff']
//       },
//       title: {
//         //text: 'Tipos de Resultados finales generados al ser revisados y aprobados'
//         text: 'Tipos de Resultados finales generados los ultimos 2 años'
//       },
//       xaxis: {
//         categories: anios,
//       },
//       tooltip: {
//         y: {
//           formatter: function (val) {
//             // return val + "K"
//             return val
//           }
//         }
//       },
//       fill: {
//         opacity: 1
      
//       },
//       legend: {
//         position: 'top',
//         horizontalAlign: 'left',
//         offsetX: 40
//       }
//       };

//       var chart = new ApexCharts(document.querySelector("#myChartProcesamientos"), options);
//       chart.render();
// }

function myChartProcesamientos(anios, noencontrados, aptos, noaptos){
  var dom = document.getElementById("myChartProcesamientos");
  var myChart = echarts.init(dom);

  window.addEventListener('resize',function(){
    myChart.resize();
  })

  var app = {};

  var option;



  option = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        // Use axis to trigger tooltip
        type: 'shadow' // 'shadow' as default; can also be 'line' or 'shadow'
      }
    },
    toolbox: {
      show: true,
      feature: {
        mark: { show: true },
        // dataView: { show: true, readOnly: false },
        restore: { show: true },
        saveAsImage: { show: true }
      }
    },    
    legend: {},
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: {
      type: 'value'
    },
    yAxis: {
      type: 'category',
      data: anios
    },
    series: [
      {
        name: 'No se encontró información',
        type: 'bar',
        stack: 'total',
        label: {
          show: true
        },
        emphasis: {
          focus: 'series'
        },
        data: noencontrados
      },
      {
        name: 'Persona Apta para Puesto',
        type: 'bar',
        stack: 'total',
        label: {
          show: true
        },
        emphasis: {
          focus: 'series'
        },
        data: aptos
      },
      {
        name: 'Persona NO Apta para Puesto',
        type: 'bar',
        stack: 'total',
        label: {
          show: true
        },
        emphasis: {
          focus: 'series'
        },
        data: noaptos
      }
    ]
  };

  if (option && typeof option === 'object') {
      myChart.setOption(option);
  }
}


// function graficaDona2(datosX){
//     var options = {
//         series: datosX,
//         chart: {
//         height: 390,
//         type: 'radialBar',
//       },
//       plotOptions: {
//         radialBar: {
//           offsetY: 0,
//           startAngle: 0,
//           endAngle: 270,
//           hollow: {
//             margin: 5,
//             size: '30%',
//             background: 'transparent',
//             image: undefined,
//           },
//           dataLabels: {
//             name: {
//               show: false,
//             },
//             value: {
//               show: false,
//             }
//           }
//         }
//       },
//       colors: ['#1ab7ea', '#aa84ff', '#39539E', '#0077B5', '#0f7700'],
//       labels: ['OSINT', 'Redes Sociales', 'Buscadores Web', 'Sitios de Noticias', 'Darkweb'],
//       legend: {
//         show: true,
//         floating: true,
//         fontSize: '16px',
//         position: 'left',
//         offsetX: 16,
//         offsetY: 15,
//         labels: {
//           useSeriesColors: true,
//         },
//         markers: {
//           size: 0
//         },
//         formatter: function(seriesName, opts) {
//           return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex]
//         },
//         itemMargin: {
//           vertical: 3
//         }
//       },
//       responsive: [{
//         breakpoint: 480,
//         options: {
//           legend: {
//               show: false
//           }
//         }
//       }]
//       };

//       var chart = new ApexCharts(document.querySelector("#miDona2"), options);
//       chart.render();
    
// }

function graficaDona2(datosX){

  var dom = document.getElementById("miDona2");
  var myChartDona = echarts.init(dom);

  window.addEventListener('resize',function(){
    myChartDona.resize();
  })

  var app = {};

  var option;


  option = {
    title: {
      text: 'Información recopilada por fuente',
      subtext: '',
      left: 'center'
    },
    tooltip: {
      trigger: 'item'
    },
    legend: {
      orient: 'vertical',
      left: 'left'
    },
    toolbox: {
      show: true,
      feature: {
        // mark: { show: true },
        // dataView: { show: true, readOnly: false },
        restore: { show: true },
        saveAsImage: { show: true }
      }
    },    
    series: [
      {
        name: 'Total de informacion obtenida por fuente',
        type: 'pie',
        radius: '50%',
        data: [
          {value: datosX[0], name: "OSINT"},
          {value: datosX[1], name: "Redes Sociales"},
          {value: datosX[2], name: "Buscadores Web"},
          {value: datosX[3], name: "Sitios de Noticias"},
          {value: datosX[4], name: "Darkweb"}
        ],
        emphasis: {
          itemStyle: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        }
      }
    ]
  };

  if (option && typeof option === 'object') {
    myChartDona.setOption(option);
  }

  
}

// function barrasProcesamientoMensual(anios, anioAct, AnioAnt, AnioAntAnt){
//     // barras2
//     var options = {
//         series: [{
//         name: anios[0],
//         data: anioAct
//       }, {
//         name: anios[1],
//         data: AnioAnt
//       }, {
//         name: anios[2],
//         data: AnioAntAnt
//       }],
//         chart: {
//         type: 'bar',
//         height: 350
//       },
//       plotOptions: {
//         bar: {
//           horizontal: false,
//           columnWidth: '55%',
//           endingShape: 'rounded'
//         },
//       },
//       dataLabels: {
//         enabled: false
//       },
//       stroke: {
//         show: true,
//         width: 2,
//         colors: ['transparent']
//       },
//       xaxis: {
//         categories: ['Ene','Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
//       },
//       yaxis: {
//         title: {
//           text: 'Num. Analisis'
//         }
//       },
//       fill: {
//         opacity: 1
//       },
//       tooltip: {
//         y: {
//           formatter: function (val) {
//             //return "$ " + val + " thousands"
//             return val;
//           }
//         }
//       }
//       };

//       var chart = new ApexCharts(document.querySelector("#barras2"), options);
//       chart.render();
// }

function barrasProcesamientoMensual(anios, anioAct, AnioAnt, AnioAntAnt){
  var dom = document.getElementById("barras2");
  var myChartBarra = echarts.init(dom);


  window.addEventListener('resize',function(){
    myChartBarra.resize();
  })

  var app = {};
  
  var option;
  
  lbAnioAct = anios[0]
  lbAnioAnt = anios[1]
  anio3 = anios[2]
  // alert(anio1)
  option = {
    legend: {},
    tooltip: {},
    dataset: {
      dimensions: ['product', lbAnioAct, lbAnioAnt, anio3],
      source: [
        { product: 'Ene', [lbAnioAct]: anioAct[0], [lbAnioAnt]:  AnioAnt[0], [anio3]:    AnioAntAnt[0] },
        { product: 'Feb', [lbAnioAct]: anioAct[1], [lbAnioAnt]:  AnioAnt[1], [anio3]:    AnioAntAnt[1] },
        { product: 'Mar', [lbAnioAct]: anioAct[2], [lbAnioAnt]:  AnioAnt[2], [anio3]:    AnioAntAnt[2] },
        { product: 'Abr', [lbAnioAct]: anioAct[3], [lbAnioAnt]:  AnioAnt[3], [anio3]:    AnioAntAnt[3] },
        { product: 'May', [lbAnioAct]: anioAct[4], [lbAnioAnt]:  AnioAnt[4], [anio3]:    AnioAntAnt[4] },
        { product: 'Jun', [lbAnioAct]: anioAct[5], [lbAnioAnt]:  AnioAnt[5], [anio3]:    AnioAntAnt[5] },
        { product: 'Jul', [lbAnioAct]: anioAct[6], [lbAnioAnt]:  AnioAnt[6], [anio3]:    AnioAntAnt[6] },
        { product: 'Ago', [lbAnioAct]: anioAct[7], [lbAnioAnt]:  AnioAnt[7], [anio3]:    AnioAntAnt[7] },
        { product: 'Sep', [lbAnioAct]: anioAct[8], [lbAnioAnt]:  AnioAnt[8], [anio3]:    AnioAntAnt[8] },
        { product: 'Oct', [lbAnioAct]: anioAct[9], [lbAnioAnt]:  AnioAnt[9], [anio3]:    AnioAntAnt[9] },
        { product: 'Nov', [lbAnioAct]: anioAct[10], [lbAnioAnt]: AnioAnt[10],  [anio3]:  AnioAntAnt[10] },
        { product: 'Dic', [lbAnioAct]: anioAct[11], [lbAnioAnt]: AnioAnt[11],  [anio3]:  AnioAntAnt[11] }
      ]
    },
    xAxis: { type: 'category' },
    yAxis: {},
    // Declare several bar series, each will be mapped
    // to a column of dataset.source by default.
    series: [{ type: 'bar' }, { type: 'bar' }, { type: 'bar' }]
  };

  console.log(option)
  
  if (option && typeof option === 'object') {
    myChartBarra.setOption(option);
  }
}

function graficaDona(datos) {
    // pie

    data = {
        datasets: [{
            // data: [10, 20, 30, 7],
            data: datos,
            backgroundColor: [
                'rgba(0, 217, 109, 1)',
                'rgba(255, 255, 235, 1)',
                'rgba(255, 147, 38, 1)',
                'rgba(255, 0,0, 1)',
            ],
            borderColor: [
                'rgba(0, 0, 0, 1)',
                'rgba(0, 0, 0, 1)',
                'rgba(0, 0, 0, 1)',
                'rgba(0, 0, 0, 1)'
            ],
            borderWidth: 1
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            '0-1 Verde',
            '1-4 Amarillo',
            '4-7 Naranja',
            '7-9 Rojo'
        ]
    };

    var ctx = document.getElementById('myChart3');
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        // options: options
    });
}

function graficaRadar(riesgos, datos, colors) {
    // radar ...
    options = {
        scale: {
            angleLines: {
                display: true
            },
            // ticks: {
            //     suggestedMin: 50,
            //     suggestedMax: 100
            // }
        },
        legend: {
            display: false,
            labels: {
                fontColor: 'rgb(255, 99, 132)'
            }
        }
    };

    data = {
        // labels: ['Matar', 'Violar', 'Secuestrar', 'Atropellar'],
        labels: riesgos,
        datasets: [{
            // data: [200, 100, 40, 150],
            data: datos,
            backgroundColor: colors,
            // backgroundColor: [
            //     'rgba(255, 99, 132, 0.2)',
            //     'rgba(54, 162, 235, 0.2)',
            //     'rgba(153, 102, 255, 0.2)',
            //     'rgba(255, 159, 64, 0.2)'
            // ],
            // borderColor: [
            //     'rgba(255, 99, 132, 1)'
            // ],
            borderWidth: 1
        }]
    }

    var ctx = document.getElementById('myChart2');
    var myRadarChart = new Chart(ctx, {
        type: 'radar',
        data: data,
        options: options
    });
}


function graficaBarras(datos) {
    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            datasets: [{
                //data: [120, 109, 30, 50, 20, 30, 80, 210, 321, 1211, 121, 101],
                data: datos,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false,
                labels: {
                    fontColor: 'rgb(255, 99, 132)'
                }
            }
        }
    });
}

function loadOdometro(promedio) {

    $("#demo").myfunc({
        /**Max value of the meter*/
        val: promedio,
        /**Max value of the meter*/
        maxVal: 10,
        /**Division value of the meter*/
        divFact: 1,
        /**more than this leval, color will be red*/
        // dangerLevel: 8,
        /**reading begins angle*/
        /**more than this leval, color will be red*/
        initLevel: 0,
        /**more than this leval, color will be red*/
        infoLevel: 1,
        /**more than this leval, color will be red*/
        warningLevel: 4,
        /**more than this leval, color will be red*/
        dangerLevel: 7,
        initDeg: -45,
        /**indicator number width*/
        numbW: 40,
        /**indicator number height*/
        numbH: 18,
        /**Label on guage Face*/
        gagueLabel: 'Riesgo'
    });
    $("#demo").hide();
}


function number_format(number, decimals, decPoint, thousandsSep) { // eslint-disable-line camelcase
    //  discuss at: https://locutus.io/php/number_format/
    // original by: Jonas Raoni Soares Silva (https://www.jsfromhell.com)
    // improved by: Kevin van Zonneveld (https://kvz.io)
    // improved by: davook
    // improved by: Brett Zamir (https://brett-zamir.me)
    // improved by: Brett Zamir (https://brett-zamir.me)
    // improved by: Theriault (https://github.com/Theriault)
    // improved by: Kevin van Zonneveld (https://kvz.io)
    // bugfixed by: Michael White (https://getsprink.com)
    // bugfixed by: Benjamin Lupton
    // bugfixed by: Allan Jensen (https://www.winternet.no)
    // bugfixed by: Howard Yeend
    // bugfixed by: Diogo Resende
    // bugfixed by: Rival
    // bugfixed by: Brett Zamir (https://brett-zamir.me)
    //  revised by: Jonas Raoni Soares Silva (https://www.jsfromhell.com)
    //  revised by: Luke Smith (https://lucassmith.name)
    //    input by: Kheang Hok Chin (https://www.distantia.ca/)
    //    input by: Jay Klehr
    //    input by: Amir Habibi (https://www.residence-mixte.com/)
    //    input by: Amirouche
    //   example 1: number_format(1234.56)
    //   returns 1: '1,235'
    //   example 2: number_format(1234.56, 2, ',', ' ')
    //   returns 2: '1 234,56'
    //   example 3: number_format(1234.5678, 2, '.', '')
    //   returns 3: '1234.57'
    //   example 4: number_format(67, 2, ',', '.')
    //   returns 4: '67,00'
    //   example 5: number_format(1000)
    //   returns 5: '1,000'
    //   example 6: number_format(67.311, 2)
    //   returns 6: '67.31'
    //   example 7: number_format(1000.55, 1)
    //   returns 7: '1,000.6'
    //   example 8: number_format(67000, 5, ',', '.')
    //   returns 8: '67.000,00000'
    //   example 9: number_format(0.9, 0)
    //   returns 9: '1'
    //  example 10: number_format('1.20', 2)
    //  returns 10: '1.20'
    //  example 11: number_format('1.20', 4)
    //  returns 11: '1.2000'
    //  example 12: number_format('1.2000', 3)
    //  returns 12: '1.200'
    //  example 13: number_format('1 000,50', 2, '.', ' ')
    //  returns 13: '100 050.00'
    //  example 14: number_format(1e-8, 8, '.', '')
    //  returns 14: '0.00000001'

    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''

    var toFixedFix = function(n, prec) {
        if (('' + n).indexOf('e') === -1) {
            return +(Math.round(n + 'e+' + prec) + 'e-' + prec)
        } else {
            var arr = ('' + n).split('e')
            var sig = ''
            if (+arr[1] + prec > 0) {
                sig = '+'
            }
            return (+(Math.round(+arr[0] + 'e' + sig + (+arr[1] + prec)) + 'e-' + prec)).toFixed(prec)
        }
    }

    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec).toString() : '' + Math.round(n)).split('.')
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }

    return s.join(dec)
}


function graficaSparkCircleMini (promedios) {
    
    
      var options3 = {
        // series: [43, 32, 12, 9],
        series: promedios,
        labels: ["Crítico", "Malo", "Aceptable", "No Riesgo"],
        chart: {
          type: 'pie',
          width: 40,
          height: 40,
          sparkline: {
            enabled: true
          }
        },
      // stroke: {
      //   width: 1
      // },
      stroke: {
        width: 1,
        
      },
      colors:['#f00','#ff0','#faa','#0f0'],
      fill: {
        // opacity: 1
        colors: ['#f00','#ff0','#faa','#0f0']
      },
      tooltip: {
        fillSeriesColor: false,
        style: {
          fontSize: '13px',
          fontFamily: 'Roboto',
        },
        x: {
          show: true,
          // format: 'dd MMM',
          style: {
            background: '#6BAC1B',
          },
        },
      },
      };

      var chart3 = new ApexCharts(document.querySelector("#chart-3"), options3);
      chart3.render();
    
}