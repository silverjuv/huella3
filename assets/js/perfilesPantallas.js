(function($) {
	$(document).ready(init);
	
	function init() {
        $("#btnAdd").on("click", addPantalla);
        $("#btnDel").on("click", confirmarDelPantalla);
        initializeCombo();
        initializeTables();
    }

    function initializeCombo() {
        let burl = $("#burl").html() + "CatUsuariosController/getPerfilesForCombo";
        $.post(burl, {}, function (result) {
            if (result != undefined) {
                $("#id_perfil").select2({
                    data: result,
                    placeholder: 'Selecciona un perfil',
                    allowClear: true,
                    with: 'resolve'
                }).on('change', function (e) {
					$("#tpasignadas").DataTable().ajax.reload();
				});
                $("#id_perfil").val(null).trigger("change");
            }
        }, 'json');
    }

    function initializeTables() {
        var burl = $("#burl").html()+"PerfilesPantallasController/listarPantallas";
		
		$("#tcatalogo").DataTable({
            "scrollY": '42vh',
			"ajax": burl,
            "dataSrc": 'data',
            "serverSide": false,
			"columns": [
				{ "data": "id_pantalla" },
	            { "data": "descripcion" }
	        ],
	        "responsive": true,
	        "info": false,
	        lengthChange:false,
	        "language": {
	            
	        },
	        "dom":'<f<t>p>',
	        "select": {
	        	"style":'single'
	        }
	        
		});

		burl = $("#burl").html()+"PerfilesPantallasController/listarPantallasDePerfil";
		$("#tpasignadas").DataTable({
            "scrollY": '42vh',
			"processing": true,
	        "serverSide": true,
			"ajax": {
				"url":burl,
				"type":"POST",
				"data": function (d){
					var s = $("#id_perfil").val();
					s = (s === null || s===undefined)?"0":s;
					d.id_perfil = s;
				}
			},
			"dataSrc": 'data',
			"columns": [
				{ "data": "id_pantalla" },
	            { "data": "descripcion" }
	        ],
	        "responsive": true,
	        "info": false,
	        "lengthChange":false,
	        "language": {
	            
	        },
	        "dom":'<f<t>p>',
	        "select": {
	        	"style":'single'
	        }
	        
		});
    }

    function addPantalla() {
        var burl = $("#burl").html()+"PerfilesPantallasController/insertar";
		var table = $("#tcatalogo").DataTable();
		var row = table.row( { selected: true } ).data();
		var id_perfil = $("#id_perfil").val();

		if(id_perfil === null){
			PNotifyAlerta("Atención","Debe Seleccionar un Perfil del listado...","error");
			return;
        }
        if(row === undefined){
			PNotifyAlerta("Atención","Debe Seleccionar un registro del catálogo de pantallas...","error");
			return;
		}

		$.post(burl,{id_pantalla:row.id_pantalla,id_perfil:id_perfil},function(result){
			if(result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);
                if(result.error=="0") {
                    $("#tpasignadas").DataTable().ajax.reload(null, false);
                }
            }
		},'json');

    }

    function confirmarDelPantalla() {
        var table = $("#tpasignadas").DataTable();
        var row = table.row( { selected: true } ).data();

        if(row === undefined){
            PNotifyAlerta("Atención","Debe Seleccionar un registro de la tabla de pantallas asignadas...","error");
            return;
        }

        const notice = PNotifyConfirm("Confirme su acción", "¿Desea quitar la pantalla: "+row.descripcion+"?", "Si, proceder");
            
        notice.on('pnotify.confirm', (v) => {
            let obj = {};
            obj.id_perfil = $("#id_perfil").val();
            obj.id_pantalla = row.id_pantalla;

            borrar(obj);
        });
        notice.on('pnotify.cancel', () => {
            console.log("rejected")
        });
    }

    function borrar(obj) {
        let burl = $("#burl").html()+"PerfilesPantallasController/quitarPantallaDePerfil";
        $.post(burl, {id_perfil: obj.id_perfil, "id_pantalla": obj.id_pantalla }, function (result) {
            if (result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);
                if(result.error=="0") {
                    $("#tpasignadas").DataTable().ajax.reload(null, false);
                }
            }
        }, 'json');
    }
    
})(jQuery);