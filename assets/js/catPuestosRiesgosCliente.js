(function($) {
    var tcr;
    var tpa;
    var ts;

	$(document).ready(init);
	
	function init() {
        $("#_agregar").on("click", mostrarAgregar);
        $("#_editar").on("click", mostrarEditar);

        $("#btnPoner").on("click", addRiesgo);
        $("#btnQuitar").on("click", confirmarDelRiesgo);
        // $("#btnDetalles").on("click", verSubriesgos);
        initializeCombo();
        initializeTables();
        $.fn.button = function(action) {
            if (action === 'loading' && this.data('loading-text')) {
              this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
              this.html(this.data('original-text')).prop('disabled', false);
            }
        };
        $('#__modal').on('shown.bs.modal', function (e) {
    		ts.columns.adjust().draw();
		});	

        $('#formPuesto').bootstrapValidator({
            excluded: ':disabled',
           live:true,
           framework: 'bootstrap',
           feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
           },
           fields: {
               descripcion: {
                   validators: {
                       notEmpty: {
                           message: 'Introduzca un nombre'
                       },
                       stringLength: {
                           max:100,
                           message: 'El tamaño permitido 100 caracteres'
                       }
                   }
               }
           }
       });
       
       $('#formPuesto').bootstrapValidator().on('success.form.bv', function(e) {
           // previene el formulario antes de enviarse
           e.preventDefault();	
           guardar();    
        });
    }


    function mostrarAgregar(){
        var id_cliente = $("#id_cliente").val();
        if(id_cliente === null){
			PNotifyAlerta("Atención","Debe Seleccionar un cliente del listado...","error");
			return;
        } 
        $('#formPuesto').form('clear');
        $('#formPuesto').bootstrapValidator('resetForm', true);
        $("#action").val("1");
        abreVentanaModal("__modal", "Agregar Puesto");
    }

    function mostrarEditar(){
        // let r = tp.row({selected: true}).data();
        // var id_cliente = $("#id_cliente").val();
        // if(id_cliente == null){
		// 	PNotifyAlerta("Atención","Debe Seleccionar un cliente del listado...","error");
		// 	return;
        // }   
        if($("#id_puesto").val() == null) {
            PNotifyAlerta("Atención", "Debe seleccionar un puesto del listado", "notice");
            return;
        } 
        $('#formPuesto').form('clear');
        $('#formPuesto').bootstrapValidator('resetForm', true);
        $("#action").val("0");
        let r = [];
        r['id_puesto']=$("#id_puesto").val();
        r['descripcion']=$('#id_puesto').select2('data')[0].text;
        $("#formPuesto").form('load',r);
        abreVentanaModal("__modal", "Editar Puesto");  
    }

    function guardar(){
        // var id_cliente = $("#id_cliente").val();
		// if(id_cliente === null){
		// 	PNotifyAlerta("Atención","Debe Seleccionar un cliente del listado...","error");
		// 	return;
        // }
        var burl = $("#burl").html()+"CatPuestosRiesgosClienteController/guardar";
	    $("#formPuesto").form('submit',{
		    ajax:true,
		    iframe:false,
		    url:burl,
		    onProgress:function(e){
			    $("#btnGuardar").button('loading');
            },
            onSubmit:function(param){
                // param.id_cliente = id_cliente;
            },
		    success:function(data){
			    if(data !== undefined && data !== null){
                    var datos=JSON.parse(data);
                    $("#btnGuardar").button('reset');
				    if(datos.error==0){
					    // $("#tPuestos").DataTable().ajax.reload();
                        initializeCombo();
                        // $("#trasignados").DataTable().ajax.reload();
                        $("#trasignados").DataTable().ajax.reload(null, false);
					    $("#__modal").modal("hide");
					    PNotifyAlerta("Atencion", datos.msg, "success");
				    }else if(datos.error==1){
                        PNotifyAlerta("Atencion", "No se realizo la acción", "error");
				    }
			    }
		    }
	    });
     }

    function initializeCombo() {
        $('#id_puesto').html('').select2();

                    let burl = $("#burl").html() + "CatPuestosRiesgosClienteController/listarPuestos";
                    $.post(burl, {}, function (resultl) {
                        if (resultl != undefined) {
                            $("#id_puesto").select2({
                                debug:true,
                                data: resultl,
                                placeholder: 'Selecciona un puesto',
                                allowClear: true,
                                with: 'resolve'
                            }).on('select2:select', function (e) {
					            $("#trasignados").DataTable().ajax.reload();
				            });

                            $("#id_puesto").val(null).trigger("change");
                            if ($("#idc").val()!=""){
                                $("#id_puesto").val($("#idp").val()).trigger("change");
                                $('#id_puesto').trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: {
                                            "id": $("#idp").val() }
                                    }
                                });
                            }
                        }
                    }, 'json');
				
    }

    function initializeTables() {
        var burl = $("#burl").html()+"CatPuestosRiesgosClienteController/listarRiesgos";
		var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";
		tcr = $("#tcriesgos").DataTable({
            "scrollY": "300px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST"
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control', "visible": false },
                { "data": "id_riesgo", "visible": false },
                { "data": "riesgo", "orderable": false },
                { "data": "subriesgos", "orderable": false }
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan,
                // "select": {
                //     "rows": {
                //         "_": "%d Filas seleccionadas",
                //         "1": "1 Fila seleccionada"
                //     }
                // }
            },
            "select": {
                'style': 'single',
                'selector': 'td:not(.control)'
            },
            dom: "<'row'<'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-7'p>>",
            "rowCallback": function(row, data, index) {
                $('td', row).attr('class', 'table-active');
            }
        });
        
        var burl = $("#burl").html()+"CatPuestosRiesgosClienteController/listarRiesgosSubriesgos";
        ts = $("#tSubriesgos").DataTable({
            "scrollY": "300px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function ( d ) {
                    let tCatalago = tcr.row({ selected: true }).data();
                    let tAsignados = tpa.row({ selected: true }).data();
                    if(tCatalago != null) {
                       d.id_riesgo = tCatalago.id_riesgo;
                    }else if(tAsignados != null){
                        d.id_riesgo = tAsignados.id_riesgo;
                    }else{
                        d.id_riesgo = 0 ;
                    }
                }
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control' },
                { "data": "id_riesgo", "visible": false },
                { "data": "riesgo", "orderable": false }
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan,
                // "select": {
                //     "rows": {
                //         "_": "%d Filas seleccionadas",
                //         "1": "1 Fila seleccionada"
                //     }
                // }
            },
            "select": {
                'style': 'single',
                'selector': 'td:not(.control)'
            },
            dom: "<'row'<'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-7'p>>",
            "rowCallback": function(row, data, index) {
                $('td', row).attr('class', 'table-active');
            }
		});

		burl = $("#burl").html()+"CatPuestosRiesgosClienteController/listarPuestosRiesgos";
		tpa = $("#trasignados").DataTable({
            "scrollY": "300px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function (d){
					var s = $("#id_puesto").val();
                    var c = $("#id_cliente").val();
                    s = (s === null || s===undefined)?"0":s;
                    c = (c === null || c===undefined)?"0":c;
                    d.id_puesto = s;
                    d.id_cliente = c;
				}
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control', "visible": false },
                { "data": "id_riesgo", "visible": false },
                { "data": "riesgo", "orderable": false },
                { "data": "subriesgos", "orderable": false }
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan,
                "select": {
                    "rows": {
                        "_": "%d Filas seleccionadas",
                        "1": "1 Fila seleccionada"
                    }
                }
            },
            "select": {
                'style': 'single',
                'selector': 'td:not(.control)'
            },
            dom: "<'row'<'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-7'p>>",
            "rowCallback": function(row, data, index) {
                $('td', row).attr('class', 'table-active');
            }
		});
    }

    function addRiesgo() {
        var burl = $("#burl").html()+"CatPuestosRiesgosClienteController/insertar";
		var table = $("#tcriesgos").DataTable();
		var row = table.row( { selected: true } ).data();
        var id_puesto = $("#id_puesto").val();
        var id_cliente = $("#id_cliente").val();
        if(id_cliente === null){
			PNotifyAlerta("Atención","Debe Seleccionar un cliente del listado...","error");
			return;
        }
		if(id_puesto === null){
			PNotifyAlerta("Atención","Debe Seleccionar un Puesto del listado...","error");
			return;
        }
        if(row === undefined){
			PNotifyAlerta("Atención","Debe Seleccionar un registro del catálogo de riesgos...","error");
			return;
        }
        $("#btnPoner").button('loading');
		$.post(burl,{"id_riesgo":row.id_riesgo,"id_puesto":id_puesto,"id_cliente":id_cliente},function(result){
			if(result != undefined) {
                $("#btnPoner").button('reset');
                PNotifyAlerta(result.title, result.msg, result.type);
                if(result.error=="0") {
                    $("#trasignados").DataTable().ajax.reload(null, false);
                }
            }
		},'json');

    }

    function confirmarDelRiesgo() {
        var table = $("#trasignados").DataTable();
        var row = table.row( { selected: true } ).data();
        var id_cliente = $("#id_cliente").val();
        if(id_cliente === null){
			PNotifyAlerta("Atención","Debe Seleccionar un cliente del listado...","error");
			return;
        }
        if(row === undefined){
            PNotifyAlerta("Atención","Debe Seleccionar un registro de la tabla de riesgos asignados...","error");
            return;
        }
        const notice = PNotifyConfirm("Confirme su acción", "¿Desea quitar el riesgo: "+row.riesgo+"?", "Si, proceder");
        notice.on('pnotify.confirm', (v) => {
            var id_puesto = $("#id_puesto").val();
            var id_riesgo = row.id_riesgo;
            $("#btnQuitar").button('loading');
            let burl = $("#burl").html()+"CatPuestosRiesgosClienteController/quitarRiesgoPuesto";
            $.post(burl, {"id_puesto": id_puesto, "id_riesgo": id_riesgo,"id_cliente":id_cliente }, function (result) {
                if (result != undefined) {
                    $("#btnQuitar").button('reset');
                    PNotifyAlerta(result.title, result.msg, result.type);
                    if(result.error=="0") {
                        $("#trasignados").DataTable().ajax.reload(null, false);
                    }
                }
            }, 'json');
        });
        notice.on('pnotify.cancel', () => {
            console.log("rejected")
        });
    } 

    // function verSubriesgos(){
    //     let tCatalago = tcr.row({ selected: true }).data();
    //     let tAsignados = tpa.row({ selected: true }).data();
    //     if(tCatalago != null && tAsignados != null) {
    //         PNotifyAlerta("Atención","Seleccione un Riesgo de solo uno de los listados","error");
	// 		return;
    //     }else if(tCatalago == null && tAsignados == null){
    //         PNotifyAlerta("Atención","Seleccione un Riesgo de alguno de los listados","error");
	// 		return;
    //     }    
    //     abreVentanaModal("__modal", "Subriesgos asociados");
    // }
    
})(jQuery);