(function($) {
    $(document).ready(init);

    function init() {

        // loadOdometro();

        setTimeout(cargaDatosEstadisticos, 0);
        // $("#a_contrato").trigger("click");
    }

})(jQuery);

var colorPalette = ['#00D8B6','#008FFB',  '#FEB019', '#FF4560', '#775DD0']

function cargaDatosEstadisticos() {
    var burl = $("#burl").html() + "HomeController/cargaNumerosAdministrativos";
    $.post(burl, {}, function(result) {
        // pagina = page;
        if (result != undefined) {
            // $("#p_tPersonas").animateNumber({ number: result.totalPersonas });
            // $("#p_tPuestos").animateNumber({ number: result.totalPuestos });
            // $("#p_tExpedientes").animateNumber({ number: result.totalExpedientes });
            $("#p_tAnalisis").animateNumber({ number: result.totalAnalisis });
            // loadOdometro(result.totalPromedio);
            // setTimeout(function() { $("#p_tConsumos").html(number_format($("#p_tConsumos").html())) }, 1000)
            setTimeout(function() { $("#p_tNoticias").html(number_format($("#p_tNoticias").html())) }, 1000)
            setTimeout(function() { $("#p_tDarkweb").html(number_format($("#p_tDarkweb").html())) }, 1000)
            // // graficaBarras(result.totalMeses);
            // // graficaRadar(result.riesgos, result.totalRiesgos, result.colorsBack);
            // $("#p_tAnalisis").animateNumber({ number: result.totalAnalisis });
            setTimeout(function() { $("#p_tAnalisis").html(number_format($("#p_tAnalisis").html())) }, 1000)

            // $("#p_tContratos").animateNumber({ number: result.totalContratos });
            $("#p_tUsuarios").animateNumber({ number: result.totalUsuarios });
            // $("#p_tClientes").animateNumber({ number: result.totalClientes });
            $("#p_tNoticias").animateNumber({ number: result.totalNoticias });
            $("#p_tDarkweb").animateNumber({ number: result.totalDarkPages });

            // $("#p_tRiesgosRegistrados").animateNumber({ number: result.totalRiesgosRegistrados });
            // graficaDona(result.rangosColors);

            graficaIniciosSesion(result);

            riesgos = [];
            // colorss = [];
            sec = 0;
            for (sec = 0; sec < result.riesgos.riesgos.length; sec++){
              riesgos[sec]= {text: result.riesgos.riesgos[sec], weight: result.riesgos.totales[sec], html: {"data-tooltip": result.riesgos.totales[sec]}};              
              // colors[sec]=
            }
            // result.riesgos.riesgos.forEach(element => {
            //   console.log(element);
            //   riesgos[sec++]= {text: element.riesgo, weight: result.riesgos.totales[sec-1], html: {"data-tooltip": result.riesgos.totales[sec-1]}};
            // });

            // graficaWordsRiesgos(riesgos, result.riesgos.colores, result.riesgos.sizes);
            cargaWords(riesgos);

            graficaDona2(result.totalPorTool);

            myChartProcesamientos(result.anios, result.nofinded, result.aptos, result.noaptos);

            mysTiempos();
        }

    }, 'json');

}

function generateDayWiseTimeSeries(baseval, count, yrange) {
    var i = 0;
    var series = [];
    while (i < count) {
      var x = baseval;
      var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;
  
      series.push([x, y]);
      baseval += 86400000;
      i++;
    }
    return series;
  }


// // function mysTiempos(){


// //     var data = generateDayWiseTimeSeries(new Date('11 Feb 2017').getTime(), 185, {
// //         min: 30,
// //         max: 90
// //       })
      
// //     var options = {
// //         series: [{
// //         name: "# de Segundos",
// //         data: data
// //       }],
// //         chart: {
// //         id: 'chart2',
// //         type: 'line',
// //         height: 230,
// //         toolbar: {
// //           autoSelected: 'pan',
// //           show: false
// //         }
// //       },
// //       title: {
// //         text: 'Tiempo promedio (segs) de analisis realizados por Dia'
// //       },      
// //       colors: ['#546E7A'],
// //       stroke: {
// //         width: 3
// //       },
// //       dataLabels: {
// //         enabled: false
// //       },
// //       fill: {
// //         opacity: 1,
// //       },
// //       markers: {
// //         size: 0
// //       },
// //       xaxis: {
// //         type: 'datetime'
// //       }
// //       };

// //       var chart = new ApexCharts(document.querySelector("#chart-line2"), options);
// //       chart.render();
    
// //       var optionsLine = {
// //         series: [{
// //         data: data
// //       }],
// //         chart: {
// //         id: 'chart1',
// //         height: 130,
// //         type: 'area',
// //         brush:{
// //           target: 'chart2',
// //           enabled: true
// //         },
// //         selection: {
// //           enabled: true,
// //           xaxis: {
// //             min: new Date('19 Jun 2017').getTime(),
// //             max: new Date('14 Aug 2017').getTime()
// //           }
// //         },
// //       },
// //       colors: ['#008FFB'],
// //       fill: {
// //         type: 'gradient',
// //         gradient: {
// //           opacityFrom: 0.91,
// //           opacityTo: 0.1,
// //         }
// //       },
// //       xaxis: {
// //         type: 'datetime',
// //         tooltip: {
// //           enabled: false
// //         }
// //       },
// //       yaxis: {
// //         tickAmount: 2
// //       }
// //       };

// //       var chartLine = new ApexCharts(document.querySelector("#chart-line"), optionsLine);
// //       chartLine.render();
// // }

function myChartProcesamientos(anios, noencontrados, aptos, noaptos){

  var dom = document.getElementById("myChartProcesamientos");
  var myChart = echarts.init(dom);

  window.addEventListener('resize',function(){
    myChart.resize();
  })

  var app = {};

  var option;



  option = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        // Use axis to trigger tooltip
        type: 'shadow' // 'shadow' as default; can also be 'line' or 'shadow'
      }
    },
    toolbox: {
      show: true,
      feature: {
        mark: { show: true },
        // dataView: { show: true, readOnly: false },
        restore: { show: true },
        saveAsImage: { show: true }
      }
    },    
    legend: {},
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: {
      type: 'value'
    },
    yAxis: {
      type: 'category',
      data: anios
    },
    series: [
      {
        name: 'No se encontró información',
        type: 'bar',
        stack: 'total',
        label: {
          show: true
        },
        emphasis: {
          focus: 'series'
        },
        data: noencontrados
      },
      {
        name: 'Persona Apta para Puesto',
        type: 'bar',
        stack: 'total',
        label: {
          show: true
        },
        emphasis: {
          focus: 'series'
        },
        data: aptos
      },
      {
        name: 'Persona NO Apta para Puesto',
        type: 'bar',
        stack: 'total',
        label: {
          show: true
        },
        emphasis: {
          focus: 'series'
        },
        data: noaptos
      }
    ]
  };

  if (option && typeof option === 'object') {
      myChart.setOption(option);
  }

    // var options = {
    //     series: [{
    //     name: 'No se encontró información',
    //     data: noencontrados
    //   }, {
    //     name: 'Persona Apta para Puesto',
    //     data: aptos
    //   }, {
    //     name: 'Persona NO Apta para Puesto',
    //     data: noaptos
    //   }],
    //     chart: {
    //     type: 'bar',
    //     height: 350,
    //     stacked: true,
    //     stackType: '100%'
    //   },
    //   plotOptions: {
    //     bar: {
    //       horizontal: true,
    //     },
    //   },
    //   stroke: {
    //     width: 1,
    //     colors: ['#fff']
    //   },
    //   title: {
    //     // text: 'Tipos de Resultados finales generados al ser revisados y aprobados'
    //     text: 'Tipos de Resultados finales generados los ultimos 2 años'
    //   },
    //   xaxis: {
    //     categories: anios,
    //   },
    //   tooltip: {
    //     y: {
    //       formatter: function (val) {
    //         return val  
    //       }
    //     }
    //   },
    //   fill: {
    //     opacity: 1
      
    //   },
    //   legend: {
    //     position: 'top',
    //     horizontalAlign: 'left',
    //     offsetX: 40
    //   }
    //   };

    //   var chart = new ApexCharts(document.querySelector("#myChartProcesamientos"), options);
    //   chart.render();
}

function graficaDona2(datosX){
  var dom = document.getElementById("miDona2");
  var myChartDona = echarts.init(dom);

  window.addEventListener('resize',function(){
    myChartDona.resize();
  })

  var app = {};

  var option;


  option = {
    title: {
      text: 'Información recopilada por fuente',
      subtext: '',
      left: 'center'
    },
    tooltip: {
      trigger: 'item'
    },
    legend: {
      orient: 'vertical',
      left: 'left'
    },
    toolbox: {
      show: true,
      feature: {
        // mark: { show: true },
        // dataView: { show: true, readOnly: false },
        restore: { show: true },
        saveAsImage: { show: true }
      }
    },    
    series: [
      {
        name: 'Total de informacion obtenida por fuente',
        type: 'pie',
        radius: '50%',
        data: [
          {value: datosX[0], name: "OSINT"},
          {value: datosX[1], name: "Redes Sociales"},
          {value: datosX[2], name: "Buscadores Web"},
          {value: datosX[3], name: "Sitios de Noticias"},
          {value: datosX[4], name: "Darkweb"}
        ],
        emphasis: {
          itemStyle: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        }
      }
    ]
  };

  if (option && typeof option === 'object') {
    myChartDona.setOption(option);
  }
    // var options = {
    //     series: datosX,
    //     chart: {
    //     height: 390,
    //     type: 'radialBar',
    //   },
    //   plotOptions: {
    //     radialBar: {
    //       offsetY: 0,
    //       startAngle: 0,
    //       endAngle: 270,
    //       hollow: {
    //         margin: 5,
    //         size: '30%',
    //         background: 'transparent',
    //         image: undefined,
    //       },
    //       dataLabels: {
    //         name: {
    //           show: false,
    //         },
    //         value: {
    //           show: false,
    //         }
    //       }
    //     }
    //   },
    //   colors: ['#1ab7ea', '#aa84ff', '#39539E', '#0077B5', '#0f7700'],
    //   labels: ['OSINT', 'Redes Sociales', 'Buscadores Web', 'Sitios de Noticias', 'Darkweb'],
    //   legend: {
    //     show: true,
    //     floating: true,
    //     fontSize: '16px',
    //     position: 'left',
    //     offsetX: 16,
    //     offsetY: 15,
    //     labels: {
    //       useSeriesColors: true,
    //     },
    //     markers: {
    //       size: 0
    //     },
    //     formatter: function(seriesName, opts) {
    //       return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex]
    //     },
    //     itemMargin: {
    //       vertical: 3
    //     }
    //   },
    //   responsive: [{
    //     breakpoint: 480,
    //     options: {
    //       legend: {
    //           show: false
    //       }
    //     }
    //   }]
    //   };

    //   var chart = new ApexCharts(document.querySelector("#miDona2"), options);
    //   chart.render();
    
}

function graficaWordsRiesgos(basic_words, colores, sizes){
    // console.log(basic_words);
    // console.log(colores);
    // var basic_words = [
    //     {text: "Lorem", weight: 13, html: {"data-tooltip": "13"}},
    //     {text: "Ipsum", weight: 10.5, html: {"data-tooltip": "13"}},
    //     {text: "Dolor", weight: 9.4, html: {"data-tooltip": "13"}},
    //     {text: "Sit", weight: 8, html: {"data-tooltip": "13"}},
    //     {text: "Amet", weight: 6.2, html: {"data-tooltip": "13"}},
    //     {text: "Consectetur", weight: 5, html: {"data-tooltip": "13"}},
    //     {text: "Adipiscing", weight: 5, html: {"data-tooltip": "13"}},
    //     {text: "Elit", weight: 5, html: {"data-tooltip": "13"}},
    //     {text: "Nam et", weight: 5, html: {"data-tooltip": "13"}},
    //     {text: "Leo", weight: 4, html: {"data-tooltip": "13"}},
    //     {text: "Sapien", weight: 4},
    //     {text: "Pellentesque", weight: 3},
    //     {text: "habitant", weight: 3},
    //     {text: "morbi", weight: 3},
    //     {text: "tristisque", weight: 3},
    //     {text: "senectus", weight: 3},
    //     {text: "et netus", weight: 3},
    //     {text: "et malesuada", weight: 3},
    //     {text: "fames", weight: 2},
    //     {text: "ac turpis", weight: 2},
    //     {text: "egestas", weight: 2},
    //     {text: "Aenean", weight: 2},
    //     {text: "vestibulum", weight: 2},
    //     {text: "elit", weight: 2},
    //     {text: "sit amet", weight: 2},
    //     {text: "metus", weight: 2},
    //     {text: "adipiscing", weight: 2},
    //     {text: "ut ultrices", weight: 2},
    //     {text: "justo", weight: 1},
    //     {text: "dictum", weight: 1},
    //     {text: "Ut et leo", weight: 1},
    //     {text: "metus", weight: 1},
    //     {text: "at molestie", weight: 1},
    //     {text: "purus", weight: 1},
    //     {text: "Curabitur", weight: 1},
    //     {text: "diam", weight: 1},
    //     {text: "dui", weight: 1},
    //     {text: "ullamcorper", weight: 1},
    //     {text: "id vuluptate ut", weight: 1},
    //     {text: "mattis", weight: 1},
    //     {text: "et nulla", weight: 1},
    //     {text: "Sed", weight: 1}
    // ];
      
      
    //   $('#graphWords').jQCloud(basic_words);
    //   alert("ok")
    $('#graphWords').jQCloud(basic_words, {
      //   // autoResize: true
      //   classPattern: null,
        colors: colores,
        fontSize: sizes,
        // fontSize: ['60px','17px'],
        // fontSize: {
        //   from: 0.1,
        //   to: 0.01
        // },
        shape: "elliptic",
          autoResize: true
      }
      );
}

function graficaDona(datos) {
    // pie

    data = {
        datasets: [{
            // data: [10, 20, 30, 7],
            data: datos,
            backgroundColor: [
                'rgba(0, 217, 109, 1)',
                'rgba(255, 255, 235, 1)',
                'rgba(255, 147, 38, 1)',
                'rgba(255, 0,0, 1)',
            ],
            borderColor: [
                'rgba(0, 0, 0, 1)',
                'rgba(0, 0, 0, 1)',
                'rgba(0, 0, 0, 1)',
                'rgba(0, 0, 0, 1)'
            ],
            borderWidth: 1
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            '0-1 Verde',
            '1-4 Amarillo',
            '4-7 Naranja',
            '7-9 Rojo'
        ]
    };

    var ctx = document.getElementById('myChart3');
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        // options: options
    });
}

function graficaRadar(riesgos, datos, colors) {
    // radar ...
    options = {
        scale: {
            angleLines: {
                display: true
            },
            // ticks: {
            //     suggestedMin: 50,
            //     suggestedMax: 100
            // }
        },
        legend: {
            display: false,
            labels: {
                fontColor: 'rgb(255, 99, 132)'
            }
        }
    };

    data = {
        // labels: ['Matar', 'Violar', 'Secuestrar', 'Atropellar'],
        labels: riesgos,
        datasets: [{
            // data: [200, 100, 40, 150],
            data: datos,
            backgroundColor: colors,
            // backgroundColor: [
            //     'rgba(255, 99, 132, 0.2)',
            //     'rgba(54, 162, 235, 0.2)',
            //     'rgba(153, 102, 255, 0.2)',
            //     'rgba(255, 159, 64, 0.2)'
            // ],
            // borderColor: [
            //     'rgba(255, 99, 132, 1)'
            // ],
            borderWidth: 1
        }]
    }

    var ctx = document.getElementById('myChart2');
    var myRadarChart = new Chart(ctx, {
        type: 'radar',
        data: data,
        options: options
    });
}


function graficaBarras(datos) {
    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            datasets: [{
                //data: [120, 109, 30, 50, 20, 30, 80, 210, 321, 1211, 121, 101],
                data: datos,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false,
                labels: {
                    fontColor: 'rgb(255, 99, 132)'
                }
            }
        }
    });
}



function graficaIniciosSesion(result) {

    var dom = document.getElementById('graphSessions');
      var myChart = echarts.init(dom, null, {
        renderer: 'canvas',
        useDirtyRect: false
      });
      var app = {};
      
      var option;

      option = {
        title: {
          text: 'No. Últimos Inicios de Sesión',
          subtext: '',
          left: 'center'
        },
        tooltip: {
          trigger: 'item'
        },
        toolbox: {
          show: true,
          feature: {
            // mark: { show: true },
            // dataView: { show: true, readOnly: false },
            restore: { show: true },
            saveAsImage: { show: true }
          }
        },          
    xAxis: {
      type: 'category',
      data: result.fechas
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        data: result.totalLogins,
        type: 'line',
        smooth: true
      }
    ]
  };

    if (option && typeof option === 'object') {
      myChart.setOption(option);
    }

    window.addEventListener('resize', myChart.resize);

    // var ctx = document.getElementById('myChartXYZ');
    // console.log(result.totalLogins)
    // console.log(result.fechas)
    // // var options = {
    // //     series: [{
    // //     name: "# Inicios de Sesión",
    // //     data: result.totalLogins
    // //   }],
    // //     chart: {
    // //     type: 'area',
    // //     height: 350,
    // //     zoom: {
    // //       enabled: false
    // //     }
    // //   },
    // //   dataLabels: {
    // //     enabled: false
    // //   },
    // //   stroke: {
    // //     curve: 'straight'
    // //   },
      
    // //   title: {
    // //     text: 'Inicios de Sesion realizados los ultimos dias',
    // //     align: 'left'
    // //   },
    // // //   subtitle: {
    // // //     text: 'Price Movements',
    // // //     align: 'left'
    // // //   },
    // //   labels: result.fechas,
    // //   xaxis: {
    // //     type: 'datetime',
    // //   },
    // //   yaxis: {
    // //     opposite: true
    // //   },
    // //   legend: {
    // //     horizontalAlign: 'left'
    // //   }
    // //   };

    // // var chart = new ApexCharts(document.querySelector("#myChartXYZ"), options);
    // // chart.render();
}


function loadOdometro(promedio) {

    $("#demo").myfunc({
        /**Max value of the meter*/
        val: promedio,
        /**Max value of the meter*/
        maxVal: 10,
        /**Division value of the meter*/
        divFact: 1,
        /**more than this leval, color will be red*/
        // dangerLevel: 8,
        /**reading begins angle*/
        /**more than this leval, color will be red*/
        initLevel: 0,
        /**more than this leval, color will be red*/
        infoLevel: 1,
        /**more than this leval, color will be red*/
        warningLevel: 4,
        /**more than this leval, color will be red*/
        dangerLevel: 7,
        initDeg: -45,
        /**indicator number width*/
        numbW: 40,
        /**indicator number height*/
        numbH: 18,
        /**Label on guage Face*/
        gagueLabel: 'Riesgo'
    });
    $("#demo").hide();
}


function number_format(number, decimals, decPoint, thousandsSep) { // eslint-disable-line camelcase
    //  discuss at: https://locutus.io/php/number_format/
    // original by: Jonas Raoni Soares Silva (https://www.jsfromhell.com)
    // improved by: Kevin van Zonneveld (https://kvz.io)
    // improved by: davook
    // improved by: Brett Zamir (https://brett-zamir.me)
    // improved by: Brett Zamir (https://brett-zamir.me)
    // improved by: Theriault (https://github.com/Theriault)
    // improved by: Kevin van Zonneveld (https://kvz.io)
    // bugfixed by: Michael White (https://getsprink.com)
    // bugfixed by: Benjamin Lupton
    // bugfixed by: Allan Jensen (https://www.winternet.no)
    // bugfixed by: Howard Yeend
    // bugfixed by: Diogo Resende
    // bugfixed by: Rival
    // bugfixed by: Brett Zamir (https://brett-zamir.me)
    //  revised by: Jonas Raoni Soares Silva (https://www.jsfromhell.com)
    //  revised by: Luke Smith (https://lucassmith.name)
    //    input by: Kheang Hok Chin (https://www.distantia.ca/)
    //    input by: Jay Klehr
    //    input by: Amir Habibi (https://www.residence-mixte.com/)
    //    input by: Amirouche
    //   example 1: number_format(1234.56)
    //   returns 1: '1,235'
    //   example 2: number_format(1234.56, 2, ',', ' ')
    //   returns 2: '1 234,56'
    //   example 3: number_format(1234.5678, 2, '.', '')
    //   returns 3: '1234.57'
    //   example 4: number_format(67, 2, ',', '.')
    //   returns 4: '67,00'
    //   example 5: number_format(1000)
    //   returns 5: '1,000'
    //   example 6: number_format(67.311, 2)
    //   returns 6: '67.31'
    //   example 7: number_format(1000.55, 1)
    //   returns 7: '1,000.6'
    //   example 8: number_format(67000, 5, ',', '.')
    //   returns 8: '67.000,00000'
    //   example 9: number_format(0.9, 0)
    //   returns 9: '1'
    //  example 10: number_format('1.20', 2)
    //  returns 10: '1.20'
    //  example 11: number_format('1.20', 4)
    //  returns 11: '1.2000'
    //  example 12: number_format('1.2000', 3)
    //  returns 12: '1.200'
    //  example 13: number_format('1 000,50', 2, '.', ' ')
    //  returns 13: '100 050.00'
    //  example 14: number_format(1e-8, 8, '.', '')
    //  returns 14: '0.00000001'

    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''

    var toFixedFix = function(n, prec) {
        if (('' + n).indexOf('e') === -1) {
            return +(Math.round(n + 'e+' + prec) + 'e-' + prec)
        } else {
            var arr = ('' + n).split('e')
            var sig = ''
            if (+arr[1] + prec > 0) {
                sig = '+'
            }
            return (+(Math.round(+arr[0] + 'e' + sig + (+arr[1] + prec)) + 'e-' + prec)).toFixed(prec)
        }
    }

    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec).toString() : '' + Math.round(n)).split('.')
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }

    return s.join(dec)
}

function cargaWords(datax){

  // datax = <?php echo json_encode($data); ?>;

  // console.log(datax); return;
  //convertimos tag y count de cada objeto a array name, value
  datay = [];
  indice = 0;
  datax.forEach( elment => {
      // console.log(elment.tag);
      datay.push({name: elment.text, value: elment.weight})
  })
      

  var chart = echarts.init(document.getElementById('graficaWords'));

      var option = {
          tooltip: {},
          series: [ {
              type: 'wordCloud',
              gridSize: 2,
              sizeRange: [12, 50],
              rotationRange: [-45, 45],
              shape: 'circle',
              width: '100%',
              // height: 400,
              drawOutOfBound: true,
              textStyle: {
                  color: function () {
                      return 'rgb(' + [
                          Math.round(Math.random() * 160),
                          Math.round(Math.random() * 160),
                          Math.round(Math.random() * 160)
                      ].join(',') + ')';
                  }
              },
              emphasis: {
                  textStyle: {
                      shadowBlur: 10,
                      shadowColor: '#333'
                  }
              },
              data: datay
          } ]
      };

      chart.setOption(option);

      // chart.on('click', function (param){
      //         window.open("<?php echo $url ?>Search/find/"+param.name);                   
      //     });

      window.onresize = chart.resize;
}