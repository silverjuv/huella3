(function($) {
    $(document).ready(init);

    function init() {
        $.preloader.stop();



        $("#btnBuscar").on("click", buscarTerminos);
        $("#termino").on("keyup", function(e) {
            // alert('ok');
            if (e.which == 13) {
                buscarTerminos()
            }
        });
    }

    function buscarTerminos() {
        // Valida longitud de captura no mayor ni menor a ... 4 caracteres y max 50
        if ($.trim($("#termino").val()).length >= 4 && $.trim($("#termino").val()).length <= 50) {
            let search = encodeURIComponent($.trim($("#termino").val()));
            document.location.href = $("#burl").html() + "NoticiasController/listar/1?search=" + search;
        } else {
            $('.toast-body').html("Su busqueda debe ser entre 4 y 50 caracteres...");
            $('.toast').toast('show');
        }
    }

})(jQuery);

$.preloader.start({
    modal: true
});