(function($) {

    $(document).ready(init);
    let uris = [],
        usufy = [];
    PNotify.defaults.styling = 'bootstrap4';

    function init() {
        $("#btnBuscarNick").on("click", ejecutarUsufy);
        $("#btnBuscarAproximaciones").on("click", ejecutarSearchfy);
        $("#btnBuscarNick_phonefy").on("click", ejecutarPhonefy);
        $("#btnBuscarNick_mailfy").on("click", ejecutarMailfy);


        $("#btnAbrirEnlaces").on("click", abrirEnlaces);
        $("#btnAgregar").on("click", guardarResultado);
        iniciaCombos();

        $("#snick").keypress(function(e) {
            if (e.which == 13)
                ejecutarSearchfy();
        });
        $("#nick").keypress(function(e) {
            if (e.which == 13)
                ejecutarUsufy();
        });

        $("#resultados").slimScroll({
            alwaysVisible: true,
            height: '350px'
        });

        $("#test").slimScroll({
            alwaysVisible: true,
            height: '350px'
        });


    }

    function iniciaCombos() {
        //let burl = $("#burl").html() + "OsintOsrframework/getUsufyPlatforms";
        let burl = $("#burl").html() + "ESocialesController/getUsufyPlatforms";
        $.post(burl, {}, function(result) {
            if (result != undefined) {
                $("#plataforma").select2({
                    debug: true,
                    data: result,
                    placeholder: 'Selecciona plataforma(s)',
                    allowClear: true,
                    language: "es",
                    width: 'resolve',
                    multiple: true
                });
                $("#plataforma").val(null).trigger("change");
                // $("#plataforma").select2({
                //     ajax: {
                //         method: 'POST',
                //         url: burl,
                //         dataType: 'json',
                //         data: function(params) {
                //             let query = {
                //                 search: params.term,
                //                 type: 'public'
                //             }

                //             console.log(query)
                //             return query;
                //         },
                //         processResults: function(data) {
                //             return {
                //                 results: data
                //             };
                //         }
                //     },
                //     minimumInputLength: 3,
                //     width: 'resolve',
                //     placeholder: 'Selecciona plataforma(s)',
                //     allowClear: true,
                //     multiple: true,
                //     language: "es",

                // }).on('change.select2', function(e) {

                // });
            } //result != undefined
        }, 'json');

        //burl = $("#burl").html() + "OsintOsrframework/getSearchfyPlatforms";
        burl = $("#burl").html() + "ESocialesController/getSearchfyPlatforms";
        $.post(burl, {}, function(result) {
            if (result != undefined) {

                $("#splataforma").select2({
                    debug: true,
                    data: result,
                    placeholder: 'Selecciona plataforma(s)',
                    allowClear: true,
                    language: "es",
                    width: 'resolve',
                    multiple: true
                });
                $("#splataforma").val(null).trigger("change");

            } //result != undefined
        }, 'json');

        burl = $("#burl").html() + "ESocialesController/getMailfyPlatforms";
        $.post(burl, {}, function(result) {
            if (result != undefined) {

                $("#mailfy").select2({
                    debug: true,
                    data: result,
                    placeholder: 'Selecciona plataforma(s)',
                    allowClear: true,
                    language: "es",
                    width: 'resolve',
                    multiple: true
                });
                $("#mailfy").val(null).trigger("change");

            } //result != undefined
        }, 'json');

        burl = $("#burl").html() + "ESocialesController/getPhonefyPlatforms";
        $.post(burl, {}, function(result) {
            if (result != undefined) {

                $("#phonefy").select2({
                    debug: true,
                    data: result,
                    placeholder: 'Selecciona plataforma(s)',
                    allowClear: true,
                    language: "es",
                    width: 'resolve',
                    multiple: true
                });
                $("#phonefy").val(null).trigger("change");

            } //result != undefined
        }, 'json');

    }

    function deshabilitarBusquedaUsufy() {
        $("#btnBuscarNick").off("click").prop("disabled", true);
        $("#nickUsufy").off("keypress");
    }

    function habilitarBusquedaUsufy() {
        $("#btnBuscarNick").on("click", ejecutarUsufy).prop("disabled", false);
        $("#nickUsufy").keypress(function(e) {
            if (e.which == 13)
                ejecutarUsufy();
        });
    }

    function ejecutarUsufy() {
        let burl = $("#burl").html() + "ESocialesController/ejecutarUsufy";
        let nick = $.trim($("#nickUsufy").val());
        let plataformas = $("#plataforma").select2('data');

        let p = plataformas.map(function(val) {
            return val.id;
        });

        if (nick == "") {
            PNotifyAlerta('Atención', 'Debe escribir un nombre de usuario para realizar la busqueda', 'notice');
            return;
        }

        //preloader
        deshabilitarBusquedaUsufy();
        let pre = $('<div/>').css({ 'margin-left': '8px', 'display': 'inline-block', 'vertical-align': 'middle' }).insertAfter($('#btnBuscarNick')).preloader({ src: 'sprites.32.png' });

        $.post(burl, { data: nick, plataformas: p }, function(result) {
            let r = '';
            let i = 0;
            if (result.json != null) {
                result.json = JSON.parse(result.json);
                let len = result.json.length;
                let plataforma = "";
                let gravatarUrl = "";

                if (len == 0) {
                    PNotifyAlerta('Info', 'No se encontó información... (1)', "info");
                    return;
                }

                $("#resultados").html("");
                usufy = [];
                for (i; i < len; i++) {
                    if (((i + 1) % 2 != 0)) {
                        r += '<div class="row">';
                        r += '<div class="col-sm-6">';
                    } else {
                        r += '<div class="col-sm-6">';
                    }

                    if (result.json[i].attributes[2].value.toLowerCase() == "gravatar") {
                        plataforma = "gravatar";
                        gravatarUrl = result.json[i].attributes[0].value;
                    }

                    usufy.push(result.json[i]);
                    r += creaCardMedia(result.json[i]);
                    r += '</div>';

                    if (((i + 1) % 2 == 0)) {
                        r += '</div>';
                    }
                }


                $("#resultados").html(r);
                $("button[id^='btnAgregarProyecto_'").off("click").on("click", mostrarAgregarAProyecto);

                if (plataforma == "gravatar") {
                    $.post(gravatarUrl, {}, function(result) {

                        if (result != undefined && result.entry && result.entry.length > 0) {
                            result = result.entry[0];
                            let g = {
                                "givenName": (result.name && result.name.givenName != null) ? result.name.givenName : "",
                                "familyName": (result.name && result.name.familyName != null) ? result.name.familyName : "",
                                "displayName": (result.displayName != null) ? result.displayName : "",
                                "preferredUsername": (result.preferredUsername != null) ? result.preferredUsername : "",
                                "thumbnailUrl": (result.thumbnailUrl != null) ? result.thumbnailUrl : "",
                                "aboutMe": (result.aboutMe != null) ? result.aboutMe : "",
                                "emails": (result.emails != null) ? result.emails : "",
                                "accounts": (result.accounts != null) ? result.accounts : ""
                            };

                            for (let i = 0; i < usufy.length; i++) {
                                if (usufy[i].attributes[2].value === "Gravatar") {
                                    usufy[i].attributes.push(g);
                                    break;
                                }
                            }
                            //console.log(usufy)
                            $("#gravatarAdditional").html(agregaGravatarInfo(g));
                            /*
                            $(".card").slimScroll({
                                alwaysVisible: true,
                                width: 350
                            });
                            */

                        }
                    }, "json");
                }


            } else {
                PNotifyAlerta('Info', 'No se encontó información... (2)', "info");
            }

        }, 'json').always(function() {
            pre.preloader('stop');
        }).fail(function(xhr, status, error) {
            pre.preloader('stop');
            PNotifyAlerta('Error', 'Algo salio mal con el módulo Usufy de OsrFramework', "error");
            console.log(xhr, status, error)
        }).always(function() {
            pre.preloader('stop');
            habilitarBusquedaUsufy();
        });
    }

    function deshabilitarBusquedaSearchfy() {
        $("#btnBuscarAproximaciones").off("click").prop("disabled", true);
        $("#snick").off("keypress");
    }

    function habilitarBusquedaSearchfy() {
        $("#btnBuscarAproximaciones").on("click", ejecutarSearchfy).prop("disabled", false);
        $("#snick").keypress(function(e) {
            if (e.which == 13)
                ejecutarSearchfy();
        });
    }

    function ejecutarSearchfy() {
        //let burl = $("#burl").html() + "OsintOsrframework/ejecutarSearchfy";  
        let burl = $("#burl").html() + "ESocialesController/ejecutarSearchfy";
        let snick = $.trim($("#snick").val());
        let splataformas = $("#splataforma").select2('data');

        let p = splataformas.map(function(val) {
            return val.id;
        });

        if (snick == "") {
            PNotifyAlerta('Atención', 'Debe escribir una aproximación del nombre de usuario para realizar la busqueda', 'notice');
            return;
        }

        deshabilitarBusquedaSearchfy();
        //preloader
        let pre = $('<div/>').css({ 'margin-left': '8px', 'display': 'inline-block', 'vertical-align': 'middle' }).insertAfter($('#btnBuscarAproximaciones')).preloader({ src: 'sprites.32.png' });

        $.post(burl, { data: snick, plataformas: p }, function(result) {
            //alert(result)
            if (result != undefined) {
                result.json = JSON.parse(result.json);
                let len = result.json.length;

                if (len == 0) {
                    PNotifyAlerta('Info', 'No se encontó información... (1)', "info");
                    return;
                }


                let h = '';
                uris = [];
                result.json.map(function(row) {
                    h += '<tr>';
                    h += '<td>' + row.attributes[0].value + '</td>';
                    h += '<td>' + row.attributes[1].value + '</td>';
                    h += '<td><a href="' + row.attributes[3].value + '" target="_blank">' + row.attributes[3].value + '</a></td>';
                    h += '</tr>';
                    uris.push(row.attributes[3].value);
                });
                $("#cuerpoResulSearchy").html(h);

            }
        }, 'json').always(function() {
            pre.preloader('stop');
        }).fail(function(xhr, status, error) {
            PNotifyAlerta('Error', 'Algo salio mal con el módulo Serchfy de OsrFramework', "error");
            console.log(xhr, status, error)
        }).always(function() {
            pre.preloader('stop');
            habilitarBusquedaSearchfy();
        });
    }


    function deshabilitarBusquedaMailfy() {
        $("#btnBuscarNick_mailfy").off("click").prop("disabled", true);
        $("#nickmailfy").off("keypress");
    }

    function habilitarBusquedaMailfy() {
        $("#btnBuscarNick_mailfy").on("click", ejecutarMailfy).prop("disabled", false);
        $("#nickmailfy").keypress(function(e) {
            if (e.which == 13)
                ejecutarMailfy();
        });
    }

    function ejecutarMailfy() {
        //let burl = $("#burl").html() + "OsintOsrframework/ejecutarSearchfy";  
        let burl = $("#burl").html() + "ESocialesController/ejecutarMailfy";
        let snick = $.trim($("#nickmailfy").val());
        let splataformas = $("#mailfy").select2('data');

        let p = splataformas.map(function(val) {
            return val.id;
        });

        if (snick == "") {
            PNotifyAlerta('Atención', 'Debe escribir un email para realizar la busqueda', 'notice');
            return;
        }

        deshabilitarBusquedaMailfy();
        //preloader
        let pre = $('<div/>').css({ 'margin-left': '8px', 'display': 'inline-block', 'vertical-align': 'middle' }).insertAfter($('#btnBuscarNick_mailfy')).preloader({ src: 'sprites.32.png' });

        $.post(burl, { data: snick, plataformas: p }, function(result) {
            //alert(result.json)
            if (result != undefined) {
                result.json = JSON.parse(result.json);
                let len = result.json.length;

                if (len == 0) {
                    PNotifyAlerta('Info', 'No se encontó información... (1)', "info");
                    return;
                }


                let h = '';
                uris = [];
                result.json.map(function(row) {
                    h += '<tr>';
                    h += '<td>' + row.attributes[0].value + '</td>';
                    h += '<td>' + row.attributes[1].value + '</td>';
                    h += '<td><a href="' + row.attributes[3].value + '" target="_blank">' + row.attributes[3].value + '</a></td>';
                    h += '</tr>';
                    uris.push(row.attributes[3].value);
                });
                $("#cuerpoResulMailfy").html(h);

            }
        }, 'json').always(function() {
            pre.preloader('stop');
        }).fail(function(xhr, status, error) {
            PNotifyAlerta('Error', 'Algo salio mal con el módulo Mailfy de OsrFramework', "error");
            console.log(xhr, status, error)
        }).always(function() {
            //alert('q xxx ');
            pre.preloader('stop');
            habilitarBusquedaMailfy();
        });
    }

    function deshabilitarBusquedaPhonefy() {
        $("#btnBuscarNick_phonefy").off("click").prop("disabled", true);
        $("#nickPhonefy").off("keypress");
    }

    function habilitarBusquedaPhonefy() {
        $("#btnBuscarNick_phonefy").on("click", ejecutarPhonefy).prop("disabled", false);
        $("#nickPhonefy").keypress(function(e) {
            if (e.which == 13)
                ejecutarPhonefy();
        });
    }

    function ejecutarPhonefy() {
        //let burl = $("#burl").html() + "OsintOsrframework/ejecutarSearchfy";  
        //alert("asdjf")
        let burl = $("#burl").html() + "ESocialesController/ejecutarPhonefy";
        let snick = $.trim($("#nickphonefy").val());
        let splataformas = $("#phonefy").select2('data');

        let p = splataformas.map(function(val) {
            return val.id;
        });

        if (snick == "") {
            PNotifyAlerta('Atención', 'Debe escribir un teléfono para realizar la busqueda', 'notice');
            return;
        }

        deshabilitarBusquedaPhonefy();
        //preloader
        let pre = $('<div/>').css({ 'margin-left': '8px', 'display': 'inline-block', 'vertical-align': 'middle' }).insertAfter($('#btnBuscarNick_phonefy')).preloader({ src: 'sprites.32.png' });

        $.post(burl, { data: snick, plataformas: p }, function(result) {
            //alert(result)
            if (result != undefined) {
                result.json = JSON.parse(result.json);
                let len = result.json.length;

                if (len == 0) {
                    PNotifyAlerta('Info', 'No se encontó información... (1)', "info");
                    return;
                }


                let h = '';
                uris = [];
                result.json.map(function(row) {
                    h += '<tr>';
                    h += '<td>' + row.attributes[0].value + '</td>';
                    h += '<td>' + row.attributes[1].value + '</td>';
                    h += '<td><a href="' + row.attributes[3].value + '" target="_blank">' + row.attributes[3].value + '</a></td>';
                    h += '</tr>';
                    uris.push(row.attributes[3].value);
                });
                $("#cuerpoResulPhonefy").html(h);

            }
        }, 'json').always(function() {
            pre.preloader('stop');
        }).fail(function(xhr, status, error) {
            PNotifyAlerta('Error', 'Algo salio mal con el módulo Phonefy de OsrFramework', "error");
            console.log(xhr, status, error)
        }).always(function() {
            pre.preloader('stop');
            habilitarBusquedaPhonefy();
        });
    }

    function abrirEnlaces() {
        uris.map(function(uri) {
            window.open(uri, "_blank");
        })
    }

    function mostrarAgregarAProyecto(e) {
        let rs = (e.currentTarget.id).split("_")[1];
        $("#redSocial").val(rs);
        abreVentanaModal("__modal", "Agregar Resultado a Proyecto");
    }

    function guardarResultado() {
        let burl = $("#burl").html() + "ClassProyectosentradas/guardarEntrada";
        let rs = $("#redSocial").val();
        let proyecto = $("#proyecto").val();

        if (proyecto == null) {
            PNotifyAlerta('Atención', 'Debe especificar en que proyecto quiere guardar la entrada...', 'notice');
            return;
        }

        let data = usufy.find(function(val) {
            return rs === val.attributes[2].value;
        });

        $.post(burl, { data: data, proyecto: proyecto }, function(result) {
            if (result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);

                if (result.error === "0") {
                    cierraVentanaModal("__modal");
                }

            }
        }, 'json');
    }

    function creaCardMedia(d) {
        let h = '';
        let logo = $("#burl").html() + 'assets/imgs/logos/';

        let plataforma = d.attributes[2].value.toLowerCase();
        let ruta = logo + findLogo(plataforma);

        h += '<div class="card shadow border-primary m-4" style="max-width: 450px;">';
        h += '<div class="row">';
        h += '<div class="col-sm-3 p-4">';
        h += '<img src="' + ruta + '" class="card-img" alt="...">';
        h += '</div>';
        h += '<div class="col-sm-6">';
        h += '<div class="card-body">';
        h += '<h5 class="card-title">' + d.attributes[2].value + '</h5>';
        h += '<p class="card-text">';
        h += '<a href="' + d.attributes[0].value + '" target="_blank">' + d.attributes[0].value + '</a>';
        h += '</p>';
        h += '</div>';
        h += '</div>';
        h += '<div class="col-sm-3 text-right p-4"> ';
        /*h += '<button id="btnAgregarProyecto_'+d.attributes[2].value+'" class="btn btn-success btn-xs">';
            h += '<i class="fas fa-folder-plus"></i>';
        h += '</button>';
        */
        h += '</div>';
        h += '</div>';
        if (plataforma == "gravatar") {
            h += '<div id="gravatarAdditional" class="card-text">';
            h += '</div>';
        }
        h += '</div>';


        return h;
    }

    function agregaGravatarInfo(g) {
        let h = "";
        h += '<div class="row">';
        h += '<div class="col-xs-12">';
        h += '<img src="' + g.thumbnailUrl + '" />';
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-6">';
        h += '<label>GivenName: </label>';
        h += '</div>';
        h += '<div class="col-xs-6">';
        h += g.givenName + " ";
        h += (g.familyName != undefined) ? g.familyName : "";
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-6">';
        h += '<label>DisplayName: </label>';
        h += '</div>';
        h += '<div class="col-xs-6">';
        h += g.displayName;
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-6">';
        h += '<label>PreferredUserName: </label>';
        h += '</div>';
        h += '<div class="col-xs-6">';
        h += g.preferredUsername;
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-6">';
        h += '<label>AboutMe: </label>';
        h += '</div>';
        h += '<div class="col-xs-6">';
        h += g.aboutMe;
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-6">';
        h += '<label>Email: </label>';
        h += '</div>';
        h += '<div class="col-xs-6">';
        h += (g.emails != undefined && g.emails.length > 0) ? g.emails[0].value : "";
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-12">';
        h += '<label>Accounts: </label>';
        h += '</div>';
        h += '</div>';

        if (g.accounts != undefined && g.accounts.length > 0) {

            for (let i = 0; i < g.accounts.length; i++) {
                h += '<div class="row">';
                h += '<div class="col-xs-6">';
                h += '<label>Url: </label>';
                h += '</div>';
                h += '<div class="col-xs-6">';
                h += '<a href="' + g.accounts[i].url + '" target="_blank">' + g.accounts[i].url + '</a>';
                h += '</div>';
                h += '</div>';

                h += '<div class="row">';
                h += '<div class="col-xs-6">';
                h += '<label>UserName: </label>';
                h += '</div>';
                h += '<div class="col-xs-6">';
                h += g.accounts[i].username;
                h += '</div>';
                h += '</div>';
            }
        }

        return h;

    }

    function findLogo(key) {
        let found = __logosPlataformas.filter(function(val) {
            return (val.logo == key)
        });

        return (found.length > 0) ? found[0].nombre : "no-logo.png";
    }

})(jQuery);