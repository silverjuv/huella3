(function($) {

    $(document).ready(init);
    let uris = [],
        usufy = [];
    PNotify.defaults.styling = 'bootstrap4';

    let intervalCheck = null;
    // let intervalCheckUsufy = null;
    let idProceso = 0;

    function init() {
        $("#btnBuscarNick_numverify").on("click", ejecutarNumverify);
        $("#btnBuscarNick_emailrep").on("click", ejecutarMailrep);


        //$("#btnAbrirEnlaces").on("click", abrirEnlaces);
        $("#btnAgregar").on("click", guardarResultado);
        //iniciaCombos();

        $("#nickemailrep").keypress(function(e) {
            if (e.which == 13)
                ejecutarSearchfy();
        });
        $("#nicknumverify").keypress(function(e) {
            if (e.which == 13)
                ejecutarUsufy();
        });

        $("#test_mail").slimScroll({
            alwaysVisible: true,
            height: '350px'
        });

        $("#test_phone").slimScroll({
            alwaysVisible: true,
            height: '350px'
        });


    }

    // function iniciaCombos() {
    //     //let burl = $("#burl").html() + "OsintOsrframework/getUsufyPlatforms";
    //     let burl = $("#burl").html() + "ESocialesController/getUsufyPlatforms";
    //     $.post(burl, {}, function(result) {
    //         if (result != undefined) {
    //             $("#plataforma").select2({
    //                 debug: true,
    //                 data: result,
    //                 placeholder: 'Selecciona plataforma(s)',
    //                 allowClear: true,
    //                 language: "es",
    //                 width: 'resolve',
    //                 multiple: true
    //             });
    //             $("#plataforma").val(null).trigger("change");
    //             // $("#plataforma").select2({
    //             //     ajax: {
    //             //         method: 'POST',
    //             //         url: burl,
    //             //         dataType: 'json',
    //             //         data: function(params) {
    //             //             let query = {
    //             //                 search: params.term,
    //             //                 type: 'public'
    //             //             }

    //             //             console.log(query)
    //             //             return query;
    //             //         },
    //             //         processResults: function(data) {
    //             //             return {
    //             //                 results: data
    //             //             };
    //             //         }
    //             //     },
    //             //     minimumInputLength: 3,
    //             //     width: 'resolve',
    //             //     placeholder: 'Selecciona plataforma(s)',
    //             //     allowClear: true,
    //             //     multiple: true,
    //             //     language: "es",

    //             // }).on('change.select2', function(e) {

    //             // });
    //         } //result != undefined
    //     }, 'json');

    //     //burl = $("#burl").html() + "OsintOsrframework/getSearchfyPlatforms";
    //     burl = $("#burl").html() + "ESocialesController/getSearchfyPlatforms";
    //     $.post(burl, {}, function(result) {
    //         if (result != undefined) {

    //             $("#splataforma").select2({
    //                 debug: true,
    //                 data: result,
    //                 placeholder: 'Selecciona plataforma(s)',
    //                 allowClear: true,
    //                 language: "es",
    //                 width: 'resolve',
    //                 multiple: true
    //             });
    //             $("#splataforma").val(null).trigger("change");

    //         } //result != undefined
    //     }, 'json');

    //     burl = $("#burl").html() + "ESocialesController/getMailfyPlatforms";
    //     $.post(burl, {}, function(result) {
    //         if (result != undefined) {

    //             $("#mailfy").select2({
    //                 debug: true,
    //                 data: result,
    //                 placeholder: 'Selecciona plataforma(s)',
    //                 allowClear: true,
    //                 language: "es",
    //                 width: 'resolve',
    //                 multiple: true
    //             });
    //             $("#mailfy").val(null).trigger("change");

    //         } //result != undefined
    //     }, 'json');

    //     burl = $("#burl").html() + "ESocialesController/getPhonefyPlatforms";
    //     $.post(burl, {}, function(result) {
    //         if (result != undefined) {

    //             $("#phonefy").select2({
    //                 debug: true,
    //                 data: result,
    //                 placeholder: 'Selecciona plataforma(s)',
    //                 allowClear: true,
    //                 language: "es",
    //                 width: 'resolve',
    //                 multiple: true
    //             });
    //             $("#phonefy").val(null).trigger("change");

    //         } //result != undefined
    //     }, 'json');

    // }

    function deshabilitarBusquedaMailrep() {
        $("#btnBuscarNick_emailrep").off("click").prop("disabled", true);
        $("#nickemailrep").off("keypress");
    }

    function habilitarBusquedaMailrep() {
        $("#btnBuscarNick_emailrep").on("click", ejecutarMailrep).prop("disabled", false);
        $("#nickemailrep").keypress(function(e) {
            if (e.which == 13)
                ejecutarMailrep();
        });
    }

    function ejecutarMailrep() {
        let burl = $("#burl").html() + "SpiderFootController/ejecutarMailrep";
        let nick = $.trim($("#nickemailrep").val());
        //let plataformas = $("#plataforma").select2('data');

        // let p = plataformas.map(function(val) {
        //     return val.id;
        // });

        if (nick == "") {
            PNotifyAlerta('Atención', 'Debe escribir un email para realizar la busqueda', 'notice');
            return;
        }

        //preloader
        deshabilitarBusquedaMailrep();
        let pre = $('<div/>').css({ 'margin-left': '8px', 'display': 'inline-block', 'vertical-align': 'middle' }).insertAfter($('#btnBuscarNick_emailrep')).preloader({ src: 'sprites.32.png' });

        $.post(burl, { data: nick }, function(result) {

            if (result != undefined) {
                //     console.log(result)
                    if (parseInt(result.status) == 0) {
                        PNotifyAlerta('Info', 'No se pudo ejecutar operacion', "error");
                        return;
                    }else{
                        // 
                        waitingDialog.show('Espere un momento...',{onHide: function () { }});
                        // PNotifyAlerta('Info', 'Operación solicitada. Espere un momento...', "info");
                        // alert(result.id)
                        idProceso = result.id;
                        intervalCheck = setInterval(resultadoEmailrep, 3000); // cada 3 segundos checar resultados
                        return;
                    }   
                }            
            // // // let r = '';
            // // // let i = 0;
            // // // if (result.json != null) {
            // // //     $("#test_mail").html(result.json);
            // // //     result.json = JSON.parse(result.json);
            // // //     let len = result.json.length;
            // // //     let plataforma = "";
            // // //     let gravatarUrl = "";

            // // //     if (len == 0) {
            // // //         PNotifyAlerta('Info', 'No se encontó información... (1)', "info");
            // // //         return;
            // // //     }

            // // //     //$("#resultados").html("");
            // // //     //$("#test_mail").html(result.json);
            // // //     return;

            // // // } else {
            // // //     PNotifyAlerta('Info', 'No se encontó información... (2)', "info");
            // // // }

        }, 'json').always(function() {
            pre.preloader('stop');
        }).fail(function(xhr, status, error) {
            pre.preloader('stop');
            PNotifyAlerta('Error', 'Algo salio mal con el módulo eMailrep de SpiderFootFramework', "error");
            console.log(xhr, status, error)
        }).always(function() {
            pre.preloader('stop');
            habilitarBusquedaMailrep();
        });
    }

    function resultadoEmailrep() {
        let burl = $("#burl").html() + "SpiderFootController/verResultadosEmailrep";

        //preloader
        deshabilitarBusquedaMailrep();
        let pre = $('<div/>').css({ 'margin-left': '8px', 'display': 'inline-block', 'vertical-align': 'middle' }).insertAfter($('#btnBuscarNick_emailrep')).preloader({ src: 'sprites.32.png' });

        $.post(burl, { idProc: idProceso }, function(result) {

            let r = '';
            let i = 0;            
            if (result != undefined) {

                if (parseInt(result.status) == 0) {
                    // aun no se realiza su proceso
                        // PNotifyAlerta('Info', 'No se pudo ejecutar operacion', "error");
                        // aun no hay resultado
                        return;
                }else{
                        // 
                        clearInterval(intervalCheck);
                        waitingDialog.hide();
                        // PNotifyAlerta('Info', 'Operación solicitada. Espere un momento...', "info");
                        // intervalCheck = setInterval(resultadoSearchfy, 3000); // cada 3 segundos checar resultados
                        // return;
                    // }
                    if (parseInt(result.status) == 1) {       
                        let r = '';
                        let i = 0;
                        if (result.json != null) {
                            $("#test_mail").html(result.json);
                            result.json = JSON.parse(result.json);
                            let len = result.json.length;
                            let plataforma = "";
                            let gravatarUrl = "";

                            if (len == 0) {
                                PNotifyAlerta('Info', 'No se encontó información... (1)', "info");
                                return;
                            }

                            //$("#resultados").html("");
                            //$("#test_mail").html(result.json);
                            return;

                        } else {
                            PNotifyAlerta('Info', 'No se encontó información... (2)', "info");
                        }
                    }else{
                        PNotifyAlerta('Info', result.msg, "error");
                        return;                        
                    }
                }
            }

        }, 'json').always(function() {
            pre.preloader('stop');
        }).fail(function(xhr, status, error) {
            pre.preloader('stop');
            PNotifyAlerta('Error', 'Algo salio mal con el módulo eMailrep de SpiderFootFramework', "error");
            console.log(xhr, status, error)
        }).always(function() {
            pre.preloader('stop');
            habilitarBusquedaMailrep();
        });
    }

    function deshabilitarBusquedaNumverify() {
        $("#btnBuscarNick_numverify").off("click").prop("disabled", true);
        $("#nicknumverify").off("keypress");
    }

    function habilitarBusquedaNumverify() {
        $("#btnBuscarNick_numverify").on("click", ejecutarNumverify).prop("disabled", false);
        $("#nicknumverify").keypress(function(e) {
            if (e.which == 13)
                ejecutarNumverify();
        });
    }

    function ejecutarNumverify() {
        // jsonx = '{\"valid\":\"true\",\"number\":\"524431862783\",\"local_format\":\"4431862783\",\"international_format\":\"+5214431862783\",\"country_prefix\":\"+52\",\"country_code\":\"MX\",\"country_name\":\"Mexico\",\"location\":\"Morelia\",\"carrier\":\"Radiomovil Dipsa SA de CV\",\"line_type\":\"mobile\"}';
        // jsonx = jsonx.replace('\\"', '');
        // alert(jsonx)
        // jsonx = JSON.parse(jsonx)
        // var jsonPretty = JSON.stringify(jsonx, null, '\t');

        // $("pre").text(jsonPretty);
        // $("#test_phone").html(jsonPretty);
        // return;

        //let burl = $("#burl").html() + "OsintOsrframework/ejecutarSearchfy";  
        let burl = $("#burl").html() + "SpiderFootController/ejecutarNumverify";
        let snick = $.trim($("#nicknumverify").val());
        //let splataformas = $("#splataforma").select2('data');
        // alert(snick.length)
        // let p = splataformas.map(function(val) {
        //     return val.id;
        // });
        if (snick.length !== 13) {
            PNotifyAlerta('Atención', 'Debe escribir el telefono en 13 posiciones, con codigo de país (+52 para mexico)', 'notice');
            return;
        }

        if (snick.indexOf("+") == -1) {
            PNotifyAlerta('Atención', 'Debe escribir el telefono con el codigo del país (+52 para mexico)', 'notice');
            return;
        }

        deshabilitarBusquedaNumverify();
        //preloader
        let pre = $('<div/>').css({ 'margin-left': '8px', 'display': 'inline-block', 'vertical-align': 'middle' }).insertAfter($('#btnBuscarNick_numverify')).preloader({ src: 'sprites.32.png' });

        $.post(burl, { data: snick }, function(result) {
            //alert(result)
            if (result != undefined) {
                jsonx = JSON.parse(result.json)
                var jsonPretty = JSON.stringify(jsonx, null, '\t');

                $("#test_phone").html(jsonPretty);
                //result.json = result.json.replace('\"', '');
                // $("#test_phone").html(result.json);
                // result.json = JSON.parse(result.json);
                // $("#test_phone").html(result.json)
                let len = jsonx.length;

                if (len == 0) {
                    PNotifyAlerta('Info', 'No se encontró información... (1)', "info");
                    return;
                }


                // let h = '';
                // uris = [];
                // result.json.map(function(row) {
                //     h += '<tr>';
                //     h += '<td>' + row.attributes[0].value + '</td>';
                //     h += '<td>' + row.attributes[1].value + '</td>';
                //     h += '<td><a href="' + row.attributes[3].value + '" target="_blank">' + row.attributes[3].value + '</a></td>';
                //     h += '</tr>';
                //     uris.push(row.attributes[3].value);
                // });
                // $("#cuerpoResulSearchy").html(h);

            }
        }, 'json').always(function() {
            pre.preloader('stop');
        }).fail(function(xhr, status, error) {
            PNotifyAlerta('Error', 'Algo salio mal con el módulo Numverify de SpiderFootFramework', "error");
            console.log(xhr, status, error)
        }).always(function() {
            pre.preloader('stop');
            habilitarBusquedaNumverify();
        });
    }


    // function abrirEnlaces() {
    //     uris.map(function(uri) {
    //         window.open(uri, "_blank");
    //     })
    // }

    // function mostrarAgregarAProyecto(e) {
    //     let rs = (e.currentTarget.id).split("_")[1];
    //     $("#redSocial").val(rs);
    //     abreVentanaModal("__modal", "Agregar Resultado a Proyecto");
    // }

    function guardarResultado() {
        let burl = $("#burl").html() + "ClassProyectosentradas/guardarEntrada";
        let rs = $("#redSocial").val();
        let proyecto = $("#proyecto").val();

        if (proyecto == null) {
            PNotifyAlerta('Atención', 'Debe especificar en que proyecto quiere guardar la entrada...', 'notice');
            return;
        }

        let data = usufy.find(function(val) {
            return rs === val.attributes[2].value;
        });

        $.post(burl, { data: data, proyecto: proyecto }, function(result) {
            if (result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);

                if (result.error === "0") {
                    cierraVentanaModal("__modal");
                }

            }
        }, 'json');
    }

    function creaCardMedia(d) {
        let h = '';
        let logo = $("#burl").html() + 'assets/imgs/logos/';

        let plataforma = d.attributes[2].value.toLowerCase();
        let ruta = logo + findLogo(plataforma);

        h += '<div class="card shadow border-primary m-4" style="max-width: 450px;">';
        h += '<div class="row">';
        h += '<div class="col-sm-3 p-4">';
        h += '<img src="' + ruta + '" class="card-img" alt="...">';
        h += '</div>';
        h += '<div class="col-sm-6">';
        h += '<div class="card-body">';
        h += '<h5 class="card-title">' + d.attributes[2].value + '</h5>';
        h += '<p class="card-text">';
        h += '<a href="' + d.attributes[0].value + '" target="_blank">' + d.attributes[0].value + '</a>';
        h += '</p>';
        h += '</div>';
        h += '</div>';
        h += '<div class="col-sm-3 text-right p-4"> ';
        /*h += '<button id="btnAgregarProyecto_'+d.attributes[2].value+'" class="btn btn-success btn-xs">';
            h += '<i class="fas fa-folder-plus"></i>';
        h += '</button>';
        */
        h += '</div>';
        h += '</div>';
        if (plataforma == "gravatar") {
            h += '<div id="gravatarAdditional" class="card-text">';
            h += '</div>';
        }
        h += '</div>';


        return h;
    }

    function agregaGravatarInfo(g) {
        let h = "";
        h += '<div class="row">';
        h += '<div class="col-xs-12">';
        h += '<img src="' + g.thumbnailUrl + '" />';
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-6">';
        h += '<label>GivenName: </label>';
        h += '</div>';
        h += '<div class="col-xs-6">';
        h += g.givenName + " ";
        h += (g.familyName != undefined) ? g.familyName : "";
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-6">';
        h += '<label>DisplayName: </label>';
        h += '</div>';
        h += '<div class="col-xs-6">';
        h += g.displayName;
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-6">';
        h += '<label>PreferredUserName: </label>';
        h += '</div>';
        h += '<div class="col-xs-6">';
        h += g.preferredUsername;
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-6">';
        h += '<label>AboutMe: </label>';
        h += '</div>';
        h += '<div class="col-xs-6">';
        h += g.aboutMe;
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-6">';
        h += '<label>Email: </label>';
        h += '</div>';
        h += '<div class="col-xs-6">';
        h += (g.emails != undefined && g.emails.length > 0) ? g.emails[0].value : "";
        h += '</div>';
        h += '</div>';

        h += '<div class="row">';
        h += '<div class="col-xs-12">';
        h += '<label>Accounts: </label>';
        h += '</div>';
        h += '</div>';

        if (g.accounts != undefined && g.accounts.length > 0) {

            for (let i = 0; i < g.accounts.length; i++) {
                h += '<div class="row">';
                h += '<div class="col-xs-6">';
                h += '<label>Url: </label>';
                h += '</div>';
                h += '<div class="col-xs-6">';
                h += '<a href="' + g.accounts[i].url + '" target="_blank">' + g.accounts[i].url + '</a>';
                h += '</div>';
                h += '</div>';

                h += '<div class="row">';
                h += '<div class="col-xs-6">';
                h += '<label>UserName: </label>';
                h += '</div>';
                h += '<div class="col-xs-6">';
                h += g.accounts[i].username;
                h += '</div>';
                h += '</div>';
            }
        }

        return h;

    }

    function findLogo(key) {
        let found = __logosPlataformas.filter(function(val) {
            return (val.logo == key)
        });

        return (found.length > 0) ? found[0].nombre : "no-logo.png";
    }

})(jQuery);