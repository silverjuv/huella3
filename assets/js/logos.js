let __logosPlataformas = [
    {
        "logo": "ebay",
        "nombre": "ebay.png"
    },
    {
        "logo": "f65",
        "nombre": "f65.png"
    },
    {
        "logo": "facebook",
        "nombre": "facebook.png"
    },
    {
        "logo": "flickr",
        "nombre": "flickr.png"
    },
    {
        "logo": "foursquare",
        "nombre": "foursquare.png"
    },
    {
        "logo": "gravatar",
        "nombre": "gravatar.png"
    },
    {
        "logo": "instagram",
        "nombre": "instagram.png"
    },
    {
        "logo": "medium",
        "nombre": "medium.png"
    },
    {
        "logo": "periscope",
        "nombre": "periscope.png"
    },
    {
        "logo": "taringa",
        "nombre": "taringa.png"
    },
    {
        "logo": "twitter",
        "nombre": "twitter.png"
    },
    {
        "logo": "mercadolibre",
        "nombre": "mercadolibre.png"
    },
    {
        "logo": "spotify",
        "nombre": "spotify.png"
    },
];