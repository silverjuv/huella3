Dropzone.autoDiscover = false;
(function($) {
    var archivo;
    $(document).ready(init);

    function init() {
        $.fn.button = function(action) {
            if (action === 'loading' && this.data('loading-text')) {
                this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
                this.html(this.data('original-text')).prop('disabled', false);
            }
        };

        $("#layout").select2({
            placeholder: 'Selecciona un layout',
            allowClear: true,
            language: "es",
            width: '100% '
        });
        $("#layout").val(null).trigger("change");
        burl = $("#burl").html() + "LayoutsUploaderController/subirLayout";
        $(".dropzone").dropzone({
            url: burl,
            dictDefaultMessage: "De clic aqui para adjuntar archivo",
            dictFileTooBig: "El archivo es demasiado grande",
            dictInvalidFileType: "Tipo de archivo invalido",
            dictResponseError: "Ocurrio un problema y no fue posible subir el archivo",
            parallelUploads: 1,
            dictRemoveFile: 'Quitar archivo',
            maxFiles: 1,
            acceptedFiles: '.xlsx',
            autoProcessQueue: false,
            addRemoveLinks: true,
            init: function() {
                archivo = this;
                $("#procesar").on("click", function() {
                    var layout = $("#layout").val();
                    if (layout === null) {
                        PNotifyAlerta("Atención", "Seleccione el tipo de layout", "error");
                        return
                    }
                    if (archivo.getQueuedFiles().length > 0) {
                        $("#procesar").button('loading');
                        $(".dz-hidden-input").prop("disabled", true);
                        archivo.processQueue();
                    } else {
                        PNotifyAlerta("Atención", "Adjunte layout", "error");
                        return
                    }
                });
                this.on("success", function(file, res) {
                    $("#procesar").button('reset');
                    res = (typeof res == "string") ? JSON.parse(res) : res;
                    PNotifyAlerta(res.title, res.msg, res.type);
                    $(".dz-hidden-input").prop("disabled", false);
                    archivo.removeAllFiles(true);
                    $("#layout").val(null).trigger("change");
                    if (res.type == "success") {}
                });
                this.on("addedfile", function(file) {
                    var tipo = file.name.split('.');
                    if (tipo[tipo.length - 1] == 'xlsx' || tipo[tipo.length - 1] == 'xls') {
                        thumbnail = $("#burl").html() + 'assets/imgs/Excel-icon.png';
                        archivo.emit("thumbnail", file, thumbnail);
                    } else {
                        PNotifyAlerta("Atención", "Tipo de archivo invalido", "error");
                        archivo.removeFile(file);
                    }
                    if (archivo.getQueuedFiles().length == 1) {
                        PNotifyAlerta("Atención", "Solo un layout por proceso", "error");
                        archivo.removeFile(file);
                    }
                });
                this.on("sending", function(data, xhr, formData) {
                    formData.append("layout", $("#layout").val());
                });
            }
        });
    }
})(jQuery);