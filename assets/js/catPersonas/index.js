(function($) {
    $(document).ready(init);

    function init() {
        $("#_agregar").on("click", function() { irAlFormulario(false); });
        $("#_editar").on("click", function() { irAlFormulario(true); });
        $("#_borrar").on("click", borrarPersona);

        $("#_exportar").on("click", function () {
            exportData();
        });

        $("#exportData").on("click", function () {
            abreVentanaModal("exportarDialog", "Exportar a...");
        });
                
        initializeTable();
    }

    function initializeTable() {
        let burl = $("#burl").html() + "CatPersonasController/listar/";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";

        $("#tpersonas").DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function(d) {

                }
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "id_persona", "visible": false },
                // { "data": "empresa" },
                { "data": "curp" },
                { "data": "ine" },
                { "data": "nombre" },
                { "data": "email" },
                { "data": "celular" },
                { "data": "direccion" }
            ],
            "responsive": true,
            "info": false,
            "lengthChange": false,
            "language": {
                "url": lan
            },
            "dom": '<f<t>p>',
            "select": {
                "style": 'single'
            }

        });
    }

    function irAlFormulario(editar) {
        let id_edicion = "";
        if (editar === true) {
            let t = $("#tpersonas").DataTable();
            let r = t.row({ selected: true }).data();

            if (r != null) {
                id_edicion = r.id_persona;
            } else {
                PNotifyAlerta("Atención", "Debe seleccionar el registro a editar", "error");
                return false;
            }
        }

        document.location.href = $("#burl").html() + "CatPersonasController/formulario/" + id_edicion;
    }

    function borrarPersona() {
        let t = $("#tpersonas").DataTable();
        let r = t.row({ selected: true }).data();

        if (r != null) {
            const notice = PNotifyConfirm("Confirme su acción", "¿Desea borrar el registro: " + r.nombre + "?", "Si, proceder");

            notice.on('pnotify.confirm', async(v) => {
                let res = await makePostRequest("CatPersonasController/borrarPersona", r.id_persona);
                if (res != undefined) {
                    PNotifyAlerta(res.title, res.msg, res.type);
                    if (res.type != "error") {
                        t.ajax.reload();
                    }
                }
            });
            notice.on('pnotify.cancel', () => {
                console.log("rejected")
            });
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
        }
    }

    function exportData() {
        
        var radioValue = $("input[name='optradio']:checked").val();
        var burl = $("#burl").html() + "CatPersonasController/exportData/?format="+radioValue;
        // var burl = $("#burl").html() + "ResultadosOdometroController/exportCSV";
        window.location=burl;        
    }

})(jQuery);