(function($) {
    $(document).ready(init);

    function init() {
        $("#btnGuardar").on("click", guardarPersona);
        $(".btnRegresar").on("click", irAlListado);
        $("#btnLlimpiar").on("click", limpiarCtrls);

        initializeCombos();
    }

    function irAlListado() {
        document.location.href = $("#burl").html() + "CatPersonasController/";
    }

    async function initializeCombos() {
        let res = await makePostRequest("CatPersonasController/getEmpresas");
        if (res != undefined) {
            let id_empresa = $("#id_empresa").val();
            if (id_empresa == "") {
                id_empresa = null;
            }
            creaCombobox("id_cliente", res, id_empresa);
        }
    }

    function limpiarCtrls() {
        $("#ine").val("");
        $("#curp").val("");
        $("#paterno").val("");
        $("#materno").val("");
        $("#nombre").val("");
        $("#email").val("");
        $("#celular").val("");
        $("#alias").val("");
        $("#direccion").val("");
        $("#linkedin").val("");
        $("#twitter").val("");
        $("#id_cliente").val(null).trigger("change");

    }

    function esCurpValido(curp) {
        var re = /^([A-Z]{4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM](AS|BC|BS|CC|CL|CM|CS|CH|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[A-Z]{3}[0-9A-Z]\d)$/i;
        return (curp.match(re) != null);
    }

    function validar() {
        let obj = {};
        obj.id_edicion = $("#id_edicion").val();
        obj.ine = $.trim($("#ine").val());
        obj.curp = $.trim($("#curp").val());
        obj.paterno = $.trim($("#paterno").val());
        obj.materno = $.trim($("#materno").val());
        obj.nombre = $.trim($("#nombre").val());
        obj.email = $.trim($("#email").val());
        obj.celular = $.trim($("#celular").val());
        obj.alias = $.trim($("#alias").val());
        obj.direccion = $.trim($("#direccion").val());
        obj.twitter = $.trim($("#twitter").val());
        obj.linkedin = $.trim($("#linkedin").val());
        obj.id_cliente = $.trim($("#id_cliente").val());

        // if (obj.ine == "" || obj.ine.trim().length < 10) {
        //     PNotifyAlerta("Atención", "El INE es obligatorio", "error");
        //     return false;
        // }
        // try {
        //     if (parseInt(obj.ine) == NaN) {
        //         PNotifyAlerta("Atención", "El INE debe ser numerico", "error");
        //         return false;
        //     }
        // } catch (e) {
        //     PNotifyAlerta("Atención", "El INE debe ser numerico", "error");
        //     return false;
        // }

        if (obj.curp == "") {
            PNotifyAlerta("Atención", "La CURP es obligatoria", "error");
            return false;
        }
        if (esCurpValido(obj.curp) == false) {
            PNotifyAlerta("Atención", "El CURP no esta bien escrito", "error");
            return false;
        }

        if (obj.paterno == "") {
            PNotifyAlerta("Atención", "El apellido paterno es obligatorio", "error");
            return false;
        }
        if (obj.nombre == "") {
            PNotifyAlerta("Atención", "El nombre es obligatorio", "error");
            return false;
        }
        if (obj.email == "") {
            PNotifyAlerta("Atención", "El email es obligatorio", "error");
            return false;
        }
        if (esEmailValido(obj.email) == false) {
            PNotifyAlerta("Atención", "El email no esta bien escrito", "error");
            return false;
        }
        if (obj.celular == "" || obj.celular.trim().length != 10) {
            PNotifyAlerta("Atención", "El celular es obligatorio en 10 posiciones", "error");
            return false;
        }
        try {
            if (parseInt(obj.celular) == NaN) {
                PNotifyAlerta("Atención", "El celular debe ser numerico", "error");
                return false;
            }
        } catch (e) {
            PNotifyAlerta("Atención", "El celular debe ser numerico", "error");
            return false;
        }

        // if (obj.alias == "") {
        //     PNotifyAlerta("Atención", "El alias o pseudonombre es obligatorio", "error");
        //     return false;
        // }
        // if (obj.direccion == "" || obj.direccion.trim().length < 20) {
        //     PNotifyAlerta("Atención", "La dirección es obligatoria", "error");
        //     return false;
        // }
        // if (obj.direccion == "" || obj.direccion.trim().length < 20) {
        //     PNotifyAlerta("Atención", "La dirección es obligatoria", "error");
        //     return false;
        // }

        return obj;
    }

    async function guardarPersona() {
        let obj = validar();
        if (obj === false) {
            return;
        }

        let res = await makePostRequest("CatPersonasController/guardarPersona", obj);
        if (res != undefined) {
            PNotifyAlerta(res.title, res.msg, res.type);
        }
    }

})(jQuery);