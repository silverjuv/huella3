(function($) {
    var tcr;
    var tpa;
    var ts;

    $(document).ready(initializeTables);

    function initializeTables() {
        $('#__modal').on('shown.bs.modal', function(e) {
            ts.columns.adjust().draw();
        });
        var burl = $("#burl").html() + "PuestosRiesgosClienteController/listarPuestos";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";
        tcr = $("#tpuestos").DataTable({
            "scrollY": "300px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST"
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control'},
                { "data": "id", "orderable": false },
                { "data": "puesto", "orderable": false }
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan,
                // "select": {
                //     "rows": {
                //         "_": "%d Filas seleccionadas",
                //         "1": "1 Fila seleccionada"
                //     }
                // }
            },
            "select": {
                'style': 'single',
                'selector': 'td:not(.control)'
            },
            dom: "<'row'<'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-7'p>>",
            "rowCallback": function(row, data, index) {
                $('td', row).attr('class', 'table-active');
            }
        });

        burl = $("#burl").html() + "PuestosRiesgosClienteController/listarPuestosRiesgos";
        tpa = $("#trasignados").DataTable({
            "scrollY": "300px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function(d) {
                    let tCatalago = tcr.row({ selected: true }).data();
                    if (tCatalago != null) {
                        d.id_puesto = tCatalago.id;
                    } else {
                        d.id_puesto = 0;
                    }
                }
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control', "visible": false  },
                { "data": "id_riesgo", "visible": false },
                { "data": "riesgo", "orderable": false },
                { "data": "subriesgos", "orderable": false }
                // {
                //     "data": "riesgo",
                //     "orderable": false,
                //     "render": function(data, type, row, meta) {
                //         return '<button type="button" class="btn btn-warning">Ver subriesgos</button>';
                //     }
                // },
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan,
                // "select": {
                //     "rows": {
                //         "_": "%d Filas seleccionadas",
                //         "1": "1 Fila seleccionada"
                //     }
                // }
            },
            "select": {
                'style': 'single',
                'selector': 'td:not(.control)'
            },
            dom: "<'row'<'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-7'p>>",
            "rowCallback": function(row, data, index) {
                $('td', row).attr('class', 'table-active');
            }
        });

        var burl = $("#burl").html() + "PuestosRiesgosClienteController/listarRiesgosSubriesgos";
        ts = $("#tSubriesgos").DataTable({
            "scrollY": "300px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function(d) {
                    let tAsignados = tpa.row({ selected: true }).data();
                    if (tAsignados != null) {
                        d.id_riesgo = tAsignados.id_riesgo;
                    } else {
                        d.id_riesgo = 0;
                    }
                }
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control' },
                { "data": "id_riesgo", "visible": false },
                { "data": "riesgo", "orderable": false }
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan,
                "select": {
                    "rows": {
                        "_": "%d Filas seleccionadas",
                        "1": "1 Fila seleccionada"
                    }
                }
            },
            "select": {
                'style': 'single',
                'selector': 'td:not(.control)'
            },
            dom: "<'row'<'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-7'p>>",
            "rowCallback": function(row, data, index) {
                $('td', row).attr('class', 'table-active');
            }
        });

        $('#tpuestos').DataTable().on('select', function(e, dt, type, indexes) {
            if (type === 'row') {
                $("#trasignados").DataTable().ajax.reload(null, false);
                console.log("si");
            }
        });

        $('#trasignados tbody').on('click', 'button', function() {
            verSubriesgos();
        });
    }

    function verSubriesgos() {
        abreVentanaModal("__modal", "Subriesgos asociados");
    }

})(jQuery);