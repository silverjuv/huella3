(function($) {
    var te;
    var tp;
    var tn;
    var pagina=0;

    $(document).ready(init());

    function init() {
        initTables();
        $("#_btnOSINT").on("click", function() {
            buscarFromOSINT();
        });
        $("#_btnNoticias").on("click", function() {
            buscarNoticias();
        });
        $("#_btnDetalles").on("click", function() {
            abrirVerExpediente();
        });
        $("#_btnRedesSociales").on("click", function() {
            buscarRedesSociales();
        });
        $("#_btnDarkweb").on("click", function() {
            buscarDarkweb();
        });

        $("#_btnWeb").on("click", function() {
            buscarWeb();
        });

        $("#_btnReload").on("click", function() {
            // reloadTable();
            $("#tPersonas").DataTable().ajax.reload(null, false);
        });

        $.fn.button = function(action) {
            if (action === 'loading' && this.data('loading-text')) {
                this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
                this.html(this.data('original-text')).prop('disabled', false);
            }
        };

        /*$('#verExpedienteDialog').on('shown.bs.modal', function (e) {
            console.log("ok");
    		tn.columns.adjust().draw();
        });*/	
    }

    function initTables() {
        //NProgress.start();			  
        var burl = $("#burl").html() + "ExpedientesController/listarPersonas";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";
        tp = $("#tPersonas").DataTable({
            "scrollY": "400px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST"
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control' },
                { "data": "id_persona", "visible": false },
                { "data": "id_cliente", "visible": false },
                { "data": "empresa", "orderable": false },
                { "data": "nombre", "orderable": false },
                { "data": "email", "visible": false },
                { "data": "celular", "visible": false },
                { "data": "ine", "visible": false },
                { "data": "direccion", "visible": false },
                { "data": "alias", "visible": false },
                {
                    "data": "noticias",
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        return '<span class="badge badge-">' + data + '</span>';
                    }
                },
                {
                    "data": "osint",
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        return '<span class="badge badge-info">' + data + '</span>';
                    }
                },
                {
                    "data": "rsociales",
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        return '<span class="badge badge-primary">' + data + '</span>';
                    }

                },
                {
                    "data": "darkweb",
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        return '<span class="badge badge-dark">' + data + '</span>';
                    }
                },
                {
                    "data": "web",
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        return '<span class="badge badge-light">' + data + '</span>';
                    }
                }
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan,
                // "select": {
                //     "rows": {
                //         "_": "%d Filas seleccionadas",
                //         "1": "1 Fila seleccionada"
                //     }
                // }
            },
            "select": {
                'style': 'multi',
                'selector': 'td:not(.control)'
            },
            dom: "<'row'<'col-sm-8'l><'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            lengthMenu: [
                [500, 400, 300, 200, 100, 1],
                ['500', '400', '300', '200', '100', '1']
            ],
            buttons: [{
                extend: '',
                className: 'btn btn-primary',
                text: 'Nuevo Expediente</br><i class="fa fa-plus"></i>',
                titleAttr: 'Nuevo Expediente',
                action: function(e, dt, node, config) {
                    $("#nuevoExpedienteDialog").modal("show");
                },
                init: function(api, node, config) {
                    $(node).removeClass('dt-button')
                }
            }],
            "drawCallback": function(settings) {
                $('#seleccionarTodos').change(function() {
                    if (this.checked) {
                        tp.rows().select();
                    } else {
                        tp.rows().deselect();
                    }
                });
            },
            "rowCallback": function(row, data, index) {
                $('td', row).attr('class', 'table-active');
            },
            "initComplete": function(settings,json) {
                if(json.id_perfil==2){
                    $("#_btnOSINT").hide();
                    $("#_btnNoticias").hide();
                    $("#_btnRedesSociales").hide();
                    $("#_btnDarkweb").hide();
                    $("#_btnWeb").hide();
                }else if(json.id_perfil==1){
                    $("#_btnOSINT").show();
                    $("#_btnNoticias").show();
                    $("#_btnRedesSociales").show();
                    $("#_btnDarkweb").show();
                    $("#_btnWeb").show();
                }
    		}
        });

        burl = $("#burl").html()+"ExpedientesController/listarFuentes";
		$.post(burl,{},function(result){
			if(result !== undefined){
				$("#fuente").select2({
					debug:true,
					data: result,
					placeholder: 'Selecciona la fuente',
					language:"es",
					width: '160px'
				}).on('select2:select', function (e) {
                    var fuente = e.params.data.id;
                    var pageno=pagina;
                    abrirExpediente(pageno,fuente);    
				});
				$("#fuente").val('N').trigger("change");
			}
		},"json");
        

        $('#pagination').on('click','a',function(e){
			e.preventDefault(); 
            var pageno = $(this).attr('data-ci-pagination-page');
            var fuente=$("#fuente").val();
			abrirExpediente(pageno,fuente);
		});
    }
    
    function abrirExpediente(page,fuente){
		var burl = $("#burl").html()+"ExpedientesController/abrirExpediente";
		let r = tp.row({selected: true}).data();
        if(r != null) {
			$.post(burl, {id_persona: r.id_persona,nombre:r.nombre,pagina:page,fuente:fuente}, function (result) {
                pagina=page;
                if (result != undefined) {
                    var html='';
                    if(result.data['data'].length > 0 ){
						for(var i=0; i<result.data['data'].length; i++){
                            if (fuente=='N' || fuente=='D' ){
							    html+=renderNoticia(result.data['data'][i]);
                            }else{
                                html+=renderNoNoticia(result.data['data'][i]);
                            }
                        }
                        // if (html!=''){
                            console.log(html+result.paginador);
                            $("#divNoticias").html(html);
                            $('#pagination').html(result.paginador);
                        // }else{
                        //     // mostrar posibles errores encontrados al consultar dicha fuente
                        //     abrirExpedienteExtra(r.id_persona, fuente);
                        // }
					}else{
                        // $("#divNoticias").html(html);
                        abrirExpedienteExtra(r.id_persona, fuente);
						$('#pagination').html('');  
					}
                }else{
					PNotifyAlerta("Atencion","ha ocurrido un problema","error");
					return;
				}
            }, 'json');
			$("#nuevoDetallesDialog").modal("show");

        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un expediente de la tabla", "notice");
        }
	}

    function abrirExpedienteExtra(id_persona, fuente){
		var burl = $("#burl").html()+"ExpedientesController/abrirExpedienteExtra";

			$.post(burl, {id_persona: id_persona, fuente:fuente}, function (result) {
                if (result != undefined) {
                    // alert(result.data)
                    html=renderNoNoticia(result.data, 1);

					$("#divNoticias").html(html);

					// $('#pagination').html('');  

                }else{
					PNotifyAlerta("Atencion","ha ocurrido un problema","error");
					return;
				}
            }, 'json');
			// $("#nuevoDetallesDialog").modal("show");

	}

    function buscarFromOSINT() {
        let rs = tp.rows({ selected: true } ).data();
        let r = tp.row({ selected: true } ).data();
        if (rs.length == 1) { 
            let burl = $("#burl").html() + "TicDefenseToolsController/ejecutarOSINT";
            $.post(burl, { data: r.id_persona }, function(result) {
                // $("#_btnOSINT").button('reset');
                if (result != undefined) {
                    if (parseInt(result.status) == 0) {
                        PNotifyAlerta('Info', 'No se pudo ejecutar operacion\n'+result.msg, "error");
                        return;
                    }else{
                        PNotifyAlerta('Info', result.msg, "success");
                        $("#tPersonas").DataTable().ajax.reload(null, false);
                        return;
                    }   
                }else {
                    PNotifyAlerta("Atencion", "No se realizo la acción", "error");
                    return;
                }
            }, 'json');
            // 1) 
            // ejecutarEmailRep(r.id_persona,"O");
            // // 2)
            // ejecutarNumVerify(r.id_persona,"O");
            // // 3)
            // ejecutarFullcontact(r.id_persona,"O");
            // // 4)
            // ejecutarOSINTClearbit(r.id_persona,"O");
            // // 5)
            // ejecutarOSINTHunter(r.id_persona,"O");
            // $("#_btnOSINT").button('loading');
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar solo una persona de la tabla", "notice");
        }
    }

    function ejecutarNumVerify(id_persona,fuente) {
        let burl = $("#burl").html() + "TicDefenseToolsController/ejecutarOSINTNumVerify";
        $.post(burl, { data: id_persona,fuente:fuente }, function(result) {
            $("#_btnOSINT").button('reset');
            if (result != undefined) {
                if(result.json.noErrores!= undefined){
                    if (result.json.noErrores == 1) {
                        PNotifyAlerta("Atencion",result.json.mensaje, "success");
                    }
                }else{
                    let resultados=JSON.parse(result.json);
                    if (resultados.NoErrores == 0) {
                        PNotifyAlerta("Atencion","Se analizarón "+resultados.NoUrls+" resultados, obteniendo :"+resultados.NoComentarios+" coincidendias en el modulo Num Verify, para la persona:"+ resultados.Persona, "success");
                        return;
                    } else {
                        PNotifyAlerta("Atencion",resultados.Mensaje, "error");
                    }
                }    
            }else {
                PNotifyAlerta("Atencion", "No se realizo la acción", "error");
                return;
            }
        }, 'json');
    }

    function ejecutarFullcontact(id_persona,fuente) {
        let burl = $("#burl").html() + "TicDefenseToolsController/ejecutarOSINTFullcontact";
        $.post(burl, { data: id_persona,fuente:fuente  }, function(result) {
            $("#_btnOSINT").button('reset');
            if (result != undefined) {
                if(result.json.noErrores!= undefined){
                    if (result.json.noErrores == 1) {
                        PNotifyAlerta("Atencion",result.json.mensaje, "success");
                    }
                }else{
                    let res=JSON.parse(result.json);
                    let arr = res.split("[");
                    let arr1 = arr[1].split("]");
                    let resultados = JSON.parse(arr1[0]);
                    if (resultados.NoErrores == 0) {
                        PNotifyAlerta("Atencion","Se analizarón "+resultados.NoUrls+" resultados, obteniendo :"+resultados.NoComentarios+" coincidendias en el modulo Full contact, para la persona:"+ resultados.Persona, "success");
                        return;
                    } else {
                        PNotifyAlerta("Atencion",resultados.Mensaje, "error");
                    }
                }    
            }else {
                PNotifyAlerta("Atencion", "No se realizo la acción", "error");
                return;
            }
        },'json');
    }

    function ejecutarEmailRep(id_persona,fuente) {
        let burl = $("#burl").html() + "TicDefenseToolsController/ejecutarOSINTEmailrep";
        $.post(burl, { data: id_persona,fuente:fuente }, function(result) {
            $("#_btnOSINT").button('reset');
            if (result != undefined) {
                if(result.json.noErrores!= undefined){
                    if (result.json.noErrores == 1) {
                        PNotifyAlerta("Atencion",result.json.mensaje, "success");
                    }
                }else{
                    let res=JSON.parse(result.json);
                    let arr = res.split("[");
                    let arr1 = arr[1].split("]");
                    let resultados = JSON.parse(arr1[0]);
                    if (resultados.NoErrores == 0) {
                        PNotifyAlerta("Atencion","Se analizarón "+resultados.NoUrls+" resultados, obteniendo :"+resultados.NoComentarios+" coincidendias en el modulo reputación de email, para la persona:"+ resultados.Persona, "success");
                        return;
                    } else {
                        PNotifyAlerta("Atencion",resultados.Mensaje, "error");
                    }
                }    
            }else {
                PNotifyAlerta("Atencion", "No se realizo la acción", "error");
                return;
            }
        }, 'json');
    }

    function ejecutarOSINTClearbit(id_persona,fuente) {
        let burl = $("#burl").html() + "TicDefenseToolsController/ejecutarOSINTClearbit";
        $.post(burl, { data: id_persona,fuente:fuente }, function(result) {
            $("#_btnOSINT").button('reset');
            if (result != undefined) {
                if(result.json.noErrores!= undefined){
                    if (result.json.noErrores == 1) {
                        PNotifyAlerta("Atencion",result.json.mensaje, "success");
                    }
                }else{
                    let res=JSON.parse(result.json);
                    let arr = res.split("[");
                    let arr1 = arr[1].split("]");
                    let resultados = JSON.parse(arr1[0]);
                    if (resultados.NoErrores == 0) {
                        PNotifyAlerta("Atencion","Se analizarón "+resultados.NoUrls+" resultados, obteniendo :"+resultados.NoComentarios+" coincidendias en el modulo Clearbit, para la persona:"+ resultados.Persona, "success");
                        return;
                    } else {
                        PNotifyAlerta("Atencion",resultados.Mensaje, "error");
                    }
                }    
            }else {
                PNotifyAlerta("Atencion", "No se realizo la acción", "error");
                return;
            }
        }, 'json');
    }

    function ejecutarOSINTHunter(id_persona,fuente) {
        let burl = $("#burl").html() + "TicDefenseToolsController/ejecutarOSINTHunter";
        $.post(burl, { data: id_persona,fuente:fuente}, function(result) {
            $("#_btnOSINT").button('reset');
            if (result != undefined) {
                $("#tPersonas").DataTable().ajax.reload();
                if(result.json.noErrores!= undefined){
                    if (result.json.noErrores == 1) {
                        PNotifyAlerta("Atencion",result.json.mensaje, "success");
                    }
                }else{
                    let res=JSON.parse(result.json);
                    let arr = res.split("[");
                    let arr1 = arr[1].split("]");
                    let resultados = JSON.parse(arr1[0]);
                    if (resultados.NoErrores == 0) {
                        PNotifyAlerta("Atencion","Se analizarón "+resultados.NoUrls+" resultados, obteniendo :"+resultados.NoComentarios+" coincidendias en el modulo Hunter, para la persona:"+ resultados.Persona, "success");
                        return;
                    } else {
                        PNotifyAlerta("Atencion",resultados.Mensaje, "error");
                    }
                }    
            }else {
                PNotifyAlerta("Atencion", "No se realizo la acción", "error");
                return;
            }
        }, 'json');
    }

    // // function buscarNoticias() {
    // //     let burl = $("#burl").html() + "ExpedientesController/buscarNoticias";
    // //     let r = tp.row({ selected: true }).data();
    // //     if (r != null) {
    // //         let datosPersona = new Array();
    // //         $.each(tp.rows({ selected: true }).data(), function(i, v) {
    // //             datosPersona.push(v);
    // //         });
    // //         $("#_btnNoticias").button('loading');
    // //         $.post(burl, { datosPersona: datosPersona,fuente:"N"}, function(result) {
    // //             $("#_btnNoticias").button('reset');
    // //             if (result != undefined) {
    // //                 PNotify.removeAll();
    // //                 $("#_btnNoticias").button('reset');
    // //                 if (result.error == "0") {
    // //                     PNotifyAlerta("Atencion", result.msg, "success");
    // //                     $("#tPersonas").DataTable().ajax.reload();
    // //                 } else {
    // //                     PNotifyAlerta("Atencion", result.msg, "error");
    // //                 }
    // //             }else {
    // //                 PNotifyAlerta("Atencion", "No se realizo la acción", "error");
    // //                 return;
    // //             }
    // //         }, 'json');
    // //     } else {
    // //         PNotifyAlerta("Atención", "Debe seleccionar una persona de la tabla", "notice");
    // //     }
    // // }
    function buscarNoticias() {
        let rs = tp.rows({ selected: true } ).data();
        let r = tp.row({ selected: true } ).data();
        if (rs.length == 1) { 
            let burl = $("#burl").html() + "ExpedientesController/buscarNoticias";
            $.post(burl, { data: r.id_persona }, function(result) {
                // $("#_btnOSINT").button('reset');
                if (result != undefined) {
                    if (parseInt(result.status) == 0) {
                        PNotifyAlerta('Info', 'No se pudo ejecutar operacion\n'+result.msg, "error");
                        return;
                    }else{
                        PNotifyAlerta('Info', result.msg, "success");
                        $("#tPersonas").DataTable().ajax.reload(null, false);
                        return;
                    }   
                }else {
                    PNotifyAlerta("Atencion", "No se realizo la acción", "error");
                    return;
                }
            }, 'json');

        } else {
            PNotifyAlerta("Atención", "Debe seleccionar solo una persona de la tabla", "notice");
        }
    }    

    // function buscarDarkweb() {
    //     let burl = $("#burl").html() + "ExpedientesController/buscarDarkWeb";
    //     let r = tp.row({ selected: true }).data();
    //     if (r != null) {
    //         let datosPersona = new Array();
    //         $.each(tp.rows({ selected: true }).data(), function(i, v) {
    //             datosPersona.push(v);
    //         });
    //         $("#_btnDarkweb").button('loading');
    //         $.post(burl, { datosPersona: datosPersona,fuente:"D" }, function(result) {
    //             $("#_btnDarkweb").button('reset');
    //             if (result != undefined) {
    //                 PNotify.removeAll();
    //                 if (result.error == "0") {
    //                     PNotifyAlerta("Atencion", result.msg, "success");
    //                     $("#tPersonas").DataTable().ajax.reload();
    //                 } else {
    //                     PNotifyAlerta("Atencion", result.msg, "error");
    //                     return;
    //                 }
    //             } else {
    //                 PNotifyAlerta("Atencion", "No se realizo la acción", "error");
    //             }
    //         }, 'json');
    //     } else {
    //         PNotifyAlerta("Atención", "Debe seleccionar una persona de la tabla", "notice");
    //     }
    // }

    function buscarDarkweb() {
        let rs = tp.rows({ selected: true } ).data();
        let r = tp.row({ selected: true } ).data();
        if (rs.length == 1) { 
            let burl = $("#burl").html() + "ExpedientesController/buscarDarkweb";
            $.post(burl, { data: r.id_persona }, function(result) {
                // $("#_btnOSINT").button('reset');
                if (result != undefined) {
                    if (parseInt(result.status) == 0) {
                        PNotifyAlerta('Info', 'No se pudo ejecutar operacion\n'+result.msg, "error");
                        return;
                    }else{
                        PNotifyAlerta('Info', result.msg, "success");
                        $("#tPersonas").DataTable().ajax.reload(null, false);
                        return;
                    }   
                }else {
                    PNotifyAlerta("Atencion", "No se realizo la acción", "error");
                    return;
                }
            }, 'json');
            
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar solo una persona de la tabla", "notice");
        }
    }

    // function buscarRedesSociales() {
    //     let burl = $("#burl").html() + "ExpedientesController/buscarRedesSociales";
    //     let rs = tp.rows({ selected: true }).data();
    //     let r = tp.row({ selected: true }).data();
    //     if (rs.length == 1) {
    //         $("#_btnRedesSociales").button('loading');
    //         $.post(burl, { id_persona: r.id_persona,fuente:"R"}, function(result) {
    //             $("#_btnRedesSociales").button('reset');
    //             if (result != undefined) {
    //                 if (result.json != undefined) {
    //                     if(result.json.noErrores!= undefined){
    //                         if (result.json.noErrores == 1) {
    //                             PNotifyAlerta("Atencion","La empresa:"+r.empresa+" "+result.json.mensaje, "success");
    //                         }
    //                     }else{    
    //                         for (var i = 0; i < result.json.length; i++) {
    //                             let res=JSON.parse(result.json[i]);
    //                             let arr = res.split("[");
    //                             let arr1 = arr[1].split("]");
    //                             let resultados = JSON.parse(arr1[0]);
    //                             if (resultados.NoErrores == 0) {
    //                                 PNotifyAlerta("Atencion","Se analizarón "+resultados.NoUrls+" resultados, obteniendo :"+resultados.NoComentarios+" coincidendias en el modulo :"+resultados.Proceso+", para la persona:"+ resultados.Persona, "success");
    //                                 $("#tPersonas").DataTable().ajax.reload();
    //                             } else {
    //                                 PNotifyAlerta("Atención",resultados.Mensaje, "error");
    //                             }
    //                         }
    //                     }    
    //                 } else {
    //                     PNotifyAlerta("Atencion", "No se realizo la acción", "error");
    //                     return;
    //                 }
    //             } else {
    //                 PNotifyAlerta("Atencion", "No se realizo la acción", "error");
    //             }
    //         }, 'json');
    //     } else {
    //         PNotifyAlerta("Atención", "Debe seleccionar solo una persona de la tabla", "notice");
    //     }
    // }
    function buscarRedesSociales() {
        let rs = tp.rows({ selected: true } ).data();
        let r = tp.row({ selected: true } ).data();
        if (rs.length == 1) { 
            let burl = $("#burl").html() + "ExpedientesController/buscarRedesSociales";
            $.post(burl, { data: r.id_persona }, function(result) {
                // $("#_btnOSINT").button('reset');
                if (result != undefined) {
                    if (parseInt(result.status) == 0) {
                        PNotifyAlerta('Info', 'No se pudo ejecutar operacion\n'+result.msg, "error");
                        return;
                    }else{
                        PNotifyAlerta('Info', result.msg, "success");
                        $("#tPersonas").DataTable().ajax.reload(null, false);
                        return;
                    }   
                }else {
                    PNotifyAlerta("Atencion", "No se realizo la acción", "error");
                    return;
                }
            }, 'json');
            
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar solo una persona de la tabla", "notice");
        }
    }    

    // function buscarWeb() {
    //     let burl = $("#burl").html() + "ExpedientesController/buscarWeb";
    //     let rs = tp.rows({ selected: true }).data();
    //     let r = tp.row({ selected: true }).data();
    //     if (rs.length == 1) {
    //         $("#_btnWeb").button('loading');
    //         $.post(burl, { data: r.id_persona,fuente:"X"}, function(result) {
    //             $("#_btnWeb").button('reset');
    //             if (result != undefined) {
    //                 if (result.json != undefined) {
    //                     $("#tPersonas").DataTable().ajax.reload();
    //                     if(result.json.noErrores!= undefined){
    //                         if (result.json.noErrores == 1) {
    //                             PNotifyAlerta("Atencion","La empresa:"+r.empresa+" "+result.json.mensaje, "success");
    //                         } 
    //                     }else{
    //                         for (var i = 0; i < result.json.length; i++) {
    //                             let res=JSON.parse(result.json[i]);
    //                             let arr = res.split("[");
    //                             let arr1 = arr[1].split("]");
    //                             let resultados = JSON.parse(arr1[0]);
    //                             if (resultados.NoErrores == 0) {
    //                                 PNotifyAlerta("Atencion","Se analizarón "+resultados.NoUrls+" resultados, obteniendo :"+resultados.NoComentarios+" coincidendias en el modulo :"+resultados.Proceso+", para la persona:"+ resultados.Persona, "success");
    //                             } 
    //                         }
    //                     }        
    //                 } else {
    //                     PNotifyAlerta("Atencion", "No se realizo la acción", "error");
    //                     return;
    //                 }
    //             } else {
    //                 PNotifyAlerta("Atencion", "No se realizo la acción", "error");
    //             }
    //         }, 'json');
    //     } else {
    //         PNotifyAlerta("Atención", "Debe seleccionar una persona de la tabla", "notice");
    //     }
    // }

    function buscarWeb() {
        let rs = tp.rows({ selected: true } ).data();
        let r = tp.row({ selected: true } ).data();
        if (rs.length == 1) { 
            let burl = $("#burl").html() + "ExpedientesController/buscarWeb";
            $.post(burl, { data: r.id_persona }, function(result) {
                // $("#_btnOSINT").button('reset');
                if (result != undefined) {
                    if (parseInt(result.status) == 0) {
                        PNotifyAlerta('Info', 'No se pudo ejecutar operacion\n'+result.msg, "error");
                        return;
                    }else{
                        PNotifyAlerta('Info', result.msg, "success");
                        $("#tPersonas").DataTable().ajax.reload(null, false);
                        return;
                    }   
                }else {
                    PNotifyAlerta("Atencion", "No se realizo la acción", "error");
                    return;
                }
            }, 'json');
            
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar solo una persona de la tabla", "notice");
        }
    }    


    function abrirVerExpediente(){
        let r = tp.rows({ selected: true } ).data();
        let r1 = tp.row({ selected: true } ).data();
        if(r.length == 1) {
            //$("#tnoticias").DataTable().ajax.reload();
            $("#fuente").val('N').trigger("change");
            abrirExpediente(0,'N');
            abreVentanaModal("verExpedienteDialog", "Expediente de: "+capitalizeWords(r1.nombre)+" (#. "+r1.id_persona+"), " + r1.email);
        } else {
            PNotifyAlerta("Atención", "Para Ver Detalles de Una persona, debe seleccionar solo una persona de la tabla", "notice");
        }
	}

    function renderNoticia(row){
		var html='<ul class="result-list alert-secondary">'+
					'<li>'+
						//'<a href="#" class="result-image" style="background-image: url(assets/imagenes_noticias/'+row.hash_url+'.JPG)"></a>'+
						'<div class="result-info">'+
						'<h4 class="title"><a href="javascript:;">'+row.target+'</a></h4>'+
						'<p class="location">'+row.url+'</p>'+
						'<p class="desc" style="height:1000px;overflow-y: scroll;" >"'+row.body_dom+'"</p>'+
						'</div>'
						'</li></ul>';
						return html;
	}

    function renderNoNoticia(row, filesData = 0){
        var html = '';
        if (filesData==1){
            if (row.trim()==''){
                row = 'No hay datos disponibles';
            }
            html='<ul class="result-list alert-secondary">'+
            '<li>'+
                //'<a href="#" class="result-image" style="background-image: url(assets/imagenes_noticias/'+row.hash_url+'.JPG)"></a>'+
                '<div class="result-info">'+
                '<p    >'+row+'</p>'+
                '</div>'
                '</li></ul>';
        }else{
		 html='<ul class="result-list alert-secondary">'+
					'<li style="overflow-x:auto">'+
						//'<a href="#" class="result-image" style="background-image: url(assets/imagenes_noticias/'+row.hash_url+'.JPG)"></a>'+
						'<div class="result-info">'+
						'<h4 class="title"><a href="javascript:;">'+row.target+'</a></h4>'+
						'<p class="location">'+row.url+'</p>'+
						'<p >"'+row.body_dom+'"</p>'+
						'</div>'
						'</li></ul>';
        }
		return html;
	}

})(jQuery);

//capitalize all words of a string. 
function capitalizeWords(string) {
    return string.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};