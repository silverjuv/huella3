let idApiConsulta = 0;
let intervaloRecargaTablas = 30000; // n segundos... 

(function ($) {
    var tr;
    var ta;
    var volts;
    var tc;
    var tp;

    $(document).ready(init());

    function init() {
        initTables();
        $("#_btnDetalles").on("click", function () {
            verDetalles();
        });

        $("#_enviarAproceso").on("click", function () {
            abreVentanaModal("enviarAnalizarDialog", "Seleccione una persona y puesto para enviar a analizar");
        });

        $("#_enviar").on("click", function () {
            enviarAnalisis();
        });

        $("#_exportar").on("click", function () {
            exportData();
        });
        

        $("#sendmailLote").on("click", function () {
            sendMailLote();
        });

        $("#_btnInforme").on("click", function () {
            verInformeIndividual();             
        });

        $("#exportData").on("click", function () {
            abreVentanaModal("exportarDialog", "Exportar a...");
        });

        $.fn.button = function (action) {
            if (action === 'loading' && this.data('loading-text')) {
                this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
                this.html(this.data('original-text')).prop('disabled', false);
            }
        };
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            tc.columns.adjust().draw();
            tr.columns.adjust().draw();
        });

        $('#verResultadosDialog').on('shown.bs.modal', function (e) {
            ta.columns.adjust().draw();
        });

        $('#enviarAnalizarDialog').on('shown.bs.modal', function (e) {
            tp.columns.adjust().draw();
        });
    }

    function initTables() {
        //NProgress.start();			  
        var burl = $("#burl").html() + "ResultadosOdometroController/verResultados";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";
        tr = $("#tResultados").DataTable({
            "scrollY": "400px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function (d) { }
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control' },
                { "data": "id_persona", "visible": false },
                {
                    "data": "tipo_entrada",
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        var span = '<span class="badge badge-info">API</span>';
                        if (data == "W") {
                            span = '<span class="badge badge-ligth"><i class="fas fa-at"></i> Web</span>';
                        } else if (data == "A") {
                            span = '<span class="badge badge-info"><i class="fas fa-filter"></i> API</span>';
                        } else if (data == "L") {
                            span = '<span class="badge badge-green"><i class="fas fa-file-excel"></i> LAYOUT</span>';
                        }
                        return span;
                    }
                },
                { "data": "fecha_resultado", "orderable": false },
                { "data": "curp", "orderable": false },
                { "data": "nombre", "orderable": false },
                { "data": "email", "visible": false },
                { "data": "celular", "visible": false },
                { "data": "ine", "visible": false },
                { "data": "direccion", "visible": false },
                { "data": "empresa", "orderable": false },
                { "data": "puesto", "orderable": false },
                { "data": "riesgo", "orderable": false },
                { "data": "ponderacion", "orderable": false },
                { "data": "lote", "orderable": false },
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan
            },
            "select": {
                'style': 'single'
            },
            dom: "<'row'<'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'><'col-sm-7'p>>",
            lengthMenu: [
                [10],
                ['10']
            ]
        });
        
        setInterval( function () {
            tr.ajax.reload();
        }, intervaloRecargaTablas ); 

        var burl = $("#burl").html() + "ResultadosOdometroController/verRiesgosAsociados";
        ta = $("#tRiesgosAsociados").DataTable({
            "scrollY": "200px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function (d) {
                    var persona = 0;
                    let p = tr.row({ selected: true }).data();
                    if (p != undefined) {
                        persona = p.id_persona;
                    }
                    d.id_persona = persona;
                }
            },
            "dataSrc": 'data',
            "columns": [
                // { "data": "acciones", "targets": 0, 'className': 'control' },
                // { "data": "puesto", "orderable": false },
                { "data": "riesgo", "orderable": false },
                { "data": "ponderacion", "orderable": false },
                { "data": "combo", "orderable": false },
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan,
                "select": {
                    "rows": {
                        "_": "%d Filas seleccionadas",
                        "1": "1 Fila seleccionada"
                    }
                }
            },
            "select": {
                'style': 'single'
            },
            dom: "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'><'col-sm-7'p>>",
            lengthMenu: [
                [10],
                ['10']
            ]
        });

        var burl = $("#burl").html() + "ResultadosOdometroController/listarPuestos";
        $.post(burl, {}, function (resultl) {
            if (resultl != undefined) {
                $("#id_puesto").select2({
                    debug: true,
                    data: resultl,
                    placeholder: 'Selecciona un puesto',
                    allowClear: true,
                    with: 'resolve'
                });
                $("#id_puesto").val(null).trigger("change");
            }
        }, 'json');

        var burl = $("#burl").html() + "ResultadosOdometroController/listarAPIconsultas";
        tc = $("#tConsultas").DataTable({
            "scrollY": "400px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST"
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control'  },
                // { "data": "cliente", "orderable": false },
                { "data": "id_persona", "visible": false },
                { "data": "nombre", "orderable": false },
                { "data": "puesto", "orderable": false },
                { "data": "fecha_programacion", "orderable": false },                
                {
                    "data": "status_osint",
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        // alert(data);
                        var span = '<span class="badge badge-info"><i class="fas fa-ban"></i> No aplica</span>';
                        if (data == "E") {
                            span = '<span class="badge badge-warning"><i class="fas fa-clock"></i> En espera</span>';
                        } else if (data == "C") {
                            span = '<span class="badge badge-danger"><i class="fas fa-imes-circle"></i> Cancelada</span>';
                        } else if (data == "P") {
                            span = '<span class="badge badge-success"><i class="fas fa-tasks"></i> Procesada</span>';
                        }  
                        return span;
                    }
                },
                {
                    "data": "status_web",
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        var span = '<span class="badge badge-info"><i class="fas fa-ban"></i> No aplica</span>';
                        if (data == "E") {
                            span = '<span class="badge badge-warning"><i class="fas fa-clock"></i> En espera</span>';
                        } else if (data == "C") {
                            span = '<span class="badge badge-danger"><i class="fas fa-imes-circle"></i> Cancelada</span>';
                        } else if (data == "P") {
                            span = '<span class="badge badge-success"><i class="fas fa-tasks"></i> Procesada</span>';
                        }  
                        return span;
                    }
                },
                {
                    "data": "status_redes",
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        var span = '<span class="badge badge-info"><i class="fas fa-ban"></i> No aplica</span>';
                        if (data == "E") {
                            span = '<span class="badge badge-warning"><i class="fas fa-clock"></i> En espera</span>';
                        } else if (data == "C") {
                            span = '<span class="badge badge-danger"><i class="fas fa-imes-circle"></i> Cancelada</span>';
                        } else if (data == "P") {
                            span = '<span class="badge badge-success"><i class="fas fa-tasks"></i> Procesada</span>';
                        }  
                        return span;
                    }
                },
                {
                    "data": "status_dark",
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        var span = '<span class="badge badge-info"><i class="fas fa-ban"></i> No aplica</span>';
                        if (data == "E") {
                            span = '<span class="badge badge-warning"><i class="fas fa-clock"></i> En espera</span>';
                        } else if (data == "C") {
                            span = '<span class="badge badge-danger"><i class="fas fa-imes-circle"></i> Cancelada</span>';
                        } else if (data == "P") {
                            span = '<span class="badge badge-success"><i class="fas fa-tasks"></i> Procesada</span>';
                        } 
                        return span;
                    }
                },
                {
                    "data": "status_noticias",
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        var span = '<span class="badge badge-info"><i class="fas fa-ban"></i> No aplica</span>';
                        if (data == "E") {
                            span = '<span class="badge badge-warning"><i class="fas fa-clock"></i> En espera</span>';
                        } else if (data == "C") {
                            span = '<span class="badge badge-danger"><i class="fas fa-imes-circle"></i> Cancelada</span>';
                        } else if (data == "P") {
                            span = '<span class="badge badge-success"><i class="fas fa-tasks"></i> Procesada</span>';
                        }  
                        return span;
                    }
                },
                { "data": "usuario", "orderable": false },
                {
                    "data": "tipo_entrada",
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        var span = '<span class="badge badge-info">API</span>';
                        if (data == "W") {
                            span = '<span class="badge badge-ligth"><i class="fas fa-at"></i> Web</span>';
                        } else if (data == "A") {
                            span = '<span class="badge badge-info"><i class="fas fa-filter"></i> API</span>';
                        } else if (data == "L") {
                            span = '<span class="badge badge-green"><i class="fas fa-file-excel"></i> LAYOUT</span>';
                        } 
                        return span;
                    }
                }
                // {
                //     "data": "status_evento",
                //     "orderable": false,
                //     "render": function (data, type, row, meta) {
                //         var span = '<span class="badge badge-info">En espera</span>';
                //         if (data == "E") {
                //             span = '<span class="badge badge-warning"><i class="fas fa-clock"></i> En espera</span>';
                //         } else if (data == "C") {
                //             span = '<span class="badge badge-danger"><i class="fas fa-imes-circle"></i> Cancelada</span>';
                //         } else if (data == "P") {
                //             span = '<span class="badge badge-success"><i class="fas fa-tasks"></i> Procesada</span>';
                //         }
                //         return span;
                //     }
                // },
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan,
                "select": {
                    "rows": {
                        "_": "%d Filas seleccionadas",
                        "1": "1 Fila seleccionada"
                    }
                }
            },
            "select": {
                'style': 'single'
            },
            dom: "<'row'<'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'><'col-sm-7'p>>",
            lengthMenu: [
                [10],
                ['10']
            ]
        });

        setInterval( function () {
            tc.ajax.reload();
        }, intervaloRecargaTablas ); 

        var burl = $("#burl").html() + "ResultadosOdometroController/listarPersonas/";
        tp = $("#tpersonas").DataTable({
            "scrollY": "200px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function (d) {

                }
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control' },
                { "data": "id_persona", "visible": false },
                { "data": "nombre", "orderable": false },
                { "data": "curp", "orderable": false },
                { "data": "ine", "orderable": false },
                { "data": "alias", "orderable": false },
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan,
                "select": {
                    "rows": {
                        "_": "%d Filas seleccionadas",
                        "1": "1 Fila seleccionada"
                    }
                }
            },
            "select": {
                'style': 'single'
            },
            dom: "<'row'<'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'><'col-sm-7'p>>",
            lengthMenu: [
                [10],
                ['10']
            ]
        });
    }


    function verDetalles() {
        let r = tr.row({ selected: true }).data();

        if (r != null) {
            idApiConsulta = r.id_api;

            $("#spamMayorRiesgo").html(r.riesgo + ", riesgo de mayor ponderación con " + r.ponderacion);
            abreVentanaModal("verResultadosDialog", "Resultados de: " + r.nombre);
            $("#demo").myfunc({
                /**Max value of the meter*/
                val: r.ponderacion,
                /**Max value of the meter*/
                maxVal: 10,
                /**Division value of the meter*/
                divFact: 1,
                /**more than this leval, color will be red*/
                initLevel: 0,
                /**more than this leval, color will be red*/
                infoLevel: 1,
                /**more than this leval, color will be red*/
                warningLevel: 4,
                /**more than this leval, color will be red*/
                dangerLevel: 7,
                /**reading begins angle*/
                initDeg: -45,
                /**indicator number width*/
                numbW: 40,
                /**indicator number height*/
                numbH: 18,
                /**Label on guage Face*/
                gagueLabel: 'Riesgo'
            });
            $("#demo").hide();
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar una persona de la tabla", "notice");
        }
    }

    function enviarAnalisis() {
        var r = tp.row({ selected: true }).data();
        var id_puesto = $("#id_puesto").val();
        if (r === undefined) {
            PNotifyAlerta("Atención", "Debe seleccionar una persona de la tabla", "notice");
            return;
        }
        if (id_puesto === null) {
            PNotifyAlerta("Atención", "Debe Seleccionar un Puesto del listado...", "notice");
            return;
        }
        $("#_enviar").button('loading');
        var burl = $("#burl").html() + "ResultadosOdometroController/enviarAnalisis/";
        $.post(burl, { "id_persona": r.id_persona, "id_puesto": id_puesto }, function (result) {
            if (result != undefined) {
                $("#_enviar").button('reset');
                if (result.error == 0) {
                    $("#tConsultas").DataTable().ajax.reload();
                    $("#enviarAnalizarDialog").modal("hide");
                    PNotifyAlerta("Atencion", result.msg, "success");
                } else if (result.error == 1) {
                    PNotifyAlerta("Atencion", result.msg, "error");
                }
            }
        }, 'json');
    }

    function exportData() {
        
        var radioValue = $("input[name='optradio']:checked").val();
        var burl = $("#burl").html() + "ResultadosOdometroController/exportData/?format="+radioValue;
        // var burl = $("#burl").html() + "ResultadosOdometroController/exportCSV";
        window.location=burl;        
    }
    
    function sendMailLote() {
        let r = tr.row({ selected: true }).data();
    
        if (r != null) {
            $('#sendmailLote').prop('disabled', true);
            const notice = PNotifyConfirm("Confirme su acción", "¿Realmente desea enviar al email del cliente resultados del Lote "+r.lote+"?", "Si, proceder");
            notice.on('pnotify.confirm', (v) => {
                let burl = $("#burl").html() + "ResultadosOdometroController/sendMailLote";
                // let id_contrato = $("#id_edicion").val();
                $.post(burl, { lote: r.lote }, function (result) {
                    if (result != undefined) {
                        PNotifyAlerta(result.title, result.msg, result.type);
                        //$("#api_key").text(result.apiKey);
    
                    }
                    $('#sendmailLote').prop('disabled', false);
                    // alert('ok')
                }, 'json');
            });
            notice.on('pnotify.cancel', () => {
                console.log("rejected");
                $('#sendmailLote').prop('disabled', false);
            });
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar una registro de la tabla", "notice");
        }
    }

    function verInformeIndividual() {
        let r = tr.row({ selected: true }).data();

            if (r != null) {
                idApiConsulta = r.id_api;           
                var burl = $("#burl").html() + "ResultadosOdometroController/getResultadosInforme";
                $.post(burl, {id_api: idApiConsulta, ponderacion : r.ponderacion, "id_persona": r.id_persona}, function (resultl) {
                    if (resultl != undefined) { 
                        $("#td_resultados").html(resultl.msg);
                        // datos persona
                        $("#nombre").val(resultl.persona.nombre + " " + resultl.persona.paterno + " " + resultl.persona.materno);
                        $("#ine").val(resultl.persona.ine);
                        $("#curp").val(resultl.persona.curp);
                        $("#email").val(resultl.persona.email);
                        $("#celular").val(resultl.persona.celular);
                        $("#twitter").val(resultl.persona.twitter);
                        $("#linkedin").val(resultl.persona.linkedin);
                        $("#alias").val(resultl.persona.alias);
                        $("#direccion").val(resultl.persona.direccion);

                        $("#ponderacion").val(r.ponderacion);

                        abreVentanaModal("informeDialog", "Informe Individual");
                    }
                }, 'json');
            } else {
                PNotifyAlerta("Atención", "Debe seleccionar una persona de la tabla", "notice");
            }   
    }

})(jQuery);

function sendMail() {
    $('#sendmail').prop('disabled', true);
    const notice = PNotifyConfirm("Confirme su acción", "¿Realmente desea enviar al email del cliente el resultado de esta persona-puesto?", "Si, proceder");
    notice.on('pnotify.confirm', (v) => {
        let burl = $("#burl").html() + "ResultadosOdometroController/sendMail";
        // let id_contrato = $("#id_edicion").val();
        $.post(burl, { id_api: idApiConsulta }, function (result) {
            if (result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);
                //$("#api_key").text(result.apiKey);

            }
            $('#sendmail').prop('disabled', false);
            // alert('ok')
        }, 'json');
    });
    notice.on('pnotify.cancel', () => {
        console.log("rejected");
        $('#sendmail').prop('disabled', false);
    });
    
}

