(function($) {
    var tr;
    var ts;
    let _avanceCtrl, _avanceValue=0

    $(document).ready(init());

    function init() {
        $("#_agregar").on("click", mostrarAgregar);
        $("#_editar").on("click", mostrarEditar);
        $("#_borrar").on("click", confirmarBorrado);
        $("#_reload").on("click", reloadListado);
        //_reload
        // $("#_borrarSubriesgo").on("click", confirmarBorradoSubriesgo);

        initTables();
        $.fn.button = function(action) {
            if (action === 'loading' && this.data('loading-text')) {
              this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
              this.html(this.data('original-text')).prop('disabled', false);
            }
        };
        $('#__modal').on('shown.bs.modal', function (e) {
    		ts.columns.adjust().draw();
        });	

        $("#ponderacion").ionRangeSlider({
            min: 0,
            max: 10,
            grid: true,
            skin: "modern",
            onChange: function(data) {
                _avanceValue = data.from;
            }
        });

        _avanceCtrl = $("#ponderacion").data("ionRangeSlider");
        _avanceCtrl.update({
            from: 0
        });

        
        /*$("#verbo").on('change',function(event){
            if(event.isTrigger==undefined){
            
            let r = tr.row({selected: true}).data();
            if(r != null) {
                if (r.verbo=="false"&&event.target.checked) {
                    eliminarSubriesgos();
                } else if(r.verbo=="true"&&event.target.checked==false){
                    eliminarSubriesgos();
                }else{
                    $("#contenedorTabla").hide();
                }
            }
            }      
        })*/
        $("#verbo").on('change',function(event){
            if ($("#verbo").prop('checked')){
                // $("#tSubriesgos").removeClass( " responsive" );
                $("#contenedorTabla").hide();
            }else{
                $("#contenedorTabla").show();
                // $("#tSubriesgos").addClass( " responsive" );
                var table = $('#tSubriesgos').DataTable();
                table.columns.adjust().draw();
            }
        });
    }

    function reloadListado(){
        $("#tRiesgos").DataTable().ajax.reload();
    }

    function initTables() {

        var burl = $("#burl").html() + "CatRiesgosController/listarAreaRiesgos";
        $.post(burl, {}, function (result) {
            if (result != undefined) {
                $("#id_area").select2({
                    data: result,
                    placeholder: 'Selecciona una area',
                    allowClear: true,
                    with: 'resolve'
                });
                $("#id_area").val(null).trigger("change");
            }
        }, 'json');

        var burl = $("#burl").html() + "CatRiesgosController/listarRiesgos";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";
        tr = $("#tRiesgos").DataTable({
            "scrollY": "400px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST"
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control' },
                { "data": "id_riesgo", "visible": false },
                { "data": "descripcion", "orderable": false },
                { "data": "verbo","visible": false},
                { "data": "ponderacion","visible": false},
                { "data": "id_area","visible": false},
                { "data": "subriesgos","visible": true},
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan
                // "search": "Buscar",
                // "select": {
                //     "rows": {
                //         "_": "%d Filas seleccionadas",
                //         "1": "1 Fila seleccionada"
                //     }
                // }
            },
            "select": {
                'style': 'single',
                'selector': 'td:not(.control)'
            },
            dom: "<'row'<'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "rowCallback": function(row, data, index) {
                $('td', row).attr('class', 'table-active');
                data.verbo=(data.verbo == "1") ?"true" : "false";
            }
        });

       
        
        var burl = $("#burl").html() + "CatRiesgosController/listarSubriesgos";
        ts = $("#tSubriesgos").DataTable({
                "scrollY": "200px",
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": burl,
                    "type": "POST",
                    "data": function ( d ) {
                        let r = tr.row({ selected: true }).data();
                        if(r != null) {
                           d.id_riesgo = r.id_riesgo;
                        }else{
                           d.id_riesgo = 0;
                        }
                    }
                },
                "dataSrc": 'data',
                "columns": [
                    { "data": "acciones", "targets": 0, 'className': 'control', "visible": false },
                    { "data": "id_riesgo", "visible": false },
                    { "data": "subriesgo", "orderable": false },
                    { "data": "borrar", "orderable": false }
                ],
                "responsive": {
                    'details': {
                        'type': 'column',
                        'target': 0
                    }
                },
                "language": {
                    "url": lan,
                    // "select": {
                    //     "rows": {
                    //         "_": "%d Filas seleccionadas",
                    //         "1": "1 Fila seleccionada"
                    //     }
                    // }
                },
                "select": {
                    'style': 'single',
                    'selector': 'td:not(.control)'
                },
                dom: "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-7'p>>",
                "rowCallback": function(row, data, index) {
                    $('td', row).attr('class', 'table-active');
                }   
            });


        $('#formRiesgo').bootstrapValidator({
            excluded: ':disabled',
           live:true,
           framework: 'bootstrap',
           feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
           },
           fields: {
               descripcion: {
                   validators: {
                       notEmpty: {
                           message: 'Introduzca un riesgo'
                       },
                       stringLength: {
                           max:100,
                           message: 'El tamaño permitido 100 caracteres'
                       }
                   }
               },
               ponderacion: {
                    validators: {
                        integer : {
                            message : 'seleccione una ponderacion',
                            min : 1,
                            max : 10
                        }
                    }
                },
                id_area: {
                    validators: {
                        notEmpty: {
                            message: 'Seleccione una area de riesgo'
                        }
                    }
                }
           }
       });
       
       $('#formRiesgo').bootstrapValidator().on('success.form.bv', function(e) {
           // previene el formulario antes de enviarse
           e.preventDefault();	
           guardar();    
        });

        $('#formSubriesgo').bootstrapValidator({
            excluded: ':disabled',
           live:true,
           framework: 'bootstrap',
           feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
           },
           fields: {
               subriesgo: {
                   validators: {
                       notEmpty: {
                           message: 'Introduzca un subriesgo'
                       },
                       stringLength: {
                           max:100,
                           message: 'El tamaño permitido 100 caracteres'
                       }
                   }
               }
           }
       });
       
       $('#formSubriesgo').bootstrapValidator().on('success.form.bv', function(e) {
           // previene el formulario antes de enviarse
           e.preventDefault();	
           guardarSubriesgo();    
        });
    }


    function mostrarAgregar(){
        $('#formRiesgo').form('clear');
        $('#formRiesgo').bootstrapValidator('resetForm', true);
        // $("#verbo").removeAttr("disabled");
        $("#ponderacion").val("0").trigger("change");
        $("#id_area").val(null).trigger("change");
        _avanceCtrl.update({
            from: 0
        });
        _avanceValue = 0;
        $("#action").val("1");
        $("#contenedorTabla").hide();
        $("#tSubriesgos").DataTable().ajax.reload();
        abreVentanaModal("__modal", "Agregar Riesgo");
    }

    function mostrarEditar(){
        let r = tr.row({selected: true}).data();
        if(r != null) {
            $('#formRiesgo').form('clear');
            $('#formRiesgo').bootstrapValidator('resetForm', true);
            $("#action").val("0");
            $("#formRiesgo").form('load',r);
            // $("#verbo").attr("disabled", true);
            $("#ponderacion").val(r.ponderacion);
            $("#id_area").val(r.id_area).trigger("change");
            _avanceCtrl.update({
                from: r.ponderacion
            });
            _avanceValue = r.ponderacion;
            if(r.verbo=="false"){
                $("#contenedorTabla").show();
                $("#tSubriesgos").DataTable().ajax.reload();
            }else{
                $("#contenedorTabla").hide();
            }
            abreVentanaModal("__modal", "Editar Riesgo y agregar verbos");
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
        }    
    }

    
    function confirmarBorrado() {
        let r = tr.row({selected: true}).data();
        if(r != null) {
            const notice = PNotifyConfirm("Confirme su acción", "¿Desea borrar el Riesgo: "+r.descripcion+"?", "Si, proceder");
            notice.on('pnotify.confirm', (v) => {
                $("#btnGuardar").button('loading');
                let burl = $("#burl").html()+"CatRiesgosController/eliminar";
                $.post(burl, {id_riesgo: r.id_riesgo }, function (result) {
                    if (result != undefined) {
                        $("#btnGuardar").button('reset');
				        if(result.error=="0"){
                            $("#tRiesgos").DataTable().ajax.reload();
                            PNotifyAlerta("Atencion", result.msg, "success");
                        }else if(result.error=="1"){
                            PNotifyAlerta("Atencion", result.msg, "error");
                        }    
                    }
                }, 'json');
                });
            notice.on('pnotify.cancel', () => {
                console.log("rejected")
            });
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
        }
    }
    
    function guardar(){
        var burl = $("#burl").html()+"CatRiesgosController/guardar";
	    $("#formRiesgo").form('submit',{
		    ajax:true,
		    iframe:false,
		    url:burl,
		    onProgress:function(e){
			    $("#btnGuardar").button('loading');
		    },
		    success:function(data){
			    if(data !== undefined && data !== null){
                    var datos=JSON.parse(data);
                    $("#btnGuardar").button('reset');
				    if(datos.error=="0"){
					    $("#tRiesgos").DataTable().ajax.reload();
					    $("#__modal").modal("hide");
					    PNotifyAlerta("Atencion", datos.msg, "success");
				    }else if(datos.error=="1"){
                        PNotifyAlerta("Atencion", datos.msg, "error");
				    }
			    }
		    }
	    });
     }

     function guardarSubriesgo(){
        let r = tr.row({selected: true}).data();
        if(r != null) {    
            var burl = $("#burl").html()+"CatRiesgosController/guardarSubriesgo";
	        $("#formSubriesgo").form('submit',{
		        ajax:true,
		        iframe:false,
                url:burl,
                onSubmit: function(param){
                    param.id_riesgo =r.id_riesgo;
                },
		        onProgress:function(e){
			        $("#_agregarSubriesgo").button('loading');
		        },
		        success:function(data){
			        if(data !== undefined && data !== null){
                        var datos=JSON.parse(data);
                        $("#_agregarSubriesgo").button('reset');
				        if(datos.error=="0"){                     
                            $('#formSubriesgo').form('clear');
                             $('#formSubriesgo').bootstrapValidator('resetForm', true);
					        $("#tSubriesgos").DataTable().ajax.reload();
					        PNotifyAlerta("Atencion", datos.msg, "success");
				        }else if(datos.error=="1"){
                            PNotifyAlerta("Atencion", datos.msg, "error");
				        }
			        }
		        }
            });
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla de riesgos", "notice");
        }   
    }

    

    // function confirmarBorradoSubriesgo() {
    //     let s = ts.row({selected: true}).data();
    //     if(s != null) {
    //         const notice = PNotifyConfirm("Confirme su acción", "¿Desea borrar el verbo: "+s.subriesgo+"?", "Si, proceder");
    //         notice.on('pnotify.confirm', (v) => {
    //             $("#_borrarSubriesgo").button('loading');
    //             let burl = $("#burl").html()+"CatRiesgosController/eliminarSubriesgo";
    //             $.post(burl, {id_riesgo: s.id_riesgo,subriesgo: s.subriesgo.trim()}, function (result) {
    //                 if (result != undefined) {
    //                     $("#_borrarSubriesgo").button('reset');
	// 			        if(result.error=="0"){
    //                         $("#tSubriesgos").DataTable().ajax.reload();
    //                         PNotifyAlerta("Atencion", result.msg, "success");
    //                     }else if(result.error=="1"){
    //                         PNotifyAlerta("Atencion", result.msg, "error");
    //                     }    
    //                 }
    //             }, 'json');
    //             });
    //         notice.on('pnotify.cancel', () => {
    //             console.log("rejected")
    //         });
    //     } else {
    //         PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
    //     }
    // }
})(jQuery);

function delSubRiesgo(id_riesgoz, subriesg) {
    // let s = ts.row({selected: true}).data();
    // if(s != null) {
        const notice = PNotifyConfirm("Confirme su acción", "¿Desea borrar el verbo: "+subriesg+"?", "Si, proceder");
        notice.on('pnotify.confirm', (v) => {
            // $("#_borrarSubriesgo").button('loading');
            let burl = $("#burl").html()+"CatRiesgosController/eliminarSubriesgo";
            $.post(burl, {id_riesgo: id_riesgoz, subriesgo: subriesg.trim()}, function (result) {
                if (result != undefined) {
                    // $("#_borrarSubriesgo").button('reset');
                    if(result.error=="0"){
                        $("#tSubriesgos").DataTable().ajax.reload();
                        PNotifyAlerta("Atencion", result.msg, "success");
                    }else if(result.error=="1"){
                        PNotifyAlerta("Atencion", result.msg, "error");
                    }    
                }
            }, 'json');
            });
        notice.on('pnotify.cancel', () => {
            console.log("rejected")
        });
    // } else {
    //     PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
    // }
}