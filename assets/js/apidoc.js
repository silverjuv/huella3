(function($) {
    $(document).ready(init);

    function init() {
        $.preloader.stop();

        $("#lenguaje").change(function() {
            //alert('ok')
            reloadData();
        });

        //setInterval(reloadData, 10000);
        reloadData();

    }

})(jQuery);

//unavez = false;

function reloadData() {
    let burl = $("#burl").html() + "ApiDocController/reloadLog";

    $.post(burl, { lengua: $("#lenguaje").val() }, function(result) {
        if (result != undefined) {
            //$(".code").html(result.log);
            // alert(result.data);
            $("#dv_logs").html(
                '<br /><pre class="code" data-language="php">' + result.data + '</pre>'
            );

            // if (unavez == false) {
            $('pre.code').highlight({ source: 0, zebra: 1, indent: 'tabs', attribute: 'data-language' });
            //     unavez = true;
            // }
        }
    }, 'json');
}

$.preloader.start({
    modal: true
});