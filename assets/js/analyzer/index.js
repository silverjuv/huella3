(function($) {
    $(document).ready(init);

    var tablePersonas = null;

    function init() {
        // $("#_agregar").on("click", function() { irAlFormulario(false); });
        // $("#_editar").on("click", function() { irAlFormulario(true); });
        $("#_analizar").on("click", programarAnalisis);

        $("#_exportar").on("click", function () {
            exportData();
        });

        $("#exportData").on("click", function () {
            abreVentanaModal("exportarDialog", "Exportar a...");
        });
                
        initializeTable();

        var burl = $("#burl").html() + "ResultadosOdometroController/listarPuestos";
        $.post(burl, {}, function (resultl) {
            if (resultl != undefined) {
                $("#id_puesto").select2({
                    debug: true,
                    data: resultl,
                    placeholder: 'Selecciona un puesto',
                    allowClear: true,
                    with: 'resolve'
                });
                $("#id_puesto").val(null).trigger("change");
            }
        }, 'json');
    }

    function initializeTable() {
        let burl = $("#burl").html() + "AnalyzerController/listar/";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";

        tablePersonas = $("#tpersonas").DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function(d) {

                }
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "id_persona", "visible": false },
                { "data": "seleccionar" },
                { "data": "curp" },
                // { "data": "ine" },
                { "data": "nombre" },
                { "data": "email" },
                { "data": "celular" },
                { "data": "analisis" }
            ],
            "responsive": true,
            "info": false,
            "lengthChange": false,
            "language": {
                "url": lan
            },
            "dom": '<f<t>p>',
            "select": {
                "style": 'single'
            }

        });
    }

    function programarAnalisis(editar) {
        
        var dataDoctos = tablePersonas.rows().data();
        // alert(dataDoctos);
        cves_folios = '';
        $.each(dataDoctos, function(key,valx) {             
            // alert(valx.id_persona); 
            if ($("#ck"+valx.id_persona).prop('checked')){
                cves_folios+=", " + valx.id_persona;
            }   
        }); 

        if (cves_folios == "") {
            PNotifyAlerta("Atención", "No se han seleccionado personas", "notice");
            return;
        }  

        
        // alert(datos[1])
        // return;
        if ($("#id_puesto").val() == "" || $("#id_puesto").val() == undefined) {
            PNotifyAlerta("Atención", "No se han seleccionado puesto", "notice");
            return;
        }  

        var data = $('#id_puesto').select2('data')
        var datos = data[0].text.split("--");
        
        if (parseInt(datos[1])<1) {
            PNotifyAlerta("Atención", "Debe seleccionar un puesto con minimo 1 riesgo asociados", "notice");
            return;
        }  
        
        
        let obj = {};
        obj.ids = cves_folios.substr(1);
        obj.idpuesto = $("#id_puesto").val();

        // if (r != null) {
            const notice = PNotifyConfirm("Confirme su acción", "¿Desea iniciar los analisis de registros selecccionados?", "Si, proceder");

            notice.on('pnotify.confirm', async(v) => {
                let res = await makePostRequest("AnalyzerController/iniciarAnalisis", obj);
                if (res != undefined) {
                    PNotifyAlerta(res.title, res.msg, res.type);
                    if (res.type != "error") {
                        let t = $("#tpersonas").DataTable();
                        t.ajax.reload();
                    }
                }
            });
            notice.on('pnotify.cancel', () => {
                console.log("rejected")
            });
        // } else {
        //     PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
        // }
    }

    function exportData() {
        
        var radioValue = $("input[name='optradio']:checked").val();
        var burl = $("#burl").html() + "CatPersonasController/exportData/?format="+radioValue;
        // var burl = $("#burl").html() + "ResultadosOdometroController/exportCSV";
        window.location=burl;        
    }

})(jQuery);