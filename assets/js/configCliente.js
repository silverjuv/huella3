(function ($) {
    $(document).ready(init);

    function init() {
        $("#btnGuardar").on("click", guardar );

        // alert(' ok 6666');
        obtenerConfiguracion();
    }

    function obtenerConfiguracion() {
        // alert(' ok')
        let burl = $("#burl").html() + "ConfigClienteController/obtenerConfiguracion";
        // let t = $("#tabla").DataTable();
        // let r = t.row({ selected: true }).data();

        // if (r != null) {
        $.post(burl, {}, function (result) {
            if (result != undefined) {
                result.data.forEach(row => {
                    //console.log(row['id_config_cliente'])
                    switch (parseInt(row['id_config_cliente'])) {
                        case 1:
                            if (parseInt(row['valor'])==1){
                                $("#reprocesar").prop ("checked", true );
                            }else{
                                $("#reprocesar").prop ("checked", false );
                            }
                            break;
                        // case 2:
                        //     if (parseInt(row['valor'])==1){
                        //         $("#regenerarexpediente").prop ("checked", true );
                        //     }else{
                        //         $("#regenerarexpediente").prop ("checked", false );
                        //     }
                        //     break;
                        // case 3:
                        //     if (parseInt(row['valor'])==1){
                        //         $("#recibiremail").prop ("checked", true );
                        //     }else{
                        //         $("#recibiremail").prop ("checked", false );
                        //     }
                        //     break;
                    }
                })

            }
        }, 'json');
        // } else {
        //     PNotifyAlerta("Atención", "Debe seleccionar el registro de la tabla a editar", "error");
        // }
    }

    function guardar() {
        //alert('ok')
        // let obj = validar();
        let burl = $("#burl").html() + "ConfigClienteController/actualizarConfiguracion";

        var obj = {};
        obj.val_1 = 0
        // obj.val_2 = 0
        obj.val_3 = 0
        if (document.getElementById("reprocesar").checked == true) {
            obj.val_1 = 1
        }
        // if (document.getElementById("regenerarexpediente").checked == true) {
        //     obj.val_2 = 1
        // }
        // if (document.getElementById("recibiremail").checked == true) {
        //     obj.val_3 = 1
        // }

        $.post(burl, { data: obj }, function (result) {
            if (result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);
            }
        }, 'json').fail(function () {
            PNotifyAlerta('Error', 'Algo salio mal, no fue posible guardar, probablemente ya exista ese dominio en su listado.', 'error');
        });
    }
})(jQuery);
