(function($) {
    $(document).ready(init);

    function init() {
        $("#_agregar").on("click", mostrarAgregar);
        $("#_editar").on("click", mostrarEditar);
        $("#_borrar").on("click", confirmarBorrado);
        $("#_activar").on("click", deshabilitar);
        $("#btnGuardar").on("click", guardar);
        // $("#_apikey").on("click", generarApikey);
        $("#_exportar").on("click", function () {
            exportData();
        });
        $("#exportData").on("click", function () {
            abreVentanaModal("exportarDialog", "Exportar a...");
        });

        initializeTable();
        iniciarCombos();
    }

    function exportData() {
        
        var radioValue = $("input[name='optradio']:checked").val();
        var burl = $("#burl").html() + "CatUsuariosController/exportData/?format="+radioValue;
        // var burl = $("#burl").html() + "ResultadosOdometroController/exportCSV";
        window.location=burl;        
    }

    function initializeTable() {
        let burl = $("#burl").html() + "CatUsuariosController/listar";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";

        $("#tusuarios").DataTable({
            "scrollY": '35vh',
            "processing": true,
            "serverSide": false,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function(d) {

                }
            },
            "dataSrc": "data",
            "columns": [
                { "data": "id_usuario", "visible": false },
                { "data": "empresa" },
                { "data": "nombre" },                                
                { "data": "correo" },
                { "data": "perfil" },
                // {"data": "api_key"},
                { "data": "inactivo" },
            ],
            "language": {
                "url": lan
            },
            "select": 'single',
            "lengthChange": false,
            "pageLength": 10,
            "paging": true,
            "info": false
        }).on('init.dt', function() {

        });
    }

    function limpiaCtrls() {
        $("#correo").val("");
        $("#paterno").val("");
        $("#materno").val("");
        $("#nombre").val("");
        $("#pass").val("");
        $("#pass2").val("");
        $("#perfil").val(null).trigger("change");
        $("#id_cliente").val(null).trigger("change");
    }

    function mostrarAgregar() {
        limpiaCtrls();
        _idPuesto = 0;
        $("#id_edicion").val("");
        abreVentanaModal("__modal", "Agregar Registro");
    }

    function iniciarCombos() {
        let burl = $("#burl").html() + "CatUsuariosController/getPerfilesForCombo";
        $.post(burl, {}, function(result) {
            if (result != undefined) {
                $("#perfil").select2({
                    data: result,
                    placeholder: 'Selecciona un perfil',
                    allowClear: true,
                    with: 'resolve'
                });
                $("#perfil").val(null).trigger("change");
            }
        }, 'json');

        burl = $("#burl").html() + "CatUsuariosController/getClientesForCombo";
        $.post(burl, {}, function(result) {
            if (result != undefined) {
                $("#id_cliente").select2({
                    data: result,
                    placeholder: 'Selecciona un cliente',
                    allowClear: true,
                    with: 'resolve'
                }).on("change", function() {

                });
                $("#id_cliente").val(null).trigger("change");
            }
        }, 'json');

    }

    function validar() {
        let obj = {};
        obj.correo = $.trim($("#correo").val());
        obj.pass = $.trim($("#pass").val());
        obj.pass2 = $.trim($("#pass2").val());
        obj.paterno = $.trim($("#paterno").val());
        obj.materno = $.trim($("#materno").val());
        obj.nombre = $.trim($("#nombre").val());
        obj.id_perfil = $("#perfil").val();
        obj.id_cliente = $("#id_cliente").val();


        let accion = ($("id_edicion").val() != "") ? "editar" : "insertar";

        if (obj.paterno === "") {
            PNotifyAlerta("Atención", "El campo Apellido Paterno es obligatorio", "error");
            return false;
        }

        if (obj.nombre === "") {
            PNotifyAlerta("Atención", "El campo Nombre es obligatorio", "error");
            return false;
        }

        if (obj.id_perfil == null) {
            PNotifyAlerta("Atención", "Es necesario que seleccione el perfil del usuario", "error");
            return false;
        }

        if (obj.id_cliente == null) {
            PNotifyAlerta("Atención", "Es necesario que seleccione el cliente asociado al usuario", "error");
            return false;
        }


        if (accion === "insertar") {
            if (obj.pass == "") {
                PNotifyAlerta("Atención", "El campo contraseña es obligatorio", "error");
                return false;
            }
            if (obj.pass2 !== obj.pass) {
                PNotifyAlerta("Atención", "Las contraseñas no coinciden, favor de volverlas a escribir", "error");
                return false;
            }
        } else {
            if (obj.pass2 !== obj.pass) {
                PNotifyAlerta("Atención", "Las contraseñas no coinciden, favor de volverlas a escribir", "error");
                return false;
            }
        }

        return obj;

    }

    function guardar() {
        let obj = validar();
        let burl = $("#burl").html() + "CatUsuariosController/guardar";

        if (obj === false) {
            return false;
        } else {
            obj.id_edicion = $.trim($("#id_edicion").val());
            if (obj.id_edicion != "") {
                obj.accion = "editar";
            } else {
                obj.accion = "insertar";
            }

            $.post(burl, { data: obj }, function(result) {
                if (result != undefined) {
                    PNotifyAlerta(result.title, result.msg, result.type);
                    if (result.error === "0") {
                        $("#tusuarios").DataTable().ajax.reload(null, false);
                        cierraVentanaModal("__modal");
                    }
                }
            }, 'json').fail(function() {
                PNotifyAlerta('Error', 'Algo salio mal, no fue posible guardar...', 'error');
            });
        }
    }

    function mostrarEditar() {
        let burl = $("#burl").html() + "CatUsuariosController/getUsuarioById";
        let t = $("#tusuarios").DataTable();
        let r = t.row({ selected: true }).data();

        if (r != null) {
            $.post(burl, { id_usuario: r.id_usuario }, function(result) {
                if (result != undefined) {
                    if (result.id_usuario != null) {
                        setDatosToForm(result);
                        $("#id_edicion").val(result.id_usuario);
                        abreVentanaModal("__modal", "Editar Registro");
                    }
                }
            }, 'json');
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar el registro de la tabla a editar", "notice");
        }
    }

    function setDatosToForm(d) {
        limpiaCtrls();
        $("#correo").val(d.correo);
        $("#paterno").val(d.paterno);
        $("#materno").val(d.materno);
        $("#nombre").val(d.nombre);
        $("#pass").val("");
        $("#pass2").val("");
        $("#perfil").val(d.id_perfil).trigger("change");
        $("#id_cliente").val(d.id_cliente).trigger("change");
    }

    function confirmarBorrado() {

        let t = $("#tusuarios").DataTable();
        let r = t.row({ selected: true }).data();

        if (r != null) {
            const notice = PNotifyConfirm("Confirme su acción", "¿Desea borrar el usuario: " + r.correo + "?", "Si, proceder");

            notice.on('pnotify.confirm', (v) => {
                borrarUsuario(r.id_usuario);
            });
            notice.on('pnotify.cancel', () => {
                console.log("rejected")
            });
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
        }
    }

    // function generarApikey() {
    //     let t = $("#tusuarios").DataTable();
    //     let r = t.row({ selected: true }).data();
    //     if (r != null) {
    //         const notice = PNotifyConfirm("Confirme su acción", "¿Desea crear una nueva API key para el usuario : " + r.nombre + "?, si existe una API key sera inhabilitada.", "Si, proceder");
    //         notice.on('pnotify.confirm', (v) => {
    //             let burl = $("#burl").html() + "CatUsuariosController/crearApikey";
    //             $.post(burl, { id_usuario: r.id_usuario }, function(result) {
    //                 if (result != undefined) {
    //                     PNotifyAlerta(result.title, result.msg, result.type);
    //                     if (result.error === "0") {
    //                         $("#tusuarios").DataTable().ajax.reload(null, false);
    //                     }
    //                 }
    //             }, 'json');
    //         });
    //         notice.on('pnotify.cancel', () => {
    //             console.log("rejected")
    //         });
    //     } else {
    //         PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
    //     }
    // }

    function borrarUsuario(id_usuario) {
        let burl = $("#burl").html() + "CatUsuariosController/deleteUsuarioById";
        $.post(burl, { id_usuario: id_usuario }, function(result) {
            if (result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);
                if (result.error === "0") {
                    $("#tusuarios").DataTable().ajax.reload(null, false);
                }
            }
        }, 'json');
    }

    function deshabilitar() {
        let t = $("#tusuarios").DataTable();
        let r = t.row({ selected: true }).data();

        if (r != null) { //TODO
            let burl = $("#burl").html() + "CatUsuariosController/deshabilitarUsuarioById";
            $.post(burl, { id_usuario: r.id_usuario }, function(result) {
                if (result != undefined) {
                    PNotifyAlerta(result.title, result.msg, result.type);
                    if (result.type == "success") {
                        $("#tusuarios").DataTable().ajax.reload(null, false);
                    }
                }
            }, 'json');
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
        }
    }

})(jQuery);