(function ($) {
    $(document).ready(init);

    function init() {
        // alert('pasas')
        $("#btnGuardar").on("click", guardar);
        // $("#_agregar").on("click", mostrarAgregar);
        // $("#_editar").on("click", mostrarEditar);
        // // $("#_borrar").on("click", confirmarBorrado);
        // $("#_activar").on("click", deshabilitar);
        // $("#btnGuardar").on("click", guardar);

        // initializeTable();
        // iniciarCombos(); 
        let burl = $("#burl").html() + "ContratosController/getClientesForCombo";
        // alert(burl)
        $.post(burl, {}, function (result) {
            if (result != undefined) {
                $("#cliente").select2({
                    data: result,
                    placeholder: 'Selecciona el cliente',
                    allowClear: true,
                    width: 'resolve'
                });

                // $("#cliente").val(null).trigger("change");
                // alert($("hd_cliente").val())
                $("#cliente").val($("#hd_cliente").val()).trigger("change");

            }
        }, "json");

    }

})(jQuery)

function guardar() {
    //alert('ok')
    // let obj = validar();
    let burl = $("#burl").html() + "CambiarClienteController/actualizarConfiguracion";

    var obj = {};
    obj.client = $("#cliente").val();
    // obj.val_1 = 0
    // obj.val_2 = 0
    // obj.val_3 = 0
    // if (document.getElementById("reprocesar").checked == true) {
    //     obj.val_1 = 1
    // }
    // if (document.getElementById("regenerarexpediente").checked == true) {
    //     obj.val_2 = 1
    // }
    // if (document.getElementById("recibiremail").checked == true) {
    //     obj.val_3 = 1
    // }

    $.post(burl, { data: obj }, function (result) {
        if (result != undefined) {
            PNotifyAlerta(result.title, result.msg, result.type);
            location.reload();
        }
    }, 'json').fail(function () {
        PNotifyAlerta('Error', 'Algo salio mal, no fue posible guardar, probablemente ya exista ese dominio en su listado.', 'error');
    });
}