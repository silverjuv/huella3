(function($) {
    $(document).ready(init);

    function init() {
        $("#_agregar").on("click", mostrarAgregar);
        $("#btnGuardar").on("click", guardar);
        $("#_editar").on("click", mostrarEditar);
        $("#_borrar").on("click", confirmarBorrado);
        $("#btnApiKey").on("click", generarApikey);
        $("#_exportar").on("click", function () {
            exportData();
        });
        $("#exportData").on("click", function () {
            abreVentanaModal("exportarDialog", "Exportar a...");
        });

        // actualizaFechas();

        initializeTable();

        initializeSelects();
        initializeDatesCtrls();
    }

    function exportData() {
        
        var radioValue = $("input[name='optradio']:checked").val();
        var burl = $("#burl").html() + "ContratosController/exportData/?format="+radioValue;
        // var burl = $("#burl").html() + "ResultadosOdometroController/exportCSV";
        window.location=burl;        
    }
    
    function initializeTable() {
        let lan = $("#burl").html() + "assets/template/plugins/DataTables/spanish.json";
        let burl = $("#burl").html() + "ContratosController/listar";

        $("#tcontratos").DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function(d) {
                    //d.filtros = filtros;
                }
            },
            "dataSrc": "data",

            "columns": [

                { "data": "id_contrato", "visible": false },
                { "data": "cliente", "orderable": false },
                { "data": "servicio", "orderable": false },
                { "data": "fecha_inicio", "orderable": false },
                { "data": "fecha_fin", "orderable": false },
                { "data": "procesamientos", "orderable": false },
                { "data": "consumos", "orderable": false },
                { "data": "fuentes", "orderable": false },
                { "data": "status", "orderable": false }
            ],

            "language": {
                "url": lan
            },
            "select": 'single',
            "lengthChange": false,
            "pageLength": 10,
            "info": false,
            "dom": "<'row'<'col-sm-12'f>> <'row'<'col-sm-12't>> <'row mt-2'<'col-sm-12'p>>"
        }).on('init.dt', function() {

        });
    }

    function initializeSelects() {
        let burl = $("#burl").html() + "ContratosController/getClientesForCombo";
        $.post(burl, {}, function(result) {
            if (result != undefined) {
                $("#cliente").select2({
                    data: result,
                    placeholder: 'Selecciona el cliente',
                    allowClear: true,
                    width: 'resolve'
                });

                $("#cliente").val(null).trigger("change");
            }
        }, "json");

        burl = $("#burl").html() + "ContratosController/getFuentes";
        $.post(burl, {}, function(result) {
            if (result != undefined) {
                $("#fuentes").select2({
                    data: result,
                    placeholder: 'Selecciona las fuentes',
                    allowClear: true,
                    width: 'resolve',
                    multiple: true
                });

                $("#fuentes").val(null).trigger("change");
            }
        }, "json");

        $("#servicio").select2({
            placeholder: 'Selecciona el servicio',
            allowClear: true,
            width: 'resolve'
        }).on("change", function() {
            if (this.value == "1") {
                $("#procesamientos").val("");
            } else if (this.value == "2") {
                $("#finicio").val(null);
                $("#ffin").val(null);
            }
        });

        $("#servicio").val(null).trigger("change");

        $("#status").select2({
            placeholder: 'Selecciona el status',
            allowClear: true,
            width: 'resolve'
        });

    }

    function initializeDatesCtrls() {
        $("#finicio, #ffin").datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "yyyy-mm-dd",
            language: "es"
        });
    }

    function limpiaCtrls() {
        $("#cliente").val(null).trigger("change");
        $("#servicio").val(null).trigger("change");

        const formatYmd = date => date.toISOString().slice(0, 10);
        var date = formatYmd(new Date()); // 2020-05-06
        // var date = new Date();
        // date.setMonth(date.getMonth() + 1, 1);
        // $("#finicio").val(date);
        // $("#ffin").val(date);
        $("#finicio").datepicker('update', date);
        $("#ffin").datepicker('update', date);

        $("#procesamientos").val("0");
        $("#fuentes").val(null).trigger("change");
        $("#api_key").html("");
        $("#apiRow").addClass("d-none");
    }

    function mostrarAgregar() {
        limpiaCtrls();
        $("#id_edicion").val("");
        abreVentanaModal("__modal", "Agregar Registro");
    }

    function validarRegistro() {
        let obj = {};
        obj.cliente = $("#cliente").val();
        obj.servicio = $("#servicio").val();
        obj.finicio = $("#finicio").datepicker("getDate");
        obj.ffin = $("#ffin").datepicker("getDate");
        obj.procesamientos = $.trim($("#procesamientos").val());
        obj.fuentes = $("#fuentes").val();
        obj.status = $("#status").val();


        if (obj.cliente == null) {
            PNotifyAlerta("Atención", "Seleccione el cliente", "error")
            return false;
        }
        if (obj.servicio == null) {
            PNotifyAlerta("Atención", "Seleccione el tipo de servicio", "error")
            return false;
        } else {
            //Por tiempo
            if (obj.servicio == "T") {

                if (obj.finicio == null) {
                    PNotifyAlerta("Atención", "Seleccione la fecha de inicio del servicio", "error")
                    return false;
                } else {
                    obj.finicio = dateToString(obj.finicio, "-");
                }
                if (obj.ffin == null) {
                    PNotifyAlerta("Atención", "Seleccione la fecha de fin del servicio", "error")
                    return false;
                } else {
                    obj.ffin = dateToString(obj.ffin, "-");
                }

                obj.procesamientos == 0;

            } else if (obj.servicio == "P") {
                //por procesamiento
                if (obj.procesamientos == "") {
                    PNotifyAlerta("Atención", "Escriba el número de procesamientos a los que tendra derecho el cliente", "error")
                    return false;
                }
                obj.procesamientos = parseInt(obj.procesamientos, 10);

                // feha default, el dia de hoy.. en su pc.. 
                const formatYmd = date => date.toISOString().slice(0, 10);
                var date = formatYmd(new Date()); // 2020-05-06
                // var date = new Date();
                // date.setMonth(date.getMonth() + 1, 1);
                obj.finicio = date;
                obj.ffin = date;
            }
            if (obj.fuentes.length == 0) {
                PNotifyAlerta("Atención", "Debe asociar al menos una fuente", "error")
                return false;
            }

            // alert(obj.fuentes)
            // return false;
        }
        if (obj.status == null) {
            PNotifyAlerta("Atención", "Seleccione el status", "error")
            return false;
        }
        return obj;
    } //fun

    function guardar() {
        let burl = '';
        let obj = validarRegistro();
        if (obj === false) {
            return false;
        }

        let id_edicion = $("#id_edicion").val();
        if (id_edicion == "") {
            burl = $("#burl").html() + "ContratosController/guardarContrato";
        } else {
            burl = $("#burl").html() + "ContratosController/editarContrato";
        }

        $.post(burl, { data: obj, id_contrato: id_edicion }, function(result) {
            if (result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);
                cierraVentanaModal("__modal");

                if (result.type != "error") {
                    $("#tcontratos").DataTable().ajax.reload(null, false);
                }
                return false;
            }
        }, "json");
    }

    function mostrarEditar() {
        let burl = $("#burl").html() + "ContratosController/getContratoById";
        let t = $("#tcontratos").DataTable();
        let r = t.row({ selected: true }).data();

        if (r != null) {
            $.post(burl, { id_contrato: r.id_contrato }, function(result) {
                if (result != undefined) {
                    if (result.id_contrato != null) {
                        setDatosToForm(result);
                        actualizaFechas();

                        $("#id_edicion").val(result.id_contrato);
                        abreVentanaModal("__modal", "Editar Registro");
                    }
                }
            }, 'json');
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar el registro de la tabla a editar", "notice");
        }
    }

    function setDatosToForm(d) {
        limpiaCtrls();
        $("#cliente").val(d.id_cliente).trigger("change");
        $("#servicio").val(d.tipo_servicio).trigger("change");
        $("#api_key").text(d.api_key)
        $("#apiRow").removeClass("d-none");
        if (d.tipo_servicio == "T") {
            $("#finicio").datepicker('update', d.fecha_inicio);
            $("#ffin").datepicker('update', d.fecha_fin);
        } else if (d.tipo_servicio == "P") {
            $("#procesamientos").val(d.num_procesamientos)
        }

        if (d.fuentes.length > 0) {
            $("#fuentes").val(d.fuentes).trigger("change");
        }

        $("#status").val(d.status).trigger("change");
    }

    function confirmarBorrado() {

        let t = $("#tcontratos").DataTable();
        let r = t.row({ selected: true }).data();

        if (r != null) {
            const notice = PNotifyConfirm("Confirme su acción", "¿Desea borrar el contrato seleccionado?", "Si, proceder");

            notice.on('pnotify.confirm', (v) => {
                borrarContrato(r.id_contrato);
            });
            notice.on('pnotify.cancel', () => {
                console.log("rejected")
            });
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
        }
    }

    function borrarContrato(id_contrato) {
        let burl = $("#burl").html() + "ContratosController/deleteContratoById";
        $.post(burl, { id_contrato: id_contrato }, function(result) {
            if (result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);
                if (result.type != "error") {
                    $("#tcontratos").DataTable().ajax.reload(null, false);
                }
            }
        }, 'json');
    }

    function generarApikey() {
        const notice = PNotifyConfirm("Confirme su acción", "¿Desea crear una nueva API key para este contrato?... si existe una API key será reemplazada por la nueva.", "Si, proceder");
        notice.on('pnotify.confirm', (v) => {
            let burl = $("#burl").html() + "ContratosController/generarApikey";
            let id_contrato = $("#id_edicion").val();
            $.post(burl, { id_contrato: id_contrato }, function(result) {
                if (result != undefined) {
                    PNotifyAlerta(result.title, result.msg, result.type);
                    $("#api_key").text(result.apiKey);
                }
            }, 'json');
        });
        notice.on('pnotify.cancel', () => {
            console.log("rejected")
        });
    }

})(jQuery)

function actualizaFechas() {
    if ($("#servicio").val() == 'P') {
        // por numero de procesamientos
        $("#finicio").prop("disabled", true);
        $("#ffin").prop("disabled", true);
        $("#procesamientos").prop("disabled", false);
    } else {
        // por periodo
        $("#finicio").prop("disabled", false);
        $("#ffin").prop("disabled", false);
        $("#procesamientos").prop("disabled", true);
    }
}