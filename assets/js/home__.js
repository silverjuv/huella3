(function($) {
    $(document).ready(init);

    function init() {

        // loadOdometro();

        setTimeout(cargaDatosEstadisticos, 0);
        // $("#a_contrato").trigger("click");
    }

})(jQuery);


function cargaDatosEstadisticos() {
    var burl = $("#burl").html() + "HomeController/cargaNumerosTop";
    $.post(burl, {}, function(result) {
        // pagina = page;
        if (result != undefined) {
            $("#p_tPersonas").animateNumber({ number: result.totalPersonas });
            $("#p_tPuestos").animateNumber({ number: result.totalPuestos });
            $("#p_tExpedientes").animateNumber({ number: result.totalExpedientes });
            $("#p_tConsumos").animateNumber({ number: result.totalConsumos });
            loadOdometro(result.totalPromedio);
            setTimeout(function() { $("#p_tConsumos").html(number_format($("#p_tConsumos").html())) }, 1000)
            graficaBarras(result.totalMeses);
            graficaRadar(result.riesgos, result.totalRiesgos, result.colorsBack);
            graficaDona(result.rangosColors);
        }

    }, 'json');

}

function graficaDona(datos) {
    // pie

    data = {
        datasets: [{
            // data: [10, 20, 30, 7],
            data: datos,
            backgroundColor: [
                'rgba(0, 217, 109, 1)',
                'rgba(255, 255, 235, 1)',
                'rgba(255, 147, 38, 1)',
                'rgba(255, 0,0, 1)',
            ],
            borderColor: [
                'rgba(0, 0, 0, 1)',
                'rgba(0, 0, 0, 1)',
                'rgba(0, 0, 0, 1)',
                'rgba(0, 0, 0, 1)'
            ],
            borderWidth: 1
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            '0-1 Verde',
            '1-4 Amarillo',
            '4-7 Naranja',
            '7-9 Rojo'
        ]
    };

    var ctx = document.getElementById('myChart3');
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        // options: options
    });
}

function graficaRadar(riesgos, datos, colors) {
    // radar ...
    options = {
        scale: {
            angleLines: {
                display: true
            },
            // ticks: {
            //     suggestedMin: 50,
            //     suggestedMax: 100
            // }
        },
        legend: {
            display: false,
            labels: {
                fontColor: 'rgb(255, 99, 132)'
            }
        }
    };

    data = {
        // labels: ['Matar', 'Violar', 'Secuestrar', 'Atropellar'],
        labels: riesgos,
        datasets: [{
            // data: [200, 100, 40, 150],
            data: datos,
            backgroundColor: colors,
            // backgroundColor: [
            //     'rgba(255, 99, 132, 0.2)',
            //     'rgba(54, 162, 235, 0.2)',
            //     'rgba(153, 102, 255, 0.2)',
            //     'rgba(255, 159, 64, 0.2)'
            // ],
            // borderColor: [
            //     'rgba(255, 99, 132, 1)'
            // ],
            borderWidth: 1
        }]
    }

    var ctx = document.getElementById('myChart2');
    var myRadarChart = new Chart(ctx, {
        type: 'radar',
        data: data,
        options: options
    });
}


function graficaBarras(datos) {
    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            datasets: [{
                //data: [120, 109, 30, 50, 20, 30, 80, 210, 321, 1211, 121, 101],
                data: datos,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false,
                labels: {
                    fontColor: 'rgb(255, 99, 132)'
                }
            }
        }
    });
}

function loadOdometro(promedio) {

    $("#demo").myfunc({
        /**Max value of the meter*/
        val: promedio,
        /**Max value of the meter*/
        maxVal: 10,
        /**Division value of the meter*/
        divFact: 1,
        /**more than this leval, color will be red*/
        // dangerLevel: 8,
        /**reading begins angle*/
        /**more than this leval, color will be red*/
        initLevel: 0,
        /**more than this leval, color will be red*/
        infoLevel: 1,
        /**more than this leval, color will be red*/
        warningLevel: 4,
        /**more than this leval, color will be red*/
        dangerLevel: 7,
        initDeg: -45,
        /**indicator number width*/
        numbW: 40,
        /**indicator number height*/
        numbH: 18,
        /**Label on guage Face*/
        gagueLabel: 'Riesgo'
    });
    $("#demo").hide();
}


function number_format(number, decimals, decPoint, thousandsSep) { // eslint-disable-line camelcase
    //  discuss at: https://locutus.io/php/number_format/
    // original by: Jonas Raoni Soares Silva (https://www.jsfromhell.com)
    // improved by: Kevin van Zonneveld (https://kvz.io)
    // improved by: davook
    // improved by: Brett Zamir (https://brett-zamir.me)
    // improved by: Brett Zamir (https://brett-zamir.me)
    // improved by: Theriault (https://github.com/Theriault)
    // improved by: Kevin van Zonneveld (https://kvz.io)
    // bugfixed by: Michael White (https://getsprink.com)
    // bugfixed by: Benjamin Lupton
    // bugfixed by: Allan Jensen (https://www.winternet.no)
    // bugfixed by: Howard Yeend
    // bugfixed by: Diogo Resende
    // bugfixed by: Rival
    // bugfixed by: Brett Zamir (https://brett-zamir.me)
    //  revised by: Jonas Raoni Soares Silva (https://www.jsfromhell.com)
    //  revised by: Luke Smith (https://lucassmith.name)
    //    input by: Kheang Hok Chin (https://www.distantia.ca/)
    //    input by: Jay Klehr
    //    input by: Amir Habibi (https://www.residence-mixte.com/)
    //    input by: Amirouche
    //   example 1: number_format(1234.56)
    //   returns 1: '1,235'
    //   example 2: number_format(1234.56, 2, ',', ' ')
    //   returns 2: '1 234,56'
    //   example 3: number_format(1234.5678, 2, '.', '')
    //   returns 3: '1234.57'
    //   example 4: number_format(67, 2, ',', '.')
    //   returns 4: '67,00'
    //   example 5: number_format(1000)
    //   returns 5: '1,000'
    //   example 6: number_format(67.311, 2)
    //   returns 6: '67.31'
    //   example 7: number_format(1000.55, 1)
    //   returns 7: '1,000.6'
    //   example 8: number_format(67000, 5, ',', '.')
    //   returns 8: '67.000,00000'
    //   example 9: number_format(0.9, 0)
    //   returns 9: '1'
    //  example 10: number_format('1.20', 2)
    //  returns 10: '1.20'
    //  example 11: number_format('1.20', 4)
    //  returns 11: '1.2000'
    //  example 12: number_format('1.2000', 3)
    //  returns 12: '1.200'
    //  example 13: number_format('1 000,50', 2, '.', ' ')
    //  returns 13: '100 050.00'
    //  example 14: number_format(1e-8, 8, '.', '')
    //  returns 14: '0.00000001'

    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''

    var toFixedFix = function(n, prec) {
        if (('' + n).indexOf('e') === -1) {
            return +(Math.round(n + 'e+' + prec) + 'e-' + prec)
        } else {
            var arr = ('' + n).split('e')
            var sig = ''
            if (+arr[1] + prec > 0) {
                sig = '+'
            }
            return (+(Math.round(+arr[0] + 'e' + sig + (+arr[1] + prec)) + 'e-' + prec)).toFixed(prec)
        }
    }

    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec).toString() : '' + Math.round(n)).split('.')
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }

    return s.join(dec)
}