(function($) {
	$(document).ready(init);
	
	function init() {
		$("#_agregar").on("click", mostrarAgregar);
        $("#_editar").on("click", mostrarEditar);
        $("#_borrar").on("click", confirmarBorrado);
        $("#btnGuardar").on("click", guardar);
		initializeTable();
	}

	function initializeTable() {
		let burl = $("#burl").html() + "CatPerfilesController/listar";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";

        $("#tperfiles").DataTable({
            "scrollY": '35vh',
            "processing": true,
	        "serverSide": false,
			"ajax": {
				"url":burl,
				"type":"POST",
				"data": function (d){
                    
				}
			},
            "dataSrc": "data",
            "columns": [
                {"data": "id_perfil", "visible": false},
                {"data": "descripcion"}
            ],
            "language": {
                "url": lan
            },
            "select": 'single',
            "lengthChange": false,
            "pageLength": 10,
            "paging": true,
            "info": false
        }).on('init.dt', function(){
            
        });
	}

	function limpiaCtrls() {
        $("#correo").val("");
        $("#paterno").val("");
        $("#materno").val("");
        $("#nombre").val("");
        $("#pass").val("");
        $("#pass2").val("");
        $("#perfil").val(null).trigger("change");
    }

    function mostrarAgregar() {
        limpiaCtrls();
        $("#id_edicion").val("");
        abreVentanaModal("__modal", "Agregar Registro");
	}

	function validar() {
        let obj = {};
        obj.descripcion = $.trim($("#descripcion").val());

        if(obj.descripcion === "") {
            PNotifyAlerta("Atención", "El campo Descripción es obligatorio", "error");
            return false;
        }

        
        return obj;

    }
	
	function guardar() {
        let obj = validar();
        let burl = $("#burl").html()+"CatPerfilesController/guardar";

        if(obj === false) {
            return false;
        } else {
            obj.id_edicion = $.trim($("#id_edicion").val());
            if(obj.id_edicion != "") {
                obj.accion = "editar";
            } else {
                obj.accion = "insertar";
            }

            $.post(burl, {data: obj}, function (result) {
                if (result != undefined) {
                    PNotifyAlerta(result.title, result.msg, result.type);
                    if(result.error==="0") {
                        $("#tperfiles").DataTable().ajax.reload(null, false);
                        cierraVentanaModal("__modal");
                    }
                }
            }, 'json').fail(function() {
                PNotifyAlerta('Error', 'Algo salio mal, no fue posible guardar...', 'error');
            });
        }
    }

    function mostrarEditar() {
        let burl = $("#burl").html()+"CatPerfilesController/getPerfilById";
        let t = $("#tperfiles").DataTable();
        let r = t.row({selected: true}).data();
        
        if(r != null) {
            $.post(burl, {id_perfil: r.id_perfil}, function (result) {
                if (result != undefined) {
                    if(result.id_perfil != null) {
                        setDatosToForm(result);
                        $("#id_edicion").val(result.id_perfil);
                        abreVentanaModal("__modal", "Editar Registro");
                    }
                }
            }, 'json');
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar el registro de la tabla a editar", "notice");
        }
    }

    function setDatosToForm(d) {
        limpiaCtrls();
        $("#descripcion").val(d.descripcion);
    }

    function confirmarBorrado() {
        
        let t = $("#tperfiles").DataTable();
        let r = t.row({selected: true}).data();
        
        if(r != null) {
            const notice = PNotifyConfirm("Confirme su acción", "¿Desea borrar el perfil: "+r.descripcion+"?", "Si, proceder");
            
            notice.on('pnotify.confirm', (v) => {
                borrar(r.id_perfil);
            });
            notice.on('pnotify.cancel', () => {
                console.log("rejected")
            });
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
        }
    }

    function borrar(id_perfil) {
        let burl = $("#burl").html()+"CatPerfilesController/deletePerfilById";
        $.post(burl, {id_perfil: id_perfil }, function (result) {
            if (result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);
                if(result.error==="0") {
                    $("#tperfiles").DataTable().ajax.reload(null, false);
                }
            }
        }, 'json');
    }

})(jQuery);