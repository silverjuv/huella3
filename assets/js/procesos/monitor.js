statusHg = true;
intevarloCheck = null;
intarvaloCheckeoDemons = 10000; // 10000 cada 10 segundos
intarvaloRecargaLog = 30000; // 10000 cada 10 segundos

(function($) {
    $(document).ready(init);

    function init() {
        $.preloader.stop();

        $("#_iniciar").on("click", startHuellamon);
        $("#_detener").on("click", stopHuellamon);
        $("#_actualizar").on("click", recargaLogger);

        // desabilitados al inicio hasta checar q si est corriendo huellamongen.py
        $('#_iniciar').prop('disabled', true);
        $('#_detener').prop('disabled', true);

        setInterval(recargaLogger, intarvaloRecargaLog);
        recargaLogger();

        // checaDemonio();
        // // checar temporalmente el status de los demonios
        // intevarloCheck = setInterval(checaDemonio, intarvaloCheckeoDemons);
        checaDemonioProcsOS();
        setInterval(checaDemonioProcsOS, intarvaloCheckeoDemons);

        // checaDemonioNoticias();
        // intervaloCheckDemnoticias = setInterval(checaDemonioNoticias, intarvaloCheckeoDemons);
        checaDemonioProcsPers();
        setInterval(checaDemonioProcsPers, intarvaloCheckeoDemons);

        checaDemonioProcsPuesPers();
        setInterval(checaDemonioProcsPuesPers, intarvaloCheckeoDemons);        

        checaDemonioProcsNews();
        setInterval(checaDemonioProcsNews, intarvaloCheckeoDemons);

        checaDemonioProcsDarkWeb();
        setInterval(checaDemonioProcsDarkWeb, intarvaloCheckeoDemons);
    }

})(jQuery);

unavez = false;

function recargaLogger() {
    let burl = $("#burl").html() + "MonitorController/reloadLog";

    $.post(burl, { size: $("#slTamLog").val() }, function(result) {
        if (result != undefined) {
            //$(".code").html(result.log);
            $("#dv_logs").html(
                '<pre class="code" data-language="php">' + result.log + '</pre>'
            );

            // if (unavez == false) {
            $('pre.code').highlight({ source: 0, zebra: 1, indent: 'space', list: 'ol', attribute: 'data-language' });
            //     unavez = true;
            // }
        }
    }, 'json');
}

function startHuellamon() {
    $('#_iniciar').prop('disabled', true);
    $('#_detener').prop('disabled', true);
    clearInterval(intevarloCheck);
    intevarloCheck = setInterval(checaDemonio, intarvaloCheckeoDemons);

    let burl = $("#burl").html() + "MonitorController/startHuellamon";

    $.post(burl, {}, function(result) {
        if (result != undefined) {
            PNotifyAlerta("Atencion", "Huellamon Iniciado", "success");
        }
    });

}

function stopHuellamon() {
    $('#_iniciar').prop('disabled', true);
    $('#_detener').prop('disabled', true);
    clearInterval(intevarloCheck);
    intevarloCheck = setInterval(checaDemonio, 10000);

    let burl = $("#burl").html() + "MonitorController/stopHuellamon";

    $.post(burl, {}, function(result) {
        if (result != undefined) {
            PNotifyAlerta("Atencion", "Huellamon Apagado", "success");
        }
    });

}

function restartHuellamon() {

    let burl = $("#burl").html() + "MonitorController/startHuellamon";

    $.post(burl, {}, function(result) {
        if (result != undefined) {
            PNotifyAlerta("Atencion", "Huellamon Reiniciado", "success");
        }
    });

}

function checaDemonioProcsOS() {
    let burl = $("#burl").html() + "MonitorController/checaDemonio?" + getRandomArbitrary(1, 1000);

    $('#progProcsOS').css('width', 0 + '%').html('');
    $('#progProcsOS').addClass('progress-bar-animated active');    
    $('#progProcsOS').removeClass('bg-success');
    $('#progProcsOS').removeClass('bg-danger');
    $('#progProcsOS').addClass('bg-warning');

    $.post(burl, { dem: 'procs_osint' }, function(result) {
        if (result != undefined) {
            //alert(parseInt(result.substring(1)))
            if (parseInt(result.substring(1)) == 1) {
                //if (result.trim().length > 0) {
                // statusHg = true;
                // checaDemonio2();
                $('#progProcsOS').css('width', 100 + '%');
                $('#progProcsOS').removeClass('progress-bar-animated active');
                $('#progProcsOS').removeClass('bg-warning').html('Complete');
                $('#progProcsOS').addClass('bg-success').html('OK');
                $("#sp_status_procs_osi").html('Demonio Procesos OSINT funcionando correctamente!');

            } else {
                // statusHg = false;
                // $('#_iniciar').prop('disabled', true);
                // $('#_detener').prop('disabled', true);
                //$("#sp_status_hg").html('Demonio general no esta inicializado, no podra iniciar o detener el demonio Analizador Personas - Puesto! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="_iniciar_hg" onclick="iniciaDemGen();" type="button" class="btn btn-lime btn-sm">                <i class="fas fa-play"></i>                Iniciar servicio            </button>');
                $('#progProcsOS').css('width', 100 + '%');
                $('#progProcsOS').removeClass('progress-bar-animated active');
                $('#progProcsOS').removeClass('bg-warning');
                $('#progProcsOS').addClass('bg-danger').html('');                  
                $("#sp_status_procs_osi").html('Demonio Procesos OSINT no esta inicializado. ');
            }
        }
    });
}

function checaDemonioProcsPers() {
    let burl = $("#burl").html() + "MonitorController/checaDemonio?" + getRandomArbitrary(1, 1000);

    $('#progExpsPers').css('width', 0 + '%').html('');
    $('#progExpsPers').addClass('progress-bar-animated active');    
    $('#progExpsPers').removeClass('bg-success');
    $('#progExpsPers').removeClass('bg-danger');
    $('#progExpsPers').addClass('bg-warning');

    $.post(burl, { dem: 'procs_pers' }, function(result) {
        if (result != undefined) {
            //alert(parseInt(result.substring(1)))
            if (parseInt(result.substring(1)) == 1) {
                //if (result.trim().length > 0) {
                // statusHg = true;
                // checaDemonio2();
                $('#progExpsPers').css('width', 100 + '%');
                $('#progExpsPers').removeClass('progress-bar-animated active');
                $('#progExpsPers').removeClass('bg-warning').html('Complete');
                $('#progExpsPers').addClass('bg-success').html('OK');

                $("#sp_status_procspers").html('Demonio Procesos Expediente Personas funcionando correctamente!');

            } else {
                // statusHg = false;
                // $('#_iniciar').prop('disabled', true);
                // $('#_detener').prop('disabled', true);
                //$("#sp_status_hg").html('Demonio general no esta inicializado, no podra iniciar o detener el demonio Analizador Personas - Puesto! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="_iniciar_hg" onclick="iniciaDemGen();" type="button" class="btn btn-lime btn-sm">                <i class="fas fa-play"></i>                Iniciar servicio            </button>');
                $('#progExpsPers').css('width', 100 + '%');
                $('#progExpsPers').removeClass('progress-bar-animated active');
                $('#progExpsPers').removeClass('bg-warning');
                $('#progExpsPers').addClass('bg-danger').html('');    
                $("#sp_status_procspers").html('Demonio Procesos Expediente Personas no esta inicializado. ');
            }
        }
    });
}


function checaDemonioProcsPuesPers() {
    let burl = $("#burl").html() + "MonitorController/checaDemonio?" + getRandomArbitrary(1, 1000);

    $('#progPuesPers').css('width', 0 + '%').html('');
    $('#progPuesPers').addClass('progress-bar-animated active');    
    $('#progPuesPers').removeClass('bg-success');
    $('#progPuesPers').removeClass('bg-danger');
    $('#progPuesPers').addClass('bg-warning');

    $.post(burl, { dem: 'procs_ppers' }, function(result) {
        if (result != undefined) {
            //alert(parseInt(result.substring(1)))
            if (parseInt(result.substring(1)) == 1) {
                //if (result.trim().length > 0) {
                // statusHg = true;
                // checaDemonio2();
                $('#progPuesPers').css('width', 100 + '%');
                $('#progPuesPers').removeClass('progress-bar-animated active');
                $('#progPuesPers').removeClass('bg-warning').html('Complete');
                $('#progPuesPers').addClass('bg-success').html('OK');

                $("#sp_status_procspuespers").html('Demonio Procesos Expediente Personas funcionando correctamente!');

            } else {
                // statusHg = false;
                // $('#_iniciar').prop('disabled', true);
                // $('#_detener').prop('disabled', true);
                //$("#sp_status_hg").html('Demonio general no esta inicializado, no podra iniciar o detener el demonio Analizador Personas - Puesto! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="_iniciar_hg" onclick="iniciaDemGen();" type="button" class="btn btn-lime btn-sm">                <i class="fas fa-play"></i>                Iniciar servicio            </button>');
                $('#progPuesPers').css('width', 100 + '%');
                $('#progPuesPers').removeClass('progress-bar-animated active');
                $('#progPuesPers').removeClass('bg-warning');
                $('#progPuesPers').addClass('bg-danger').html('');    
                $("#sp_status_procspuespers").html('Demonio Procesos Expediente Personas no esta inicializado. ');
            }
        }
    });
}

function checaDemonioProcsNews() {
    let burl = $("#burl").html() + "MonitorController/checaDemonio?" + getRandomArbitrary(1, 1000);

    $('#progNots').css('width', 0 + '%').html('');
    $('#progNots').addClass('progress-bar-animated active');    
    $('#progNots').removeClass('bg-success');
    $('#progNots').removeClass('bg-danger');
    $('#progNots').addClass('bg-warning');

    $.post(burl, { dem: 'proc_nots' }, function(result) {
        if (result != undefined) {
            //alert(parseInt(result.substring(1)))
            if (parseInt(result.substring(1)) == 1) {
                //if (result.trim().length > 0) {
                // statusHg = true;
                // checaDemonio2();
                $('#progNots').css('width', 100 + '%');
                $('#progNots').removeClass('progress-bar-animated active');
                $('#progNots').removeClass('bg-warning').html('Complete');
                $('#progNots').addClass('bg-success').html('OK');
                $("#sp_status_noticias").html('Demonio capturador de Noticias funcionando correctamente!');

            } else {
                // statusHg = false;
                // $('#_iniciar').prop('disabled', true);
                // $('#_detener').prop('disabled', true);
                //$("#sp_status_hg").html('Demonio general no esta inicializado, no podra iniciar o detener el demonio Analizador Personas - Puesto! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="_iniciar_hg" onclick="iniciaDemGen();" type="button" class="btn btn-lime btn-sm">                <i class="fas fa-play"></i>                Iniciar servicio            </button>');
                $('#progNots').css('width', 100 + '%');
                $('#progNots').removeClass('progress-bar-animated active');
                $('#progNots').removeClass('bg-warning');
                $('#progNots').addClass('bg-danger').html('');                  
                $("#sp_status_noticias").html('Demonio capturador de Noticias no esta inicializado. ');
            }
        }
    });
}


function checaDemonioProcsDarkWeb() {
    let burl = $("#burl").html() + "MonitorController/checaDemonio?" + getRandomArbitrary(1, 1000);

    $('#progDark').css('width', 0 + '%').html('');
    $('#progDark').addClass('progress-bar-animated active');    
    $('#progDark').removeClass('bg-success');
    $('#progDark').removeClass('bg-danger');
    $('#progDark').addClass('bg-warning');
progDark
    $.post(burl, { dem: 'proc_darkw' }, function(result) {
        if (result != undefined) {
            //alert(parseInt(result.substring(1)))
            if (parseInt(result.substring(1)) == 1) {
                //if (result.trim().length > 0) {
                // statusHg = true;
                // checaDemonio2();
                $('#progDark').css('width', 100 + '%');
                $('#progDark').removeClass('progress-bar-animated active');
                $('#progDark').removeClass('bg-warning').html('Complete');
                $('#progDark').addClass('bg-success').html('OK');
                $("#sp_status_darkw").html('Demonio capturador de Noticias funcionando correctamente!');

            } else {
                // statusHg = false;
                // $('#_iniciar').prop('disabled', true);
                // $('#_detener').prop('disabled', true);
                //$("#sp_status_hg").html('Demonio general no esta inicializado, no podra iniciar o detener el demonio Analizador Personas - Puesto! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="_iniciar_hg" onclick="iniciaDemGen();" type="button" class="btn btn-lime btn-sm">                <i class="fas fa-play"></i>                Iniciar servicio            </button>');
                $('#progDark').css('width', 100 + '%');
                $('#progDark').removeClass('progress-bar-animated active');
                $('#progDark').removeClass('bg-warning');
                $('#progDark').addClass('bg-danger').html('');                  
                $("#sp_status_darkw").html('Demonio capturador de Noticias no esta inicializado. ');
            }
        }
    });
}

// // function checaDemonio() {
// //     let burl = $("#burl").html() + "MonitorController/checaDemonio?" + getRandomArbitrary(1, 1000);

// //     $.post(burl, { dem: 'hg' }, function(result) {
// //         if (result != undefined) {
// //             //alert(parseInt(result.substring(1)))
// //             if (parseInt(result.substring(1)) == 1) {
// //                 //if (result.trim().length > 0) {
// //                 statusHg = true;
// //                 checaDemonio2();

// //                 $("#sp_status_hg").html('Demonio general actualmente funcionando correctamente!');

// //             } else {
// //                 statusHg = false;
// //                 $('#_iniciar').prop('disabled', true);
// //                 $('#_detener').prop('disabled', true);
// //                 //$("#sp_status_hg").html('Demonio general no esta inicializado, no podra iniciar o detener el demonio Analizador Personas - Puesto! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="_iniciar_hg" onclick="iniciaDemGen();" type="button" class="btn btn-lime btn-sm">                <i class="fas fa-play"></i>                Iniciar servicio            </button>');
// //                 $("#sp_status_hg").html('Demonio general no esta inicializado, no podra iniciar o detener el demonio Analizador Personas - Puesto! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Debe iniciarlo el administrador del sistema, desde el servidor web.');
// //             }
// //         }
// //     });
// // }

// // function checaDemonio2() {
// //     let burl = $("#burl").html() + "MonitorController/checaDemonio?" + getRandomArbitrary(1, 1000);

// //     $.post(burl, { dem: 'hs' }, function(result) {
// //         if (result != undefined) {
// //             if (parseInt(result.substring(1)) == 1) {
// //                 //statusHg = true;
// //                 $('#_iniciar').prop('disabled', true);
// //                 $('#_detener').prop('disabled', false);
// //             } else {
// //                 $('#_iniciar').prop('disabled', false);
// //                 $('#_detener').prop('disabled', true);
// //             }
// //         }
// //     });
// // }

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

// function iniciaDemGen(){
//     $('#_iniciar_hg').prop('disabled', true);

//     let burl = $("#burl").html() + "MonitorController/iniciaDemonioGeneral?" + getRandomArbitrary(1, 1000);

//     $.post(burl, {  }, function(result) {
//         if (result != undefined) {
//             $('#_iniciar_hg').hide();
//         }
//     });    
// }
function checaDemonioNoticias() {
    let burl = $("#burl").html() + "MonitorController/checaDemonio?" + getRandomArbitrary(1, 1000);

    $.post(burl, { dem: 'notics' }, function(result) {
        if (result != undefined) {
            //alert(parseInt(result.substring(1)))
            if (parseInt(result.substring(1)) == 1) {
                $("#sp_status_noticias").html('Demonio capturador de noticias actualmente funcionando correctamente!');
            } else {
                $("#sp_status_noticias").html('Demonio capturador de noticias no esta inicializado.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Checar con el administrador del sistema si esta programada su ejecución posterior.');
            }
        }
    });
}

$.preloader.start({
    modal: true
});