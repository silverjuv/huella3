Dropzone.autoDiscover = false;
(function($) {
    $(document).ready(init);

    function init() {
        iniciarDropZone();
    }

    function iniciarDropZone() {
        let burl = $("#burl").html() + "MiPerfilController/subirAvatar";
        $(".dropzone").dropzone({
            url: burl,
            dictDefaultMessage: "Arrastre su avatar aquí",
            dictFileTooBig: "El archivo es demasiado grande",
            dictInvalidFileType: "Tipo de archivo invalido",
            dictResponseError: "Ocurrio un problema y no fue posible subir el archivo",
            parallelUploads: 1,
            acceptedFiles: '.jpg, .jpeg, .png',

            init: function() {
                this.on("success", function(file, res) {
                    res = (typeof res == "string")? JSON.parse(res):res;
                    PNotifyAlerta(res.title, res.msg, res.type);
                    console.log(typeof res)
                    if(res.type == "success") {
                        document.location.reload();
                    }
                })
            }
        });
    }

})(jQuery);