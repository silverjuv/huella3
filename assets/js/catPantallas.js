(function($) {
	$(document).ready(init);
	
	function init() {
		$("#_agregar").on("click", mostrarAgregar);
        $("#_editar").on("click", mostrarEditar);
        $("#_borrar").on("click", confirmarBorrado);
        $("#btnGuardar").on("click", guardar);
		initializeTable();
		cargarMenusPadres();
	}

	function initializeTable() {
		let burl = $("#burl").html() + "CatPantallasController/listar";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";

        $("#tpantallas").DataTable({
            "scrollY": '35vh',
            "processing": true,
	        "serverSide": true,
			"ajax": {
				"url":burl,
				"type":"POST",
				"data": function (d){
                    
				}
			},
            "dataSrc": "data",
            "columns": [
                {"data": "id_pantalla", "visible": true},
                {"data": "nombre_menu"},
                {"data": "controlador"},
                {"data": "pantalla_publica"},
                {"data": "sin_menu"}
            ],
            "language": {
                "url": lan
            },
            "select": 'single',
            "lengthChange": false,
            "pageLength": 10,
            "paging": true,
            "info": false
        }).on('init.dt', function(){
            
        });
	}

	function limpiaCtrls() {
        $("#nombre_menu").val("");
        $("#descripcion").val("");
        $("#icono").val("");
        $("#controlador").val("");
        $("#orden").val("0");
        $("#tiene_hijos").prop("checked", false);
        $("#pantalla_publica").prop("checked", false);
        $("#sin_menu").prop("checked", false);
        $("#id_padre").val(null).trigger("change");
    }

    function mostrarAgregar() {
        limpiaCtrls();
        $("#id_edicion").val("");
        abreVentanaModal("__modal", "Agregar Registro");
	}
	
	function cargarMenusPadres() {
        let burl = $("#burl").html() + "CatPantallasController/getMenusPadresForCombo";
        $.post(burl, {}, function (result) {
            if (result != undefined) {
                $("#id_padre").select2({
                    data: result,
                    placeholder: 'Selecciona un perfil',
                    allowClear: true,
                    with: 'resolve'
                });
                $("#id_padre").val(null).trigger("change");
            }
        }, 'json');
	}

	function validar() {
        let obj = {};
        obj.nombre_menu = $.trim($("#nombre_menu").val());
        obj.descripcion = $.trim($("#descripcion").val());
        obj.controlador = $.trim($("#controlador").val());
        obj.id_padre = $.trim($("#id_padre").val());
        obj.orden = $.trim($("#orden").val());
        obj.icono = $.trim($("#icono").val());
        obj.tiene_hijos = $("#tiene_hijos").prop("checked")?"1":"0";
        obj.pantalla_publica = $("#pantalla_publica").prop("checked")?"1":"0";
        obj.sin_menu = $("#sin_menu").prop("checked")?"1":"0";
        
        //solo se validan estos dos campo porque todos los demas pueden ser nulos
        if(obj.descripcion === "") {
            PNotifyAlerta("Atención", "El campo Descripción es obligatorio", "error");
            return false;
        }
        
        return obj;

    }
	
	function guardar() {
        let obj = validar();
        let burl = $("#burl").html()+"CatPantallasController/guardar";

        if(obj === false) {
            return false;
        } else {
            obj.id_edicion = $.trim($("#id_edicion").val());
            if(obj.id_edicion != "") {
                obj.accion = "editar";
            } else {
                obj.accion = "insertar";
            }

            $.post(burl, {data: obj}, function (result) {
                if (result != undefined) {
                    PNotifyAlerta(result.title, result.msg, result.type);
                    if(result.error==="0") {
                        $("#tpantallas").DataTable().ajax.reload(null, false);
                        cierraVentanaModal("__modal");
                    }
                }
            }, 'json').fail(function() {
                PNotifyAlerta('Error', 'Algo salio mal, no fue posible guardar...', 'error');
            });
        }
    }

    function mostrarEditar() {
        let burl = $("#burl").html()+"CatPantallasController/getPantallaById";
        let t = $("#tpantallas").DataTable();
        let r = t.row({selected: true}).data();
        
        if(r != null) {
            $.post(burl, {id_pantalla: r.id_pantalla}, function (result) {
                if (result != undefined) {
                    if(result.id_pantalla != null) {
                        setDatosToForm(result);
                        $("#id_edicion").val(result.id_pantalla);
                        abreVentanaModal("__modal", "Editar Registro");
                    }
                }
            }, 'json');
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar el registro de la tabla a editar", "notice");
        }
    }

    function setDatosToForm(d) {
        limpiaCtrls();
        $("#nombre_menu").val(d.nombre_menu);
        $("#descripcion").val(d.descripcion);
        $("#icono").val(d.icono);
        $("#controlador").val(d.controlador);
        $("#orden").val(d.orden);
        $("#tiene_hijos").prop("checked", (d.tiene_hijos=="1"));
        $("#pantalla_publica").prop("checked", (d.pantalla_publica=="1"));
        $("#sin_menu").prop("checked", (d.sin_menu=="1"));
        $("#id_padre").val(d.id_padre).trigger("change");
    }

    function confirmarBorrado() {
        
        let t = $("#tpantallas").DataTable();
        let r = t.row({selected: true}).data();
        
        if(r != null) {
            const notice = PNotifyConfirm("Confirme su acción", "¿Desea borrar la pantalla: "+r.nombre_menu+"?", "Si, proceder");
            
            notice.on('pnotify.confirm', (v) => {
                borrar(r.id_pantalla);
            });
            notice.on('pnotify.cancel', () => {
                console.log("rejected")
            });
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
        }
    }

    function borrar(id_pantalla) {
        let burl = $("#burl").html()+"CatPantallasController/deletePantallaById";
        $.post(burl, {id_pantalla: id_pantalla }, function (result) {
            if (result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);
                if(result.error==="0") {
                    $("#tpantallas").DataTable().ajax.reload(null, false);
                }
            }
        }, 'json');
    }

})(jQuery);