(function($) {
    $(document).ready(init);

    function init() {

        // $("#_agregar").on("click", mostrarAgregar);
        $("#_editar").on("click", mostrarEditar);
        // $("#_borrar").on("click", confirmarBorrado);
        // $("#_activar").on("click", deshabilitar);
        $("#btnGuardar").on("click", guardar);

        initializeTable();
        // iniciarCombos();
        // alert('ok2')
    }

    function initializeTable() {
        let burl = $("#burl").html() + "ParametrosController/listar";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";

        $("#tparametros").DataTable({
            "scrollY": '35vh',
            "processing": true,
            "serverSide": false,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function(d) {

                }
            },
            "dataSrc": "data",
            "columns": [
                { "data": "cve_param", "visible": true },
                { "data": "valor" },
            ],
            "language": {
                "url": lan
            },
            "select": 'single',
            "lengthChange": false,
            "pageLength": 10,
            "paging": true,
            "info": false
        }).on('init.dt', function() {

        });
    }

    function limpiaCtrls() {
        $("#cve_param").val("");
        $("#valor").val("");
        // $("#materno").val("");
        // $("#nombre").val("");
        // $("#pass").val("");
        // $("#pass2").val("");
        // $("#perfil").val(null).trigger("change");
        // $("#id_cliente").val(null).trigger("change");
    }

    // function mostrarAgregar() {
    //     limpiaCtrls();
    //     _idPuesto = 0;
    //     $("#id_edicion").val("");
    //     abreVentanaModal("__modal", "Agregar Registro");
    // }



    function validar() {
        let obj = {};
        // obj.cve_param = $.trim($("#cve_param").val());
        obj.valor = $.trim($("#valor").val());

        let accion = ($("id_edicion").val() != "") ? "editar" : "insertar";

        if (obj.valor === "") {
            PNotifyAlerta("Atención", "El campo Valor es obligatorio", "error");
            return false;
        }

        return obj;

    }

    function guardar() {
        let obj = validar();
        let burl = $("#burl").html() + "ParametrosController/guardar";

        if (obj === false) {
            return false;
        } else {
            obj.id_edicion = $.trim($("#id_edicion").val());
            if (obj.id_edicion != "") {
                obj.accion = "editar";
            } else {
                obj.accion = "insertar";
            }

            $.post(burl, { data: obj }, function(result) {
                if (result != undefined) {
                    PNotifyAlerta(result.title, result.msg, result.type);
                    if (result.error === "0") {
                        $("#tparametros").DataTable().ajax.reload(null, false);
                        cierraVentanaModal("__modal");
                    }
                }
            }, 'json').fail(function() {
                PNotifyAlerta('Error', 'Algo salio mal, no fue posible guardar...', 'error');
            });
        }
    }

    function mostrarEditar() {
        let burl = $("#burl").html() + "ParametrosController/getParametroById";
        let t = $("#tparametros").DataTable();
        let r = t.row({ selected: true }).data();

        if (r != null) {
            $.post(burl, { cve_param: r.cve_param }, function(result) {
                if (result != undefined) {
                    if (result.cve_param != null) {
                        setDatosToForm(result);
                        $("#id_edicion").val(result.cve_param);
                        abreVentanaModal("__modal", "Editar Registro");
                    }
                }
            }, 'json');
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar el registro de la tabla a editar", "notice");
        }
    }

    function setDatosToForm(d) {
        limpiaCtrls();
        $("#cve_param").val(d.cve_param);
        $("#valor").val(d.valor);
    }


})(jQuery);