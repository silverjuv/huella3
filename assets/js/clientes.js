(function($) {
    $(document).ready(init);

    function init() {
        $("#_agregar").on("click", mostrarAgregar);
        $("#_editar").on("click", mostrarEditar);
        $("#_borrar").on("click", confirmarBorrado);
        $("#btnGuardar").on("click", guardar);
        $("#_exportar").on("click", function () {
            exportData();
        });
        $("#exportData").on("click", function () {
            abreVentanaModal("exportarDialog", "Exportar a...");
        });


        initializeTable();
    }


    function exportData() {
        
        var radioValue = $("input[name='optradio']:checked").val();
        var burl = $("#burl").html() + "ClientesController/exportData/?format="+radioValue;
        // var burl = $("#burl").html() + "ResultadosOdometroController/exportCSV";
        window.location=burl;        
    }

    function initializeTable() {
        let burl = $("#burl").html() + "ClientesController/listar";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";

        $("#tabla").DataTable({
            "scrollY": '35vh',
            "processing": true,
            "serverSide": false,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function(d) {

                }
            },
            "dataSrc": "data",
            "columns": [
                { "data": "id_cliente", "visible": false },
                { "data": "descripcion" },
                { "data": "direccion" },
                { "data": "email_contacto" },
                { "data": "telefono" },
                { "data": "usuarios" }
            ],
            "language": {
                "url": lan
            },
            "select": 'single',
            "lengthChange": false,
            "pageLength": 10,
            "paging": true,
            "info": false
        });
    }

    function limpiaCtrls() {
        $("#cliente").val("");
        $("#email_contacto").val("");
        $("#telefono").val("");
        $("#direccion").val("");
    }

    function mostrarAgregar() {
        limpiaCtrls();
        $("#id_edicion").val("");
        abreVentanaModal("__modal", "Agregar Registro");
    }

    function mostrarEditar() {
        let burl = $("#burl").html() + "ClientesController/getRowById";
        let t = $("#tabla").DataTable();
        let r = t.row({ selected: true }).data();

        if (r != null) {
            $.post(burl, { id: r.id_cliente }, function(result) {
                if (result != undefined) {
                    if (result.id_cliente != null) {
                        setDatosToForm(result);
                        $("#id_edicion").val(result.id_cliente);
                        abreVentanaModal("__modal", "Editar Registro");
                    }
                }
            }, 'json');
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar el registro de la tabla a editar", "error");
        }
    }

    function setDatosToForm(d) {
        limpiaCtrls();
        $("#cliente").val(d.descripcion);
        $("#email_contacto").val(d.email_contacto);
        $("#telefono").val(d.telefono);
        $("#direccion").val(d.direccion);
    }

    function confirmarBorrado() {

        let t = $("#tabla").DataTable();
        let r = t.row({ selected: true }).data();

        if (r != null) {
            const notice = PNotifyConfirm("Confirme su acción", "¿Desea borrar el registro: " + r.descripcion + "?", "Si, proceder");

            notice.on('pnotify.confirm', (v) => {
                borrar(r.id_cliente);
            });
            notice.on('pnotify.cancel', () => {
                console.log("rejected")
            });
        } else {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "error");
        }
    }

    function borrar(id) {
        let burl = $("#burl").html() + "ClientesController/deleteRowById";
        $.post(burl, { id: id }, function(result) {
            if (result != undefined) {
                PNotifyAlerta(result.title, result.msg, result.type);
                if (result.error === "0") {
                    $("#tabla").DataTable().ajax.reload(null, false);
                }
            }
        }, 'json');
    }

    function validar() {
        let obj = {};
        obj.descripcion = $.trim($("#cliente").val());
        obj.email_contacto = $.trim($("#email_contacto").val());
        obj.telefono = $.trim($("#telefono").val());
        obj.direccion = $.trim($("#direccion").val());

        if (obj.descripcion === "") {
            PNotifyAlerta("Atención", "El campo Nombre del cliente es obligatorio", "error");
            return false;
        }


        if (obj.email_contacto == "") {
            PNotifyAlerta("Atención", "El email principal es obligatorio", "error");
            return false;
        }
        if (esEmailValido(obj.email_contacto) == false) {
            PNotifyAlerta("Atención", "El email no esta bien escrito", "error");
            return false;
        }
        if (obj.telefono == "" || obj.telefono.trim().length != 10) {
            PNotifyAlerta("Atención", "El télefono es obligatorio en 10 posiciones", "error");
            return false;
        }
        try {
            if (parseInt(obj.telefono) == NaN) {
                PNotifyAlerta("Atención", "El télefono debe ser numerico", "error");
                return false;
            }
        } catch (e) {
            PNotifyAlerta("Atención", "El télefono debe ser numerico", "error");
            return false;
        }


        if (obj.direccion === "") {
            PNotifyAlerta("Atención", "El campo Direccion del cliente es obligatorio", "error");
            return false;
        }

        return obj;

    }

    function guardar() {
        let obj = validar();
        let burl = $("#burl").html() + "ClientesController/guardar";

        if (obj === false) {
            return false;
        } else {
            obj.id_edicion = $.trim($("#id_edicion").val());
            if (obj.id_edicion != "") {
                obj.accion = "editar";
            } else {
                obj.accion = "insertar";
            }

            $.post(burl, { data: obj }, function(result) {
                if (result != undefined) {
                    PNotifyAlerta(result.title, result.msg, result.type);
                    if (result.type === "success") {
                        $("#tabla").DataTable().ajax.reload(null, false);
                        cierraVentanaModal("__modal");
                    }
                }
            }, 'json').fail(function() {
                PNotifyAlerta('Error', 'Algo salio mal, no fue posible guardar...', 'error');
            });
        }
    }

})(jQuery);