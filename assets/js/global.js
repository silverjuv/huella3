PNotify.defaults.styling = 'bootstrap4';

let tiktokChecker = null;

$(document).ready(__initScriptsScreen);

function __initScriptsScreen() {
  $("#linkIrInicio").on("click", function(event) {
    event.preventDefault();
    
    location.href = $("#burl").html() + "HomeController/";
    return false;
  });

  setInterval(checkSess,120000); // CADA 2 MINUTOS

}

function checkSess(){
  let burl = $("#burl").html() + "HomeController/checkSess";
  $.post(burl, { }, function(result) {

    if (result != undefined) {
      if (parseInt(result.sess) == 0) {
        window.location=$("#burl").html() + "LoginController";
      }
    }
  }
  );
}

function PNotifyAlerta(titulo, msg, type){
	PNotify.alert({
		title: titulo,
		text: msg,
		type: type,
    Buttons: { closer: true}
	});
}

function PNotifyConfirm(titulo, msg, txtBtnOk, txtBtnCancel, textTrusted) {

    txtBtnOk = txtBtnOk || "Aceptar";
    txtBtnCancel = txtBtnCancel || "Cancelar";
    textTrusted = textTrusted || false;

    return PNotify.alert({
        title: titulo,
        text: msg,
        hide: false,
        textTrusted: textTrusted,
        buttons: [
            {
              text: txtBtnOk,
              addClass: '',
              primary: true,
              // Whether to trigger this button when the user hits enter in a single line
              // prompt. Also, focus the button if it is a modal prompt.
              promptTrigger: true,
              click: (notice, value) => {
                notice.close();
                notice.fire('pnotify.confirm', {notice, value});
              }
            },
            {
              text: txtBtnCancel,
              addClass: '',
              click: (notice) => {
                notice.close();
                notice.fire('pnotify.cancel', {notice});
              }
            }
          ],
          modules: {
            Confirm: {
              confirm: true
            }
          }
      });

}

function abreVentanaModal(idVentana, titulo) {
    $("#"+idVentana+"_title").html(titulo);
    $("#"+idVentana).modal("show");
}

function cierraVentanaModal(idVentana) {
    $("#"+idVentana).modal("hide");
}

function esEmailValido(email) {
	var re = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i;
	return (email.match(re) != null);
}

function dateToString(d,separador) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  return [d.getFullYear(), pad(d.getMonth()+1), pad(d.getDate())].join(separador);
}

function makePostRequest(uri, params) {
  let dfr = $.Deferred();
  uri = $("#burl").html() + uri;

  $.post(uri, {data: params}, function(r) {
    if(r != undefined) {
      dfr.resolve(r);
    } else {
      dfr.resolve(false);
    }
  }, "json").fail(function(){
    dfr.reject('Service Fail');
  });

  return dfr.promise();
}

function creaCombobox(idSelect, data, initVal, multiple, templateResult) {
  multiple = (multiple === true) ? true : false;
  initVal = (initVal != null) ? initVal : null;
  templateResult = (typeof templateResult == "function") ? templateResult : null

  let opts = {
    data: data,
    placeholder: 'Seleccione una opción',
    allowClear: true,
    with: 'resolve',
    multiple: multiple,
    dropdownAutoWidth: true
  };
  if(templateResult != null) {
    opts.templateResult = templateResult;
  }

  $("#" + idSelect).select2(opts);

  
  if(initVal) {
    $("#" + idSelect).val(initVal).trigger("change");
  } else {
    $("#" + idSelect).val(null).trigger("change");
  }
}