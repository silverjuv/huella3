(function($) {
    var tp;

    $(document).ready(init());

    function init() {
        $("#_agregar").on("click", mostrarAgregar);
        $("#_editar").on("click", mostrarEditar);
        $("#_borrar").on("click", confirmarBorrado);
        initTables();
        $.fn.button = function(action) {
            if (action === 'loading' && this.data('loading-text')) {
              this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
              this.html(this.data('original-text')).prop('disabled', false);
            }
        };
    }


    function initTables() {
        //NProgress.start();
        var burl = $("#burl").html() + "CatPuestosController/listarClientes";
        $.post(burl, {}, function (result) {
            if (result != undefined) {
                $("#id_cliente").select2({
                    data: result,
                    placeholder: 'Selecciona un cliente',
                    allowClear: true,
                    with: 'resolve'
                }).on('select2:select', function (e) {
                    $("#tPuestos").DataTable().ajax.reload();
				});
                $("#id_cliente").val(null).trigger("change");
            }
        }, 'json');
        
        var burl = $("#burl").html() + "CatPuestosController/listarPuestos";
        var lan = $("#burl").html() + "../assets/template/plugins/DataTables/spanish.json";
        tp = $("#tPuestos").DataTable({
            "scrollY": "300px",
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": burl,
                "type": "POST",
                "data": function ( d ) {
                    d.id_cliente = $("#id_cliente").val();
                }
            },
            "dataSrc": 'data',
            "columns": [
                { "data": "acciones", "targets": 0, 'className': 'control' },
                { "data": "id_puesto", "visible": false },
                { "data": "descripcion", "orderable": false },
                { "data": "asignar", "orderable": false },
            ],
            "responsive": {
                'details': {
                    'type': 'column',
                    'target': 0
                }
            },
            "language": {
                "url": lan
            },
            "select": {
                'style': 'single',
                'selector': 'td:not(.control)'
            },
            dom: "<'row'<'col-sm-2'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "rowCallback": function(row, data, index) {
                $('td', row).attr('class', 'table-active');
            }
        });

        $('#formPuesto').bootstrapValidator({
            excluded: ':disabled',
           live:true,
           framework: 'bootstrap',
           feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
           },
           fields: {
               descripcion: {
                   validators: {
                       notEmpty: {
                           message: 'Introduzca un nombre'
                       },
                       stringLength: {
                           max:100,
                           message: 'El tamaño permitido 100 caracteres'
                       }
                   }
               }
           }
       });
       
       $('#formPuesto').bootstrapValidator().on('success.form.bv', function(e) {
           // previene el formulario antes de enviarse
           e.preventDefault();	
           guardar();    
        });
    }

    function mostrarAgregar(){
        var id_cliente = $("#id_cliente").val();
        if(id_cliente === null){
			PNotifyAlerta("Atención","Debe Seleccionar un cliente del listado...","error");
			return;
        } 
        $('#formPuesto').form('clear');
        $('#formPuesto').bootstrapValidator('resetForm', true);
        $("#action").val("1");
        abreVentanaModal("__modal", "Agregar Puesto");
    }

    function mostrarEditar(){
        let r = tp.row({selected: true}).data();
        var id_cliente = $("#id_cliente").val();
        if(id_cliente == null){
			PNotifyAlerta("Atención","Debe Seleccionar un cliente del listado...","error");
			return;
        }   
        if(r === undefined) {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
            return;
        } 
        $('#formPuesto').form('clear');
        $('#formPuesto').bootstrapValidator('resetForm', true);
        $("#action").val("0");
        $("#formPuesto").form('load',r);
        abreVentanaModal("__modal", "Editar Puesto");  
    }

    function confirmarBorrado() {
        let r = tp.row({selected: true}).data();
        var id_cliente = $("#id_cliente").val();
        if(r === undefined) {
            PNotifyAlerta("Atención", "Debe seleccionar un registro de la tabla", "notice");
            return;
        }
        if(id_cliente === null){
			PNotifyAlerta("Atención","Debe Seleccionar un cliente del listado...","error");
			return;
        }    
        const notice = PNotifyConfirm("Confirme su acción", "¿Desea borrar el puesto: "+r.descripcion+"?", "Si, proceder");
        notice.on('pnotify.confirm', (v) => {
            // $("#btnGuardar").button('loading');
            let burl = $("#burl").html()+"CatPuestosController/eliminar";
            $.post(burl, {id_puesto: r.id_puesto,id_cliente:id_cliente }, function (result) {
                if (result != undefined) {
                    // $("#btnGuardar").button('reset');
				    if(result.error==0){
                        $("#tPuestos").DataTable().ajax.reload();
                        PNotifyAlerta("Atencion", result.msg, "success");
                    }else if(result.error==1){
                        PNotifyAlerta("Atencion", result.msg, "error");
                    }    
                }
            }, 'json');
        });
        notice.on('pnotify.cancel', () => {
            console.log("rejected")
        });
    }
    
    function guardar(){
        var id_cliente = $("#id_cliente").val();
		if(id_cliente === null){
			PNotifyAlerta("Atención","Debe Seleccionar un cliente del listado...","error");
			return;
        }
        var burl = $("#burl").html()+"CatPuestosController/guardar";
	    $("#formPuesto").form('submit',{
		    ajax:true,
		    iframe:false,
		    url:burl,
		    onProgress:function(e){
			    $("#btnGuardar").button('loading');
            },
            onSubmit:function(param){
                param.id_cliente = id_cliente;
            },
		    success:function(data){
			    if(data !== undefined && data !== null){
                    var datos=JSON.parse(data);
                    $("#btnGuardar").button('reset');
				    if(datos.error==0){
					    $("#tPuestos").DataTable().ajax.reload();
					    $("#__modal").modal("hide");
					    PNotifyAlerta("Atencion", datos.msg, "success");
				    }else if(datos.error==1){
                        PNotifyAlerta("Atencion", "No se realizo la acción", "error");
				    }
			    }
		    }
	    });
     }
})(jQuery);    

function asignarRiesgos(idc, idx){
    window.location="PuestosRiesgosController?idc="+idc+"&idp="+idx;
}